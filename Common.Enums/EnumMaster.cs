﻿using System.ComponentModel;

namespace Common.Enums
{
    public enum Day
    {
        SUNDAY = 0,
        MONDAY = 1,
        TUESDAY = 2,
        WEDNESDAY = 3,
        THURSDAY = 4,
        FRIDAY = 5,
        SATURDAY = 6,
    }

    public enum Month
    {
        [Description("Jan")]
        JAN = 1,
        [Description("Feb")]
        FEB = 2,
        [Description("Mar")]
        MAR = 3,
        [Description("Apr")]
        APR = 4,
        [Description("Mei")]
        MEI = 5,
        [Description("Jun")]
        JUN = 6,
        [Description("Jul")]
        JUL = 7,
        [Description("Agt")]
        AUG = 8,
        [Description("Sep")]
        SEP = 9,
        [Description("Okt")]
        OKT = 10,
        [Description("Nov")]
        NOV = 11,
        [Description("Des")]
        DES = 12,
    }

    public enum FileType
    {
        [Description("Pdf")]
        PDF,
        [Description("Ms. Word")]
        DOC,
        [Description("Ms. Power Point")]
        PPT,
        [Description("Ms. Excel")]
        XLS,
        [Description("Jpg")]
        JPG,
        [Description("Png")]
        PNG,
        [Description("Bmp")]
        BMP,
        [Description("Gif")]
        GIF,
        [Description("Others")]
        OTHERS
    }

    public enum Currency
    {
        [Description("US Dollar")]
        USD = 1,
        [Description("Rupiah")]
        IDR = 2
    }

    public enum OptionType
    {
        AUC,
        AUCVALUE,
        ACTUAL,
        SUBNUMBER,
        LOCATION,
        CURRENCY,
        FIXASSET,
        CONDITION,
        STATUSASSET,
        USER,
        PLANT
    }

    public enum StatusAsset
    {
        [Description("On Progress")]
        DRAFT,
        [Description("Menunggu persetujuan keuangan")]
        WAITING_APPROVAL_FINANCE,
        [Description("Disetujui Keuangan")]
        APPROVE_BY_FINANCE,
        [Description("Ditolak Keuangan")]
        REJECT_BY_FINANCE
    }

    public enum StatusTCR
    {
        [Description("On Progress")]
        DRAFT,
        [Description("Menunggu persetujuan keuangan")]
        WAITING_APPROVAL_FINANCE,
        [Description("Disetujui Keuangan")]
        APPROVE_BY_FINANCE,
        [Description("Ditolak Keuangan")]
        REJECT_BY_FINANCE
    }

    public enum StatusMDM
    {
        [Description("Proses MDM")]
        PROGRESS,        
        [Description("OK")]
        COMPLETE
    }

    public enum StatusSPC
    {
        [Description("Proses SPC")]
        DRAFT,
        [Description("OK")]
        COMPLETED
    }

    public enum storage
    {
        [Description("fa-storage")]
        FASTORAGE,
        [Description("auc-storage-")]
        AUCSTRORAGE,
    }

    public enum SheetExcel
    {
        USD,
        IDR
    }
    public enum MasterExcelColumn
    {
        //actual cost (realisasi)
        [Description("Document Number")]
        DOC_NUMBER,
        [Description("Ref Document Number")]
        REF_NUMBER,
        [Description("Offsetting account type")]
        OFFSETTING_ACC,
        [Description("Document type")]
        DOC_TYPE,
        [Description("Original bus. trans")]
        ORIGINAL_TRANS,
        [Description("Project Definition")]
        PROJECT_DEFINITION,
        [Description("WBS Element")]
        WBS,
        [Description("CO object name")]
        CO_NAME,
        [Description("Cost Element")]
        COST_ELEMENT,
        [Description("Cost element descr.")]
        COST_ELEMENT_DESC,
        [Description("Fiscal Year")]
        FISCAL_YEAR,
        [Description("Document Date")]
        DOC_DATE,
        [Description("Posting Date")]
        POST_DATE,
        [Description("Created on")]
        CREATED_ON,
        [Description("Transaction Currency")]
        TRANSACTION_CURR,
        [Description("Vbl. value/TranCurr.")]
        VAL_TRANSACTION,
        [Description("Val/COArea Crcy")]
        CO_AREA,
        [Description("Val in Hard Curr")]
        VAL_HARD,
        [Description("Val.in hard.curr")]
        VAL_HARD2,
        [Description("Purchasing Document")]
        PURCHASING_DOC,
        [Description("Total quantity")]
        TOTAL_QTY,
        [Description("Item")]
        ITEM,
        [Description("Purchase order text")]
        PO_TEXT,
        [Description("Name")]
        NAME,
        [Description("User Name")]
        USERNAME,
        [Description("Period")]
        PERIOD,

        //auc
        [Description("Asset")]
        ASSET,
        [Description("Subnumber")]
        SUB_NUMBER,
        [Description("Capitalized on")]
        CAPITALIZED_ON,
        [Description("Asset description")]
        ASSET_DESC,
        [Description("Acquis.val.")]
        ACQUI_VAL,
        [Description("Accum.dep.")]
        ACCUM_DEP,
        [Description("Book val.")]
        BOOK_VAL,
        [Description("Currency")]
        CURRENCY,
        [Description("Company Code")]
        COMPANY_CODE,
        [Description("Asset Class")]
        ASSET_CLASS,
        [Description("Original asset")]
        ORIG_ASSET,
        [Description("Plant")]
        PLANT,
        [Description("Serial number")]
        SERIAL_NUMBER,
        [Description("Useful life")]
        USEFUL_LIFE,
        [Description("Usef.life in periods")]
        USEFUL_PERIOD,
        [Description("Scrap Value")]
        SCRAP_VALUE,
        [Description("Profit Center")]
        PROFIT_CENTER,
        [Description("Ordinary dep.posted")]
        ORDINARY_DEP_POST,
        [Description("First acquisition on")]
        FIRST_ACQUI,
        [Description("Cost Center")]
        COST_CENTER,
        [Description("Asset Condition")]
        ASSET_CONDITION,
        [Description("Evaluation group 1")]
        EVALUATION_GROUP1,
        [Description("Evaluation group 3")]
        EVALUATION_GROUP3,
        [Description("Evaluation group 4")]
        EVALUATION_GROUP4,
        [Description("Evaluation group 5")]
        EVALUATION_GROUP5,
        [Description("Location")]
        LOCATION,
        [Description("Balance sheet item")]
        BALANCE_SHEET_ITEM,
        [Description("Original value")]
        ORIG_VALUE,        
        [Description("Org.acquisition year")]
        ORIG_ACQUI_YEAR,
        [Description("Depreciation key")]
        DEPREC_KEY,
        [Description("Accum.ordinary dep.")]
        ACCUM_ORDINARY,
        [Description("Planned ordinary dep")]
        PLANNED_ORDINARY,
        [Description("No. of units deprec.")]
        NO_UNIT_DEPREC,

    }

    public enum Condition
    {
        [Description("Baik")]
        GOOD,
        [Description("Tidak Baik")]
        BAD
    }

    public enum UploadFileType
    {
        PHOTO,
        VIDEO,
        PDF,
        OTHER
    }

    public enum ApprovalType
    {
        MAPPING,
        TCR
    }

    public enum ScrapType
    {
        [Description("For Capitalization")]
        CAPITALIZATION,
        [Description("Not For Capitalization (only scrap)")]
        NON_CAPITALIZATION,
    }

    public enum AcquisitionType
    {
        [Description("Akuisisi pada tahun sebelum")]
        BEFORE,
        [Description("Akuisisi pada tahun berjalan")]
        CURRENT
    }

    public enum TransactionType
    {
        [Description("CC - Settlement Prior Year - 200")]
        CC_SETTLEMENT_PRIOR_YEAR_200,
        [Description("CC - Settlement Current Year - 250")]
        CC_SETTLEMENT_CURRENT_YEAR_200,
        [Description("PC - Settlement Prior Year - Z04")]
        PC_SETTLEMENT_PRIOR_YEAR_Z04,
        [Description("PC - Settlement Current Year - Z05")]
        PC_SETTLEMENT_CURRENT_YEAR_Z05,
        [Description("DryHole - Settlement Prior Year - Z02")]
        DRYHOLE_SETTLEMENT_PRIOR_YEAR_Z02,
        [Description("DryHole - Settlement Current Year - Z03")]
        DRYHOLE_SETTLEMENT_PRIOR_YEAR_Z05,
        [Description("Scrap - Settlement Prior Year - Z07")]
        SCRAP_SETTLEMENT_PRIOR_YEAR_Z07,
        [Description("Scrap - Settlement Current Year - Z08")]
        SCRAP_SETTLEMENT_PRIOR_YEAR_Z08,
    }

    public enum AssetTransaction
    {
        [Description("100 - Kaptalisasi")]
        CAPITALIZATION_100,
        [Description("101 - Min Cost")]
        MINCOST_101        
    }
}
