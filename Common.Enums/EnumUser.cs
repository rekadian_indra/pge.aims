﻿using System.ComponentModel;

namespace Common.Enums
{
    public enum UserModule
    {
        [Description("User Management")]
        UserManagement,
        [Description("Data Master")]
        DataMaster,
        [Description("Crew")]
        Crew,
        [Description("Whitelist")]
        Whitelist,
    }

    public enum UserRole
    {
        [Description("Superadmin")]
        Superadmin,
        [Description("Administrator")]
        Administrator,
        [Description("Finance")]
        Finance,
        [Description("PhysicalCheck")]
        PhysicalCheck,
    }
}
