﻿using System.ComponentModel;

namespace Common.Enums
{
    public enum Field
    {
        //reuseable field
        Id,
        IsDeleted,
        CreatedBy,
        CreatedDateTime,
        CreatedDateTimeUtc,
        ModifiedBy,
        ModifiedDateTime,
        ModifiedDateTimeUtc,
        Text,
        Value,
        StartDate,
        EndDate,
        Iterator,

        //user management field
        ApplicationName,
        ApplicationId,
        UserId,
        UserName,
        IsAnonymous,
        LastActivityDate,
        RoleId,
        RoleName,
        Description,
        PropertyNames,
        PropertyValueStrings,
        PropertyValueBinary,
        LastUpdatedDate,
        ModuleId,
        ModuleName,
        ParentModule,
        ActionId,
        ActionName,
        Password,
        PasswordFormat,
        PasswordSalt,
        Email,
        PasswordQuestion,
        PasswordAnswer,
        IsApproved,
        IsLockedOut,
        CreateDate,
        LastLoginDate,
        LastPasswordChangedDate,
        LastLockoutDate,
        FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowsStart,
        Comment,

        //APP FIELD
        //Asset
        AssetId,
        AssetIdOffline,
        AssetNumber,
        SubNumber,
        ValueIdr,
        ValueUsd,
        LocationId,
        Location,
        Photo,
        Latitude,
        Longitude,
        [Description("Penjelasan / Catatan Mapping")]
        MappingDescription,
        StatusMapping,

        //AUC
        WBSElement,
        AssetClass,
        Plant,
        CostCenter,
        AcquisValIdr,
        AcquisValUsd,
        BookVal,
        CapitalizedOn,
        AccumDep,
        CompanyCode,
        OriginalAsset,
        SerialNumber,
        UsefulLife,
        UsefulPeriod,
        ScrapVal,
        ProfitCenter,
        OrdinaryDep,
        FirstAcquisition,
        AssetCondition,
        EvaluationGroup1,
        EvaluationGroup3,
        EvaluationGroup4,
        EvaluationGroup5,
        BalanceSheetItem,
        OriginalValue,
        OriginalAcquisitionYear,
        DepreciationKey,
        AccumOrdinaryDep,
        PlannedOrdinaryDep,
        NoUnitDeprec,
        AucNumber,


        //Actual
        ActualCostId,
        DocNumber,
        DocNumber2,
        RefNumber,
        OffsettingAccountType,
        DocType,
        ProjectDefinition,
        CoObjectName,
        CostElement,
        CostElementDesc,
        FiscalYear,
        DocDate,
        PostingDate,
        CreatedOn,
        TransactionCurrency,
        ValueTranCurr,
        ValueCoArea,
        ValueHardCurr,
        PurchasingDoc,
        TotalQuantity,
        Item,
        PurchaseOrderText,
        Name,
        Period,
        Balance,
        MappingValueIdr,

        //Mapping
        MappingId,
        SubmitDate,
        Status,
        StatusPretify,
        PlantId,
        Code,
        PlantMapping,

        //mapping asset
        MappingAssetId,

        //Mapping asset actual cost
        MappingAssetActualCostId,

        //TCR
        TCRId,
        ProjectName,
        Currency,
        StatusTcr,
        Notes,
        AssetHolder,

        //MDM
        MDMRequestId,
        PreparedBy,
        RequestTitle,
        LegalRequestor,
        StatusMDM,

        //SPC
        SPCRequestId,
        StatusSPC,
        NoRequestSPC005,
        NoRequestSPC011,
        ScrapType,

        //physical check
        PhysicalCheckId,
        PhysicalCheckDate,
        Condition,
        CheckedBy,
        ImageId,
        AreaId,
        DocumentNumber,
        WBSElementNumber,
        SelectedFixedAsset,
        AmountActual
    }

    public enum Table
    {
        //user management table
        Action,
        Application,
        Membership,
        Module,
        Profile,
        Role,
        User,

        //app table
        AUC,
        MappingAssets,
        Asset,
        Plant,
        Plant1
    }

    public enum State
    {
        Create,
        Update,
        Destroy
    }
}
