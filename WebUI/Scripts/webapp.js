﻿const FORM_SELECTOR = "form.form-horizontal";

var kendoGridFilterable = {
    messages: {
        info: "Tampilkan yang memiliki nilai:", // sets the text on top of the filter menu
        filter: "Filter", // sets the text for the "Filter" button
        clear: "Hapus", // sets the text for the "Clear" button

        // when filtering boolean numbers
        isTrue: "Benar", // sets the text for "isTrue" radio button
        isFalse: "Salah", // sets the text for "isFalse" radio button

        //changes the text of the "And" and "Or" of the filter menu
        and: "Dan",
        or: "Atau"
    },
    operators: {
        //string: {
        //    eq: "Is equal to",
        //    neq: "Is not equal to",
        //    startswith: "Starts with",
        //    contains: "Contains",
        //    endswith: "Ends with"
        //},
        //filter menu for "string" type columns
        string: {
            //eq: "Sama Dengan",
            //neq: "Tidak Sama Dengan",
            //startswith: "Memiliki Awalan",
            contains: "Memiliki Kata",
            //endswith: "Memiliki Akhiran"
        },
        //filter menu for "number" type columns
        number: {
            eq: "Sama Dengan",
            //neq: "Tidak Sama Dengan",
            //gte: "Lebih Besar Atau Sama Dengan",
            gt: "Lebih Besar",
            //lte: "Lebih Kecil Atau Sama Dengan",
            lt: "Lebih Kecil"
        },
        //filter menu for "date" type columns
        date: {
            eq: "Sama Dengan",
            //neq: "Tidak Sama Dengan",
            //gte: "Setelah Atau Sama Dengan",
            //gt: "Setelah",
            //lte: "Sebelum Atau Sama Dengan"
            //lt: "Sebelum"
        },
        //filter menu for foreign key values
        enums: {
            eq: "Sama Dengan",
            //neq: "Tidak Sama Dengan"
        }
    },
    extra: false
};

var rangeDateFilterable = {
    ui: function (e) {
        var datePicker = $(e).kendoDatePicker({
            format: "dd/MM/yyyy",
            parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
        }).data("kendoDatePicker");

        datePicker.element.prop("readonly", "readonly");
    },
    operators: {
        date: {
            gte: "Setelah Atau Sama Dengan",
            lte: "Sebelum Atau Sama Dengan"
        }
    },
    extra: true
};

/**
 * @param {Function} uiOption function to init ui
 * @returns {kendo.ui.GridColumnFilterable} grid column filterable
 */
var dropDownListFilterable = function (uiOption) {
    if (typeof (uiOption) !== "function") {
        throw "uiOption must be function";
    }

    return {
        ui: uiOption,
        operators: {
            string: {
                eq: "Sama Dengan"
            },
            number: {
                eq: "Sama Dengan"
            },
            enums: {
                eq: "Sama Dengan"
            }
        },
        extra: false
    };
}

$(function () {
    requiredSign();
    initClientSideValidation();

    $.ajaxSetup({ cache: false }); //disable cache for IE

    //tooltip for input form
    $("input[type=text]").tooltip({
        trigger: "focus",
        title: function () {
            var title = "";
            var titleArray = [];

            if ($(this).attr("data-val-required") !== null)
                titleArray[titleArray.length] = $(this).attr("data-val-required");

            if ($(this).attr("data-val-requiredif") !== null)
                titleArray[titleArray.length] = $(this).attr("data-val-requiredif");

            if ($(this).attr("data-val-number") !== null)
                titleArray[titleArray.length] = $(this).attr("data-val-number");

            if ($(this).attr("data-val-date") !== null)
                titleArray[titleArray.length] = $(this).attr("data-val-date");

            title = titleArray.join(" ");

            if (title.trim() == "") {
                $(this).tooltip('disable');
            }

            return title;
        },
        container: "body",
    });
});

kendo.culture("id-ID");

//datepicker format
$(".form-control-datepicker").kendoDatePicker({
    format: "dd/MM/yyyy",
    parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"]
}).prop("readonly", "readonly");

$(".form-control-datetimepicker").kendoDateTimePicker({
    format: "dd/MM/yyyy HH:mm",
    parseFormats: ["dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy HH.mm.ss"],
    interval: 30
}).prop("readonly", "readonly");

//numeric textbox
$(".form-control-numeric").kendoNumericTextBox({
    //min: 0,
    //max: 2147483647,
    decimals: 0,
    format: "n0",
    spinners: false
});

$(".form-control-decimal").kendoNumericTextBox({
    min: -2147483647,
    //max: 2147483647,
    decimals: 2,
    format: "n2",
    spinners: false
});

$(".form-control-decimal-n3").kendoNumericTextBox({
    //max: 2147483647,
    decimals: 3,
    format: "n3",
    spinners: false
});

$(".form-control-decimal-n4").kendoNumericTextBox({
    //max: 2147483647,
    decimals: 4,
    format: "n4",
    spinners: false
});

//kendo textarea
$(".form-control-editor").kendoEditor({
    tools: [
        //{ name: "insertLineBreak", shift: false },
        //{ name: "insertParagraph", shift: true },
        "formatting",
        "bold",
        "italic",
        "underline",
        "strikethrough",
        "justifyLeft",
        "justifyCenter",
        "justifyRight",
        "justifyFull",
        "indent",
        "outdent",
        "createLink",
        "unlink",
        "viewHtml",
    ],
    resizable: true,
    encoded: false,
    paste: function (ev) {
        var temp = "";
        var pText = "";

        temp = $.parseHTML(ev.html);
        pText = $(temp).text();
        ev.html = pText;
    }
});

/**
 * init client side validation
 * @param {JQuery} form jQuery object
 */
//function initClientSideValidation(form = null) {
//    if (form === null) {
//        form = $(FORM_SELECTOR);
//    }
    
//    if (form.length > 0) {
//        var formChange = false;
//        var validator = form.data("validator");
        
//        //handle client validation on unsupported component. example : kendo dropdownlist, kendo editor, etc.
//        //to use this add class always-validate to component
//        if (validator !== undefined && validator !== null) {
//            validator.settings.ignore = ":hidden:not(.always-validate)";
//        }

//        //enable or disable button submit on client side validation
//        form.find("input, textarea").on("keyup blur", validateForm);

//        //confirmation form data not save yet
//        form.each(function () {
//            if (!$(this).hasClass("not-track-change")) {
//                $(this).find("input, select, textarea").on("change", function () {
//                    formChange = true;
//                });
//                $(this).submit(function () {
                    
//                    $(this).find("button[type=\"submit\"]").attr("disabled", true);
//                    formChange = false;
//                });
//            }
//        });

//        $(window).on("beforeunload", function () {
//            if (formChange) {
//                return false;
//            }
//        });
//    }
//}

function initClientSideValidation(form = null, modal = null) {
    if (modal !== null) {
        form = modal.find(FORM_SELECTOR);
    } else {
        if (form === null) {
            form = $(FORM_SELECTOR);
        }
    }

    if (form.length > 0) {
        var formChange = false;
        var validator = form.data("validator");

        //handle client validation on unsupported component. example : kendo dropdownlist, kendo editor, etc.
        //to use this add class always-validate to component
        if (validator !== undefined && validator !== null) {
            validator.settings.ignore = ":hidden:not(.always-validate)";
        }

        if (modal !== null) {
            //enable or disable button submit on client side validation
            form.find("input, textarea").on("keyup blur", function (e) {
                validateForm(null, null, modal);
            });
        } else {
            //enable or disable button submit on client side validation
            form.find("input, textarea").on("keyup blur", validateForm);

            //confirmation form data not save yet
            form.each(function () {
                if (!$(this).hasClass("not-track-change")) {
                    $(this).find("input, select, textarea").on("change", function () {
                        formChange = true;
                    });
                    $(this).submit(function () {
                        $(this).find("button[type=\"submit\"]").attr("disabled", true);
                        formChange = false;
                    });
                }
            });

            $(window).on("beforeunload", function () {
                if (formChange) {
                    return false;
                }
            });
        }
    }
}

/**
 * required sign using asterisk
 * @param {JQuery} form jQuery object
 */
function requiredSign(form = null) {
    if (form === null) {
        form = $(FORM_SELECTOR);
    }

    form.find("input[type=text], input[type=hidden], textarea, select, input[type=password]").each(function () {
        var req = $(this).attr("data-val-required") || $(this).attr("data-val-requiredif");

        if (undefined !== req) {
            var label = $(this).parentsUntil("form").find("label[for=\"" + $(this).attr("id") + "\"]");
            var text = label.text();

            if (text.length > 0) {
                label.append("<span style=\"color:red\" class=\"asterisk\"> *</span>");
            }
        }
    });
}

/**
 * client side form validation
 * @param {JQuery.Event} e jQuery event
 * @param {JQuery} form jQuery object
 */
//function validateForm(e = null, form = null) {
//    if (e !== null && form !== null) {
//        throw "please select e or form";
//    }
    
//    if (e !== null) {
//        form = $(this).closest("form");
//    } else {
//        if (form === null) {
//            form = $(FORM_SELECTOR);
//        }
//    }

//    var validator = form.data("validator");

//    if (validator !== undefined && validator !== null) {       
//        if (form) {
//            $("button[type=\"submit\"]").prop("disabled", false);
//        } else {
//            $("button[type=\"submit\"]").prop("disabled", "disabled");
//        }
//    }
//}

function validateForm(e = null, form = null, modal = null) {
    if (e !== null && form !== null) {
        throw "please select e or form";
    }

    var validator;

    if (modal !== null) {
        form = modal.find(FORM_SELECTOR);

        validator = form.data("validator");

        if (validator !== undefined && validator !== null) {
            if (form.valid()) {
                modal.find("#btnSave").prop("disabled", false);
            } else {
                modal.find("#btnSave").prop("disabled", "disabled");
            }
        }
    } else {
        if (e !== null) {
            form = $(this).closest("form");
        } else {
            if (form === null) {
                form = $(FORM_SELECTOR);
            }
        }

        validator = form.data("validator");

        if (validator !== undefined && validator !== null) {
            if (form.valid()) {
                $("button[type=\"submit\"]").prop("disabled", false);
            } else {
                $("button[type=\"submit\"]").prop("disabled", "disabled");
            }
        }
    }
}

/**
 * @param {string} xmlDate parse xmldate
 * @returns {Date} 2013-12-04T18:10:05.768
 */
function TimeStampToDate(xmlDate) {
    var dt = new Date();
    var dtS = xmlDate.slice(xmlDate.indexOf("T") + 1, xmlDate.indexOf("."))
    var TimeArray = dtS.split(":");
    //
    (TimeArray);
    dt.setHours(TimeArray[0]);
    dt.setMinutes(TimeArray[1]);
    dtS = xmlDate.slice(0, xmlDate.indexOf("T"))
    TimeArray = dtS.split("-");
    //console.log(TimeArray);
    dt.setFullYear(TimeArray[0]);
    dt.setMonth(TimeArray[1] - 1);
    dt.setDate(TimeArray[2]);
    //console.log(dt);
    return dt;
}

/**
 * mengambil tanggal dari datetimeco
 * @param {Date} datetime 12/31/2013 12:00:00 AM
 * @return {string} 12/31/2013
 */
function GetDate(datetime) {
    var parts = datetime.split(" ");
    var date = parts[0];

    return date;
}

/**
 * mengambil jam dari datetime
 * @param {Date} datetime 12/31/2013 12:00:00 AM
 * @return {string} 12:00
 */
function GetTime(datetime) {
    //kamus
    var parts = datetime.split(" ");
    var ampm = parts[2];
    var dateObject = new Date(datetime);

    //hour & minute
    var hour = dateObject.getHours();
    var minute = dateObject.getMinutes();
    if (ampm === "PM")
        hour += 12;
    hour = String("00" + hour).slice(-2);
    minute = String("00" + minute).slice(-2);

    //time string
    var time = hour + ":" + minute;

    return time;
}

/**
 * mengembalikan tanggal ditambah x days
 * @param {Date} original 12/30/2013 12:00:00 AM
 * @param {Number} days 1
 * @return {Date} 12/31/2013 12:00:00 AM
 */
function AddDays(original, days) {
    var newDate = new Date(original.setTime(original.getTime() + days * 86400000));
    return newDate;
}

/**
 * fungsi desimal
 * @param {number} n number
 * @param {string} sep sparator
 * @returns {string} 1.000.000
 */
function ThousandSeparator(n, sep) {
    var sRegExp = new RegExp("(-?[0-9]+)([0-9]{3})"), sValue = n + "";
    if (sep === undefined) { sep = ","; }
    while (sRegExp.test(sValue)) {
        sValue = sValue.replace(sRegExp, "$1" + sep + "$2");
    }
    return sValue;
}

/**
 * Disable spell check kendo editor
 * @param {kendo.ui.Editor} editor kendo editor
 */
function kendoEditorDisableSpellCheck(editor) {
    $(editor.body).attr("spellcheck", false);
    $(editor.body).css("padding", "10px");
}

/**
 * html decode
 * untuk web grid bisa menggunakan atribut encoded di kolom
 * @param {string} text text
 * @returns {jQuery} dom selector
 */
function FormatText(text) {
    var textContainer = $("<div></div>");
    if (text !== null) {
        textContainer.html(text.replace(/\n/g, "<br>"));
    }
    return textContainer.html();
}

/**
 * @param {string} selector dom selector
 * menghapus nilai dari $(selector)
 */
function clearValue(selector) {
    $(selector).val("");
}

/**
 * menampilkan info data kosong
 * @param {JQuery} el: jquery element
 * @param {string} message: pesan
 */
function displayEmptyGrid(el, message) {
    el.removeClass();
    el.attr("style", "");
    el.html(message);
}

/**
 * kendo grid mengambil data dari row
 * @param {string} e element selector
 * @returns {JQuery} jquery element
 */
function getDataRowGrid(e) {
    return $(e.target).closest("tr");
}

/**
 * kendo grid delete local with custom icon
 * @param {datasource} datasource grid
 * @param {item} item tobe deleted
 * @param {ParentStorage} parent local storage tobe deleted
 * @param {childStorage} child local storage tobe deleted
 */
function goToDeleteLocal(datasource, item, parentStorage, childStorage) {
    swal(
        {
            title: "Hapus Data",
            text: "Apakah anda yakin untuk menghapus data ini?",
            type: "warning",
            cancelButtonText: "Batal",
            showCancelButton: true,
            confirmButtonClass: "btn btn-primary",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        },
        function () {
            var localData = JSON.parse(localStorage[parentStorage]);
            for (var i = 0; i < localData.length; i++) {
                if (localData[i].Id === item.Id) {
                    localData.splice(i, 1);
                    if (childStorage != null)
                        localStorage.removeItem(`${childStorage}${item.Id}`);
                    break;
                }
            }
            localStorage[parentStorage] = JSON.stringify(localData);
            datasource.read();
            swal("Status", "Data berhasil dihapus", "success");
        });
}

/**
 * konfirmasi delete sebelum di redirect
 * response: { Success: t/f, Message }
 * @param {string} url url
 * @param {string} datasource datasource
 * @param {Function} callback function callback
 */
function goToDeletePage(url, datasource, callback = null) {
    if (callback !== null && typeof (callback) !== "function") {
        throw "callback must be function";
    }

    swal(
        {
            title: "Hapus Data",
            text: "Apakah anda yakin untuk menghapus data ini?",
            type: "warning",
            cancelButtonText: "Batal",
            showCancelButton: true,
            confirmButtonClass: "btn btn-primary",
            confirmButtonText: "Ya",
            closeOnConfirm: false
        },
        function () {
            swal({
                title: "Loading",
                text: "Harap Menunggu...",
                imageUrl: APP_NAME + "Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden",
                //imageSize: "80x80"
            });

            $.ajax({
                url: url,
                type: "POST",
                success: function (data) {
                    if (data.Success === true) {
                        if (datasource !== null) {
                            datasource.read();
                        }

                        swal("Status", "Data berhasil dihapus", "success");
                        if (callback != null)
                            callback(data);
                    }
                    else {
                        swal("Error", data.Message, "error");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Server Error. Harap hubungi administrator");
                }
            });
        });
}

//delete via post
function postToDeletePage(url, data, datasource) {
    swal(
        {
            title: "Hapus Data",
            text: "Apakah anda yakin untuk menghapus data ini?",
            type: "warning",
            cancelButtonText: "Batal",
            showCancelButton: true,
            confirmButtonClass: "btn btn-primary",
            confirmButtonText: "Ya",
            closeOnConfirm: false
        },
        function () {
            swal({
                title: "Loading",
                text: "Harap Menunggu...",
                imageUrl: APP_NAME + "Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden",
            });

            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (response) {
                    if (response.Success === true) {
                        if (datasource !== null) {
                            datasource.read();
                        }
                        swal("Status", "Data berhasil dihapus", "success");
                    }
                    else {
                        swal(response.Message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Server Error. Harap hubungi administrator");
                }
            });
        }
    );
}

function deleteThenRedirect(url, data, redirectPage) {
    swal(
        {
            title: "Hapus Data",
            text: "Apakah anda yakin untuk menghapus data ini?",
            type: "warning",
            cancelButtonText: "Batal",
            showCancelButton: true,
            confirmButtonClass: "btn btn-primary",
            confirmButtonText: "Ya",
            closeOnConfirm: false
        },
        function () {
            swal({
                title: "Loading",
                text: "Harap Menunggu...",
                imageUrl: "/Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden",
            });

            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (response) {
                    if (response.Success === true) {
                        window.location.href = redirectPage;
                    }
                    else {
                        swal(response.Message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Server Error. Harap hubungi administrator");
                }
            });
        }
    );
}

/**
 * @param {string} el element selector
 * disable $(el) dengan menambahkan kelas disabled
 */
function disableMe(el) {
    $(el).addClass("disabled");
}

/**
 * @param {string} a string a
 * @param {string} b string b
 * @returns {string} mengecek kalau a == null menampilkan b
 */
function isnull(a, b) {
    b = b || "";
    return a || b;
}

/**
 * @param {string} value value
 * @returns {string} decode hasil penyimpanan dari kendo grid
 */
function htmlDecode(value) {
    var result = "";
    if (value !== null) {
        result = value.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    }

    return result;
}

/**
 * menghapus isi combobox jika masukan user tidak ada pada pilihan
 * @param {JQuery.Event} e event
 */
function comboBoxOnChange(e) {
    if (this.value() && this.selectedIndex === -1) {   //or use this.dataItem to see if it has an object
        this.text("");
    }
}

/**
 * @param {string} area param area
 * @param {string} controller param controller
 * @param {string} action param action
 * @returns {string} http://APP_ADDRESS/area/controler/action
 */
function urlAction(area, controller, action) {
    var result = "";

    if (area !== "") {
        result = kendo.format("{0}{1}/{2}/{3}", APP_ADDRESS, area, controller, action);
    } else {
        result = kendo.format("{0}{1}/{2}", APP_ADDRESS, controller, action);
    }

    return result;
}

/**
 * @param {string} url param url
 * @param {string} optionType param optionType
 * @param {string} mapField param mapField
 * @param {Array} args param args
 * @returns {kendo.data.DataSource} kendo dataSource
 */
function generateOptionDataSource(url, optionType, mapField, args = null) {
    return ds = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "POST"
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return kendo.stringify(options);
                } else {
                    var i;

                    if (options.filter) {
                        var filters = options.filter.filters;

                        for (i in filters) {
                            var filter = filters[i];

                            if (filter.field === "Text") {
                                //mapField is database field
                                filter.field = mapField;
                            }
                        }
                    }

                    if (options.sort) {
                        var sorts = options.sort;

                        for (i in sorts) {
                            var sort = sorts[i];

                            if (sort.field === "Text") {
                                //mapField is database field
                                sort.field = mapField;
                            }
                        }
                    }

                    options.optionType = optionType;

                    if (args !== null) {
                        options.args = args;
                    }

                    return options;
                }
            }
        },
        schema: {
            model: {
                id: "Value",
                fields: {
                    "Text": { type: "string" },
                    "Value": { type: "string" }
                }
            }
        },
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
        sort: { field: "Text", dir: "asc" }
    });
}

/**
 * @param {kendo.ui.GridDataBoundEvent} e param event
 */
function dataBoundTooltip(e) {
    e.sender.element.find("[data-toggle=\"tooltip\"]").tooltip({ container: "body" });
}

/**
 * @param {kendo.ui.TreeListDataBoundEvent} e param event
 * @param {string} edit title edit
 * @param {string} destroy title delete
 */
function dataBoundTreeListCommand(e, edit, destroy) {
    var treeList = e.sender;

    var $rows = treeList.tbody.find("tr[role=\"row\"]");

    if ($rows.exists()) {
        $rows.each(function (i) {
            $(this).find("[data-command=\"edit_\"]")
                .attr("data-toggle", "tooltip")
                .attr("data-placement", "auto bottom")
                .attr("title", edit)
                .html("<span class=\"fa fa-lg fa-pencil-square\"></span>");

            $(this).find("[data-command=\"delete\"]")
                .attr("data-toggle", "tooltip")
                .attr("data-placement", "auto bottom")
                .attr("title", destroy)
                .html("<span class=\"fa fa-lg fa-trash\"></span>");
        });
    } else {
        treeList.element.find(".k-grid-content").css({
            "display": ""
        });
    }
}

/**
 * @param {kendo.ui.GridFilterMenuInitEvent} e param event
 */
function filterMenuInitRangeDate(e) {
    var dropDown = e.container.find("select:eq(0)").data("kendoDropDownList"),
        start = e.container.find("input:eq(0)").data("kendoDatePicker"),
        end = e.container.find("input:eq(1)").data("kendoDatePicker"),
        dataItem = dropDown.dataSource.at(1);

    //custom first dropdown
    dropDown.dataSource.remove(dataItem);

    //custom logic dropdown
    dropDown = e.container.find("select:eq(1)").data("kendoDropDownList");
    dataItem = dropDown.dataSource.at(1);
    dropDown.dataSource.remove(dataItem);

    //custom second dropdown
    dropDown = e.container.find("select:eq(2)").data("kendoDropDownList");
    dataItem = dropDown.dataSource.at(0);
    dropDown.dataSource.remove(dataItem);

    //add on change event to start
    start.bind("change", function (e) {
        var startDate = start.value(),
            endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    });

    //add on change event to end
    end.bind("change", function (e) {
        var endDate = end.value(),
            startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    });
}

/**
 * @param {kendo.ui.GridFilterMenuOpenEvent} e param event
 */
function filterMenuOpenRangeDate(e) {
    var dropDown = e.container.find("select:eq(0)").data("kendoDropDownList"),
        start = e.container.find("input:eq(0)").data("kendoDatePicker"),
        end = e.container.find("input:eq(1)").data("kendoDatePicker");

    var filter = e.sender.dataSource.filter(),
        filters = filter !== undefined && filter !== null ? filter.filters : [],
        selectedFilters = [];

    //custom first dropdown
    dropDown.value("gte");
    dropDown.trigger("change");

    //custom logic dropdown
    dropDown = e.container.find("select:eq(1)").data("kendoDropDownList");
    dropDown.value("and");
    dropDown.trigger("change");

    //custom second dropdown
    dropDown = e.container.find("select:eq(2)").data("kendoDropDownList");
    dropDown.value("lte");
    dropDown.trigger("change");

    for (var i in filters) {
        filter = filters[i];

        if (filter.field === e.field) {
            selectedFilters.push(filter);
        }
    }

    if (selectedFilters.length === 1) {
        filter = filters[i];

        if (filter.operator === "lte") {
            end.value(new Date(filter.value()));
            end.trigger("change");
            start.value(null);
            start.trigger("change");
        }
    }
}

/**
 * @param {kendo.ui.GridFilterMenuInitEvent} e param event
 */
function filterMenuInitNumeric(e) {
    //check ctrl+v, it should number too
    var $inputNumber = e.container.find("input:eq(0)");

    $inputNumber.on("paste", function (e) {
        setTimeout(function () {
            isnum = /^[\d-]+$/.test($inputNumber.val());

            if (!isnum) {
                $inputNumber.val("");
                $inputNumber.css({ "border-color": "#ff0000" });
                setTimeout(function () {
                    $inputNumber.css({ "border-color": "" });
                }, 200);
            }
        }, 0); //just break the callstack to let the event finish               
    });

    // check every onkeydown event, if it's string or not.
    $inputNumber.on("keydown", inputNumStrip);
}

/**
 * An event for the input should not alphabetic, can do Numeric and Minus
 * @param {JQuery.Event} e param event
*/
function inputNumStrip(e) {
    // Allow: backspace, delete, tab, escape, enter, ctrl, alt and -
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 17, 18, 110, 109, 189, 173]) !== -1 ||
        // Allow: Ctrl+A,Ctrl+C,Ctrl+V, Command+A
        ((e.keyCode === 65 || e.keyCode === 86 || e.keyCode === 67) && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }

    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
        $(e.target).css({ "border-color": "#ff0000" });
        setTimeout(function () {
            $(e.target).css({ "border-color": "" });
        }, 200);
    }
}

/**
 * @param {kendo.data.ObservableArray} data callback dropdown event
 * @returns {Array} array object grid datasource after clean up unused properties
 */
function cleanupDataSource(data) {
    var result = [];

    for (var i = 0; i < data.length; i++) {
        for (var prop in data[i]) {
            switch (prop) {
                case "defaults":
                case "fields":
                case "idField":
                case "_defaultId":
                    delete data[i][prop];
                    break;
                default:
                    break;
            }
        }

        result.push(data[i]);
    }

    return result;
}

/**
 * @param {object} filter kendo dataSource filter
 */
function cleanupDataSourceFilter(filter) {
    var prop;

    if (filter.logic !== null && filter.filters !== null && filter.filters.length > 0) {
        //cleanup property field, operator and value
        for (prop in filter) {
            switch (prop) {
                case "field":
                case "operator":
                case "value":
                    delete filter[prop];
                    break;
                default:
                    break;
            }
        }

        for (var i in filter.filters) {
            var f = filter.filters[i];

            //call recursive to cleanup filter in filters
            cleanupDataSourceFilter(f);
        }
    } else {
        //cleanup property logic and filters
        for (prop in filter) {
            switch (prop) {
                case "logic":
                case "filters":
                    delete filter[prop];
                    break;
                default:
                    break;
            }
        }
    }
}

/**
 * @param {string} url url
 * @param {kendo.data.DataSource} dataSource dataSource which that options to put into session
 * @param {Function} callback function callback
 */
function putDataSourceOption(url, dataSource, callback = null) {
    if (callback !== null && typeof (callback) !== "function") {
        throw "callback must be function";
    }

    var options = {
        Page: dataSource.page(),
        PageSize: dataSource.pageSize(),
        Sorts: dataSource.sort(),
        Filter: dataSource.filter()
    };

    //parse type of date 
    if (options.Filter !== null && options.Filter !== undefined && options.Filter.filters.length > 0) {
        //level 1
        for (var i in options.Filter.filters) {
            var f1 = options.Filter.filters[i];

            if (Object.prototype.toString.call(f1.value) === "[object Date]") {
                f1.value = kendo.toString(f1.value, "yyyy-MM-ddTHH:mm:ss");
            }

            if (f1.logic !== null && f1.logic !== undefined && f1.filters !== null
                && f1.filters !== undefined && f1.filters.length > 0) {
                //level 2
                for (var j in f1.filters) {
                    var f2 = f1.filters[j];

                    if (Object.prototype.toString.call(f2.value) === "[object Date]") {
                        f2.value = kendo.toString(f2.value, "yyyy-MM-ddTHH:mm:ss");
                    }
                }
            }
        }
    }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=UTF-8",
        url: url,
        data: JSON.stringify(options),
        success: function (e) {
            if (callback !== null) {
                callback();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("Server Error. Call administrator");
        }
    });
}

/**
 * @param {object} source dataSource option from session
 * @param {object} target taget dataSource option
 */
function mapDataSourceOption(source, target) {
    if (source.Page !== null) {
        target.page = source.Page;
    }

    if (source.PageSize !== null) {
        target.pageSize = source.PageSize;
    }

    if (source.Sorts !== null && source.Sorts.length > 0) {
        target.sort = source.Sorts;
    }

    if (source.Filter !== null) {
        cleanupDataSourceFilter(source.Filter);
        target.filter = source.Filter;
    }
}


/**
 * Generate incremental id on kendo grid 
 *  @param {string} id field id to generate
 */
function generateID(id, datasource) {
    // Get the latest sequential ID for this sector. 
    AutoID = 1;
    if (datasource.data().length > 1) {
        var temp = datasource.aggregates()[id];
        AutoID = parseInt(temp.max) + 1; // Save the new ID 
        localStorage.setItem(id, AutoID);
    } else {
        localStorage.setItem(id, AutoID);
    }
    return AutoID;
} 

function dropdownListGenerator(element, url, option, field, optionLabel, filter = true) {
    var dataSource = generateOptionDataSource(
        url,
        option,
        field,
    );

    if (filter) {
        element.kendoDropDownList({
            optionLabel: optionLabel,
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: dataSource,
            filter: "contains",
            template: "#= Text #",
        });
    } else {
        element.kendoDropDownList({
            optionLabel: optionLabel,
            dataTextField: "Text",
            dataValueField: "Value",
            dataSource: dataSource,
            template: "#= Text #",
        });
    }
    
}

//disable enable grid button
function onDataBound(grid, deleteOnly, status, e, fieldStatus) {
    if (!deleteOnly) {
        $("#"+grid+" tbody tr .k-grid-edit").each(function () {
            var currentDataItem = $("#" + grid).data("kendoGrid").dataItem($(this).closest("tr")),
                statusDb = currentDataItem[fieldStatus];
            if ($.inArray(statusDb, status) == -1) {
                $(this).removeClass("k-grid-edit")
                    .children().eq(0)
                    .addClass("k-state-disabled")
                    .attr({
                        "data-placement": "right",
                        "title": "Status data sedang menunggu persetujuan / sudah selesai, sudah tidak bisa diedit"
                    });
            }


        })
    }

    $("#" + grid +" tbody tr .k-grid-deleted").each(function () {
        var currentDataItem = $("#" + grid).data("kendoGrid").dataItem($(this).closest("tr")),
            statusDb = currentDataItem[fieldStatus];
        if ($.inArray(statusDb, status) == -1) {
            $(this).removeClass("k-grid-deleted")
                .children().eq(0)
                .addClass("k-state-disabled")
                .attr({
                    "data-placement": "right",
                    "title": "Status data sedang menunggu persetujuan / sudah selesai, sudah tidak bisa dihapus"
                });
        }
    })

    $("#" + grid + " tbody tr .k-grid-download").each(function () {
        var currentDataItem = $("#" + grid).data("kendoGrid").dataItem($(this).closest("tr")),
            statusDb = currentDataItem[fieldStatus];
        if (statusDb == "APPROVE_BY_FINANCE" || statusDb == "COMPLETE") {

        } else {
            $(this).removeClass("k-grid-download")
                .children().eq(0)
                .addClass("k-state-disabled")
                .attr({
                    "data-placement": "right",
                    "title": "Status data masih dalam proses pengerjaan"
                });
        }
    })
}

//maping data for datasource kendo
function mappingData(source, target, data) {
    target.add(data);
    source.remove(data);
}

/**
 * Get template from kendo template
 * @param {string} template param content
 * @param {object} data param data
 */
function getTemplate(template, data) {
    var result = kendo.template(template);
    return result(data);
}

//kendo upload
$(".form-control-upload").kendoUpload({
    async: {
        saveUrl: urlAction("/FileManagement/Save"),
        batch: true,
        autoUpload: true,
        multiple: true,
    },
    localization: {
        select: "Select File...",
    },
    multiple: true,
    showFileList: false
});

//function kendoUploadOnSelect(e, docType, imageType, videoType, fileType) {
//    docType = (typeof docType !== "undefined") ? docType : true;
//    imageType = (typeof imageType !== "undefined") ? imageType : true;
//    videoType = (typeof videoType !== "undefined") ? videoType : true;
//    fileType = (typeof fileType !== "undefined") ? fileType : true;
//    var file = e.files[0],
//        fileExtension = file.extension.toLowerCase(),
//        allowedExtension = [],
//        extension = {
//            docType: [".pdf", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx"],
//            imageType: [".jpg", ".jpeg", ".png"],
//            //videoType: [".mp4", ".mkv", ".mpeg", ".mpg", ".wmv"],
//            videoType: [".mp4", ".ogg", ".WebM"],
//            fileType: [".jpg", ".jpeg", ".png", ".mp4", ".pdf"],
//        },
//        size = {
//            allType: 1048576000,
//            //docType: 15728640,
//            docType: 31457280,
//            imageType: 10485760,
//            videoType: 1048576000,
//            fileType: 52428800,
//            //videoType: 1048576000
//        },
//        sizeWord = {
//            allType: "500 MB",
//            docType: "30 MB",
//            imageType: "30 MB",
//            videoType: "1 GB",
//            fileType: "50 MB"
//        },
//        fileWord = {
//            allType: "file",
//            docType: "document file",
//            imageType: "image file",
//            videoType: "video file",
//            fileType: "image and video file"
//        },
//        flagSize = false,
//        flagExtension = false,
//        message = "Server error! Please contact your administrator.",
//        option1 = undefined,
//        option2 = undefined;

//    if (docType && imageType && videoType) {
//        // allow all list file type to upload.
//        allowedExtension = $.merge($.merge($.merge([], extension.docType), extension.imageType), extension.videoType);
//        option1 = new option(allowedExtension, size.allType, sizeWord.allType, fileWord.allType);
//        filterProcess(option1);
//    } else if (!docType && imageType && videoType) {
//        // allow image & video file type to upload.
//        allowedExtension = $.merge($.merge([], extension.imageType), extension.videoType);
//        option1 = new option(extension.imageType, size.imageType, sizeWord.imageType, fileWord.imageType);
//        option2 = new option(extension.videoType, size.videoType, sizeWord.videoType, fileWord.videoType);
//        filterProcess(option1, option2);
//    } else if (docType && !imageType && videoType) {
//        // allow doc & video file type to upload.
//        allowedExtension = $.merge($.merge([], extension.docType), extension.videoType);
//        option1 = new option(extension.docType, size.docType, sizeWord.docType, fileWord.docType);
//        option2 = new option(extension.videoType, size.videoType, sizeWord.videoType, fileWord.videoType);
//        filterProcess(option1, option2);
//    } else if (docType && imageType && !videoType) {
//        // allow doc & image file type to upload.
//        allowedExtension = $.merge($.merge([], extension.docType), extension.imageType);
//        option1 = new option(extension.docType, size.docType, sizeWord.docType, fileWord.docType);
//        option2 = new option(extension.imageType, size.imageType, sizeWord.imageType, fileWord.imageType);
//        filterProcess(option1, option2);
//    } else {
//        if (docType) {
//            // allow doc file type to upload.
//            allowedExtension = extension.docType;
//            option1 = new option(extension.docType, size.docType, sizeWord.docType, fileWord.docType);
//            filterProcess(option1);
//        } else if (imageType) {
//            // allow image file type to upload.
//            allowedExtension = extension.imageType;
//            option1 = new option(extension.imageType, size.imageType, sizeWord.imageType, fileWord.imageType);
//            filterProcess(option1);
//        } else if (videoType) {
//            // allow video file type to upload.
//            allowedExtension = extension.videoType;
//            option1 = new option(extension.videoType, size.videoType, sizeWord.videoType, fileWord.videoType);
//            filterProcess(option1);
//        } else if (fileType) {
//            // allow doc & image file type to upload.
//            allowedExtension = extension.fileType;
//            option1 = new option(extension.fileType, size.fileType, sizeWord.fileType, fileWord.fileType);
//            filterProcess(option1);
//        } else {
//            message = "Please select the file type option to handle the upload."
//            errorMessage();
//            e.preventDefault();
//        }
//    }

//    if (flagSize) {
//        errorMessage();
//        e.preventDefault();
//    }

//    if (flagExtension) {
//        errorMessage();
//        e.preventDefault();
//    }

//    function option(extension, size, wordSize, wordType) {
//        this.extension = extension;
//        this.size = size;
//        this.wordSize = wordSize;
//        this.wordType = wordType;
//    }

//    function filterProcess(option1, option2) {
//        if (option2 === undefined) {
//            if (option1.extension !== undefined && option1.size !== undefined &&
//                option1.wordSize !== undefined && option1.wordType !== undefined) {
//                if ($.inArray(fileExtension, allowedExtension) === -1) {
//                    flagExtension = true;
//                    message = fileTypeErrMsg(allowedExtension);
//                } else {
//                    if (file.size > option1.size) {
//                        flagSize = true;
//                        message = fileSizeErrMsg(option1.wordType, option1.wordSize);
//                    }
//                }
//            } else {
//                errorMessage();
//                e.preventDefault();
//            }
//        } else {
//            if (option1.extension !== undefined && option1.size !== undefined &&
//                option1.wordSize !== undefined && option1.wordType !== undefined &&
//                option2.extension !== undefined && option2.size !== undefined &&
//                option2.wordSize !== undefined && option2.wordType !== undefined) {
//                if ($.inArray(fileExtension, allowedExtension) === -1) {
//                    flagExtension = true;
//                    message = fileTypeErrMsg(allowedExtension);
//                }
//                if ($.inArray(fileExtension, option1.extension) > -1) {
//                    if (file.size > option1.size) {
//                        flagSize = true;
//                        message = fileSizeErrMsg(option1.wordType, option1.wordSize);
//                    }
//                } else {
//                    if (file.size > option2.size) {
//                        flagSize = true;
//                        message = fileSizeErrMsg(option2.wordType, option2.wordSize);
//                    }
//                }
//            } else {
//                errorMessage();
//                e.preventDefault();
//            }
//        }
//    }

//    function fileSizeErrMsg(wordType, wordSize) {
//        var ret = kendo.format("Ukuran {0} tidak boleh melebihi {1}.", wordType, wordSize);
//        return ret;
//    }

//    function fileTypeErrMsg(arrExtension) {
//        var list = arrExtension.join(", ");
//        var ret = kendo.format("File yang diperbolehkan hanya berupa {0}.", list);
//        return ret;
//    }

//    function errorMessage() {
//        setTimeout(function () {
//            swal({
//                title: "Error",
//                text: message,
//                type: "error",
//            });
//        }, 200);
//    }
//}

function detailInitAsset(detailCell, ds) {
    $("<div/>").appendTo(detailCell).kendoGrid({
        dataSource: ds,
        filterable: kendoGridFilterable,
        pageable: {
            pageSizes: true
        },
        sortable: true,
        noRecords: {
            template: "Data Tidak Tersedia"
        },
        columns: [
            {
                field: "Photo",
                title: "",
                width: "65px",
                template: $("#photo-template").html(),
                filterable: false,
                sortable: false
            },
            {
                field: "AssetNumber",
                title: "Fixed Asset Number",
                width: "150px",
            },
            {
                field: "Description",
                title: "Fixed Asset Description",
                width: "220px",
                template: "#=Description#",
            },
            {
                field: "ValueIdr",
                width: "150px",
                title: "Value IDR",
                format: "{0:c2}",
                template: "<div class=\"text-right\">#= kendo.toString(ValueIdr, \"c2\")#</div>"
            },
            {
                field: "ValueUsd",
                title: "Value USD",
                width: "150px",
                format: "{0:n2}",
                template: "<div class=\"text-right\">#= kendo.toString(ValueUsd, \"n2\")#</div>"
            },
            {
                field: "Location",
                title: "Physical Location",
                width: "150px",
                filterable: false,
                sortable: false
            }
        ],
    });
}

function kendoUploadOnSelect(e, docType, imageType, videoType, fileType) {
    docType = (typeof docType !== "undefined") ? docType : true;
    imageType = (typeof imageType !== "undefined") ? imageType : true;
    videoType = (typeof videoType !== "undefined") ? videoType : true;
    fileType = (typeof fileType !== "undefined") ? fileType : true;
    var file = e.files[0],
        fileExtension = file.extension.toLowerCase(),
        allowedExtension = [],
        extension = {
            docType: [".pdf", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx"],
            imageType: [".jpg", ".jpeg", ".png"],
            videoType: [".mp4", ".ogg", ".WebM"],
            fileType: [".jpg", ".jpeg", ".png", ".mp4", ".pdf"],
        },
        size = {
            allType: 1048576000,
            docType: 31457280,
            imageType: 10485760,
            videoType: 1048576000,
            fileType: 52428800,
        },
        sizeWord = {
            allType: "500 MB",
            docType: "30 MB",
            imageType: "30 MB",
            videoType: "1 GB",
            fileType: "50 MB"
        },
        fileWord = {
            allType: "file",
            docType: "document file",
            imageType: "image file",
            videoType: "video file",
            fileType: "image and video file"
        },
        flagSize = false,
        flagExtension = false,
        message = "Server error! Please contact your administrator.",
        option1 = undefined,
        option2 = undefined;

    if (docType && imageType && videoType) {
        // allow all list file type to upload.
        allowedExtension = $.merge($.merge($.merge([], extension.docType), extension.imageType), extension.videoType);
        option1 = new option(allowedExtension, size.allType, sizeWord.allType, fileWord.allType);
        filterProcess(option1);
    } else if (!docType && imageType && videoType) {
        // allow image & video file type to upload.
        allowedExtension = $.merge($.merge([], extension.imageType), extension.videoType);
        option1 = new option(extension.imageType, size.imageType, sizeWord.imageType, fileWord.imageType);
        option2 = new option(extension.videoType, size.videoType, sizeWord.videoType, fileWord.videoType);
        filterProcess(option1, option2);
    } else if (docType && !imageType && videoType) {
        // allow doc & video file type to upload.
        allowedExtension = $.merge($.merge([], extension.docType), extension.videoType);
        option1 = new option(extension.docType, size.docType, sizeWord.docType, fileWord.docType);
        option2 = new option(extension.videoType, size.videoType, sizeWord.videoType, fileWord.videoType);
        filterProcess(option1, option2);
    } else if (docType && imageType && !videoType) {
        // allow doc & image file type to upload.
        allowedExtension = $.merge($.merge([], extension.docType), extension.imageType);
        option1 = new option(extension.docType, size.docType, sizeWord.docType, fileWord.docType);
        option2 = new option(extension.imageType, size.imageType, sizeWord.imageType, fileWord.imageType);
        filterProcess(option1, option2);
    } else {
        if (docType) {
            // allow doc file type to upload.
            allowedExtension = extension.docType;
            option1 = new option(extension.docType, size.docType, sizeWord.docType, fileWord.docType);
            filterProcess(option1);
        } else if (imageType) {
            // allow image file type to upload.
            allowedExtension = extension.imageType;
            option1 = new option(extension.imageType, size.imageType, sizeWord.imageType, fileWord.imageType);
            filterProcess(option1);
        } else if (videoType) {
            // allow video file type to upload.
            allowedExtension = extension.videoType;
            option1 = new option(extension.videoType, size.videoType, sizeWord.videoType, fileWord.videoType);
            filterProcess(option1);
        } else if (fileType) {
            // allow doc & image file type to upload.
            allowedExtension = extension.fileType;
            option1 = new option(extension.fileType, size.fileType, sizeWord.fileType, fileWord.fileType);
            filterProcess(option1);
        } else {
            message = "Please select the file type option to handle the upload."
            errorMessage();
            e.preventDefault();
        }
    }

    if (flagSize) {
        errorMessage();
        e.preventDefault();
    }

    if (flagExtension) {
        errorMessage();
        e.preventDefault();
    }

    function option(extension, size, wordSize, wordType) {
        this.extension = extension;
        this.size = size;
        this.wordSize = wordSize;
        this.wordType = wordType;
    }

    function filterProcess(option1, option2) {
        if (option2 === undefined) {
            if (option1.extension !== undefined && option1.size !== undefined &&
                option1.wordSize !== undefined && option1.wordType !== undefined) {
                if ($.inArray(fileExtension, allowedExtension) === -1) {
                    flagExtension = true;
                    message = fileTypeErrMsg(allowedExtension);
                } else {
                    if (file.size > option1.size) {
                        flagSize = true;
                        message = fileSizeErrMsg(option1.wordType, option1.wordSize);
                    }
                }
            } else {
                errorMessage();
                e.preventDefault();
            }
        } else {
            if (option1.extension !== undefined && option1.size !== undefined &&
                option1.wordSize !== undefined && option1.wordType !== undefined &&
                option2.extension !== undefined && option2.size !== undefined &&
                option2.wordSize !== undefined && option2.wordType !== undefined) {
                if ($.inArray(fileExtension, allowedExtension) === -1) {
                    flagExtension = true;
                    message = fileTypeErrMsg(allowedExtension);
                }
                if ($.inArray(fileExtension, option1.extension) > -1) {
                    if (file.size > option1.size) {
                        flagSize = true;
                        message = fileSizeErrMsg(option1.wordType, option1.wordSize);
                    }
                } else {
                    if (file.size > option2.size) {
                        flagSize = true;
                        message = fileSizeErrMsg(option2.wordType, option2.wordSize);
                    }
                }
            } else {
                errorMessage();
                e.preventDefault();
            }
        }
    }

    function fileSizeErrMsg(wordType, wordSize) {
        var ret = kendo.format("Ukuran {0} tidak boleh melebihi {1}.", wordType, wordSize);
        return ret;
    }

    function fileTypeErrMsg(arrExtension) {
        var list = arrExtension.join(", ");
        var ret = kendo.format("File yang diperbolehkan hanya berupa {0}.", list);
        return ret;
    }

    function errorMessage() {
        setTimeout(function () {
            swal({
                title: "Error",
                text: message,
                type: "error",
            });
        }, 200);
    }
}