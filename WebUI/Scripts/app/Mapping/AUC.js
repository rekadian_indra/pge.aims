﻿var $actualCost = $("#actualCost");
var $formAUC = $("#formAUC");
var $aucOptions = $("#aucOptions");
var $actualOptions = $("#actualOptions");

var AUCds = null;
var mandatoryFields = ["WBSElement", "DocNumber", "ValueIdr"];



function editAUC(e, datasource, parentData) {
    //kamus
    var label = "";

    //algoritma
    e.container.find(".k-edit-label").each(function () {
        label = $(this).find("label").attr("for");
        if (mandatoryFields.includes(label)) {
            $(this).find("label").append("<span style=\"color:red\"> *</span>");
        }
    })

    if (e.model.isNew()) {
        //set field
        var id = generateID("Id", datasource);
        e.model.set("Id", id);
        $(".k-window-title").text(`Tambah (${parentData.AssetNumber} - ${parentData.Description})`);
    }
    else {
        $(".k-window-title").text(`Edit (${parentData.AssetNumber} - ${parentData.Description})`);
    }
}

function deleteItemChild(e) {
    e.preventDefault();
    var data = this.dataItem(getDataRowGrid(e));
    goToDeleteLocal(this.dataSource, data, `${_AUCSTORAGE_}${data.AssetIdOffline}`, null);
}