﻿function uploadPhotoEditor(saveUrl, removeUrl) {
    $("#img-thumb").css({ cursor: "pointer" });

    $("#img-upload").kendoUpload({
        async: {
            saveUrl: saveUrl,
            removeUrl: removeUrl,
        },
        multiple: false,
        upload: onUpload,
        success: function (e) {
            if (e.operation == "remove") {
            } else {
                var file = e.response[0];
                $("#img-thumb").find("img").attr("src", file.absolutepath);
                $("#Photo").val(file.filepath);
            }
            $(".sweet-alert").hide().prev().hide();
        }
    })
    .closest(".k-upload")
    .hide();

    $("#img-thumb").on("click", function (e) {
        $("#img-upload").trigger("click");
    });
}

function onUpload(e) {
    // An array with information about the uploaded files
    var files = e.files;

    // Checks the extension of each file and aborts the upload if it is not .jpg
    $.each(files, function () {
        if (this.extension.toLowerCase() != ".jpeg" && this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".png") {
            alert("Only .jpg files can be uploaded")
            e.preventDefault();
        } else {
            swal({
                title: "Loading",
                text: "Upload Proccess...",
                imageUrl: "/Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden",
            });
        }
    });
}
