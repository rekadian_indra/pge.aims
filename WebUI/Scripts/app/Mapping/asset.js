﻿var $gridFA = $("#gridFA");
var FAds = null;
var _FASTORAGE_ = "fa-storage";
var _AUCSTORAGE_ = "auc-storage-";
var _DESTROY_ = "destroy";
var _STATE_ = "__STATE__";

var fields = ["AssetIdOffline", "AssetNumber", "SubNumber", "Description", "ValueIdr", "ValueUsd", "LocationId", "Photo"];
var aucFields = ["AssetId", "ActualCostId", "WBSElement", "ValueIdr", "ValueUsd"];

function optionData(element, url, option, field, optionLabel) {
    var dataSource = generateOptionDataSource(
        url,
        option,
        field,
    );

    element.kendoDropDownList({
        optionLabel: optionLabel,
        dataTextField: "Text",
        dataValueField: "Value",
        dataSource: dataSource,
        filter: "contains",
    });
}

function hideEditor(element, selector) {
    element.container.find(selector).parent("div .k-edit-label").hide();
    element.container.find(selector).parent().next("div .k-edit-field").hide();
}

function editor(container, options, url, option, field, optionLabel) {
    optionData($('<input required name="' + options.field + '"/>').appendTo(container), url, option, field, optionLabel);
}

function contentEditor(container, options) {
    var defaultTools = kendo.ui.Editor.defaultTools;
    defaultTools["insertLineBreak"].options.shift = false;
    delete defaultTools["insertParagraph"].options;

    $('<textarea required data-required-msg="This Field Is Required" name="' + options.field + '" style="width:90%"></textarea>')
        .appendTo(container)
        .kendoEditor({
            tools: [
                "formatting",
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "indent",
                "outdent",
                "createLink",
                "unlink",
                "viewHtml",
            ],
            resizable: true,
        });
}

function uploadForm(container, options, linkNoImage, saveUrl, removeUrl) {
    var imageUri = linkNoImage;

    var $input = $("<input type=\"hidden\" required=\"required\" data-required-msg=\"This Field Is Required\" name=" + options.field + " />"),
        $thumbnail = $("<div class=\"thumbnail\" style=\"margin-bottom:0px\"><img class=\"center-cropped\" src=\"" + imageUri + "\"/></div>"),
        $colum = $("<div class=\"col-xs-12\"><div class=\"inputt col-xs-3\"></div><div class=\"infoo col-xs-8\"></div></div><br/>"),
        $padding = $("<div style=\"padding-bottom:90px;\"></div>");

    $thumbnail.css({ cursor: "pointer" });

    if (options.model.Photo) {
        $thumbnail.find("img").attr("src", options.model.Photo);
    }

    $colum.appendTo(container);
    $padding.appendTo(container);
    $input.appendTo(container);
    var $inputt = container.find(".inputt");
    var $infoo = container.find(".infoo");
    $infoo.append($("#upload-info").html());

    $thumbnail.appendTo($inputt);

    $("<input type=\"file\" name=\"files\" class=\"hide\" />")
        .appendTo($inputt)
        .kendoUpload({
            async: {
                saveUrl: saveUrl,
                removeUrl: removeUrl,
            },
            multiple: false,
            upload: onUpload,
            success: function (e) {

                if (e.operation == "remove") {
                    container.find($input).val("");
                    container.find($input).trigger("change");
                } else {
                    var file = e.response[0];
                    container.find($thumbnail).find("img").attr("src", file.absolutepath);
                    container.find($input).val(file.filepath);
                    container.find($input).trigger("change");
                }
                $(".sweet-alert").hide().prev().hide();
            }
        })
        .closest(".k-upload")
        .hide();

    container.find($thumbnail).on("click", function (e) {
        container.find("input[type=\"file\"]").trigger("click");
    });
}

function onUpload(e) {
    // An array with information about the uploaded files
    var files = e.files;

    // Checks the extension of each file and aborts the upload if it is not .jpg
    $.each(files, function () {
        if (this.extension.toLowerCase() != ".jpeg" && this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".png") {
            alert("Only .jpg files can be uploaded")
            e.preventDefault();
        } else {
            swal({
                title: "Loading",
                text: "Upload Proccess...",
                imageUrl: "/Content/sweet-alert/ajax-loader.gif",
                closeOnConfirm: false,
                confirmButtonClass: "hidden",
            });
        }
    });
}

function edit(e, datasource) {
    //kamus

    //algoritma
    e.container.find(".k-edit-label").each(function () {
        $(this).find("label").append("<span style=\"color:red\"> *</span>");
    })

    if (e.model.isNew()) {
        //set field
        var id = generateID("AssetIdOffline", datasource);
        e.model.set("AssetIdOffline", id);
        $(".k-window-title").text("Tambah");
    }
    else {
        $(".k-window-title").text("Edit");
    }
}

function deleteItem(e) {
    e.preventDefault();
    var data = this.dataItem(getDataRowGrid(e));
    goToDeleteLocal(this.dataSource, data, _FASTORAGE_, _AUCSTORAGE_);
}

$("form.form-horizontal").submit(function (e) {
    //kamus
    var $form = $(this),
        localDataAssets,
        localDataAUC,
        childStorage,
        error = [],
        incrementAuc = 0;


    //algo
    if (localStorage.getItem(_FASTORAGE_) === "[]") {
        error.push("Please fill data asset first")
    } else {
        localDataAssets = JSON.parse(localStorage[_FASTORAGE_]);
        console.log("masuk 1");
        $.each(localDataAssets, function (i, v) {
            console.log(i);
            $.each(fields, function (j, f) {
                if (v[_STATE_] != _DESTROY_) {
                    var value = v[f],
                        input = kendo.format("<input name=\"Assets[{0}].{1}\" value=\"{2}\" type=\"hidden\" />", i, f, value);

                    $(input).appendTo($form);
                }

            });
            childStorage = `${_AUCSTORAGE_}${v.AssetIdOffline}`;

            //check if empty
            console.log(localStorage.getItem(childStorage) === "[]");
            if (localStorage.getItem(childStorage) === "[]") {
                error.push(`Please fill data AUC for Fix Asset Number : ${v.AssetNumber}`);
            } else {
                localDataAUC = JSON.parse(localStorage[childStorage]);
                $.each(localDataAUC, function (k, val) {
                    if (v[_STATE_] != _DESTROY_) {
                        $.each(aucFields, function (l, aucf) {
                            var value = val[aucf],
                                input = kendo.format("<input name=\"AssetActualCosts[{0}].{1}\" value=\"{2}\" type=\"hidden\" />", incrementAuc, aucf, value);

                            $(input).appendTo($form);
                        });
                        incrementAuc++;
                    }
                });
            }
        });
    }

    if (error.length > 0) {
        e.preventDefault();
        alert(error.join("\n"));
    } else {
        localStorage.clear();
    }
});

function calculateValueFA(storage, idrField, usdField, dataSource, id) {
    //kamus
    var localData,
        sumIdr = 0,
        sumUsd = 0;

    if (localStorage.getItem(storage) != "[]") {
        localData = JSON.parse(localStorage[storage]);

        $.each(localData, function (k, val) {
            sumIdr += val[idrField];
            sumUsd += val[usdField];
        });
        console.log(id);
        dataSource.getByUid(id).set(idrField, sumIdr);
        dataSource.getByUid(id).set(usdField, sumUsd);
        dataSource.sync();
    }
}