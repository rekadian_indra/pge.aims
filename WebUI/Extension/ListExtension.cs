﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebUI.Extension
{
    public static class ListExtension
    {
        static Random random = new Random();

        public static IEnumerable<T> RandomPermutation<T>(this IEnumerable<T> sequence)
        {
            T[] retArray = sequence.ToArray();


            for (int i = 0; i < retArray.Length - 1; i += 1)
            {
                int swapIndex = random.Next(i, retArray.Length);
                if (swapIndex != i)
                {
                    T temp = retArray[i];
                    retArray[i] = retArray[swapIndex];
                    retArray[swapIndex] = temp;
                }
            }

            return retArray;
        }
    }
}