﻿using System.Web.Mvc;

namespace WebUI.Extension
{
    public static class ControllerExtensions
    {
        private const string KEY = "message";
        private const string TYPE = "info";

        /**
         * mengembalikan string message yang sudah diformat
         */
        public static string GetMessage(this Controller instance)
        {
            string result = "";
            if (instance.TempData[KEY] != null && instance.TempData[TYPE] != null)
            {
                if((bool)instance.TempData[TYPE])
                    result = "<div class=\"alert alert-info\"><a class=\"close\" data-dismiss=\"alert\">×</a>" + instance.TempData[KEY].ToString() + "</div>";
                else
                    result = "<div class=\"alert alert-danger\"><a class=\"close\" data-dismiss=\"alert\">×</a>" + instance.TempData[KEY].ToString() + "</div>";
            }

            return result;
        }

        /**
         * set message dan template yang digunakan
         * @param template {0} berhasil diubah
         */
        public static void SetMessage(this Controller instance, string value, string template = null)
        {
            string message = "";
            if (template == null)
            {
                message = value;
            }
            else
            {
                message = string.Format(template, value);
            }
            instance.TempData[KEY] = message;
            instance.TempData[TYPE] = true;
        }

        public static void SetMessage(this Controller instance, string value, bool isSuccess = true, string template = null)
        {
            string message = "";
            if (template == null)
            {
                message = value;
            }
            else
            {
                message = string.Format(template, value);
            }
            instance.TempData[KEY] = message;
            instance.TempData[TYPE] = isSuccess;
        }
    }
}