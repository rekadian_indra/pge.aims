﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//notes
// on edit set datasource with agregate

using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    //[AuthorizeUser(ModuleName = UserModule.Crew)]
    [Authorize]
    public class MappingController : BaseController<MappingFormStub>
    {   
        public MappingController(
            IAUCRepository repoAUC,
            IActualCostRepository repoActual,
            ILocationRepository repoLocation,
            IAssetRepository repoAsset,
            IMappingRepository repoMapping,
            IViewMappingAssetRepository repoViewMappingAsset,
            IViewActualRepository repoViewActual,
            IViewMappingAssetActualCostRepository repoViewMappingAssetActualCost,
            IMappingAssetRepository repoMappingAsset,
            IMappingAssetActualCostRepository repoMappingAssetActualCost,
            IViewAUCRepository repoViewAUC,
            IViewMappingRepository repoVIewMapping,
            IViewAssetRepository repoViewAsset,
            IPlantRepository repoPlant
        )
        {
            RepoAUC = repoAUC;
            RepoActual = repoActual;
            RepoLocation = repoLocation;
            RepoAsset = repoAsset;
            RepoMapping = repoMapping;
            RepoViewMappingAsset = repoViewMappingAsset;
            RepoViewActual = repoViewActual;
            RepoViewMappingAssetActualCost = repoViewMappingAssetActualCost;
            RepoMappingAsset = repoMappingAsset;
            RepoMappingAssetActualCost = repoMappingAssetActualCost;
            RepoViewAUC = repoViewAUC;
            RepoViewMapping = repoVIewMapping;
            RepoViewAsset = repoViewAsset;
            RepoPlant = repoPlant;
        }

        [MvcSiteMapNode(Title = TitleSite.Mapping, ParentKey = KeySite.Dashboard, Key = KeySite.IndexMapping)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        #region create
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexMapping, Key = KeySite.CreateMapping)]
        public override async Task<ActionResult> Create()
        {
            //lib
            MappingFormStub model = new MappingFormStub();

            //algorithm
            await Task.Delay(0);
            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Create(MappingFormStub model)
        {
            if (ModelState.IsValid)
            {
                Mapping dbObject = new Mapping();

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoMapping.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject("AppGlobalMessage", "CreateSuccess").ToString();
                this.SetMessage(model.Description, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        #endregion

        #region edit
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexMapping, Key = KeySite.EditMapping)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();
            MappingFormStub model;

            //algoritma
            Mapping dbObject = await RepoMapping.FindByPrimaryKeyAsync(primaryKey);
            model = new MappingFormStub(dbObject);

            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Edit(MappingFormStub model)
        {
            if (ModelState.IsValid)
            {
                Mapping dbObject = RepoMapping.FindByPrimaryKey(model.MappingId);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoMapping.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(model.Description, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        #endregion

        #region delete
        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();

            //algoritma
            Mapping dbObject = await RepoMapping.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!RepoMapping.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<ActionResult> DeleteMappingActualCost(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();
            ResponseModel response = new ResponseModel(true);

            //algoritma
            if (id.Length == 1)
            {
                MappingAssetActualCost dbObject = await RepoMappingAssetActualCost.FindByPrimaryKeyAsync(primaryKey);

                if (!RepoMappingAssetActualCost.Delete(dbObject))
                {
                    string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                    response.SetFail(message);
                }
            }
            else if (id.Length > 1)
            {
                List<MappingAssetActualCost> dbObjects = new List<MappingAssetActualCost>();

                await RepoMappingAssetActualCost.DeleteAllAsync(dbObjects);
                try
                {
                    foreach (object i in id)
                    {
                        MappingAssetActualCost dbObject = await RepoMappingAssetActualCost.FindByPrimaryKeyAsync(i.ToInteger());
                        RepoMappingAssetActualCost.Delete(dbObject);
                        //dbObjects.Add(dbObject);
                    }
                    //await RepoMappingAssetActualCost.DeleteAllAsync(dbObjects);
                }
                catch
                {
                    string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                    response.SetFail(message);
                }                             
            }            

            return Json(response);
        }
        #endregion

        #region mapping FA
        [MvcSiteMapNode(Title = TitleSite.MappingFA, ParentKey = KeySite.IndexMapping, Key = KeySite.PageMappingFA, PreservedRouteParameters = "id")]
        public async Task<ActionResult> MappingFA(int? id)
        {
            if (id != null)
            {
                //kamus
                Mapping dbObject = RepoMapping.FindByPrimaryKey(id);
                MappingPresentationStub model = new MappingPresentationStub(dbObject);

                //algoritma
                await Task.Delay(0);
                return View("FormFA", model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Create + " " +  TitleSite.Asset, ParentKey = KeySite.PageMappingFA, Key = KeySite.CreateFA, PreservedRouteParameters = "id")]
        public async Task<ActionResult> CreateFA(int? id)
        {
            if (id != null)
            {
                //lib
                AssetFormStub model = new AssetFormStub(id.Value);
                Mapping mapping = RepoMapping.FindByPrimaryKey(id);

                //algorithm
                await Task.Delay(0);
                ViewBag.Description = mapping.Description;
                ViewBag.Status = mapping.Status;
                return View("_FormFA", model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<ActionResult> CreateFA(AssetFormStub model)
        {
            if (ModelState.IsValid)
            {
                Asset dbObject = new Asset();

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoAsset.SaveAsync(dbObject);

                //message
                return RedirectToAction(ActionSite.MappingAUCCreate, new { @Id = model.MappingId, AssetId = dbObject.AssetId });
            }
            else
            {
                //lib
                Mapping mapping = RepoMapping.FindByPrimaryKey(model.MappingId);

                //algorithm
                await Task.Delay(0);
                ViewBag.Description = mapping.Description;
                ViewBag.Status = mapping.Status;
                return View("_FormFA", model);
            }
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Edit + " " + TitleSite.Asset, ParentKey = KeySite.PageMappingFA, Key = KeySite.EditFA, PreservedRouteParameters = "id, AssetId")]
        public async Task<ActionResult> EditFA(int? id, int? AssetId)
        {
            if (id != null && AssetId != null)
            {
                //lib
                Mapping mapping = RepoMapping.FindByPrimaryKey(id.Value); 
                Asset dbObject = RepoAsset.FindByPrimaryKey(AssetId.Value);
                AssetFormStub model = new AssetFormStub(dbObject, id.Value);

                //algorithm
                await Task.Delay(0);
                ViewBag.Description = mapping.Description;
                ViewBag.Name = dbObject.AssetNumber;
                ViewBag.Status = mapping.Status;
                return View("_FormFA", model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<ActionResult> EditFA(AssetFormStub model)
        {
            if (ModelState.IsValid)
            {
                Asset dbObject = RepoAsset.FindByPrimaryKey(model.AssetId);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoAsset.SaveAsync(dbObject);

                //message
                return RedirectToAction(ActionSite.MappingAUCCreate, new { @Id = model.MappingId, AssetId = model.AssetId });
            }
            else
            {
                //lib
                Mapping mapping = RepoMapping.FindByPrimaryKey(model.MappingId);

                //algorithm
                await Task.Delay(0);
                ViewBag.Description = mapping.Description;
                ViewBag.Name = model.AssetNumber;
                ViewBag.Status = mapping.Status;
                return View("_FormFA", model);
            }
        }

        #endregion

        #region mapping AUC
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Create + " " + TitleSite.AUC, ParentKey = KeySite.EditFA, Key = KeySite.CreateAUC)]
        public async Task<ActionResult> MappingAUCCreate(int? Id, int? AssetId)
        {
            if (Id != null && AssetId != null)
            {
                //kamus
                MappingAsset mappingAsset;
                MappingActualCostPresentationStub model;
                Asset asset = RepoAsset.FindByPrimaryKey(AssetId.Value);
                FilterQuery filter;
                
                //algoritma
                
                model = new MappingActualCostPresentationStub();
                model.MappingId = Id.Value;
                
                //find mapping asset id
                filter = new FilterQuery(Field.MappingId,FilterOperator.Equals, Id.Value, FilterLogic.And);
                filter.AddFilter(Field.AssetId, FilterOperator.Equals, AssetId.Value);

                mappingAsset = await RepoMappingAsset.FindAsync(filter);
                model.MappingAssetId = mappingAsset.MappingAssetId;
                model.Status = mappingAsset.Mapping.Status;

                ViewBag.AssetNumber = asset.AssetNumber;
                ViewBag.AssetDesc = asset.Description;
                return View("FormAUC", model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddActual(List<MappingAssetActualCostFormStub> model, double auc)
        {
            //kamus
            double leftOversAUC = 0;
            MappingAssetActualCost dbObject;
            ResponseModel response;
            FilterQuery filter;
            ViewAuc viewAUC = new ViewAuc();
            List<MappingAssetActualCost> dbObjects = new List<MappingAssetActualCost>();

            //algoritma
            for (int i = 0; i < model.Count; i++)
            {
                dbObject = new MappingAssetActualCost();

                //hitung sisa auc
                if (i == 0)
                {
                    if (auc >= model[0].Balance)
                    {
                        model[i].ValueIdr = model[i].Balance;
                    }
                    else
                    {
                        model[i].ValueIdr = auc;
                    }

                    leftOversAUC = auc - (model[0].Balance);
                }
                else
                {
                    if (leftOversAUC > 0)
                    {
                        leftOversAUC = leftOversAUC - (model[i].Balance);
                    }
                    
                    if (leftOversAUC >= model[i].Balance)
                    {
                        model[i].ValueIdr = model[i].Balance;
                    }
                    else if (leftOversAUC < model[i].Balance || leftOversAUC < 1)
                    {
                        model[i].ValueIdr = leftOversAUC;
                    }
                }

                //cari usd konverter & hitung nilai usd
                filter = new FilterQuery(Field.WBSElement, FilterOperator.Equals, model[i].WBSElement);
                viewAUC = RepoViewAUC.Find(filter);

                model[i].ValueUsd = model[i].ValueIdr / (viewAUC != null ? viewAUC.Convertion : 0);

                model[i].MapDbObject(dbObject);
                dbObjects.Add(dbObject);
            }

            //**** kode lama *****
            //foreach (MappingAssetActualCostFormStub obj in model)
            //{
            //    dbObject = new MappingAssetActualCost();

            //    obj.MapDbObject(dbObject);
            //    dbObjects.Add(dbObject);
            //}
            //**** end of kode lama *****

            //simpan ke database
            await RepoMappingAssetActualCost.SaveAllAsync(dbObjects, true);
            response = new ResponseModel(true);

            return Json(response);
        }

        public async Task<ActionResult> EditActual(MappingAssetActualCostFormStub model)
        {
            //kamus
            MappingAssetActualCost dbObject;
            ViewActual view = new ViewActual();
            ViewAuc viewAUC = new ViewAuc();
            ResponseModel response;
            FilterQuery filter = new FilterQuery(Field.ActualCostId, FilterOperator.Equals, model.ActualCostId);
            FilterQuery filterViewAUC = new FilterQuery(Field.WBSElement, FilterOperator.Equals, model.WBSElement);
            List<string> error = new List<string>();

            //algoritma
            //check balance
            dbObject = RepoMappingAssetActualCost.FindByPrimaryKey(new object[1] { model.MappingAssetActualCostId});

            view = RepoViewActual.Find(filter);
            viewAUC = RepoViewAUC.Find(filterViewAUC);

            //check actual and AUC balance
            //if ((view.Balance + dbObject.ValueIdr) <= 0)
            //{
            //    error.Add($"Sisa biaya untuk realisasi {view.DocNumber} sudah habis silakan hapus realisasi {view.DocNumber} dari list");
            //}
            //if ((view.Balance + dbObject.ValueIdr - model.ValueIdr) < 0)
            //{
            //    error.Add($"Sisa biaya untuk realisasi {view.DocNumber} : Rp {DisplayFormat.NumberFormat(view.Balance + dbObject.ValueIdr)},-");
            //}

            //if ((viewAUC.Balance + dbObject.ValueIdr - model.ValueIdr) < 0)
            //{
            //    error.Add($"Sisa biaya untuk AUC {view.WBSElement} : Rp  {DisplayFormat.NumberFormat(viewAUC.Balance + dbObject.ValueIdr)},-");
            //}

            if (error.Count > 0)
            {
                response = new ResponseModel(false);
                response.Message = string.Join("\n", error);
            }
            else
            {
                //calculate usd value
                model.ValueUsd = model.ValueIdr / viewAUC.Convertion;
                model.MapDbObject(dbObject);

                await RepoMappingAssetActualCost.SaveAsync(dbObject);
                response = new ResponseModel(true);
            }

            return Json(response);
        }
        #endregion

        #region binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Mapping> dbObjects;
            List<MappingPresentationStub> models;
            Task<List<Mapping>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(Field.PlantId, FilterOperator.Equals, User.AreaId);

            dbObjectsTask = RepoMapping.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoMapping.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Mapping, MappingPresentationStub>(dbObjects);            

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMappingAsset(params object[] args)
        {
            //lib
            int count;
            List<ViewMappingAsset> dbObjects;
            List<AssetPresentationStub> models;
            Task<List<ViewMappingAsset>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (args != null && args.Any())
            {
                int mappingId = int.Parse(args.First().ToString());
                if (param.Filter == null)
                    param.Filter = new FilterQuery(Field.MappingId, FilterOperator.Equals, mappingId);
                else
                    param.Filter.AddFilter(Field.MappingId, FilterOperator.Equals, mappingId);
            }

            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMappingAsset.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMappingAssetActualCost(params object[] args)
        {
            //lib
            int count;
            List<ViewMappingAssetActualCost> dbObjects;
            List<MappingAssetActualCostFormStub> models;
            Task<List<ViewMappingAssetActualCost>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (args != null && args.Any())
            {
                int mappingAssetId = int.Parse(args.First().ToString());
                if (param.Filter == null)
                    param.Filter = new FilterQuery(Field.MappingAssetId, FilterOperator.Equals, mappingAssetId);
                else
                    param.Filter.AddFilter(Field.MappingAssetId, FilterOperator.Equals, mappingAssetId);
            }

            dbObjectsTask = RepoViewMappingAssetActualCost.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMappingAssetActualCost.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewMappingAssetActualCost, MappingAssetActualCostFormStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMappingActualCost(params object[] args)
        {
            //lib
            List<ViewActual> actuals;
            Task<List<ViewActual>> dbObjectsTask = null;
            List<ActualPresentationStub> actualModels = new List<ActualPresentationStub>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            List<int> mappingActualCostIds = new List<int>();
            List<MappingAssetActualCost> mappingAssetActuals = new List<MappingAssetActualCost>();
            Task<List<MappingAssetActualCost>> mappingAssetActualCostsTask = null;            
            FilterQuery filterMapping = new FilterQuery(FilterLogic.And);

            //algorithm
            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                filter.AddFilter(Field.Plant, FilterOperator.Equals, User.AreaCode);

            
            if (args != null && args.Any())
            {
                int mappingAssetId = int.Parse(args.First().ToString());

                filter.AddFilter(Field.Balance, FilterOperator.NotEquals, 0);
                filterMapping.AddFilter(Field.MappingAssetId, FilterOperator.Equals, mappingAssetId);
            
                dbObjectsTask = RepoViewActual.FindAllAsync(filter);
                mappingAssetActualCostsTask = RepoMappingAssetActualCost.FindAllAsync(filterMapping);

                await Task.WhenAll(dbObjectsTask, mappingAssetActualCostsTask);

                actuals = dbObjectsTask.Result;
                mappingAssetActuals = mappingAssetActualCostsTask.Result;               

                mappingActualCostIds = mappingAssetActuals.Select(x => x.ActualCostId).ToList();

                if (mappingActualCostIds.Any())
                    actualModels = ListMapper.MapList<ViewActual, ActualPresentationStub>(actuals.Where(x => !mappingActualCostIds.Contains(x.ActualCostId)));
                else
                    actualModels = ListMapper.MapList<ViewActual, ActualPresentationStub>(actuals);
            }
            else
            {            
                dbObjectsTask = RepoViewActual.FindAllAsync(filter);
                mappingAssetActualCostsTask = RepoMappingAssetActualCost.FindAllAsync(filterMapping);

                await Task.WhenAll(dbObjectsTask, mappingAssetActualCostsTask);

                actuals = dbObjectsTask.Result;
                mappingAssetActuals = mappingAssetActualCostsTask.Result;               

                actualModels = ListMapper.MapList<ViewActual, ActualPresentationStub>(actuals, mappingAssetActuals);
            }

            return JsonConvert.SerializeObject(actualModels);
        }

        public async Task<string> BindingMappingActual(params object[] args)
        {
            //lib
            List<ViewActual> actuals;
            Task<List<ViewActual>> dbObjectsTask = null;
            List<ActualPresentationStub> actualModels = new List<ActualPresentationStub>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            List<int> mappingActualCostIds = new List<int>();
            List<MappingAssetActualCost> mappingAssetActuals = new List<MappingAssetActualCost>();
            Task<List<MappingAssetActualCost>> mappingAssetActualCostsTask = null;
            FilterQuery filterMapping = new FilterQuery(FilterLogic.And);            

            if (args != null && args.Any())
            {
                int mappingAssetId = int.Parse(args.First().ToString());

                filter.AddFilter(Field.Balance, FilterOperator.NotEquals, 0);
                filterMapping.AddFilter(Field.MappingAssetId, FilterOperator.Equals, mappingAssetId);

                dbObjectsTask = RepoViewActual.FindAllAsync(filter);
                mappingAssetActualCostsTask = RepoMappingAssetActualCost.FindAllAsync(filterMapping);

                await Task.WhenAll(dbObjectsTask, mappingAssetActualCostsTask);

                actuals = dbObjectsTask.Result;
                mappingAssetActuals = mappingAssetActualCostsTask.Result;

                mappingActualCostIds = mappingAssetActuals.Select(x => x.ActualCostId).ToList();

                if (mappingActualCostIds.Any())
                    actualModels = ListMapper.MapList<ViewActual, ActualPresentationStub>(actuals.Where(x => !mappingActualCostIds.Contains(x.ActualCostId)));
                else
                    actualModels = ListMapper.MapList<ViewActual, ActualPresentationStub>(actuals);
            }
            else
            {
                dbObjectsTask = RepoViewActual.FindAllAsync(filter);
                mappingAssetActualCostsTask = RepoMappingAssetActualCost.FindAllAsync(filterMapping);

                await Task.WhenAll(dbObjectsTask, mappingAssetActualCostsTask);

                actuals = dbObjectsTask.Result;
                mappingAssetActuals = mappingAssetActualCostsTask.Result;

                actualModels = ListMapper.MapList<ViewActual, ActualPresentationStub>(actuals, mappingAssetActuals);
            }

            return JsonConvert.SerializeObject(actualModels);
        }

        [HttpPost]
        public async Task<JsonResult> GetValueActualCost(int? id)
        {
            //kamus
            Task<ActualCost> dbObjectsTask = null;

            //algoritma
            if (id.HasValue)
            {
                dbObjectsTask = RepoActual.FindActualCostByKeyAsync(id.Value);

                await Task.WhenAll(dbObjectsTask);
                ActualCost dbObjects = dbObjectsTask.Result;

                //if (dbObjects.TransactionCurrency != null)
                //{
                //    //if (dbObjects.TransactionCurrency == Currency.IDR.ToString())
                //    //    return (dbObjects.ValueCoArea != null ? $"Rp {DisplayFormat.NumberFormat(dbObjects.ValueCoArea)}" : 0);
                //    //else if (dbObjects.TransactionCurrency == Currency.USD.ToString())
                //    //    return (dbObjects.ValueCoArea != null ? $"US$ {DisplayFormat.NumberFormat(dbObjects.ValueCoArea)}" : 0);
                //    //else
                //    //    return 0;
                //    return (dbObjects.ValueCoArea != null ? dbObjects.ValueCoArea.Value : 0);
                //}
                //else
                //{
                //    return 0;
                //}
                return Json(new ActualPresentationStub(dbObjects));
            }
            else
            {
                return Json("");
            }
        }

        #endregion

        #region approval finance
        [HttpPost]
        public async Task<ActionResult> SendToFinance(MappingAssetFormStub model)
        {
            //kamus
            List<MappingAssetActualCost> draftMappings;
            Mapping dbObject;
            ResponseModel response;
            FilterQuery filter;
            List<string> err = new List<string>();

            //algoritma
            //obejct mapping
            dbObject = await RepoMapping.FindByPrimaryKeyAsync(new object[1] { model.MappingId });

            if (!dbObject.MappingAssets.Where(x => x.Asset.IsDeleted == false).Any())
            {
                response = new ResponseModel(false);
                response.Message = "Belum ada asset yang di mapping";
                return Json(response);
            }


            foreach(MappingAsset ma in dbObject.MappingAssets.Where(x => x.Asset.IsDeleted == false))
            {
                draftMappings = new List<MappingAssetActualCost>();

                if (!ma.MappingAssetActualCosts.Any())
                {
                    err.Add(ma.Asset.AssetNumber);
                }
                else
                {
                    draftMappings = ma.MappingAssetActualCosts.Where(x => x.WBSElement == null).ToList();
                    if (draftMappings.Any())
                    {
                        err.Add(ma.Asset.AssetNumber);
                    }
                }
                
            }
           
            if (err.Any())
            {
                response = new ResponseModel(false);
                response.Message = $"Masi terdapat proses mapping yang belum selesai untuk asset : {string.Join(", ", err)} (Realisasi belum dimapping terhadap AUC)";
            }
            else
            {
                dbObject.Status = StatusAsset.WAITING_APPROVAL_FINANCE.ToString();
                await RepoMapping.SaveAsync(dbObject);
                response = new ResponseModel(true);

                //message
                string template = HttpContext.GetGlobalResourceObject("AppGlobalMessage", "ApprovalSuccess").ToString();
                this.SetMessage(dbObject.Description,true, template);
            }

            return Json(response);
        }
        #endregion
        
    }
}
