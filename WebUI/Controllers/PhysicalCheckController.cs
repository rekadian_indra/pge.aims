﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Business.Models;
using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize]
    public class PhysicalCheckController : BaseController<PhysicalCheckFormStub>
    {
        public PhysicalCheckController(ILogRepository repoLog, IViewMappingAssetRepository repoViewMappingAsset,
            IPhysicalCheckRepository repoPhysicalCheck,
            IPhysicalCheckAssetRepository repoPhysicalCheckAsset,
            IAssetRepository repoAsset,
            IViewPhysicalCheckRepository repoViewPhysicalCheck)
        {
            RepoLog = repoLog;
            RepoViewMappingAsset = repoViewMappingAsset;
            RepoPhysicalCheck = repoPhysicalCheck;
            RepoPhysicalCheckAsset = repoPhysicalCheckAsset;
            RepoAsset = repoAsset;
            RepoViewPhysicalCheck = repoViewPhysicalCheck;            
        }

        //if using ModelBinder to get user login use SGUser parameter
        //if using Principal dont get user login SGUser parameter
        //public ActionResult Index(SGUser user)
        //{
        //}

        [MvcSiteMapNode(Title = TitleSite.PhysicalCheck, ParentKey = KeySite.Dashboard, Key = KeySite.PhysicalCheckIndex)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Report, ParentKey = KeySite.PhysicalCheckIndex, Key = KeySite.Report)]
        public async Task<ActionResult> Report()
        {
            await Task.Delay(0);
            return View("Report");
        }

        #region create
        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.PhysicalCheckIndex, Key = KeySite.CreatePhysicalCheck)]
        public override async Task<ActionResult> Create()
        {
            //lib
            PhysicalCheckFormStub model = new PhysicalCheckFormStub();

            //algorithm
            await Task.Delay(0);
            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(PhysicalCheckFormStub model)
        {
            if (ModelState.IsValid)
            {
                PhysicalCheck dbObject = new PhysicalCheck();

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoPhysicalCheck.SaveAsync(dbObject);

                if (model.ImageGallery != "[]")
                {
                    List<ImagePresentationStub> gallery = new JavaScriptSerializer().Deserialize<List<ImagePresentationStub>>(model.ImageGallery);
                    foreach (ImagePresentationStub data in gallery)
                    {
                        data.PhysicalCheckId = dbObject.PhysicalCheckId;
                        PhysicalCheckAsset nf = new PhysicalCheckAsset();
                        data.MapDbObject(nf);
                        await RepoPhysicalCheckAsset.SaveAsync(nf);
                    }
                }

                //message
                string template = HttpContext.GetGlobalResourceObject("AppGlobalMessage", "CreateSuccess").ToString();
                this.SetMessage(TitleSite.PhysicalCheck, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        #endregion

        #region edit
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.PhysicalCheckIndex, Key = KeySite.EditPhysicalCheck)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();
            PhysicalCheckFormStub model;
            List<PhysicalCheckAsset> imageObjects;
            FilterQuery filter = new FilterQuery(Field.PhysicalCheckId, FilterOperator.Equals, primaryKey);

            //algoritma
            PhysicalCheck dbObject = await RepoPhysicalCheck.FindByPrimaryKeyAsync(primaryKey);
            imageObjects = await RepoPhysicalCheckAsset.FindAllAsync(filter);
            model = new PhysicalCheckFormStub(dbObject, imageObjects);
            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Edit(PhysicalCheckFormStub model)
        {   
            //kamus
            PhysicalCheckAsset nf = new PhysicalCheckAsset();

            if (ModelState.IsValid)
            {
                PhysicalCheck dbObject = RepoPhysicalCheck.FindByPrimaryKey(model.PhysicalCheckId);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoPhysicalCheck.SaveAsync(dbObject);

                FilterQuery filter = new FilterQuery(Field.PhysicalCheckId, FilterOperator.Equals, dbObject.PhysicalCheckId);
                RepoPhysicalCheckAsset.DeleteAll(RepoPhysicalCheckAsset.FindAll(filter));

                if (model.ImageGallery != "[]")
                {
                    List<ImagePresentationStub> gallery = new JavaScriptSerializer().Deserialize<List<ImagePresentationStub>>(model.ImageGallery);
                    foreach (ImagePresentationStub data in gallery)
                    {

                        nf = new PhysicalCheckAsset();
                        data.PhysicalCheckId = dbObject.PhysicalCheckId;
                        data.ImageId = 0;
                        data.MapDbObject(nf);
                        await RepoPhysicalCheckAsset.SaveAsync(nf);
                    }
                }

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(dbObject.Asset.Description, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        public async Task<ActionResult> EditMobile(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();
            PhysicalCheckFormStub model;            
            FilterQuery filter = new FilterQuery(Field.AssetId, FilterOperator.Equals, primaryKey);
            List<SortQuery> sorts = new List<SortQuery> { new SortQuery(Field.PhysicalCheckDate, SortOrder.Descending) };
            
            //algoritma
            Task<Asset> assetTask = RepoAsset.FindAsync(filter);
            Task<PhysicalCheck> lastCheckTask = RepoPhysicalCheck.FindAsync(sorts, filter);

            await Task.WhenAll(assetTask, lastCheckTask);

            //imageObjects = await RepoPhysicalCheckAsset.FindAllAsync(filter);
            model = new PhysicalCheckFormStub(assetTask.Result, lastCheckTask.Result);

            return View("FormMobile", model);
        }

        [HttpPost]
        public async Task<ActionResult> EditMobile(PhysicalCheckFormStub model)
        {
            //kamus
            if (ModelState.IsValid)
            {
                PhysicalCheck dbObject = new PhysicalCheck();

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoPhysicalCheck.SaveAsync(dbObject);

                if (model.ImageGallery != "[]")
                {
                    List<ImagePresentationStub> gallery = new JavaScriptSerializer().Deserialize<List<ImagePresentationStub>>(model.ImageGallery);
                    foreach (ImagePresentationStub data in gallery)
                    {
                        data.PhysicalCheckId = dbObject.PhysicalCheckId;
                        PhysicalCheckAsset nf = new PhysicalCheckAsset();
                        data.MapDbObject(nf);
                        await RepoPhysicalCheckAsset.SaveAsync(nf);
                    }
                }

                //message
                string template = HttpContext.GetGlobalResourceObject("AppGlobalMessage", "CreateSuccess").ToString();
                this.SetMessage(TitleSite.PhysicalCheck, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("FormMobile", model);
            }
        }
        #endregion

        #region binding
        public override async Task<string> Binding(params object[] args)
        {
            ////lib
            //int count;
            //List<PhysicalCheck> dbObjects;
            //List<PhysicalCheckPresentationStub> models;
            //Task<List<PhysicalCheck>> dbObjectsTask = null;
            //Task<int> countTask = null;
            //QueryRequestParameter param = QueryRequestParameter.Current;

            ////algorithm
            //dbObjectsTask = RepoPhysicalCheck.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            //countTask = RepoPhysicalCheck.CountAsync(param.Filter);

            ////get and count data from db in background thread
            //await Task.WhenAll(dbObjectsTask, countTask);

            ////get callback from task
            //dbObjects = dbObjectsTask.Result;
            //count = countTask.Result;

            ////map db data to model
            //models = ListMapper.MapList<PhysicalCheck, PhysicalCheckPresentationStub>(dbObjects);

            //lib
            int count;
            List<ViewPhysicalCheck> dbObjects;
            List<PhysicalCheckPresentationStub> models;
            Task<List<ViewPhysicalCheck>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);


            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(Field.Plant, FilterOperator.Equals, User.AreaCode);

            dbObjectsTask = RepoViewPhysicalCheck.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewPhysicalCheck.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewPhysicalCheck, PhysicalCheckPresentationStub>(dbObjects);


            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public string BindingLastDate(params object[] args)
        {
            ////lib
            //int count;
            //List<PhysicalCheck> dbObjects;
            //List<PhysicalCheckPresentationStub> models;
            //QueryRequestParameter param = QueryRequestParameter.Current;

            ////algorithm
            //dbObjects = RepoPhysicalCheck.FindAllDistinct(param.Skip, param.Take, param.Sorts, param.Filter);
            //count = RepoPhysicalCheck.CountDistinct(param.Filter);


            ////map db data to model
            //models = ListMapper.MapList<PhysicalCheck, PhysicalCheckPresentationStub>(dbObjects);


            //lib
            int count;
            List<ViewPhysicalCheck> dbObjects;
            List<PhysicalCheckPresentationStub> models;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);


            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(Field.Plant, FilterOperator.Equals, User.AreaCode);

            dbObjects = RepoViewPhysicalCheck.FindAllDistinct(param.Skip, param.Take, param.Sorts, param.Filter);
            count = RepoViewPhysicalCheck.CountDistinct(param.Filter);


            //map db data to model
            models = ListMapper.MapList<ViewPhysicalCheck, PhysicalCheckPresentationStub>(dbObjects);
            
            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingAsset()
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                filter.AddFilter(Field.Plant, FilterOperator.Equals, User.AreaName);

            //algorithm
            List<ViewMappingAsset> dbObjects = await RepoViewMappingAsset.FindAllAsync(filter);
            foreach (ViewMappingAsset a in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = $"{a.AssetNumber} - {a.Description}",
                    Value = a.AssetId.ToString()
                };

                options.Add(o);
            }

            return JsonConvert.SerializeObject(options);
        }
        #endregion

        #region delete
        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();

            //algoritma
            PhysicalCheck dbObject = await RepoPhysicalCheck.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!RepoPhysicalCheck.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion
    }
}
