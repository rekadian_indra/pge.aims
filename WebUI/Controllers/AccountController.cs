﻿using Business.Abstract;
using Business.Infrastructure;
using Common.Enums;
using Newtonsoft.Json;
using SecurityGuard.Interfaces;
using SecurityGuard.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebUI.Infrastructure;
using WebUI.Infrastructure.Abstract;
using WebUI.Infrastructure.Concrete;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class AccountController : BaseController
    {
        private IAuthProvider RepoAuth;
        private IMembershipService MembershipService;

        public AccountController(IAuthProvider repoAuth, 
            IMembershipRepository repoMembership,
            IRoleRepository repoRole)
        {
            MembershipService = new MembershipService(Membership.Provider);
            RepoAuth = repoAuth;

            RepoMembership = repoMembership;
            RepoRole = repoRole;
        }

        public ActionResult Login(string ReturnUrl = null)
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "Dashboard");
            else
            {
                if (ReturnUrl != null)
                    ViewBag.ReturnUrl = ReturnUrl;
                return View();
            }
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ActiveDirectoryUtil AdModel = new ActiveDirectoryUtil();
                MembershipUser user = null;
                string superadmin = ConfigurationManager.AppSettings["Superadmin"];
                AdModel.Login(model.Username, model.Password);
                user = MembershipService.GetUser(model.Username);
                if (model.Username.ToLower() == superadmin)
                    CreateAuthenticationTicket(model.Username, AdModel.Roles);
                else if (AdModel.IsAuthenticated && user != null)
                {
                    bool validate = MembershipService.ValidateUser(model.Username, model.Password);
                    if (!validate)
                    {
                        string oldPass = user.ResetPassword();
                        user.ChangePassword(oldPass, model.Password);
                        MembershipService.UpdateUser(user);
                    }

                    CreateAuthenticationTicket(model.Username, AdModel.Roles);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Username / password salah atau tidak terdaftar.");
                }
            }

            // If we got this far, something failed, redisplay form
            return RedirectToAction("Login");

        }

        /// <summary>
        /// clear session
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            RepoAuth.Logout();

            Session["userId"] = null;
            Session["username"] = null;
            Session["rolename"] = null;

            FormsAuthentication.SignOut();

            return Redirect(FormsAuthentication.LoginUrl);
        }

        public void CreateAuthenticationTicket(string username, List<string> roles)
        {
            //kamus
            //Business.Entities.Role role;
            //List<Business.Entities.Module> modules;
            List<ModuleAction> appModules = new List<ModuleAction>();
            UserProfile userProfile = new UserProfile();
            string[] roleNames;
            Business.Entities.Membership membership = new Business.Entities.Membership();

            FieldHelper fieldHelper = new FieldHelper(Table.User, Field.UserName);
            FilterQuery filters = new FilterQuery(fieldHelper, FilterOperator.Equals, username);

            //algoritma
            //get module & action
            roleNames = Roles.GetRolesForUser(username);
            membership = RepoMembership.Find(null, filters);

            //foreach (string roleName in roleNames)
            //{
            //    role = roleRepo.FindByName(roleName);
            //    modules = role.ModulesInRoles.Select(m => m.Module).ToList();
            //    foreach (Business.Entities.Module module in modules)
            //    {
            //        appModules.Add(new ModuleAction { ModuleName = module.moduleName });                   //}
            //    }
            //}

            CustomPrincipal serializeModel = new CustomPrincipal();
            //serializeModel.Modules = appModules;            
            serializeModel.Roles = new List<string>(roleNames);
            serializeModel.AreaId = membership.AreaId;

            string userData = JsonConvert.SerializeObject(serializeModel);                

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
              1, username, DateTime.Now, DateTime.Now.AddHours(8), true, userData);
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            Response.Cookies.Add(faCookie);
        }



        //[Authorize(Roles = "User, Admin")]
        //public ActionResult Edit()
        //{
        //    string username = (string)Session["username"];
        //    User user = repo.FindByPk(username);
        //    if (user == null)
        //    {
        //        if (repo.ResponseCode == Utilities._FAIL_UNAUTHORIZE)
        //        {
        //            FormsAuthentication.SignOut();
        //            return Redirect(FormsAuthentication.LoginUrl);
        //        }
        //    }

        //    //kamus

        //    //algoritma
        //    user.Password = "";

        //    //ViewBag
        //    ViewBag.PageTitle = "Edit Profile";
        //    ViewBag.action = "edit";

        //    return View("Form", user);
        //}

        //[Authorize(Roles = "User, Admin")]
        //[HttpPost]
        //public ActionResult Edit(User user)
        //{
        //    //kamus
        //    bool loadView = false;

        //    //validation
        //    ModelState.Remove("Password");
        //    if (ModelState.IsValid)
        //    {
        //        if (user.Password != "")
        //        {
        //            user.Password = Utilities.Sha256_hash(user.Password);
        //        }
        //        repo.EditSelf(user);

        //        if (repo.ResponseCode == Utilities._SUCCESS) //berhasil
        //        {
        //            TempData[AppMessage._SESS_MESSSAGE] = "User " + user.Username + " berhasil diubah";

        //            return RedirectToAction("Detail");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", repo.ResponseMessage);
        //            user.Password = "";
        //            loadView = true;
        //        }
        //    }
        //    else
        //    {
        //        user.Password = "";
        //        loadView = true;
        //    }

        //    //algoritma

        //    //ViewBag
        //    ViewBag.PageTitle = "Edit User - " + user.Username;
        //    ViewBag.action = "edit";

        //    return View("Form", user);
        //}

        //[Authorize(Roles = "User, Admin")]
        //public ActionResult EditPassword()
        //{
        //    //kamus
        //    EditPasswordViewModel model = new EditPasswordViewModel();

        //    //ViewBag
        //    ViewBag.PageTitle = "Edit Password";
        //    ViewBag.action = "edit";

        //    return View("FormPassword", model);
        //}

        //[Authorize(Roles = "User, Admin")]
        //[HttpPost]
        //public ActionResult EditPassword(EditPasswordViewModel model)
        //{
        //    //kamus
        //    bool loadView = false;
        //    string username = (string)Session["username"];

        //    //validation
        //    if (ModelState.IsValid)
        //    {
        //        model.OldPassword = Utilities.Sha256_hash(model.OldPassword);
        //        model.Password = Utilities.Sha256_hash(model.Password);

        //        repo.EditPassword(username, model.OldPassword, model.Password);

        //        if (repo.ResponseCode == Utilities._SUCCESS) //berhasil
        //        {
        //            TempData[AppMessage._SESS_MESSSAGE] = "Password " + username + " berhasil diubah";

        //            return RedirectToAction("Detail");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", repo.ResponseMessage);
        //            model.OldPassword = "";
        //            model.Password = "";
        //            model.RepeatPassword = "";
        //            loadView = true;
        //        }
        //    }
        //    else
        //    {
        //        model.OldPassword = "";
        //        model.Password = "";
        //        model.RepeatPassword = "";
        //        loadView = true;
        //    }

        //    //algoritma

        //    //ViewBag
        //    ViewBag.PageTitle = "Edit Password";
        //    ViewBag.action = "edit";

        //    return View("FormPassword", model);
        //}

        //[Authorize(Roles="User, Admin")]
        //public ActionResult Detail()
        //{
        //    //kamus
        //    string username = (string) Session["username"];
        //    User user = null;
        //    Department department = new Department();
        //    Position position = new Position();
        //    Planner planner = new Planner();
        //    ReleaseCodePR releaseCodePR = null;
        //    ReleaseCodeSE releaseCodeSE = null;

        //    //algoritma
        //    user = repo.FindByPk(username);
        //    if (user == null)
        //    {
        //        if (repo.ResponseCode == Utilities._FAIL_UNAUTHORIZE)
        //        {
        //            FormsAuthentication.SignOut();
        //            return Redirect(FormsAuthentication.LoginUrl);
        //        }
        //        else
        //        {
        //            throw new HttpException(404, repo.ResponseMessage);
        //        }
        //    }

        //    if (user.DepartmentCode != null)
        //    {
        //        department = repoDepartment.FindByPk(user.DepartmentCode);
        //    }
        //    if (user.Position != null)
        //    {
        //        position = repoPosition.FindByPk(user.Position);
        //    }
        //    if (user.ReleaseCodePr != null)
        //    {
        //        releaseCodePR = repoReleaseCodePR.FindByPk(user.ReleaseCodePr);
        //    }
        //    if (user.ReleaseCodeSe != null)
        //    {
        //        releaseCodeSE = repoReleaseCodeSE.FindByPk(user.ReleaseCodeSe);
        //    }

        //    //ViewBag
        //    ViewBag.PageTitle = "My Profile";
        //    ViewBag.department = department;
        //    ViewBag.position = position;
        //    ViewBag.planner = planner;
        //    ViewBag.ReleaseCodePR = releaseCodePR;
        //    ViewBag.ReleaseCodeSE = releaseCodeSE;

        //    return View(user);
        //}
    }
}
