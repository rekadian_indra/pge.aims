﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    [AuthorizeUser(ModuleName = UserModule.Crew)]
    public class CrewController : BaseController<CrewFormStub>
    {
        private ICrewRepository RepoCrew { get; set; }
        
        public CrewController(
            ICrewRepository repoCrew,
            IViewCrewRepository repoViewCrew,
            ICompanyRepository repoCompany
        )
        {
            RepoCrew = repoCrew;
            RepoViewCrew = repoViewCrew;
            RepoCompany = repoCompany;
        }

        [MvcSiteMapNode(Title = TitleSite.Crew, ParentKey = KeySite.Dashboard, Key = KeySite.IndexCrew)]
        public override async Task<ActionResult> Index()
        {
            List<SelectListItem> statusOption = DisplayFormat.EnumToSelectList<CrewStatus>();
            ViewBag.StatusOption = JsonConvert.SerializeObject(statusOption);

            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.IndexCrew, Key = KeySite.CreateCrew)]
        public override async Task<ActionResult> Create()
        {
            //lib
            Crew dbObject;
            CrewFormStub model;
            string[] split;
            int index = 0;
            List<SortQuery> sorts = new List<SortQuery>();

            //algorithm
            sorts.Add(new SortQuery(Field.CrewId, SortOrder.Descending));
            dbObject = await RepoCrew.FindAsync(sorts);

            if (dbObject != null)
            {
                split = dbObject.CrewId.Split('-');

                if (split.Any() && split.Length > 1)
                    index = int.Parse(split[1]);
            }
            
            model = new CrewFormStub(index);

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(CrewFormStub model)
        {
            if (ModelState.IsValid)
            {
                Crew dbObject = new Crew();
                Company company = await RepoCrew.FindCompanyByKeyAsync(model.CompanyId);

                //map dbObject
                model.MapDbObject(dbObject, company);

                //save dbObject
                RepoCrew.Save(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject("AppGlobalMessage", "CreateSuccess").ToString();
                this.SetMessage(model.CrewName, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        [SiteMapTitle(TitleSite.Breadcrumb)]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexCrew, Key = KeySite.EditCrew)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            Crew dbObject = await RepoCrew.FindByPrimaryKeyAsync(id);
            CrewFormStub model = new CrewFormStub(dbObject);

            ViewBag.Breadcrumb = model.CrewName;

            return View("Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(CrewFormStub model)
        {
            if (ModelState.IsValid)
            {
                Crew dbObject = RepoCrew.FindByPrimaryKey(model.CrewId);
                Company company = await RepoCrew.FindCompanyByKeyAsync(model.CompanyId);

                //set activity log
                model.LogActivity = new LogActivityModel<Crew>(dbObject);

                //map dbObject
                model.MapDbObject(dbObject, company);

                //save dbObject
                RepoCrew.Save(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(model.CrewName, template);

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Name = model.CrewName;

                return View("Form", model);
            }
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            Crew dbObject = await RepoCrew.FindByPrimaryKeyAsync(id);
            ResponseModel response = new ResponseModel(true);

            if (!RepoCrew.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        #region binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<ViewCrew> dbObjects;
            List<CrewPresentationStub> models;
            Task<List<ViewCrew>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoViewCrew.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewCrew.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewCrew, CrewPresentationStub>(dbObjects, DateTime.Now);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }        

        #endregion
    }
}
