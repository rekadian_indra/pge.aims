﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize]
    public class GeomappingController : BaseController
    {
        public GeomappingController(ILogRepository repoLog, 
            IViewMappingAssetRepository repoViewMappingAsset,
            IPhysicalCheckRepository repoPhysicalCheck)
        {
            RepoLog = repoLog;
            RepoViewMappingAsset = repoViewMappingAsset;
            RepoPhysicalCheck = repoPhysicalCheck;
        }

        //if using ModelBinder to get user login use SGUser parameter
        //if using Principal dont get user login SGUser parameter
        //public ActionResult Index(SGUser user)
        //{
        //}

        [MvcSiteMapNode(Title = TitleSite.Geomapping, ParentKey = KeySite.Dashboard, Key = KeySite.GeomapingIndex)]
        public override async Task<ActionResult> Index()
        {
            //kamus
            List<ViewMappingAsset> dbObjects;
            FilterQuery filter = null;
            List<AssetPresentationStub> models;
            PhysicalCheck pc;

            //algoritma
            filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

            filter.AddFilter(Field.StatusSPC, FilterOperator.Equals, StatusSPC.COMPLETED.ToString());


            dbObjects = RepoViewMappingAsset.FindAll(filter);
            models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects);

            foreach(AssetPresentationStub a in models)
            {
                filter = new FilterQuery(Field.AssetId, FilterOperator.Equals, a.AssetId);
                pc = RepoPhysicalCheck.Find(filter);
                a.Condition = pc != null ? pc.Condition : "";
            }

            ViewBag.models = JsonConvert.SerializeObject(models);

            return await base.Index();
        }

        #region binding
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<ViewMappingAsset> dbObjects;
            List<AssetPresentationStub> models;
            Task<List<ViewMappingAsset>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FilterQuery temp = new FilterQuery();

            //algorithm
            if (param.Filter == null)
            {
                param.Filter = new FilterQuery(Field.StatusSPC, FilterOperator.Equals, StatusSPC.DRAFT.ToString(), FilterLogic.Or);
                param.Filter.AddFilter(Field.StatusSPC, FilterOperator.IsNull);
            }
            else
            {
                temp = new FilterQuery(Field.StatusSPC, FilterOperator.Equals, StatusSPC.DRAFT.ToString(), FilterLogic.Or);
                temp.AddFilter(Field.StatusSPC, FilterOperator.IsNull);

                param.Filter.AddFilter(temp);
            }

            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMappingAsset.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion
    }
}
