﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    //[AuthorizeUser(ModuleName = UserModule.Crew)]
    [Authorize]
    public class SPCController : BaseController<SPCRequestFormStub>
    {
        private const string KEY = "excelTemplateSPC";

        public SPCController(            
            ISPCRequestRepository repoSPCRequest,
            ISPCRequestAssetsRepository repoSPCRequestAsset,
            IViewMappingAssetActualCostRepository repoViewMappingAssetActualCost,
            IViewMappingAssetRepository repoViewMappingAsset,
            IAssetRepository repoAsset,
            IPlantRepository repoPlant
        )
        {
            RepoSPCRequest = repoSPCRequest;
            RepoSPCRequestAsset = repoSPCRequestAsset;
            RepoViewMappingAsset = repoViewMappingAsset;
            RepoViewMappingAssetActualCost = repoViewMappingAssetActualCost;
            RepoAsset = repoAsset;
            RepoPlant = repoPlant;
        }

        [MvcSiteMapNode(Title = TitleSite.SPC, ParentKey = KeySite.Dashboard, Key = KeySite.SPCIndex)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        #region create
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.SPCIndex, Key = KeySite.CreateSPC)]
        public override async Task<ActionResult> Create()
        {
            //lib
            SPCRequestFormStub model = new SPCRequestFormStub();

            //algorithm
            await Task.Delay(0);
            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Create(SPCRequestFormStub model)
        {
            if (ModelState.IsValid)
            {
                SPCRequest dbObject = new SPCRequest();

                //map dbObject
                if(model.ScrapType == ScrapType.NON_CAPITALIZATION.ToString())
                {
                    model.NoRequestSPC011 = "";
                }
                model.MapDbObject(dbObject);                

                //save dbObject
                await RepoSPCRequest.SaveAsync(dbObject);
                
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = DisplayFormat.ShortDateFormat };

                if (model.SPCRequestAssets != null && model.SPCRequestAssets != "")
                {
                    List<SPCRequestAssetFormStub> formModels = JsonConvert.DeserializeObject<List<SPCRequestAssetFormStub>>(model.SPCRequestAssets, dateTimeConverter);
                    List<SPCRequestAsset> dbAssets = new List<SPCRequestAsset>();
                    foreach (SPCRequestAssetFormStub a in formModels)
                    {
                        a.SPCRequestId = dbObject.SPCRequestId;
                        SPCRequestAsset dbAsset = new SPCRequestAsset();
                        a.MapDbObject(dbAsset);
                        dbAssets.Add(dbAsset);
                    }
                    await RepoSPCRequestAsset.SaveAllAsync(dbAssets);
                }
                
                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage("SPC", true, template);

                return RedirectToAction("Index");
            }
            else
            {
                model.OptionsScrapType = DisplayFormat.EnumToSelectList<ScrapType>();
                model.OptAcquisitionType = DisplayFormat.EnumToSelectList<AcquisitionType>();
                return View("Form", model);
            }
        }
        #endregion

        #region edit
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        //[MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.TCRIndex, Key = KeySite.EditTCR)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();
            Task<SPCRequest> dbObject;
            SPCRequestFormStub model;                        
            Task<List<ViewMappingAsset>> dbObjectsTask = null;            
            FilterQuery filter = new FilterQuery(Field.StatusMDM, FilterOperator.Equals, StatusMDM.COMPLETE);

            //algoritma
            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(filter);
            dbObject = RepoSPCRequest.FindByPrimaryKeyAsync(primaryKey);
            await Task.WhenAll(dbObjectsTask, dbObject);            
            model = new SPCRequestFormStub(dbObject.Result, dbObjectsTask.Result.Distinct().ToList());

            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Edit(SPCRequestFormStub model)
        {
            if (ModelState.IsValid)
            {
                SPCRequest dbObject = await RepoSPCRequest.FindByPrimaryKeyAsync(model.SPCRequestId);
                if(dbObject == null)
                {
                    dbObject = new SPCRequest();
                }

                //map dbObject
                if (model.ScrapType == ScrapType.NON_CAPITALIZATION.ToString())
                {
                    model.NoRequestSPC011 = "";
                }
                model.MapDbObject(dbObject);                

                //save dbObject
                await RepoSPCRequest.SaveAsync(dbObject);
                
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = DisplayFormat.ShortDateFormat };

                if (model.SPCRequestAssets != null && model.SPCRequestAssets != "")
                {
                    List<SPCRequestAssetFormStub> formModels = JsonConvert.DeserializeObject<List<SPCRequestAssetFormStub>>(model.SPCRequestAssets, dateTimeConverter);
                    List<SPCRequestAsset> dbAssets = new List<SPCRequestAsset>();
                    foreach (SPCRequestAssetFormStub a in formModels)
                    {
                        SPCRequestAsset dbAsset = new SPCRequestAsset();
                        a.SPCRequestId = dbObject.SPCRequestId;
                        a.MapDbObject(dbAsset);
                        dbAssets.Add(dbAsset);
                    }
                    await RepoSPCRequestAsset.SaveAllAsync(dbAssets);
                }

                if (model.TempDel != null && model.TempDel != "")
                {                    
                    List<SPCRequestAssetFormStub> formModels = JsonConvert.DeserializeObject<List<SPCRequestAssetFormStub>>(model.TempDel, dateTimeConverter);                    
                    foreach (SPCRequestAssetFormStub a in formModels)
                    {
                        SPCRequestAsset dbAsset = new SPCRequestAsset();                        
                        dbAsset  = await RepoSPCRequestAsset.FindByPrimaryKeyAsync(a.SPCRequestId, a.AssetId);
                        a.MapDbObject(dbAsset);                        
                        await RepoSPCRequestAsset.DeleteAsync(dbAsset);
                    }                    
                }

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage("SPC", true, template);

                return RedirectToAction("Index");
            }
            else
            {
                model.OptionsScrapType = DisplayFormat.EnumToSelectList<ScrapType>();
                model.OptAcquisitionType = DisplayFormat.EnumToSelectList<AcquisitionType>();
                return View("Form", model);
            }
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<ActionResult> AddAsset(params object[] id)
        {
            //kamus
            //int primaryKey = id[0].ToInteger();
            SPCRequestAssetFormStub model = new SPCRequestAssetFormStub();

            //algoritma                        
            await Task.Delay(0);

            return PartialView("_Form", model);
        }


        #endregion

        #region delete
        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();

            //algoritma
            SPCRequest dbObject = await RepoSPCRequest.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!RepoSPCRequest.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<ActionResult> setComplete(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();

            //algoritma
            SPCRequest dbObject = await RepoSPCRequest.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);
            dbObject.Status = StatusSPC.COMPLETED.ToString();

            if (!RepoSPCRequest.Save(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        #region binding
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<SPCRequest> dbObjects;
            List<SPCRequestPresentationStub> models;
            Task<List<SPCRequest>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FieldHelper fh = new FieldHelper(Table.Plant1, Field.PlantId);
            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(fh, FilterOperator.Equals, User.AreaId);

            dbObjectsTask = RepoSPCRequest.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoSPCRequest.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<SPCRequest, SPCRequestPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMappingAsset(params object[] args)
        {
            //lib
            List<ViewMappingAsset> dbObjects;
            List<SPCAssetOptionsPresentationStub> models;
            Task<List<ViewMappingAsset>> dbObjectsTask = null;
            List<int> assetIds = new List<int>();
            FilterQuery filter = new FilterQuery(Field.StatusMDM, FilterOperator.Equals, StatusMDM.COMPLETE);
            filter.AddFilter(Field.SPCRequestId, FilterOperator.IsNull);

            //algorithm
            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                filter.AddFilter(Field.Plant, FilterOperator.Equals, User.AreaName);

            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            
                models = ListMapper.MapList<ViewMappingAsset, SPCAssetOptionsPresentationStub>(dbObjects).Distinct().ToList();
            
            return JsonConvert.SerializeObject(new { data = models });
        }

        public async Task<string> BindingMappingAssetActualCost(params object[] args)
        {
            //lib
            int count;
            List<ViewMappingAssetActualCost> dbObjects;
            List<MappingAssetActualCostFormStub> models;
            Task<List<ViewMappingAssetActualCost>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (args != null && args.Any())
            {
                int mappingAssetId = int.Parse(args.First().ToString());
                if (param.Filter == null)
                    param.Filter = new FilterQuery(Field.MappingAssetId, FilterOperator.Equals, mappingAssetId);
                else
                    param.Filter.AddFilter(Field.MappingAssetId, FilterOperator.Equals, mappingAssetId);
            }

            dbObjectsTask = RepoViewMappingAssetActualCost.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMappingAssetActualCost.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewMappingAssetActualCost, MappingAssetActualCostFormStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        [HttpPost]
        public async Task<string> GetAssetCount(params object[] args)
        {
            //lib           
            int total;
            FilterQuery filter = new FilterQuery(Field.StatusMDM, FilterOperator.Equals, StatusMDM.COMPLETE);
            filter.AddFilter(Field.SPCRequestId, FilterOperator.IsNull);

            //algorithm
            total = await RepoViewMappingAsset.CountAsync(filter);                                   

            return total.ToString();
        }

        public async Task<string> BindingStatus(params object[] args)
        {
            //lib
            List<SelectListItem> items = DisplayFormat.EnumToSelectList<StatusSPC>();
            await Task.Delay(0);
            return JsonConvert.SerializeObject(items);
        }

        public async Task<string> BindingPlant(params object[] args)
        {
            //lib
            List<Plant> dbObjects;
            List<PlantPresentationStub> models = new List<PlantPresentationStub>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            if (User.AreaId.HasValue && User.AreaId > 0 && User != null && !User.IsInRole(UserRole.Superadmin))
                filter.AddFilter(Field.PlantId, FilterOperator.Equals, User.AreaId);

            //algorithm
            //get callback from task
            dbObjects = await RepoPlant.FindAllAsync(filter);

            if (dbObjects != null)
            {
                models = ListMapper.MapList<Plant, PlantPresentationStub>(dbObjects);
            }
            return JsonConvert.SerializeObject(models);
        }
        #endregion

        #region download
        public async Task<ActionResult> GetTemplate(int Id)
        {
            //kamus
            SPCRequest dbItem = await RepoSPCRequest.FindByPrimaryKeyAsync(Id);
            FileStream fileStream;
            XSSFWorkbook workbook;
            DownloadParameterStub downloadParam = null;
            ResponseModel response = new ResponseModel(true);

            SPCExcelStub param = new SPCExcelStub();
            string filePath = "~/Uploads/Template/", fileName = "SPC_template.xlsx";
            FilterQuery filter = new FilterQuery(Field.SPCRequestId, FilterOperator.Equals, dbItem.SPCRequestId);
            List<ViewMappingAsset> assets = await RepoViewMappingAsset.FindAllAsync(filter);

            //remove warning opening excel
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            //check file exist
            if (System.IO.File.Exists(Server.MapPath($"{filePath}{fileName}")))
            {
                //read file and set to workbook
                fileStream = new FileStream(Server.MapPath($"{filePath}{fileName}"), FileMode.Open, FileAccess.ReadWrite);
                workbook = param.SetData(fileStream, dbItem, assets, User);
                fileStream.Close();

                fileName = $"SPC Lampiran ({DateTime.Now.ToShortDateString()}).xlsx";

                downloadParam = new DownloadParameterStub
                {
                    MemoryStream = new MemoryStream(),
                    FileDownloadName = fileName,
                    ContentType = MimeMapping.GetMimeMapping(fileName)
                };

                //write workbook to stream
                workbook.Write(downloadParam.MemoryStream);

                //store download parameter to temp data
                TempData[KEY] = downloadParam;
            }

            if (downloadParam == null)
            {
                response.SetFail("MDM Template not found");
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Download()
        {
            DownloadParameterStub param = TempData[KEY] as DownloadParameterStub;

            if (param is DownloadParameterStub)
            {
                return File(param.MemoryStream.ToArray(), param.ContentType, param.FileDownloadName);
            }

            return new EmptyResult();
        }
        #endregion
    }
}
