﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]

    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Finance })]
    public class ApprovalController : BaseController
    {   
        public ApprovalController(
            IMappingRepository repoMapping,
            ITCRRepository repoTCR
        )
        {
            RepoMapping = repoMapping;
            RepoTCR = repoTCR;
        }

        [MvcSiteMapNode(Title = TitleSite.Approval, ParentKey = KeySite.Dashboard, Key = KeySite.Approval)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Finance })]
        public async Task<ActionResult> Approve(int id)
        {
            //kamus
            Mapping dbObject;

            //algoritma
            dbObject = await RepoMapping.FindByPrimaryKeyAsync(id);
            dbObject.Status = StatusAsset.APPROVE_BY_FINANCE.ToString();
            ResponseModel response = new ResponseModel(true);

            if (!RepoMapping.Save(dbObject))
            {
                response = new ResponseModel(false);
            }

            return Json(response);
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Finance })]
        public async Task<ActionResult> ApproveTCR(int id)
        {
            //kamus
            TCR dbObject;

            //algoritma
            dbObject = await RepoTCR.FindByPrimaryKeyAsync(id);
            dbObject.Status = StatusTCR.APPROVE_BY_FINANCE.ToString();
            ResponseModel response = new ResponseModel(true);

            if (!RepoTCR.Save(dbObject))
            {
                response = new ResponseModel(false);
            }

            return Json(response);
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Finance })]
        public async Task<ActionResult> Revise(ReviseFormStub model, string table)
        {
            //kamus
            TCR dbObject;
            Mapping dbMapping;
            ResponseModel response = new ResponseModel(true);

            //algoritma
            if (ModelState.IsValid)
            {
                if(table == "TCR")
                {
                    dbObject = await RepoTCR.FindByPrimaryKeyAsync(model.Id);                    
                    dbObject.Status = StatusTCR.REJECT_BY_FINANCE.ToString();
                    dbObject.Notes = model.Notes;
                    if (!RepoTCR.Save(dbObject))
                    {
                        response = new ResponseModel(false);
                    }
                }
                else
                {
                    dbMapping = await RepoMapping.FindByPrimaryKeyAsync(model.Id);
                    dbMapping.Status = StatusAsset.REJECT_BY_FINANCE.ToString();
                    dbMapping.Notes = model.Notes;
                    if (!RepoMapping.Save(dbMapping))
                    {
                        response = new ResponseModel(false);
                    }
                }
            }                                                

            return Json(response);
        }

        #region binding


        #endregion
    }
}
