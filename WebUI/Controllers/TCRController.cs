﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    //[AuthorizeUser(ModuleName = UserModule.Crew)]
    [Authorize]
    public class TCRController : BaseController<TCRFormStub>
    {   
        public TCRController(
            ITCRRepository repoTCR,
            ILocationRepository repoLocation,
            IViewMappingAssetRepository repoViewMappingAsset,
            IAssetRepository repoAsset,
            ICostCenterRepository repoCostCenter,
            IPlantRepository repoPlant
        )
        {
            RepoTCR = repoTCR;
            RepoLocation = repoLocation;
            RepoViewMappingAsset = repoViewMappingAsset;
            RepoAsset = repoAsset;
            RepoCostCenter = repoCostCenter;
            RepoPlant = repoPlant;
        }

        [MvcSiteMapNode(Title = TitleSite.TCR, ParentKey = KeySite.Dashboard, Key = KeySite.TCRIndex)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        #region create
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.TCRIndex, Key = KeySite.CreateTCR)]
        public override async Task<ActionResult> Create()
        {
            //lib
            TCRFormStub model = new TCRFormStub();

            //algorithm
            await Task.Delay(0);
            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Create(TCRFormStub model)
        {
            if (ModelState.IsValid)
            {
                TCR dbObject = new TCR();

                //map dbObject
                model.MapDbObject(dbObject);
                model.MapDbObjectAsset(dbObject, RepoTCR.FindAllAssetForMapping());

                //save dbObject
                await RepoTCR.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.ProjectName, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }
        #endregion

        #region edit
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.TCRIndex, Key = KeySite.EditTCR)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();
            TCRFormStub model;

            //algoritma
            TCR dbObject = await RepoTCR.FindByPrimaryKeyAsync(primaryKey);
            model = new TCRFormStub(dbObject);

            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Edit(TCRFormStub model)
        {
            if (ModelState.IsValid)
            {
                TCR dbObject = await RepoTCR.FindByPrimaryKeyAsync(model.TCRId);

                //map dbObject
                model.MapDbObject(dbObject);
                model.MapDbObjectAsset(dbObject, RepoTCR.FindAllAssetForMapping());

                //save dbObject
                await RepoTCR.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(model.ProjectName, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        #endregion

        #region delete
        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();

            //algoritma
            TCR dbObject = await RepoTCR.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!RepoTCR.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        #region binding
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<TCR> dbObjects;
            List<TCRPresentationStub> models;
            Task<List<TCR>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;            

            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(Field.Plant, FilterOperator.Equals, User.AreaId);

            dbObjectsTask = RepoTCR.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoTCR.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<TCR, TCRPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMappingAsset(params object[] args)
        {
            //lib
            List<ViewMappingAsset> dbObjects;
            List<AssetPresentationStub> models;
            Task<List<ViewMappingAsset>> dbObjectsTask = null;
            List<int> assetIds = new List<int>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            //algorithm
            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;

            //exclude other asset in other tcr
            foreach (TCR tcr in RepoTCR.FindAll())
            {
                assetIds.AddRange(tcr.Assets.Select(x => x.AssetId));
            }

            //map db data to model
            if (assetIds.Any())
                models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects.Where(x => !assetIds.Contains(x.AssetId)));
            else
                models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(models);
        }

        public async Task<string> BindingAssetTCR(params object[] args)
        {
            //lib
            List<ViewMappingAsset> dbObjects;
            List<AssetPresentationStub> models = new List<AssetPresentationStub>();
            FilterQuery filter = null;
            List<int> assetIds = new List<int>();

            //algorithm
            if(args != null && args.Any())
            {
                int TCRId = int.Parse(args.First().ToString());
                if(TCRId != 0)
                {
                    filter = new FilterQuery(Field.TCRId, FilterOperator.Equals, TCRId);

                    if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                        filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

                    //get callback from task
                    dbObjects = await RepoViewMappingAsset.FindAllAsync(filter);

                    if(dbObjects != null)
                    {
                        //assetIds = RepoTCR.FindByPrimaryKey(TCRId).Assets.Select(x => x.AssetId).ToList();
                        //if(assetIds.Any())
                            //models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects.Where(x => assetIds.Contains(x.AssetId)));
                        //else
                            models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects);
                    }
                }
            }
            return JsonConvert.SerializeObject(models);
        }

        public async Task<string> BindingCostCenter(params object[] args)
        {
            //lib
            List<CostCenter> dbObjects;
            List<CostCenterPresentationStub> models = new List<CostCenterPresentationStub>();

            //algorithm
            //get callback from task
            dbObjects = await RepoCostCenter.FindAllAsync();

            if (dbObjects != null)
            {
                models = ListMapper.MapList<CostCenter, CostCenterPresentationStub>(dbObjects);
            }
            return JsonConvert.SerializeObject(models);
        }

        public async Task<string> BindingPlant(params object[] args)
        {
            //lib
            List<Plant> dbObjects;
            List<PlantPresentationStub> models = new List<PlantPresentationStub>();
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            if (User.AreaId.HasValue && User.AreaId > 0 && User != null && !User.IsInRole(UserRole.Superadmin))
                filter.AddFilter(Field.PlantId, FilterOperator.Equals, User.AreaId);

            //algorithm
            //get callback from task
            dbObjects = await RepoPlant.FindAllAsync(filter);

            if (dbObjects != null)
            {
                models = ListMapper.MapList<Plant, PlantPresentationStub>(dbObjects);
            }
            return JsonConvert.SerializeObject(models);
        }


        #endregion

        #region download
        public async Task<ActionResult> Download(int Id)
        {
            //kamus
            TCR dbItem = await RepoTCR.FindByPrimaryKeyAsync(Id);

            TCRExcelStub param = new TCRExcelStub();
            string filename = $"TCR {dbItem.ProjectName}.xlsx";
            FilterQuery filter = new FilterQuery(Field.TCRId, FilterOperator.Equals, dbItem.TCRId);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

            List<ViewMappingAsset> assets = await RepoViewMappingAsset.FindAllAsync(filter);

            //TaskReportFormStub model = new TaskReportFormStub();
            //model.AreaId = Id;
            //model.Periode = DateTime.Now.Date;
            //FilterQuery filter = new FilterQuery(Field.AreaId, FilterOperator.Equals, model.AreaId);
            //List<SortQuery> sorts = new List<SortQuery> { new SortQuery(Field.Name, SortOrder.Ascending) };
            //IEnumerable<Business.Entities.PowerPlant> plants = await RepoPowerPlant.FindAllAsync(sorts, filter);
            //IEnumerable<Well> wells = await RepoWell.FindAllAsync(sorts, filter);
            //IEnumerable<MstSeparator> separators = await RepoMasterSeparator.FindAllAsync(sorts, filter);
            //IEnumerable<InterfacePoint> interfacePoints = await RepoInterfacePoint.FindAllAsync(sorts, filter);
            //IEnumerable<Scrubber> scrubbers = await RepoScrubber.FindAllAsync(sorts, filter);
            //Area area = RepoArea.FindByPrimaryKey(model.AreaId);

            //model.AreaName = area.Name;

            //param.TaskReport = model;
            //param.PowerPlants = plants;
            //param.Wells = wells;
            //param.Separators = separators;
            //param.InterfacePoints = interfacePoints;
            //param.Scrubbers = scrubbers;

            byte[] excel = TCRExcelStub.GenerateTemplate(dbItem, assets);
            return File(excel, "application/x-msexcel", filename);
        }
        #endregion
    }
}
