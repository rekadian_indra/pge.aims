﻿using MediaToolkit;
using MediaToolkit.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebUI.Infrastructure;
using WebUI.Models;
using static WebUI.Models.ResponseModel;

namespace WebUI.Controllers
{
    public class FileManagementController : Controller
    {
        private const string FILE_DIRECTORY = "~/Uploads/";
        private const string FFMPEG_PATH = "~/MediaToolkit/ffmpeg.exe";
        /**
         * save file using kendo file uploader
         * menangani kalau nama file sama
         * The Name of the Upload component is "files"
         * currently not support multiple uploads
         * 
         * @return filepath ex "~/Uploads/rekadia.jpg"
         */
        public string Save(IEnumerable<HttpPostedFileBase> files)
        {
            //kamus lokal
            string fileName = "", savedFilename = "", physicalPath = "", imagePath = "", friendlyFilename = "", duration = "";
            List<FileResponseModel> listResponse = new List<FileResponseModel>();

            //algoritma
            if (files != null)
            {
                foreach (var file in files)
                {
                    var identifier = Guid.NewGuid();
                    bool fileDirectory = Directory.Exists(Server.MapPath(FILE_DIRECTORY));
                    if (fileDirectory == false)
                    {
                        Directory.CreateDirectory(Server.MapPath(FILE_DIRECTORY));
                    }

                    fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    friendlyFilename = DisplayFormat.URLFriendly(fileName) + Path.GetExtension(file.FileName);
                    savedFilename = identifier + "_" + friendlyFilename;

                    //save file
                    physicalPath = Path.Combine(Server.MapPath(FILE_DIRECTORY), savedFilename);
                    imagePath = FILE_DIRECTORY + savedFilename;
                    file.SaveAs(physicalPath);

                    //get duration video
                    string ext = Path.GetExtension(file.FileName).ToLower();
                    if (ext == ".avi" || ext == ".mkv" || ext == ".mov" || ext == ".mp4" || ext == ".mpeg" || ext == ".mpg" || ext == ".wmv")
                    {
                        MediaFile inputFile = new MediaFile
                        {
                            Filename = physicalPath
                        };

                        //create outputFile
                        identifier = Guid.NewGuid();
                        fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        friendlyFilename = string.Format("{0}.mp4", DisplayFormat.URLFriendly(fileName));
                        savedFilename = string.Format("{0}_{1}", identifier, friendlyFilename);
                        physicalPath = Path.Combine(Server.MapPath(FILE_DIRECTORY), savedFilename);
                        imagePath = FILE_DIRECTORY + savedFilename;
                        MediaFile outputFile = new MediaFile
                        {
                            Filename = physicalPath
                        };

                        MediaToolkit.Options.ConversionOptions options = new MediaToolkit.Options.ConversionOptions
                        {
                            AudioSampleRate = MediaToolkit.Options.AudioSampleRate.Hz44100,
                            TargetStandard = MediaToolkit.Options.TargetStandard.FILM,
                            VideoBitRate = 355,
                            VideoFps = 25,
                            VideoSize = MediaToolkit.Options.VideoSize.Hd480
                        };

                        using (Engine engine = new Engine(Server.MapPath(FFMPEG_PATH)))
                        {
                            engine.GetMetadata(inputFile);
                            engine.Convert(inputFile, outputFile, options);
                            engine.GetMetadata(outputFile);
                        }

                        Metadata metadata = outputFile.Metadata;
                        if (metadata != null)
                        {
                            if (System.IO.File.Exists(inputFile.Filename))
                            {
                                System.IO.File.Delete(inputFile.Filename);
                            }
                            duration = string.Format("{0:D2}:{1:D2}", metadata.Duration.Minutes, metadata.Duration.Seconds);
                        }
                    }

                    listResponse.Add(
                        new FileResponseModel
                        {
                            filepath = imagePath,
                            filename = savedFilename,
                            absolutepath = VirtualPathUtility.ToAbsolute(imagePath),
                            duration = duration
                        });
                }
            }

            return new JavaScriptSerializer().Serialize(listResponse);
        }

        /**
         * remove file using kendo file uploader
         */
        public ActionResult Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"

            if (fileNames != null)
            {
                foreach (string fullName in fileNames)
                {
                    string fileName = Path.GetFileName(fullName);
                    string physicalPath = Path.Combine(Server.MapPath(DirectoryHelper.Upload), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        // The files are not actually removed in this demo
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        [HttpPost]
        public JsonResult Remove(string filePath)
        {
            ResponseModel response = new ResponseModel(true);
            string serverPath = Server.MapPath(filePath);

            if (System.IO.File.Exists(serverPath))
            {
                System.IO.File.Delete(serverPath);
            }
            else
            {
                response.SetFail("");
            }

            return Json(response);
        }
    }
}
