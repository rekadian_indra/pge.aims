﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using LogAction.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebUI.Infrastructure;

namespace WebUI.Controllers
{
    //[LogActionFilter]
    public abstract class BaseController : BaseController<object>
    {
    }

    //[LogActionFilter]
    public abstract class BaseController<TFormStub> : Controller
        where TFormStub : class
    {
        public ILogRepository RepoLog { get; set; }

        //AIMS
        public IAssetRepository RepoAsset { get; set; }
        public IAUCRepository RepoAUC { get; set; }
        public IActualCostRepository RepoActual { get; set; }
        public ILocationRepository RepoLocation { get; set; }
        public IMappingRepository RepoMapping { get; set; }
        public IViewMappingAssetRepository RepoViewMappingAsset { get; set; }
        public IViewActualRepository RepoViewActual { get; set; }
        public IViewMappingAssetActualCostRepository RepoViewMappingAssetActualCost { get; set; }
        public IMappingAssetRepository RepoMappingAsset { get; set; }
        public IMappingAssetActualCostRepository RepoMappingAssetActualCost { get; set; }
        public IViewAUCRepository RepoViewAUC { get; set; }
        public ITCRRepository RepoTCR { get; set; }
        public IPhysicalCheckRepository RepoPhysicalCheck { get; set; }
        public IMDMRequestRepository RepoMDMRequest { get; set; }
        public IMDMAssetsRepository RepoMDMAsset { get; set; }
        public IPhysicalCheckAssetRepository RepoPhysicalCheckAsset { get; set; }
        public ISPCRequestAssetsRepository RepoSPCRequestAsset { get; set; }
        public ISPCRequestRepository RepoSPCRequest { get; set; }
        public ICostCenterRepository RepoCostCenter { get; set; }
        public IPlantRepository RepoPlant { get; set; }

        public IModuleRepository RepoModule { get; set; }
        public IUserRepository RepoUser { get; set; }
        public IMembershipRepository RepoMembership { get; set; }
        public IMembershipUserRepository RepoMembershipUser { get; set; }
        public IRoleRepository RepoRole { get; set; }
        public IViewUserRepository RepoViewUser { get; set; }
        public IViewMDMRepository RepoViewMDM { get; set; }
        public IViewMappingRepository RepoViewMapping { get; set; }
        public IViewPhysicalCheckRepository RepoViewPhysicalCheck { get; set; }
        public IViewAssetRepository RepoViewAsset { get; set; }

        //Dummy
        //protected ICompanyRepository RepoCompany { get; set; }
        //protected IAirportRepository RepoAirport { get; set; }
        //protected IViewWhitelistRepository RepoViewWhitelist { get; set; }
        //protected IViewCrewRepository RepoViewCrew { get; set; }

        protected string AppAddress
        {
            get
            {
                string result = Request.ApplicationPath == "/" ?
                    string.Format("{0}://{1}/", Request.Url.Scheme, Request.Url.Authority) :
                    string.Format("{0}://{1}{2}/", Request.Url.Scheme, Request.Url.Authority, Request.ApplicationPath);
                return result;
            }
        }

        protected virtual new Principal User
        {
            get
            {
                return HttpContext.User as Principal;
            }
        }

        public BaseController()
        {
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            ViewBag.AppAddress = AppAddress;
        }

        #region Main action

        public virtual async Task<ActionResult> Index()
        {
            await Task.Delay(0);
            return View();
        }

        public virtual async Task<ActionResult> Create()
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Create(TFormStub model)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        public virtual async Task<ActionResult> Edit(params object[] id)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(TFormStub model)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Delete(params object[] id)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        #endregion

        #region Binding

        public virtual async Task<string> Binding(params object[] args)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
        }

        public async Task<string> BindingOption(OptionType? optionType, params object[] args)
        {
            //lib
            List<SelectListItem> options;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            switch (optionType)
            {
                case OptionType.ACTUAL:
                    options = await ActualOptionAsync(param);
                    break;
                case OptionType.AUC:
                    options = await AUCOptionAsync(param);
                    break;
                case OptionType.AUCVALUE:
                    options = await AUCValueOptionAsync(param);
                    break;
                case OptionType.SUBNUMBER:
                    options = await SubNumberOptionAsync();
                    break;
                case OptionType.LOCATION:
                    options = await LocationOptionAsync(param);
                    break;
                case OptionType.CURRENCY:
                    options = await CurrencyOptionAsync(param);
                    break;
                case OptionType.FIXASSET:
                    options = await AssetOptionAsync(param);
                    break;
                case OptionType.CONDITION:
                    options = await ConditionOptionAsync(param);
                    break;
                case OptionType.STATUSASSET:
                    options = await StatusAssetOptionAsync(param);
                    break;
                case OptionType.USER:
                    options = await UsersOptionAsync(param);
                    break;
                case OptionType.PLANT:
                    options = await PlantOptionAsync(param);
                    break;
                default:
                    return null;
            }
            return JsonConvert.SerializeObject(options);
        }

        #endregion

        #region Http404 handling

        protected override void HandleUnknownAction(string actionName)
        {
            // If controller is ErrorController dont 'nest' exceptions
            if (GetType() != typeof(ErrorController))
                InvokeHttp404(HttpContext);
        }

        public ActionResult InvokeHttp404(HttpContextBase httpContext)
        {
            IController errorController = DependencyResolver.Current.GetService<ErrorController>();
            RouteData errorRoute = new RouteData();

            errorRoute.Values.Add("controller", "Error");
            errorRoute.Values.Add("action", "Http404");
            errorRoute.Values.Add("url", httpContext.Request.Url.OriginalString);
            errorController.Execute(new RequestContext(httpContext, errorRoute));

            return new EmptyResult();
        }

        #endregion

        #region Helper

        protected virtual async Task<List<SelectListItem>> AUCOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<ViewAuc> dbObjects = await RepoViewAUC.FindAllAsync(param.Sorts, param.Filter);
            int iterator = 0;

            //algorithm
            foreach (ViewAuc c in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = $"{c.WBSElement} - Rp {DisplayFormat.NumberFormat(c.Balance)}",
                    Value = c.WBSElement
                };

                options.Add(o);
                iterator++;
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> AUCValueOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<ViewAuc> dbObjects = await RepoViewAUC.FindAllAsync(param.Sorts, param.Filter);
            int iterator = 0;

            //algorithm
            foreach (ViewAuc c in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = $"{c.WBSElement} - Rp {DisplayFormat.NumberFormat(c.Balance)}",
                    Value = c.Balance.ToString()
                };

                options.Add(o);
                iterator++;
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> AssetOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            //FilterQuery filter = new FilterQuery
            if (param != null && param.Filter != null)
            {
                param.Filter.AddFilter(new FilterQuery(Field.StatusSPC, FilterOperator.Equals, StatusSPC.COMPLETED));
            }
            else
            {
                param.Filter = new FilterQuery(Field.StatusSPC, FilterOperator.Equals, StatusSPC.COMPLETED);
            }

            //algorithm
            List<ViewMappingAsset> dbObjects = await RepoViewMappingAsset.FindAllAsync(param.Sorts, param.Filter);
            foreach (ViewMappingAsset a in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = $"{a.AssetNumber} - {a.Description}",
                    Value = a.AssetId.ToString()
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> ActualOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<ActualCost> dbObjects = await RepoActual.FindAllAsync(param.Sorts, param.Filter);

            //algorithm
            foreach (ActualCost c in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = c.DocNumber,
                    Value = c.ActualCostId.ToString()
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> LocationOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Location> dbObjects = await RepoLocation.FindAllAsync(param.Sorts, param.Filter);

            //algorithm
            foreach (Location c in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = c.Name,
                    Value = c.LocationId.ToString()
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> SubNumberOptionAsync()
        {
            //kamus
            List<SelectListItem> options = new List<SelectListItem>();

            //algoritma
            SelectListItem o = new SelectListItem
            {
                Text = 0.ToString(),
                Value = 0.ToString(),
            };
            options.Add(o);
            o = new SelectListItem
            {
                Text = 1.ToString(),
                Value = 1.ToString(),
            };
            options.Add(o);
            await Task.Delay(0);

            return options;
        }

        protected virtual async Task<List<SelectListItem>> CurrencyOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<Currency>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> StatusAssetOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<StatusAsset>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> ConditionOptionAsync(QueryRequestParameter param)
        {
            //lib
            IEnumerable<SelectListItem> options = DisplayFormat.EnumToSelectList<Condition>();

            //algorithm
            DisplayFormat.EnumSelectListFilterSortProccess(param, ref options);

            await Task.Delay(0);

            return options.ToList();
        }

        protected virtual async Task<List<SelectListItem>> UsersOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            FilterQuery filter = param.Filter != null && param.Filter.Filters.Any(n => n.Field == Field.UserName) ? param.Filter.Filters.FirstOrDefault(n => n.Field == Field.UserName) : null;
            string key = filter != null ? filter.Value.ToString() : string.Empty;
            IEnumerable<ActiveDirectoryUtil> listUsers = string.IsNullOrEmpty(key) ? new List<ActiveDirectoryUtil>() : ActiveDirectoryUtil.GetUsers(key);
            List<User> dbObjects = await RepoUser.FindAllAsync();

            listUsers = listUsers.Where(m => !dbObjects.Any(n => n.UserName.ToLower() == m.UserName.ToLower()));

            //algorithm
            foreach (ActiveDirectoryUtil u in listUsers)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = u.UserName,
                    Value = u.Email
                };

                options.Add(o);
            }

            return options;
        }

        protected virtual async Task<List<SelectListItem>> PlantOptionAsync(QueryRequestParameter param)
        {
            //lib
            List<SelectListItem> options = new List<SelectListItem>();
            List<Plant> dbObjects = new List<Plant>();
            if (User.AreaId > 0 && !User.IsInRole(UserRole.Superadmin))
                dbObjects = await RepoPlant.FindAllAsync(param.Sorts, param.Filter);
            else              
                dbObjects = await RepoPlant.FindAllAsync(param.Sorts);            

            //algorithm
            foreach (Plant c in dbObjects)
            {
                SelectListItem o = new SelectListItem
                {
                    Text = c.Name,
                    Value = c.PlantId.ToString()
                };

                options.Add(o);
            }

            return options;
        }
        #endregion
    }
}
