﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    //[AuthorizeUser(ModuleName = UserModule.Crew)]
    [Authorize]
    public class ActualController : BaseController<ActualFormStub>
    {
        public ActualController(
            IActualCostRepository repoActual,
            IAUCRepository repoAuc,
            IViewMappingAssetActualCostRepository repoViewMappingAssetActualCost
        )
        {
            RepoActual = repoActual;
            RepoAUC = repoAuc;
            RepoViewMappingAssetActualCost = repoViewMappingAssetActualCost;
        }

        [MvcSiteMapNode(Title = TitleSite.Actual, ParentKey = KeySite.Dashboard, Key = KeySite.IndexActual)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexActual, Key = KeySite.EditActual)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus            
            FilterQuery filter = new FilterQuery(Field.ActualCostId, FilterOperator.Equals, id.ElementAt(0).ToString());
            ActualCost dbObject = await RepoActual.FindAsync(filter);

            return View("Form", new ActualFormStub(dbObject));
        }


        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Edit(ActualFormStub model)
        {
            //kamus            
            if (ModelState.IsValid)
            {
                ActualCost dbObject = RepoActual.FindByPrimaryKey(model.ActualCostId.ToInteger());                

                //set activity log
                model.LogActivity = new LogActivityModel<ActualCost>(dbObject);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoActual.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(model.DocNumber2, template);

                return RedirectToAction("Index");
            }
            else
            {                
                return View("Form", model);
            }
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            ActualCost dbObject = await RepoActual.FindByPrimaryKeyAsync(id.ElementAt(0).ToInteger());
            ResponseModel response = new ResponseModel(true);

            if (!RepoActual.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<JsonResult> UploadExcel(HttpPostedFileBase[] files)
        {
            // kamus            
            ResponseModel resp = new ResponseModel(true);
            resp.Message = "Upload Excel Success";

            ActualImportStub model = new ActualImportStub();
            FilterQuery filter = new FilterQuery(FilterLogic.And);
            List<ActualFormStub> models = null;

            ActualCost dbItem = null;
            List<ActualCost> dbItems = null;
            List<ActualCost> tempItems = new List<ActualCost>();

            // Run tasks in background                
            var actualsTask = RepoActual.FindAllAsync(filter);

            await Task.WhenAll(actualsTask);

            dbItems = actualsTask.Result;

            model.ParseFile(files);

            models = model.ModelsFromExcel;

            if (models.Any())
            {
                foreach (var item in models)
                {
                    dbItem = new ActualCost();

                    //var temp = dbItems.Where(n => n.DocNumber.ToLower() == item.DocNumber.ToLower());
                    var temp = dbItems.Where(n => n.DocNumber2.ToLower() == item.DocNumber2.ToLower() && n.PostingDate == item.PostingDate);
                    if (temp.Any())
                    {
                        dbItem = temp.FirstOrDefault();
                        item.MapDbObject(dbItem);
                    }
                    else
                    {
                        item.MapDbObject(dbItem);
                    }

                    tempItems.Add(dbItem);
                }

                await RepoActual.SaveAllAsync(tempItems);
            }

            //message
            if (model.ErrParseExcel.Count > 0)
            {
                string errMsg = string.Join("<br/>", model.ErrParseExcel);
                resp.SetFail(errMsg);
                //this.SetMessage(errMsg, false);
            }
            //else if(models.Count() == 0)
            //{
            //    string errMsg = "Data Not Found";
            //    resp.SetFail(errMsg);
            //}
            return Json(resp);
        }

        #region binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<ActualCost> dbObjects;
            List<ActualPresentationStub> models;
            Task<List<ActualCost>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoActual.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoActual.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ActualCost, ActualPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingAsset(params object[] args)
        {
            //lib
            int count;
            List<ViewMappingAssetActualCost> dbObjects;
            List<AssetPresentationStub> models;
            IEnumerable<Asset> assets;
            Task<List<ViewMappingAssetActualCost>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FieldHelper fh = new FieldHelper(Table.AUC, Field.Plant);

            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
            {
                param.Filter.AddFilter(fh, FilterOperator.Equals, User.AreaCode);                
            }

            dbObjectsTask = RepoViewMappingAssetActualCost.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMappingAssetActualCost.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            
            assets = dbObjects.Select(n => n.Asset).Distinct();
            count = assets.Count();

            //map db data to model
            models = ListMapper.MapList<Asset, AssetPresentationStub>(assets);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion
    }
}
