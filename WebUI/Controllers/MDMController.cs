﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    //[AuthorizeUser(ModuleName = UserModule.Crew)]
    [Authorize]
    public class MDMController : BaseController<MDMRequestFormStub>
    {
        private const string KEY = "excelTemplate";

        public MDMController(            
            IMDMAssetsRepository repoMDMAsset,
            IMDMRequestRepository repoMDMRequest,
            IViewMappingAssetRepository repoViewMappingAsset,
            IAssetRepository repoAsset,
            IViewMDMRepository repoViewMDM
        )
        {
            RepoMDMRequest = repoMDMRequest;
            RepoMDMAsset = repoMDMAsset;
            RepoViewMappingAsset = repoViewMappingAsset;
            RepoAsset = repoAsset;
            RepoViewMDM = repoViewMDM;
        }

        [MvcSiteMapNode(Title = TitleSite.MDM, ParentKey = KeySite.Dashboard, Key = KeySite.MDMIndex)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        #region create
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Create, ParentKey = KeySite.MDMIndex, Key = KeySite.CreateMDM)]
        public override async Task<ActionResult> Create()
        {
            //lib
            MDMRequestFormStub model = new MDMRequestFormStub();

            //algorithm
            await Task.Delay(0);
            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Create(MDMRequestFormStub model)
        {
            if (ModelState.IsValid)
            {
                MDMRequest dbObject = new MDMRequest();

                //map dbObject
                model.MapDbObject(dbObject);                

                //save dbObject
                await RepoMDMRequest.SaveAsync(dbObject);

                var format = "dd/MM/yyyy";
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                if (model.MDMAsset != null && model.MDMAsset != "")
                {
                    List<MDMAssetFormStub> formModels = JsonConvert.DeserializeObject<List<MDMAssetFormStub>>(model.MDMAsset, dateTimeConverter);
                    List<MDMAsset> dbAssets = new List<MDMAsset>();
                    foreach (MDMAssetFormStub a in formModels)
                    {
                        a.MDMRequestId = dbObject.MDMRequestId;
                        MDMAsset dbAsset = new MDMAsset();
                        a.MapDbObject(dbAsset);
                        dbAssets.Add(dbAsset);
                    }
                    await RepoMDMAsset.SaveAllAsync(dbAssets);
                }
                
                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage(model.RequestTitle, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }
        #endregion

        #region edit
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.MDMIndex, Key = KeySite.EditMDM)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();
            MDMRequestFormStub model;

            //algoritma
            MDMRequest dbObject = await RepoMDMRequest.FindByPrimaryKeyAsync(primaryKey);
            model = new MDMRequestFormStub(dbObject);

            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Edit(MDMRequestFormStub model)
        {
            if (ModelState.IsValid)
            {
                MDMRequest dbObject = await RepoMDMRequest.FindByPrimaryKeyAsync(model.MDMRequestId);
                if(dbObject == null)
                {
                    dbObject = new MDMRequest();
                }

                //map dbObject
                model.MapDbObject(dbObject);
                //model.MapDbObjectAsset(dbObject, RepoTCR.FindAllAssetForMapping());

                //save dbObject
                await RepoMDMRequest.SaveAsync(dbObject);

                var format = "dd/MM/yyyy";
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                if (model.MDMAsset != null && model.MDMAsset != "")
                {
                    List<MDMAssetFormStub> formModels = JsonConvert.DeserializeObject<List<MDMAssetFormStub>>(model.MDMAsset, dateTimeConverter);
                    List<MDMAsset> dbAssets = new List<MDMAsset>();
                    foreach (MDMAssetFormStub a in formModels)
                    {
                        MDMAsset dbAsset = new MDMAsset();
                        a.MDMRequestId = dbObject.MDMRequestId;
                        a.MapDbObject(dbAsset);
                        dbAssets.Add(dbAsset);
                    }
                    await RepoMDMAsset.SaveAllAsync(dbAssets);
                }

                if (model.TempDelMDMAsset != null && model.TempDelMDMAsset != "")
                {                    
                    List<MDMAssetFormStub> formModels = JsonConvert.DeserializeObject<List<MDMAssetFormStub>>(model.TempDelMDMAsset, dateTimeConverter);                    
                    foreach (MDMAssetFormStub a in formModels)
                    {
                        MDMAsset dbAsset = new MDMAsset();                        
                        dbAsset  = await RepoMDMAsset.FindByPrimaryKeyAsync(a.MDMRequestId, a.AssetId);
                        a.MapDbObject(dbAsset);                        
                        await RepoMDMAsset.DeleteAsync(dbAsset);
                    }                    
                }

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(model.RequestTitle, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.CompletedMDM, ParentKey = KeySite.MDMIndex, Key = KeySite.CompletedMDM)]
        public async Task<ActionResult> SetCompletedMDM(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();
            List<MDMAssetFinishFormStub> models = new List<MDMAssetFinishFormStub>();
            FilterQuery filter = new FilterQuery(Field.MDMRequestId, FilterOperator.Equals, id[0].ToString());

            //algoritma
            List<MDMAsset> dbObjects = await RepoMDMAsset.FindAllAsync(filter);
            foreach(MDMAsset i in dbObjects)
            {
                MDMAssetFinishFormStub model = new MDMAssetFinishFormStub(i);
                models.Add(model);
            }

            MDMAssetFinishFormStub[] newModels = models.ToArray();

            return View("MDMCompleted", newModels);
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public ActionResult MDMFinishPartial(string model)
        {
            List<MDMAssetFinishFormStub> models = JsonConvert.DeserializeObject<List<MDMAssetFinishFormStub>>(HttpUtility.HtmlDecode(model));
            return PartialView("_MDMCompleted", models);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<ActionResult> SetCompletedMDM(MDMAssetFinishFormStub[] models)
        {
            bool allSaved = true;
            int id = 0;
            if (ModelState.IsValid)
            {
                if (models.HasValue())
                {
                    foreach (MDMAssetFinishFormStub i in models)
                    {
                        Asset dbObject = await RepoAsset.FindByPrimaryKeyAsync(i.AssetId);
                        if (dbObject.AssetNumber == null || dbObject.AssetNumber == "")
                        {
                            if(i.AssetNumber != null && i.AssetNumber != ""){
                                dbObject.AssetNumber = i.AssetNumber;
                                if (!RepoAsset.Save(dbObject))
                                {
                                    allSaved = false;
                                }
                            }
                            else
                            {
                                allSaved = false;
                            }
                        }
                        id = i.MDMRequestId;
                    }

                    if (allSaved)
                    {
                        MDMRequest dbObject = RepoMDMRequest.FindByPrimaryKey(id);
                        dbObject.Status = StatusMDM.COMPLETE.ToString();
                        RepoMDMRequest.Save(dbObject);
                    }
                }
                //string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                this.SetMessage("", true, "Proses Completed MDM Berhasil");
                return RedirectToAction("Index");
            }
            else
            {
                return View("MDMCompleted", models);
            }
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<ActionResult> AddAsset(params object[] id)
        {
            //kamus
            //int primaryKey = id[0].ToInteger();
            MDMAssetFormStub model = new MDMAssetFormStub();

            //algoritma                        
            await Task.Delay(0);

            return PartialView("_Form", model);
        }


        #endregion

        #region delete
        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();

            //algoritma
            MDMRequest dbObject = await RepoMDMRequest.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!RepoMDMRequest.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }
        #endregion

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<ActionResult> setComplete(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();

            //algoritma
            MDMRequest dbObject = await RepoMDMRequest.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);
            dbObject.Status = StatusMDM.COMPLETE.ToString();

            if (!RepoMDMRequest.Save(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        #region binding
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            //int count;
            //List<MDMRequest> dbObjects;
            //List<MDMRequestPresentationStub> models;
            //Task<List<MDMRequest>> dbObjectsTask = null;
            //Task<int> countTask = null;
            //QueryRequestParameter param = QueryRequestParameter.Current;

            ////algorithm
            //dbObjectsTask = RepoMDMRequest.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            //countTask = RepoMDMRequest.CountAsync(param.Filter);

            ////get and count data from db in background thread
            //await Task.WhenAll(dbObjectsTask, countTask);

            ////get callback from task
            //dbObjects = dbObjectsTask.Result;
            //count = countTask.Result;

            ////map db data to model
            //models = ListMapper.MapList<MDMRequest, MDMRequestPresentationStub>(dbObjects);

            
            int count;
            List<ViewMDM> dbObjects;
            List<MDMRequestPresentationStub> models;
            Task<List<ViewMDM>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;


            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);


            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(Field.Plant, FilterOperator.Equals, User.AreaCode);

            dbObjectsTask = RepoViewMDM.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMDM.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewMDM, MDMRequestPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMappingAsset(params object[] args)
        {
            //lib
            List<ViewMappingAsset> dbObjects;
            List<MDMAssetOptionsPresentationStub> models;
            Task<List<ViewMappingAsset>> dbObjectsTask = null;
            List<int> assetIds = new List<int>();
            FilterQuery filter = new FilterQuery(Field.StatusTcr, FilterOperator.Equals, StatusTCR.APPROVE_BY_FINANCE);
            filter.AddFilter(Field.MDMRequestId, FilterOperator.IsNull);

            //algorithm
            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            
                models = ListMapper.MapList<ViewMappingAsset, MDMAssetOptionsPresentationStub>(dbObjects).Distinct().ToList();

            return JsonConvert.SerializeObject(new { data = models });
        }

        public async Task<string> BindingStatus(params object[] args)
        {
            //lib
           List<SelectListItem> items = DisplayFormat.EnumToSelectList<StatusMDM>();
            await Task.Delay(0);
           return JsonConvert.SerializeObject(items);
        }

        [HttpPost]
        public async Task<string> GetAssetCount(params object[] args)
        {
            //lib           
            int total;
            FilterQuery filter = new FilterQuery(Field.StatusTcr, FilterOperator.Equals, StatusTCR.APPROVE_BY_FINANCE);
            filter.AddFilter(Field.MDMRequestId, FilterOperator.IsNull);

            //algorithm
            total = await RepoViewMappingAsset.CountAsync(filter);

            return total.ToString();
        }
        #endregion

        #region download
        public async Task<ActionResult> GetTemplate(int Id)
        {
            //kamus
            MDMRequest dbItem = await RepoMDMRequest.FindByPrimaryKeyAsync(Id);
            FileStream fileStream;
            XSSFWorkbook workbook;
            DownloadParameterStub downloadParam = null;
            ResponseModel response = new ResponseModel(true);

            MDMExcelStub param = new MDMExcelStub();
            string filePath = "~/Uploads/Template/", fileName = "MDM_template.xlsx";
            FilterQuery filter = new FilterQuery(Field.MDMRequestId, FilterOperator.Equals, dbItem.MDMRequestId);
            List<ViewMappingAsset> assets = await RepoViewMappingAsset.FindAllAsync(filter);

            //remove warning opening excel
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            //check file exist
            if (System.IO.File.Exists(Server.MapPath($"{filePath}{fileName}")))
            {
                //read file and set to workbook
                fileStream = new FileStream(Server.MapPath($"{filePath}{fileName}"), FileMode.Open, FileAccess.ReadWrite);
                workbook = param.SetData(fileStream, dbItem, assets, User);
                fileStream.Close();

                fileName = $"MDM Lampiran ({DateTime.Now.ToShortDateString()}).xlsx";

                downloadParam = new DownloadParameterStub
                {
                    MemoryStream = new MemoryStream(),
                    FileDownloadName = fileName,
                    ContentType = MimeMapping.GetMimeMapping(fileName)
                };

                //write workbook to stream
                workbook.Write(downloadParam.MemoryStream);

                //store download parameter to temp data
                TempData[KEY] = downloadParam;
            }

            if (downloadParam == null)
            {
                response.SetFail("MDM Template not found");
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Download()
        {
            DownloadParameterStub param = TempData[KEY] as DownloadParameterStub;

            if (param is DownloadParameterStub)
            {
                return File(param.MemoryStream.ToArray(), param.ContentType, param.FileDownloadName);
            }

            return new EmptyResult();
        }

        [HttpPost]
        public async Task<JsonResult> UploadExcel(int id, HttpPostedFileBase[] files)
        {
            // kamus            
            ResponseModel resp = new ResponseModel(true);
            resp.Message = "Upload Excel Success";                        

            List<MDMAssetFinishFormStub> modelObjects = new List<MDMAssetFinishFormStub>();
            FilterQuery filter = new FilterQuery(Field.MDMRequestId, FilterOperator.Equals, id.ToString());

            //algoritma
            List<MDMAsset> dbObjects = await RepoMDMAsset.FindAllAsync(filter);
            foreach (MDMAsset i in dbObjects)
            {
                MDMAssetFinishFormStub modelObject = new MDMAssetFinishFormStub(i);
                modelObjects.Add(modelObject);
            }
            MDMImportStub model = new MDMImportStub();
            model.ModelsFromExcel = modelObjects;

            model.ParseFile(files);            

            //message
            if (model.ErrParseExcel.Count > 0)
            {
                string errMsg = string.Join("<br/>", model.ErrParseExcel);
                resp.SetFail(errMsg);
                //this.SetMessage(errMsg, false);
            }            
            resp.Data = HttpUtility.HtmlEncode(JsonConvert.SerializeObject(model.ModelsFromExcel));
            
            return Json(resp);
        }        
        #endregion
    }
}
