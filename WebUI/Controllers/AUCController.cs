﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    //[AuthorizeUser(ModuleName = UserModule.Crew)]
    [Authorize]
    public class AUCController : BaseController<AUCFormStub>
    {   
        public AUCController(
            IAUCRepository repoAUC,
            IViewMappingAssetActualCostRepository repoViewMappingAssetActualCost,
            IMappingAssetActualCostRepository repoMappingAssetActualCost
        )
        {
            RepoAUC = repoAUC;
            RepoViewMappingAssetActualCost = repoViewMappingAssetActualCost;
            RepoMappingAssetActualCost = repoMappingAssetActualCost;
        }

        [MvcSiteMapNode(Title = TitleSite.AUC, ParentKey = KeySite.Dashboard, Key = KeySite.IndexAUC)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexAUC, Key = KeySite.EditAUC)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus                        
            AUC dbObject = await RepoAUC.FindByPrimaryKeyAsync(id.ElementAt(0).ToString());

            return View("Form", new AUCFormStub(dbObject));
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Edit(AUCFormStub model)
        {
            //kamus            
            if (ModelState.IsValid)
            {
                AUC dbObject = RepoAUC.FindByPrimaryKey(model.WBSElement);

                //set activity log
                model.LogActivity = new LogActivityModel<AUC>(dbObject);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoAUC.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(model.WBSElement, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            AUC dbObject = await RepoAUC.FindByPrimaryKeyAsync(id.ElementAt(0));
            ResponseModel response = new ResponseModel(true);

            if (!RepoAUC.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public async Task<JsonResult> UploadExcel(HttpPostedFileBase[] files)
        {
            // kamus            
            ResponseModel resp = new ResponseModel(true);
            resp.Message = "Upload Excel Success";

            AUCImportStub model = new AUCImportStub();
            FilterQuery filter = new FilterQuery(FilterLogic.And);
            List<AUCFormStub> models = null;

            AUC dbItem = null;
            AUC oldItem = null;
            List<AUC> dbItems = null;
            List<AUC> tempItems = new List<AUC>();            

            // Run tasks in background                            
            var aucsTask = RepoAUC.FindAllAsync();

            await Task.WhenAll(aucsTask);
            
            dbItems = aucsTask.Result;

            model.ParseFile(files);

            models = model.ModelsFromExcel;

            if (models.Any())
            {
                foreach (var item in models)
                {
                    filter = new FilterQuery(Field.WBSElement, FilterOperator.Equals, item.WBSElement);
                    oldItem = RepoAUC.Find(filter);

                    dbItem = new AUC();
                    
                    if (oldItem != null)
                    {
                        dbItem = oldItem;
                        item.AcquisValIdr = oldItem.AcquisValIdr + item.AcquisValIdr;
                        item.AcquisValUsd = oldItem.AcquisValUsd + item.AcquisValUsd;
                        
                        item.MapDbObject(dbItem);
                    }
                    else
                    {
                        item.MapDbObject(dbItem);
                    }

                    tempItems.Add(dbItem);
                }

                await RepoAUC.SaveAllAsync(tempItems);
            }

            //message
            if (model.ErrParseExcel.Count > 0)
            {
                string errMsg = string.Join("<br/>", model.ErrParseExcel);
                resp.SetFail(errMsg);
                //this.SetMessage(errMsg, false);
            }
            //else if(models.Count() == 0)
            //{
            //    string errMsg = "Data Not Found";
            //    resp.SetFail(errMsg);
            //}
            return Json(resp);
        }
        
        #region binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<AUC> dbObjects;
            List<AUCPresentationStub> models;
            Task<List<AUC>> dbObjectsTask = null;
            Task<List<MappingAssetActualCost>> mappingsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FilterQuery filter = new FilterQuery(Field.WBSElement, FilterOperator.IsNotNull);
            FieldHelper fh = new FieldHelper(Table.AUC, Field.Plant);

            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
            {
                filter.AddFilter(fh, FilterOperator.Equals, User.AreaCode);
            }

            dbObjectsTask = RepoAUC.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoAUC.CountAsync(param.Filter);
            mappingsTask = RepoMappingAssetActualCost.FindAllAsync(filter);                

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask, mappingsTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<AUC, AUCPresentationStub>(dbObjects, mappingsTask.Result);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        #endregion
    }
}
