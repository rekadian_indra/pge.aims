﻿using Business.Abstract;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using LogAction.Abstract;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(ILogRepository repoLog, IViewMappingAssetRepository repoViewMappingAsset)
        {
            RepoLog = repoLog;
            RepoViewMappingAsset = repoViewMappingAsset;
        }

        //if using ModelBinder to get user login use SGUser parameter
        //if using Principal dont get user login SGUser parameter
        //public ActionResult Index(SGUser user)
        //{
        //}

        [MvcSiteMapNode(Title = "<span class=\"fa fa-home\"></span>", ParentKey = KeySite.Home, Key = KeySite.Dashboard)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        #region binding
        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<ViewMappingAsset> dbObjects;
            List<AssetPresentationStub> models;
            Task<List<ViewMappingAsset>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FilterQuery temp = new FilterQuery();

            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);


            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

            temp = new FilterQuery(Field.StatusSPC, FilterOperator.Equals, StatusSPC.DRAFT.ToString(), FilterLogic.Or);
            temp.AddFilter(Field.StatusSPC, FilterOperator.IsNull);

            param.Filter.AddFilter(temp);

            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMappingAsset.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }
        #endregion
    }
}
