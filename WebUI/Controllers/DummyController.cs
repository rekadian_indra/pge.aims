﻿using Business.Abstract;
using System.Collections.Generic;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Extension;
using Business.Entities;
using WebUI.Models;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using Common.Enums;

namespace WebUI.Controllers
{
    //[AuthorizeUser]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    //[AllowCrossSite]
    public class DummyController : BaseController
    {
        private IDummyRepository RepoDummy { get; set; }
        
        public DummyController(IDummyRepository repoDummy)
        {
            RepoDummy = repoDummy;
        }

        public string Iframe()
        {
            return "<a href=\"#\">test</a>";
        }

        [MvcSiteMapNode(Title = "Dummy", ParentKey = "Dashboard", Key = "IndexDummy")]
        public ActionResult Index()
        {
            return View();
        }

        [MvcSiteMapNode(Title = "Create", ParentKey = "IndexDummy", Key = "CreateDummy")]
        public ActionResult Create()
        {
            DummyFormStub model = new DummyFormStub();

            return View("Form", model);
        }

        [HttpPost]
        public ActionResult Create(DummyFormStub model)
        {
            if (ModelState.IsValid)
            {
                DummyTable dbObject = new DummyTable();

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                RepoDummy.Save(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject("MyGlobalMessage", "CreateSuccess").ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        [MvcSiteMapNode(Title = "Edit", ParentKey = "IndexDummy", Key = "EditDummy")]
        public ViewResult Edit(int id)
        {
            DummyFormStub model = new DummyFormStub(RepoDummy.FindByPrimaryKey(id));

            ViewBag.Name = model.Name;

            return View("Form", model);
        }

        [HttpPost]
        public ActionResult Edit(DummyFormStub model)
        {
            if (ModelState.IsValid)
            {
                DummyTable dbObject = RepoDummy.FindByPrimaryKey(model.Id);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                RepoDummy.Save(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject("MyGlobalMessage", "EditSuccess").ToString();
                this.SetMessage(model.Name, template);

                return RedirectToAction("Index");
            }
            else
            {
                DummyTable dbItem = RepoDummy.FindByPrimaryKey(model.Id);
                ViewBag.Name = dbItem.Name;

                return View("Form", model);
            }
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            DummyTable dbItem = RepoDummy.FindByPrimaryKey(id);
            ResponseModel response = new ResponseModel(true);

            //if (!RepoDummyTable.IsAllowDelete(dbItem))
            //{
            //    message = HttpContext.GetGlobalResourceObject("MyGlobalMessage", "DeleteFailedForeignKey").ToString();
            //    response.SetFail(message);
            //}
            //else
            //    RepoDummyTable.Delete(dbItem);

            return Json(response);
        }

        #region binding

        public string Binding(string customFilter)
        {
            //lib
            int count;
            List<DummyTable> items;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (!string.IsNullOrEmpty(customFilter))
            {
                //set custom filter
                param.InitFilter();

                if (!customFilter.Equals("0"))
                    param.Filter.AddFilter("Id", FilterOperator.Equals, "28");
            }

            items = RepoDummy.FindAll(param.Skip, param.Take, param.Sorts, param.Filter);
            count = RepoDummy.Count(param.Filter);

            return JsonConvert.SerializeObject(new { total = count, data = DummyPresentationStub.MapList(items) });
        }

        #endregion
    }
}
