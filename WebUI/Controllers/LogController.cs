﻿using Business.Abstract;
using Business.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebUI.Infrastructure;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class LogController : BaseController
    {
        public LogController(ILogRepository repoLog)
        {
            RepoLog = repoLog;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Download()
        {
            DisplayFormatHelper dfh = new DisplayFormatHelper();
            List<log> logs = await RepoLog.FindAllAsync();
            LogPresentationStub stub = new LogPresentationStub();
            MemoryStream ms = stub.GenerateExcel(stub.MapList(logs));
            string filename = string.Format("log {0}.xlsx", DateTime.Now.ToString(dfh.SqlDateFormat));

            return File(ms.ToArray(), "application/vns.ms-excel", filename);
        }

        [HttpPost]
        public void Truncate()
        {
            RepoLog.Truncate();
        }

        #region binding

        public async Task<string> Binding()
        {
            GridRequestParameters param = GridRequestParameters.Current;

            Business.Infrastructure.FilterInfo filters = new Business.Infrastructure.FilterInfo
            {
                Field = "Action",
                Operator = "startswith",
                Value = "/Log"
            };
            //param.Filters = filters;
            var itemsTask = RepoLog.FindAllAsync(
                param.Skip, 
                param.Take,
                //(param.Sortings != null ? param.Sortings.ToList() : null), 
                (param.Sortings?.ToList()),
                //(param.Filters != null ? param.Filters : null));
                (param.Filters ?? null));
            var totalTask = RepoLog.CountAsync(param.Filters);

            await Task.WhenAll(itemsTask, totalTask);

            List<log> items = itemsTask.Result;
            int total = totalTask.Result;

            return JsonConvert.SerializeObject(new { total = total, data = new LogPresentationStub().MapList(items) });
        }

        #endregion
    }
}
