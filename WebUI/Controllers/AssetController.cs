﻿using Business.Abstract;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using Newtonsoft.Json;
using NPOI;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.Extension;
using WebUI.Infrastructure;
using WebUI.Models;
using static WebUI.Models.ResponseModel;
//using GemBox.Spreadsheet;
using NPOI.SS.Util;
using System.Text.RegularExpressions;

namespace WebUI.Controllers
{
    //[AllowCrossSite]
    //[OutputCache(NoStore = true, Duration = 0, Location = System.Web.UI.OutputCacheLocation.None)]
    //[AuthorizeUser(ModuleName = UserModule.Crew)]
    [Authorize]
    public class AssetController : BaseController<AssetFormFAStub>
    {
        private const string KEY = "ExcelFile";
        public AssetController(
            IAssetRepository repoAsset,
            IViewMappingAssetRepository repoViewMappingAsset,
             IViewAssetRepository repoViewAsset
        )
        {
            RepoAsset = repoAsset;
            RepoViewMappingAsset = repoViewMappingAsset;
            RepoViewAsset = repoViewAsset;
        }

        [MvcSiteMapNode(Title = TitleSite.Asset, ParentKey = KeySite.Dashboard, Key = KeySite.IndexAsset)]
        public override async Task<ActionResult> Index()
        {
            //kamus

            //algo
            FilterQuery filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

            FilterQuery temp = new FilterQuery(Field.StatusSPC, FilterOperator.Equals, StatusSPC.DRAFT.ToString(), FilterLogic.Or);
            temp.AddFilter(Field.StatusSPC, FilterOperator.IsNull);

            filter.AddFilter(temp);
            

            ViewBag.count = await RepoViewMappingAsset.CountAsync(filter);
            return await base.Index();
        }

        [MvcSiteMapNode(Title = TitleSite.Detail, ParentKey = KeySite.IndexAsset, Key = KeySite.DetailAsset)]
        public async Task<ActionResult> Detail(int id)
        {
            //kamus
            ViewMappingAsset dbObject;

            //algo
            FilterQuery filter = new FilterQuery(Field.AssetId, FilterOperator.Equals, id);
            dbObject = await RepoViewMappingAsset.FindAsync(filter);

            return View(new AssetPresentationStub(dbObject));
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        [MvcSiteMapNode(Title = TitleSite.Edit, ParentKey = KeySite.IndexAsset, Key = KeySite.EditAsset)]
        public override async Task<ActionResult> Edit(params object[] id)
        {
            //kamus
            Asset asset;
            //ViewMappingAsset mappingAsset;
            Task<Asset> assetTask;
            //Task<ViewMappingAsset> mappingAssetTask;
            //algo
            FilterQuery filter = new FilterQuery(Field.AssetId, FilterOperator.Equals, id.ElementAt(0));
            assetTask = RepoAsset.FindAsync(filter);
            //mappingAssetTask = RepoViewMappingAsset.FindAsync(filter);

            //await Task.WhenAll(assetTask, mappingAssetTask);

            await Task.WhenAll(assetTask);

            asset = assetTask.Result;
            //mappingAsset = mappingAssetTask.Result;

            AssetFormFAStub model = new AssetFormFAStub(asset);
            model.BarcodeImage = string.IsNullOrEmpty(model.BarcodeImage) ? string.Empty : Url.Content(model.BarcodeImage);

            return View("Form", model);
        }

        [HttpPost]
        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Edit(AssetFormFAStub model)
        {
            if (ModelState.IsValid)
            {
                Asset dbObject = RepoAsset.FindByPrimaryKey(model.AssetId);

                if (!string.IsNullOrEmpty(model.BarcodeImageTemp))
                {
                    string directory = ConfigurationManager.AppSettings["QRDirectory"];

                    //Convert Base64 Encoded string to Byte Array.
                    byte[] imageBytes = Convert.FromBase64String(model.BarcodeImage);

                    //Save the Byte Array as Image File.

                    if (!Directory.Exists(Server.MapPath(directory)))
                        Directory.CreateDirectory(Server.MapPath(directory));

                    string filename = string.Format("{0}{1}{2}", directory, model.AssetNumber, ".png");
                    string filePath = filename;

                    System.IO.File.WriteAllBytes(Server.MapPath(filePath), imageBytes);

                    model.BarcodeImage = filename;
                }

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoAsset.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                this.SetMessage(dbObject.AssetNumber, true, template);

                return RedirectToAction("Index");
            }
            else
            {
                return View("Form", model);
            }
        }

        [AuthorizeUser(Roles = new object[] { UserRole.Superadmin, UserRole.Administrator })]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            //kamus
            int primaryKey = id[0].ToInteger();

            //algoritma
            Asset dbObject = await RepoAsset.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!RepoAsset.Delete(dbObject))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> GenerateQRCode(string barcode)
        {
            ResponseModel response = new ResponseModel(true);
            string key = Url.Action(ActionSite.EditMobile, ControllerSite.PhysicalCheck, new { id = barcode }, Request.Url.Scheme);
            Bitmap bitmap = BarcodeHelper.QRCodeBitmap(key);
            AssetFormFAStub model = new AssetFormFAStub();

            string temp, base64;
            using (MemoryStream ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Png);
                byte[] byteImage = ms.ToArray();

                Convert.ToBase64String(byteImage);
                base64 = Convert.ToBase64String(byteImage);
                temp = string.Format("{0}{1}", "data:image/png;base64,", base64);
                model.BarcodeImage = base64;
                model.BarcodeImageTemp = temp;

                response.Data = JsonConvert.SerializeObject(model);
            }

            await Task.Delay(0);
            return Json(response);
        }

        #region binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<ViewMappingAsset> dbObjects;
            List<AssetPresentationStub> models;
            Task<List<ViewMappingAsset>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

            param.Filter.AddFilter(Field.StatusSPC, FilterOperator.Equals, StatusSPC.COMPLETED.ToString());                      

            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMappingAsset.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingOngoing(params object[] args)
        {
            //lib
            int count;
            List<ViewMappingAsset> dbObjects;
            List<AssetPresentationStub> models;
            Task<List<ViewMappingAsset>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;
            FilterQuery temp;

            //algorithm            
            if (param.Filter == null)
                param.Filter = new FilterQuery(FilterLogic.And);

            if (!User.HasAccess(UserRole.Superadmin) && User != null && User.AreaCode.ToLower() != "all")
                param.Filter.AddFilter(Field.PlantMapping, FilterOperator.Equals, User.AreaName);

            temp = new FilterQuery(Field.StatusSPC, FilterOperator.Equals, StatusSPC.DRAFT.ToString(), FilterLogic.Or);
            temp.AddFilter(Field.StatusSPC, FilterOperator.IsNull);

            param.Filter.AddFilter(temp);
            
            dbObjectsTask = RepoViewMappingAsset.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewMappingAsset.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewMappingAsset, AssetPresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingMappingAssetActualCost(params object[] args)
        {                   
            int count;
            List<ViewAsset> dbObjects;
            List<MappingAssetActualCostFormStub> models;
            Task<List<ViewAsset>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (args != null && args.Any())
            {
                int mappingAssetId = int.Parse(args.First().ToString());
                if (param.Filter == null)
                    param.Filter = new FilterQuery(Field.MappingAssetId, FilterOperator.Equals, mappingAssetId);
                else
                    param.Filter.AddFilter(Field.MappingAssetId, FilterOperator.Equals, mappingAssetId);
            }

            dbObjectsTask = RepoViewAsset.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoViewAsset.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<ViewAsset, MappingAssetActualCostFormStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<ActionResult> GetTemplatePdf(int[] id)
        {

            FilterQuery filter = new FilterQuery(FilterLogic.Or);
            List<Asset> dbObjects = new List<Asset>();
            List<Asset> newObjects = new List<Asset>();

            // lib            
            string contentType;
            string filename;
            string license = ConfigurationManager.AppSettings["GemboxLicense"];
            ResponseModel response = new ResponseModel(true);

            IWorkbook workbook = new XSSFWorkbook();
            XSSFWorkbook xssfWorkbook = workbook as XSSFWorkbook;
            DownloadParameterStub downloadParam = new DownloadParameterStub();
            ISheet sheet; IRow row; ICell cell; ICellStyle style;

            CellRangeAddress mergeCell;
            XSSFCreationHelper helper;
            XSSFDrawing drawing;
            XSSFClientAnchor anchor;
            XSSFPicture pict;
            byte[] data;
            int picInd;

            sheet = xssfWorkbook.CreateSheet("Sheet1");


            foreach (int i in id)
            {
                dbObjects.Add(await RepoAsset.FindByPrimaryKeyAsync(i));
            }
           
            int c = 0;
            int r = 0;
            sheet.SetColumnWidth(0, 3000);
            sheet.SetColumnWidth(1, 5000);
            sheet.SetColumnWidth(2, 3000);
            sheet.SetColumnWidth(3, 1000);
            sheet.SetColumnWidth(4, 3000);
            sheet.SetColumnWidth(5, 5000);
            sheet.SetColumnWidth(6, 3000);
            row = sheet.CreateRow(0);
            row = sheet.CreateRow(1);
            row = sheet.CreateRow(2);
            if (dbObjects != null)
            {
                foreach (Asset i in dbObjects)
                {
                    Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);
                    string resultText = regex.Replace(i.Description, " ").Trim();
                    string serverPath = null;

                    if (!string.IsNullOrEmpty(i.BarcodeImage))
                    {
                        if (System.IO.File.Exists(Server.MapPath(i.BarcodeImage)))
                        {
                            serverPath = i.BarcodeImage;
                        }
                    }

                    if (string.IsNullOrEmpty(serverPath))
                    {
                        FileResponseModel model = getBitmap(i);
                        i.BarcodeImage = model.filepath;
                        newObjects.Add(i);
                        serverPath = model.filepath;
                    }

                    row = sheet.GetRow(r);
                    mergeCell = new NPOI.SS.Util.CellRangeAddress(r, r, c, c + 2);
                    sheet.AddMergedRegion(mergeCell);
                    row.HeightInPoints = 90;
                    cell = row.CreateCell(c);
                    data = System.IO.File.ReadAllBytes(Server.MapPath(serverPath));
                    picInd = workbook.AddPicture(data, PictureType.PNG);


                    helper = workbook.GetCreationHelper() as XSSFCreationHelper;
                    drawing = sheet.CreateDrawingPatriarch() as XSSFDrawing;
                    anchor = helper.CreateClientAnchor() as XSSFClientAnchor;

                    anchor.Col1 = c + 1;
                    anchor.Row1 = r;
                    pict = drawing.CreatePicture(anchor, picInd) as XSSFPicture;
                    pict.Resize(1.0, 1.0);

                    mergeCell = new NPOI.SS.Util.CellRangeAddress(r + 1, r + 1, c, c + 2);
                    sheet.AddMergedRegion(mergeCell);
                    row = sheet.GetRow(r + 1);
                    cell = row.CreateCell(c);
                    string assetNum = string.IsNullOrEmpty(i.AssetNumber) ? "-" : i.AssetNumber;
                    cell.SetCellValue(assetNum);
                    cell.SetCellType(CellType.String);
                    style = (XSSFCellStyle)workbook.CreateCellStyle();
                    style.Alignment = HorizontalAlignment.Center;
                    cell.CellStyle = style;

                    mergeCell = new NPOI.SS.Util.CellRangeAddress(r + 2, r + 2, c, c + 2);
                    sheet.AddMergedRegion(mergeCell);
                    row = sheet.GetRow(r + 2);
                    cell = row.CreateCell(c);
                    cell.SetCellValue(resultText);
                    cell.SetCellType(CellType.String);
                    style = (XSSFCellStyle)workbook.CreateCellStyle();
                    style.Alignment = HorizontalAlignment.Center;
                    cell.CellStyle = style;

                    if (c == 4)
                    {
                        c = 0;
                        r += 5;
                        row = sheet.CreateRow(r);
                        row = sheet.CreateRow(r + 1);
                        row = sheet.CreateRow(r + 2);
                    }
                    else
                    {
                        c = 4;
                    }

                }

                //write to File stream
                string filePath = "~/Uploads/QrCode/", fileName = "Barcode-Temporary.xlsx";
                FileStream out1 = new FileStream(Server.MapPath($"{filePath}{fileName}"), FileMode.Create);
                workbook.Write(out1);
                out1.Close();

                //LoadFrom FileStream and Convert to pdf
                //SpreadsheetInfo.SetLicense(license);
                //var pdfFile = ExcelFile.Load(Server.MapPath($"{filePath}{fileName}"));

                //save pdf to memory stream
                MemoryStream memoryStream = new MemoryStream();
                //pdfFile.Save(memoryStream, SaveOptions.PdfDefault);

                //get content type
                filename = FileDirectory.GetUniqueFileName("Barcode-Asset", ".pdf");
                contentType = MimeMapping.GetMimeMapping(filename);

                downloadParam.MemoryStream = memoryStream;
                downloadParam.ContentType = contentType;
                downloadParam.FileDownloadName = filename;


                TempData[KEY] = downloadParam;
                return Json(response);
            }

            if (newObjects.HasValue())
            {
                await RepoAsset.SaveAllAsync(newObjects);
            }
            return null;
        }

        public ActionResult Download()
        {
            if (TempData[KEY] != null)
            {
                DownloadParameterStub param = TempData[KEY] as DownloadParameterStub;

                return File(param.MemoryStream.ToArray(), param.ContentType, param.FileDownloadName);
            }

            return new EmptyResult();
        }

        public FileResponseModel getBitmap(Asset asset)
        {
            string FILE_DIRECTORY = "~/Uploads/QrCode/";
            string physicalPath;
            FileResponseModel model = new FileResponseModel();
            string key = Url.Action(ActionSite.EditMobile, ControllerSite.PhysicalCheck, new { id = asset.AssetId }, Request.Url.Scheme);
            Bitmap bitmap = BarcodeHelper.QRCodeBitmap(key);
            physicalPath = Path.Combine(Server.MapPath(FILE_DIRECTORY), asset.AssetId.ToString()) + ".png";
            bitmap.Save(physicalPath, ImageFormat.Png);
            model.filename = (asset.AssetId.ToString() + ".png");
            model.filepath = FILE_DIRECTORY + asset.AssetId.ToString() + ".png";
            model.absolutepath = VirtualPathUtility.ToAbsolute(FILE_DIRECTORY + asset.AssetId.ToString() + ".png");
            return model;
        }

        #endregion
    }
}
