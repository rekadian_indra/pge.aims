﻿using Business.Abstract;
using Business.Concrete;
using LogAction.Abstract;
using LogAction.Concrete;
using Ninject.Modules;
using WebUI.Infrastructure.Abstract;
using WebUI.Infrastructure.Concrete;

namespace WebUI.Infrastructure
{
    public class AppModule : NinjectModule
    {
        public static NinjectModule[] Modules
        {
            get
            {
                return new[] { new AppModule() };
            }
        }

        public override void Load()
        {
            //TODO: Bind to Concrete Types Here
            //Kernel.Bind<IAirportRepository>().To<EFAirportRepository>();
            //Kernel.Bind<ICompanyRepository>().To<EFCompanyRepository>();
            //Kernel.Bind<ICrewRepository>().To<EFCrewRepository>();
            //Kernel.Bind<IWhitelistRepository>().To<EFWhitelistRepository>();
            //Kernel.Bind<IViewCrewRepository>().To<EFViewCrewRepository>();
            //Kernel.Bind<IViewWhitelistRepository>().To<EFViewWhitelistRepository>();
            Kernel.Bind<IAssetRepository>().To<EFAssetRepository>();
            Kernel.Bind<IAUCRepository>().To<EFAUCRepository>();
            Kernel.Bind<IActualCostRepository>().To<EFActualCostRepository>();
            Kernel.Bind<ILocationRepository>().To<EFLocationRepository>();
            Kernel.Bind<IMappingRepository>().To<EFMappingRepository>();
            Kernel.Bind<IViewMappingAssetRepository>().To<EFViewMappingAssetRepository>();
            Kernel.Bind<IViewActualRepository>().To<EFViewActualRepository>();
            Kernel.Bind<IViewMappingAssetActualCostRepository>().To<EFViewMappingAssetActualCostRepository>();
            Kernel.Bind<IMappingAssetRepository>().To<EFMappingAssetRepository>();
            Kernel.Bind<IMappingAssetActualCostRepository>().To<EFMappingAssetActualCostRepository>();
            Kernel.Bind<IViewAUCRepository>().To<EFViewAUCRepository>();
            Kernel.Bind<ITCRRepository>().To<EFTCRRepository>();
            Kernel.Bind<IMDMRequestRepository>().To<EFMDMRequestRepository>();
            Kernel.Bind<IMDMAssetsRepository>().To<EFMDMAssetsRepository>();
            Kernel.Bind<IPhysicalCheckRepository>().To<EFPhysicalCheckRepository>();
            Kernel.Bind<IPhysicalCheckAssetRepository>().To<EFPhysicalCheckAssetRepository>();
            Kernel.Bind<ISPCRequestRepository>().To<EFSPCRequestRepository>();
            Kernel.Bind<ISPCRequestAssetsRepository>().To<EFSPCRequstAssetsRepository>();
            Kernel.Bind<ICostCenterRepository>().To<EFCostCenterRepository>();
            Kernel.Bind<IPlantRepository>().To<EFPlantRepository>();
            Kernel.Bind<IViewMDMRepository>().To<EFViewMDMRepository>();

            //user management
            Kernel.Bind<IUserRepository>().To<EFUserRepository>();
            Kernel.Bind<IRoleRepository>().To<EFRoleRepository>();
            Kernel.Bind<IModuleRepository>().To<EFModuleRepository>();
            Kernel.Bind<IActionRepository>().To<EFActionRepository>();
            Kernel.Bind<IModulesInRoleRepository>().To<EFModulesInRoleRepository>();
            Kernel.Bind<IMembershipRepository>().To<EFMembershipRepository>();
            Kernel.Bind<IMembershipUserRepository>().To<EFMembershipUserRepository>();
            Kernel.Bind<IViewUserRepository>().To<EFViewUserRepository>();
            Kernel.Bind<IViewMappingRepository>().To<EFViewMappingRepository>();
            Kernel.Bind<IViewPhysicalCheckRepository>().To<EFViewPhysicalCheckRepository>();
            Kernel.Bind<IViewAssetRepository>().To<EFViewAssetRepository>();

            //others
            Kernel.Bind<ILogRepository>().To<EFLogRepository>();
            Kernel.Bind<IAuthProvider>().To<DummyAuthProvider>();
        }
    }
}

