﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using Business.Infrastructure;
using Common.Enums;

namespace WebUI.Infrastructure
{
    public class QueryRequestParameter
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public List<SortQuery> Sorts { get; set; }
        public FilterQuery Filter { get; set; }

        private QueryRequestParameter()
        {
            Sorts = new List<SortQuery>();
        }

        public static QueryRequestParameter Current
        {
            get
            {
                var p = new QueryRequestParameter();
                p.Populate();

                return p;
            }
        }

        public void InstanceFilter()
        {
            if (Filter == null)
                Filter = new FilterQuery();
        }

        //TODO: pull default values from config
        private void Populate()
        {
            if (HttpContext.Current != null)
            {
                HttpRequest request = HttpContext.Current.Request;

                if (request["page"] != null)
                    Page = int.Parse(request["page"]);
                if (request["pageSize"] != null)
                    PageSize = int.Parse(request["pageSize"]);
                if (request["skip"] != null)
                    Skip = int.Parse(request["skip"]);
                if (request["take"] != null)
                    Take = int.Parse(request["take"]);

                //build sorting objects
                BuildSort(request);

                //build filter objects
                BuildFilter(request);
            }
        }

        private void BuildSort(HttpRequest request)
        {
            //lib
            SortQuery sort;
            int i = 0;
            bool stop = false;

            //algorithm
            while (!stop)
            {
                string field = request["sort[" + i + "][field]"];
                string direction = request["sort[" + i + "][dir]"];

                if (string.IsNullOrEmpty(direction))
                    stop = true;

                if (field != null)
                {
                    string[] split;
                    Match match = Regex.Match(field, @"^([\w]+)[.]([\w]+)");
                    
                    if (match.Success)
                    {
                        split = field.Split('.');

                        sort = new SortQuery(
                            new FieldHelper(split[0], split[1]),
                            SortQuery.ParseOrder(direction)
                        );
                    }
                    else
                    {
                        sort = new SortQuery(
                            (Field)Enum.Parse(typeof(Field), field),
                            SortQuery.ParseOrder(direction)
                        );
                    }

                    Sorts.Add(sort);
                }

                ++i;
            }
        }

        private void BuildFilter(HttpRequest request)
        {
            //lib
            Match match;
            string field;
            string @operator;
            string value;
            string[] split;
            int i = 0;
            bool stop1 = false;
            string logic = request["filter[logic]"];

            //algorithm
            if (!string.IsNullOrEmpty(logic))
            {
                Filter = new FilterQuery(FilterQuery.ParseLogic(logic));

                while (!stop1)
                {
                    field = request["filter[filters][" + i + "][field]"];

                    if (field == null)
                    {
                        field = request["filter[filters][" + i + "][filters][0][field]"];

                        if (field == null)
                            stop1 = true;
                        else
                        {
                            //second level filter
                            FilterQuery filter;
                            int j = 0;
                            bool stop2 = false;

                            logic = request["filter[filters][" + i + "][logic]"];
                            filter = new FilterQuery(FilterQuery.ParseLogic(logic));

                            while (!stop2)
                            {
                                field = request["filter[filters][" + i + "][filters][" + j + "][field]"];

                                if (field == null)
                                    stop2 = true;
                                else
                                { 
                                    @operator = request["filter[filters][" + i + "][filters][" + j + "][operator]"];
                                    value = request["filter[filters][" + i + "][filters][" + j + "][value]"];
                                    match = Regex.Match(field, @"^([\w]+)[.]([\w]+)");

                                    if (match.Success)
                                    {
                                        //relation field
                                        split = field.Split('.');

                                        //add filter
                                        filter.AddFilter(
                                            new FieldHelper(split[0], split[1]),
                                            FilterQuery.ParseOperator(@operator),
                                            value
                                        );
                                    }
                                    else
                                    {
                                        //add filter
                                        filter.AddFilter(
                                            (Field)Enum.Parse(typeof(Field), field),
                                            FilterQuery.ParseOperator(@operator),
                                            value
                                        );
                                    }

                                    ++j;
                                }
                            }

                            //add second level filter
                            Filter.AddFilter(filter);
                        }
                    }
                    else
                    {
                        //first level filter
                        @operator = request["filter[filters][" + i + "][operator]"];
                        value = request["filter[filters][" + i + "][value]"];
                        match = Regex.Match(field, @"^([\w]+)[.]([\w]+)");

                        if (match.Success)
                        {
                            //relation field
                            split = field.Split('.');

                            //add filter
                            Filter.AddFilter(
                                new FieldHelper(split[0], split[1]),
                                FilterQuery.ParseOperator(@operator),
                                value
                            );
                        }
                        else
                        {
                            //add filter
                            Filter.AddFilter(
                                (Field)Enum.Parse(typeof(Field), field),
                                FilterQuery.ParseOperator(@operator),
                                value
                            );
                        }
                    }

                    if (!stop1)
                        ++i;
                }
            }
        }
    }
}