﻿using System.Security.Principal;

namespace WebUI.Infrastructure.Abstract
{
    public interface IProvidePrincipal
    {
        IPrincipal CreatePrincipal(string username, string password);
    }
}