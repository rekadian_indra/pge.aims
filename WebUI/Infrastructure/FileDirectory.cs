﻿using Common.Enums;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace WebUI.Infrastructure
{
    public static class FileDirectory
    {
        private const string DIRECTORY_ERR_FORMAT = "Can not create directory {0}";

        private static readonly string[] ImageExtensions = new[] 
        {
            ".jpg",
            ".jpeg",
            ".png",
            ".bmp",
            ".gif"
        };

        private static readonly string[] DocumentExtensions = new[]
        {
            ".txt",
            ".pdf",
            ".doc",
            ".docx",
            ".xls",
            ".xlsx",
            ".ppt",
            ".pptx"
        };

        private static readonly string[] VideoExtensions = new[]
        {
            ".mp4"
        };

        private static HttpServerUtility Server
        {
            get
            {
                if (HttpContext.Current == null)
                    throw new ArgumentNullException();

                return HttpContext.Current.Server;
            }
        }

        public static string Uploads
        {
            get
            {
                string path = "~/Uploads/";
                string serverPath = Server.MapPath(path);

                if (!Directory.Exists(serverPath))
                {
                    try
                    {
                        DirectoryInfo info = Directory.CreateDirectory(serverPath);

                        if (!info.Exists)
                            throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                    catch
                    {
                        throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                }

                return path;
            }
        }

        public static string Images
        {
            get
            {
                string path = $"{Uploads}Images/";
                string serverPath = Server.MapPath(path);

                if (!Directory.Exists(serverPath))
                {
                    try
                    {
                        DirectoryInfo info = Directory.CreateDirectory(serverPath);

                        if (!info.Exists)
                            throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                    catch
                    {
                        throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                }

                return path;
            }
        }

        public static string Documents
        {
            get
            {
                string path = $"{Uploads}Documents/";
                string serverPath = Server.MapPath(path);

                if (!Directory.Exists(serverPath))
                {
                    try
                    {
                        DirectoryInfo info = Directory.CreateDirectory(serverPath);

                        if (!info.Exists)
                            throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                    catch
                    {
                        throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                }

                return path;
            }
        }

        public static string Videos
        {
            get
            {
                string path = $"{Uploads}Videos/";
                string serverPath = Server.MapPath(path);

                if (!Directory.Exists(serverPath))
                {
                    try
                    {
                        DirectoryInfo info = Directory.CreateDirectory(serverPath);

                        if (!info.Exists)
                            throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                    catch
                    {
                        throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                }

                return path;
            }
        }

        public static string Others
        {
            get
            {
                string path = $"{Uploads}Others/";
                string serverPath = Server.MapPath(path);

                if (!Directory.Exists(serverPath))
                {
                    try
                    {
                        DirectoryInfo info = Directory.CreateDirectory(serverPath);

                        if (!info.Exists)
                            throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                    catch
                    {
                        throw new IOException(string.Format(DIRECTORY_ERR_FORMAT, serverPath));
                    }
                }

                return path;
            }
        }

        public static string GetDirectory(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            if (ImageExtensions.Contains(extension))
            {
                return Images;
            }

            if (DocumentExtensions.Contains(extension))
            {
                return Documents;
            }

            if (VideoExtensions.Contains(extension))
            {
                return Videos;
            }

            return Others;
        }

        /// <summary>
        /// format filepath: ~/.../.../.../sample.ext
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns>sample.ext</returns>
        public static string GetFileNameFromFilePath(string fullPath)
        {
            string filename = "";
            string[] split = fullPath.Split('/');
            //split = split.Last().Split('.');
            //filename = split.FirstOrDefault();
            filename = split.Last();

            return filename;
        }

        /// <summary>
        /// Get file name from unique file name
        /// </summary>
        /// <param name="uniqueName">20180606_075032_2085992_getting-started.pdf</param>
        /// <returns>getting-started.pdf</returns>
        public static string GetFileNameFromUniqueFileName(string uniqueName)
        {
            return uniqueName.Substring(24);
        }

        public static string GetUniqueFileName(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            string name = Path.GetFileNameWithoutExtension(fileName);

            return GetUniqueFileName(name, ext);
        }

        public static string GetUniqueFileName(string fileName, string extension)
        {
            const string format = "_{0}";

            string timeSpan = DateTime.UtcNow.ToString(DisplayFormat.TimeSpanTimeFormat);
            int random = new Random().Next(1000000, 10000000);
            StringBuilder sb = new StringBuilder();

            sb.Append(timeSpan)
              .AppendFormat(format, random)
              .AppendFormat(format, fileName)
              .Append(extension);

            return sb.ToString();
        }

        public static FileType GetFileType(string fileName)
        {
            FileType fileType;
            string ext = Path.GetExtension(fileName);

            switch (ext)
            {
                case ".pdf":
                    fileType = FileType.PDF;

                    break;
                case ".doc":
                case ".docx":
                    fileType = FileType.DOC;

                    break;
                case ".xls":
                case ".xlsx":
                    fileType = FileType.XLS;

                    break;
                case ".ppt":
                case ".pptx":
                    fileType = FileType.PPT;

                    break;
                case ".jpg":
                case ".jpeg":
                    fileType = FileType.JPG;

                    break;
                case ".png":
                    fileType = FileType.PNG;

                    break;
                case ".bmp":
                    fileType = FileType.BMP;

                    break;
                case ".gif":
                    fileType = FileType.GIF;

                    break;
                default:
                    fileType = FileType.OTHERS;

                    break;
            }

            return fileType;
        }
    }
}
