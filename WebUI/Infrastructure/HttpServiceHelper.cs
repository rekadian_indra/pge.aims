﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace WebUI.Infrastructure
{
    public class HttpServiceHelper
    {
        public string HttpGet(string url)
        {
            HttpWebRequest request = CreateRequest(url, HTTPMethod.GET.ToString());

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            string responseText = ConvertResponseToString(response);

            response.Close();

            return responseText;
        }

        public byte[] HttpGet(string url, int id)
        {
            HttpWebRequest request = CreateRequest(url, HTTPMethod.GET.ToString());

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            byte[] responseByteArray = ConvertResponseToByteArray(response);

            response.Close();

            return responseByteArray;
        }

        public string HttpPost(string url, string param)
        {
            HttpWebRequest request = CreateRequest(url, HTTPMethod.POST.ToString());
            WriteParamToRequest(param, ref request);

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            string responseText = ConvertResponseToString(response);

            response.Close();

            return responseText;
        }

        public string HttpPut(string url, string param)
        {
            HttpWebRequest request = CreateRequest(url, HTTPMethod.PUT.ToString());
            WriteParamToRequest(param, ref request);

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            string responseText = ConvertResponseToString(response);

            response.Close();

            return responseText;
        }

        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, 
            System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        #region private function

        private HttpWebRequest CreateRequest(string url, string method)
        {
            //ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            HttpWebRequest request = WebRequest.Create(url)
                                 as HttpWebRequest;
            request.Method = method;
            request.ContentType = "application/json";
            //NetworkCredential nc = new NetworkCredential("admin", "thredioadm");
            //request.Credentials = nc;

            return request;
        }

        private string ConvertResponseToString(HttpWebResponse response)
        {
            string responseText = null;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            responseText = reader.ReadToEnd();

            return responseText;
        }

        private byte[] ConvertResponseToByteArray(HttpWebResponse response)
        {
            MemoryStream ms = new MemoryStream();
            response.GetResponseStream().CopyTo(ms);
            byte[] responseByteArray = ms.ToArray();

            return responseByteArray;
        }

        private void WriteParamToRequest(string param, ref HttpWebRequest request)
        {
            byte[] formData = Encoding.UTF8.GetBytes(param.ToString());

            request.ContentLength = formData.Length;
            // Send the request:
            using (Stream post = request.GetRequestStream())
            {
                post.Write(formData, 0, formData.Length);
            }
        }

        private enum HTTPMethod
        {
            GET, // Select
            POST, // Insert
            PUT, // Update
            DELETE // Delete
        }

        public T ConvertJsonTextToObject<T>(string jsonText)
        {
            JavaScriptSerializer javasciptSerializer = new JavaScriptSerializer();
            javasciptSerializer.MaxJsonLength = Int32.MaxValue;
            T obj = javasciptSerializer.Deserialize<T>(jsonText);
            return obj;
        }

        public string ConvertObjectToJsonString(object obj)
        {
            JavaScriptSerializer javasciptSerializer = new JavaScriptSerializer();
            javasciptSerializer.MaxJsonLength = Int32.MaxValue;
            string param = javasciptSerializer.Serialize(obj);
            return param;
        }

        #endregion
    }
}