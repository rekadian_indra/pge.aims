﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.Infrastructure.Validation
{
    public class PositiveDoubleAttribute : ValidationAttribute
    {
        public PositiveDoubleAttribute(params string[] propertyNames)
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                if (((double)value) < 0)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
            }
            return null;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(Resources.AppGlobalError.PositiveInteger, name);
        }
    }
}