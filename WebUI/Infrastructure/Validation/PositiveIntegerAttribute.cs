﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.Infrastructure.Validation
{
    public class PositiveIntegerAttribute : ValidationAttribute
    {
        public PositiveIntegerAttribute(params string[] propertyNames)
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                if (((int)value) < 0)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
            }
            return null;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(Resources.AppGlobalError.PositiveInteger, name);
        }
    }
}