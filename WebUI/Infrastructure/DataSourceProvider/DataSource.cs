﻿namespace WebUI.Infrastructure
{
    public static class DataSource
    {
        public static class DataType
        {
            public const string Json = "json";
        }

        public static class ContentType
        {
            public const string ApplicationJson = "application/json; charset=UTF-8";
            public const string ApplicationUrlEncoded = "application/x-www-form-urlencoded; charset=UTF-8"; // default jquery ajax
        }

        public static class Type
        {
            public const string Post = "POST";
            public const string Get = "GET";
        }

        public static class Operation
        {
            public const string Read = "read";
            public const string Create = "create";
            public const string Update = "update";
            public const string Destroy = "destroy";
        }

        public static class Schema
        {
            public static class Property
            {
                public const string Data = "data";
                public const string Total = "total";
            }

            public static class Type
            {
                public const string String = "string";
                public const string Number = "number";
                public const string Boolean = "boolean";
                public const string Date = "date";
            }
        }
    }
}