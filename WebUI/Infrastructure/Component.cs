﻿namespace WebUI.Infrastructure
{
    public static class Component
    {
        public const string OptionLabel = "Pilih...";
        public const string AreaOptionLabel = "Semua Area";

        public static class Grid
        {
            public static class Command
            {
                public const string Edit = "Edit";
                public const string Delete = "Hapus";
                public const string Role = "Assign Role";
            }
        }
    }
}