﻿namespace WebUI.Infrastructure
{
    public static class AreaSite
    {
        public const string UserManagement = "UserManagement";
        public const string DataMaster = "DataMaster";
    }
}