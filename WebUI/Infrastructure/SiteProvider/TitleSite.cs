﻿namespace WebUI.Infrastructure
{
    public static class TitleSite
    {
        //default title
        public const string Dashboard = "Dashboard";
        public const string UserManagement = "User Management";
        public const string Create = "Add";
        public const string Edit = "Edit";
        public const string Breadcrumb = "Breadcrumb";
        public const string User = "User";
        public const string Role = "Role";

        //app title
        public const string Asset = "Fixed Asset";
        public const string AUC = "AUC";
        public const string Actual = "Realisasi";
        public const string Mapping = "Mapping AUC to FA";
        public const string MappingFA = "Mapping FA";
        public const string MappingAUC = "Mapping AUC";
        public const string Report = "Report";

        public const string Approval = "Approval";

        public const string TCR = "TCR";
        public const string Geomapping = "Geomapping";
        public const string PhysicalCheck = "Physical Check";

        public const string Detail = "Detail";
        public const string MDM = "MDM";
        public const string CompletedMDM = "Set Completed MDM";

        public const string SPC = "SPC";
        public const string SPCRequest = "SPC Request";
    }
}