﻿namespace WebUI.Infrastructure
{
    public static class KeySite
    {
        //default key
        public const string Home = "Home";
        public const string Dashboard = "Dashboard";
        public const string DashboardUserManagement = "DashboardUserManagement";

        //app key
        //public const string IndexCrew = "IndexCrew";
        //public const string CreateCrew = "CreateCrew";
        //public const string EditCrew = "EditCrew";

        //public const string IndexWhitelist = "IndexWhitelist";
        //public const string CreateWhitelist = "CreateWhitelist";
        //public const string EditWhitelist = "EditWhitelist";


        public const string IndexAsset = "IndexAsset";
        public const string DetailAsset = "DetailAsset";
        public const string EditAsset = "EditAsset";

        public const string IndexAUC = "IndexAUC";
        public const string EditAUC = "EditAUC";

        public const string IndexActual = "IndexActual";
        public const string EditActual = "EditActual";

        public const string IndexMapping = "IndexMapping";
        public const string CreateMapping = "CreateMapping";
        public const string EditMapping = "EditMapping";

        public const string PageMappingFA = "PageMappingFA";
        public const string CreateFA = "CreateFA";
        public const string CreateAUC = "CreateAUC";
        public const string EditFA = "EditFA";

        public const string Approval = "Approval";
        public const string TCRIndex = "TCRIndex";
        public const string CreateTCR = "CreateTCR";
        public const string EditTCR = "EditTCR";

        public const string GeomapingIndex = "geomapingIndex";

        public const string PhysicalCheckIndex = "PhysicalCheckIndex";
        public const string CreatePhysicalCheck = "CreatePhysicalCheck";
        public const string EditPhysicalCheck = "EditPhysicalCheck";

        public const string MDMIndex = "MDMIndex";
        public const string CreateMDM = "CreateMDM";
        public const string EditMDM = "EditMDM";
        public const string CompletedMDM = "Set Completed MDM";

        public const string SPCIndex = "SPCIndex";
        public const string CreateSPC = "CreateSPC";
        public const string EditSPC = "EditSPC";

        public const string IndexUser = "IndexUser";
        public const string IndexRole = "IndexRole";
        public const string Report = "Report";

    }
}