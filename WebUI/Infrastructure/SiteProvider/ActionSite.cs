﻿namespace WebUI.Infrastructure
{
    public static class ActionSite
    {
        //reuseable action
        public const string Index = "Index";
        public const string Login = "Login";
        public const string Create = "Create";
        public const string Edit = "Edit";
        public const string Delete = "Delete";
        public const string Binding = "Binding";
        public const string BindingOption = "BindingOption";
        public const string Remove = "Remove";
        public const string Download = "Download";

        //app action
        public const string DownloadSAP = "DownloadSAP";
        public const string UploadExcel = "UploadExcel";
        public const string DownloadExcel = "DownloadExcel";

        //Mapping
        public const string GetValueActualCost = "GetValueActualCost";
        public const string MappingFA = "MappingFA";
        public const string MappingAUCCreate = "MappingAUCCreate";
        public const string CreateFA = "CreateFA";
        public const string EditFA = "EditFA";
        public const string BindingMappingAsset = "BindingMappingAsset";
        public const string BindingMappingAssetActualCost = "BindingMappingAssetActualCost";
        public const string BindingMappingActualCost = "BindingMappingActualCost";
        public const string BindingMappingActual = "BindingMappingActual";
        public const string AddActual = "AddActual";
        public const string EditActual = "EditActual";
        public const string DeleteMappingActualCost = "DeleteMappingActualCost";
        public const string SendToFinance = "SendToFinance";

        //Approval
        public const string Approve = "Approve";
        public const string ApproveTCR = "ApproveTCR";
        public const string Revise = "Revise";

        //TCR
        public const string BindingAssetTCR = "BindingAssetTCR";

        //asset
        public const string BindingAsset = "BindingAsset";
        public const string BindingOngoing = "BindingOngoing";
        public const string Detail = "Detail";
        public const string GenerateQRCode = "GenerateQRCode";
        public const string EditMobile = "EditMobile";

        //MDM
        public const string AddAsset = "AddAsset";
        public const string SetCompletedMDM = "SetCompletedMDM";

        //user management
        public const string BindingUsers = "BindingUsers";

        //default action
        public const string AntiForgery = "AntiForgery";
        public const string Http404 = "Http404";
        public const string Http403 = "Http403";
        public const string Http401 = "Http401";
        public const string AssignRoles = "AssignRoles";
        public const string AssignModules = "AssignModules";
        public const string RevokeRoles = "RevokeRoles";
        public const string RevokeModules = "RevokeModules";
        public const string BindingRoles = "BindingRoles";
        public const string BindingModules = "BindingModules";
        public const string UserNameExists = "UserNameExists";
        public const string EmailExists = "EmailExists";
        public const string ModuleNameExists = "ModuleNameExists";
        public const string RoleNameExists = "RoleNameExists";

        //physical Check
        public const string BindingLastDate = "BindingLastDate";
        public const string Report = "Report";

    }
}