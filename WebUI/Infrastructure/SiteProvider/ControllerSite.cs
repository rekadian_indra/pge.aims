﻿namespace WebUI.Infrastructure
{
    public static class ControllerSite
    {
        //reuseable controller
        public const string Home = "Home";
        public const string Dashboard = "Dashboard";

        //default controller
        public const string Authentication = "Authentication";
        public const string User = "User";
        public const string Role = "Role";
        public const string Module = "Module";
        public const string FileManagement = "FileManagement";

        //app controller
        public const string Asset = "Asset";
        public const string AUC = "AUC";
        public const string Actual = "Actual";
        public const string Mapping = "Mapping";
        public const string Approval = "Approval";
        public const string TCR = "TCR";

        public const string Geomapping = "Geomapping";

        public const string PhysicalCheck = "PhysicalCheck";
        public const string MDM = "MDM";

        public const string SPC = "SPC";

        public const string Membership = "Membership";
    }
}