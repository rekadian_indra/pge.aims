﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebUI.Infrastructure
{
    public class AuthorizeUserAttribute : AuthorizeAttribute
    {
        private const string ERR_MSG = "Not valid type. Type must be {0}.";

        public object ModuleName { get; set; }
        public new object[] Roles { get; set; }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            //kamus
            UserModule? moduleName = null;
            List<UserRole?> roles = new List<UserRole?>();

            //algoritma
            if (ModuleName != null)
            {
                if (ModuleName.GetType() != typeof(UserModule))
                    throw new InvalidCastException(string.Format(ERR_MSG, typeof(UserModule).Name));
                moduleName = ((UserModule)ModuleName);
            }

            if (Roles != null && Roles.Any())
            {
                foreach (object r in Roles)
                {
                    if (r.GetType() != typeof(UserRole))
                        throw new InvalidCastException(string.Format(ERR_MSG, typeof(UserRole).Name));
                    roles.Add(((UserRole)r));
                }
            }

            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                bool hasAccess = true;
                Principal user = filterContext.HttpContext.User as Principal;

                if (moduleName != null)
                    hasAccess = user.HasAccess(moduleName);
                else if (roles.Any())
                    hasAccess = user.HasAccess(roles.ToArray());
                else
                    hasAccess = false;

                if (!hasAccess)
                {
                    filterContext.Result = new RedirectToRouteResult(new
                        RouteValueDictionary(new
                        {
                            action = "Http401",
                            controller = "Error",
                            area = string.Empty,
                            url = filterContext.HttpContext.Request.Url.OriginalString
                        }));
                }
            }
        }
    }
}