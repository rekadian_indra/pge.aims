﻿namespace WebUI.Infrastructure
{
    public static class GlobalErrorField
    {
        public const string DateGreater = "DateGreater";
        public const string FieldMustBeDate = "FieldMustBeDate";
        public const string FieldMustBeNumeric = "FieldMustBeNumeric";
        public const string PositiveInteger = "PositiveInteger";
        public const string PropertyNotExist = "PropertyNotExist";
        public const string PropertyValueInvalid = "PropertyValueInvalid";
        public const string PropertyValueRequired = "PropertyValueRequired";
        public const string Range = "Range";
        public const string Required = "Required";
        public const string MaxStringLength = "MaxStringLength";
        public const string Symbol = "Symbol";
        public const string Unique = "Unique";
        public const string UploadExcelFailed = "UploadExcelFailed";
        public const string Compare = "Compare";
        public const string MinStringLength = "MinStringLength";
        public const string EmailType = "EmailType";
    }
}
