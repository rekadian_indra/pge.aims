﻿namespace WebUI.Models
{
    public class ErrorPageViewModel
    {
        public string RequestedUrl { get; set; }
        public string ReferrerUrl { get; set; }
    }
}