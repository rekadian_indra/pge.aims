﻿using System.IO;

namespace WebUI.Models
{
    public class DownloadParameterStub
    {
        public MemoryStream MemoryStream { get; set; }
        public string ContentType { get; set; }
        public string FileDownloadName { get; set; }
        
    }
}