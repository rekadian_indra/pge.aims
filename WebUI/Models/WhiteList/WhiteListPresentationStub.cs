﻿using Business.Entities.Views;
using Common.Enums;
using System;

namespace WebUI.Models
{
    public class WhitelistPresentationStub : BasePresentationStub<ViewWhitelist, WhitelistPresentationStub>
    {
        public int WhitelistId { get; set; }
        public string CrewId { get; set; }
        public int AirportId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CrewName { get; set; }
        public string Status { get; set; }
        public string AirportName { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }

        public WhitelistPresentationStub() : base()
        {
        }

        public WhitelistPresentationStub(ViewWhitelist dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            CrewStatus status = (CrewStatus)Enum.Parse(typeof(CrewStatus), Status);
            Status = status.ToDescription();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}