﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class CrewFormStub : BaseFormStub<Crew, CrewFormStub>
    {
        [DisplayName("Barcode")]
        public string CrewId { get; set; }

        [DisplayName("Nama")]
        [StringLength(100, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [RegularExpression(@"([-a-zA-Z.0-9\s]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string CrewName { get; set; }

        public string Status { get; set; }

        [DisplayName("Tanggal Registrasi")]
        public DateTime RegisterDate { get; set; }

        [DisplayName("Maskapai")]
        public int CompanyId { get; set; }

        public List<SelectListItem> StatusOption { get; set; }

        public CrewFormStub() : base()
        {
        }

        public CrewFormStub(int index) : this()
        {
            CrewId = GenerateBarcode(index);
            RegisterDate = DateTime.Now.Date;
        }

        public CrewFormStub(Crew dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            if (dbObject.Companies.Any())
                CompanyId = dbObject.Companies.FirstOrDefault().CompanyId;
        }

        public override void MapDbObject(Crew dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here

        }

        public void MapDbObject(Crew dbObject, Company company)
        {
            MapDbObject(dbObject);

            dbObject.Companies.Clear();
            dbObject.Companies.Add(company);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            StatusOption = DisplayFormat.EnumToSelectList<CrewStatus>();
        }

        private string GenerateBarcode(int index)
        {
            string result;
            string today = DateTime.Now.Date.ToString(DisplayFormat.ShortableDateFormat);

            //increment index
            ++index;

            if (index < 10)
                result = $"{today}-00{index}";
            else if (index >= 10 && index < 100)
                result = $"{today}-0{index}";
            else
                result = $"{today}-{index}";

            return result;
        }
    }
}