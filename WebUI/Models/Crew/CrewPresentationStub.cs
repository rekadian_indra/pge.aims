﻿using Business.Entities.Views;
using Common.Enums;
using System;

namespace WebUI.Models
{
    public class CrewPresentationStub : BasePresentationStub<ViewCrew, CrewPresentationStub>
    {
        public string CrewId { get; set; }
        public string CrewName { get; set; }
        public string Status { get; set; }
        public DateTime RegisterDate { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        
        public CrewPresentationStub() : base()
        {
        }

        public CrewPresentationStub(ViewCrew dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            CrewStatus status = (CrewStatus)Enum.Parse(typeof(CrewStatus), Status);
            Status = status.ToDescription();
        }

        public CrewPresentationStub(ViewCrew dbObject, DateTime now) : this(dbObject)
        {
            //This example constructor with many parameter.
            //This called by MapList in Business.Infrastructure.ListMapper via controller
            DateTime dateTimeNow = now;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}