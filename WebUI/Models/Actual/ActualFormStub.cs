﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class ActualFormStub : BaseFormStub<ActualCost, ActualFormStub>
    {
        public string ActualCostId { get; set; }

        [DisplayName("Document Number")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string DocNumber2 { get; set; }

        [DisplayName("Posting Date")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public DateTime? PostingDate { get; set; }

        [DisplayName("WBS Element")]
        public string WBSElement { get; set; }

        [DisplayName("SAP Number")]
        public string DocNumber { get; set; }        

        [DisplayName("Ref Document Number")]
        public string RefNumber { get; set; }

        [DisplayName("Offsetting Account Type")]
        public string OffsettingAccountType { get; set; }

        [DisplayName("Document Type")]
        public string DocType { get; set; }

        [DisplayName("Original Bus. Trans.")]
        public string OriginalBusTrans { get; set; }

        [DisplayName("Project Definition")]
        public string ProjectDefinition { get; set; }

        [DisplayName("CO Object Name")]
        public string CoObjectName { get; set; }

        [DisplayName("Cost Element")]
        public string CostElement { get; set; }

        [DisplayName("Cost Element Desc.")]
        public string CostElementDesc { get; set; }

        [DisplayName("Fiscal Year")]
        public short? FiscalYear { get; set; }

        [DisplayName("Doc Date")]
        public DateTime? DocDate { get; set; }        

        [DisplayName("Created On")]
        public DateTime? CreatedOn { get; set; }

        [DisplayName("Transaction Currency")]
        public string TransactionCurrency { get; set; }

        [DisplayName("Vbl. Value/TranCurr.")]
        public double? ValueTranCurr { get; set; }

        [DisplayName("Val/COArea Crcy")]
        public double? ValueCoArea { get; set; }

        [DisplayName("Val in Hard Curr")]
        public double? ValueHardCurr { get; set; }

        [DisplayName("Purchasing Document")]
        public string PurchasingDoc { get; set; }

        [DisplayName("Total Quantity")]
        public int? TotalQuantity { get; set; }

        [DisplayName("Item")]
        public string Item { get; set; }

        [DisplayName("Purchase Order Text")]
        public string PurchaseOrderText { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("User Name")]
        public string UserName { get; set; }

        [DisplayName("Period")]
        public string Period { get; set; }        

        public ActualFormStub() : base()
        {
        }

        public ActualFormStub(ActualCost dbItem) : this()
        {
            ObjectMapper.MapObject<ActualCost, ActualFormStub>(dbItem, this);
            ActualCostId = dbItem.ActualCostId.ToString();
        }

        public override void MapDbObject(ActualCost dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here
            dbObject.ActualCostId = string.IsNullOrEmpty(ActualCostId) ? dbObject.ActualCostId : ActualCostId.ToInteger();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            //StatusOption = DisplayFormat.EnumToSelectList<CrewStatus>();
        }

    }
}