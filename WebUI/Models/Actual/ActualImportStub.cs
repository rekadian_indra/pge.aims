﻿using Business.Entities;
using Common.Enums;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace WebUI.Models
{
    public class ActualImportStub : BaseImportStub
    {
        public List<ActualFormStub> ModelsFromExcel { get; set; }

        public ActualImportStub() { }

        public async void ParseFile(HttpPostedFileBase[] files)
        {
            // kamus            
            ErrParseExcel = new List<string>();
            ModelsFromExcel = new List<ActualFormStub>();
            XSSFWorkbook book = null;
            XSSFSheet sheet = null;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            List<Task> taskMapping = null;

            // algoritma
            if (files != null && files.Any())
            {
                if (files[0].ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
                    Path.GetExtension(files[0].FileName.ToLower()) == ".xlsx")
                {

                    try
                    {
                        FileName = files[0].FileName;
                        book = new XSSFWorkbook(files.First().InputStream);

                        sheet = (XSSFSheet)book.GetSheetAt(0);
                        Valid = CheckTemplate(sheet, Columns);

                        if (Valid && sheet.GetRow(1) != null)
                        {
                            int rowCount = sheet.LastRowNum;
                            taskMapping = new List<Task>();

                            for (int r = 1; r <= rowCount; r++)
                                taskMapping.Add(MappingObjectAsync(sheet, r));

                            await Task.WhenAll(taskMapping);
                        }
                        else
                        {
                            ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrParseExcel.Add(string.Format(MSG_ERROR, FileName));
                    }
                }
                else
                {
                    ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                }
            }
        }

        protected override Task MappingObjectAsync(XSSFSheet sheet, int row)
        {
            int errCount = 0;
            ActualFormStub dataRow = null;
            string code = null;

            if (sheet.GetRow(row) != null)
            {
                dataRow = new ActualFormStub();

                int valCount = sheet.GetRow(row).Cells.Where(m => m.CellType != CellType.Blank).Count();

                if (valCount > 0)
                {
                    foreach (var col in Columns)
                    {
                        switch (col.ColumnNum)
                        {
                            case 0:
                                dataRow.DocNumber = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 1:
                                dataRow.DocNumber2 = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 2:
                                dataRow.RefNumber = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 3:
                                dataRow.OffsettingAccountType = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 4:
                                dataRow.DocType = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 5:
                                dataRow.OriginalBusTrans = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 6:
                                dataRow.ProjectDefinition = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 7:
                                dataRow.WBSElement = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 8:
                                dataRow.CoObjectName = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 9:
                                dataRow.CostElement = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 10:
                                dataRow.CostElementDesc = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 11:
                                dataRow.FiscalYear = ParsingCell<short?>(sheet, col, row, ref errCount);
                                break;
                            case 12:
                                dataRow.DocDate = ParsingCell<DateTime?>(sheet, col, row, ref errCount);
                                break;
                            case 13:
                                dataRow.PostingDate = ParsingRequiredCell<DateTime?>(sheet, col, row, ref errCount);
                                break;
                            case 14:
                                dataRow.CreatedOn = ParsingCell<DateTime?>(sheet, col, row, ref errCount);
                                break;
                            case 15:
                                dataRow.TransactionCurrency = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 16:
                                dataRow.ValueTranCurr = ParsingCell<double?>(sheet, col, row, ref errCount);
                                break;
                            case 17:
                                dataRow.ValueCoArea = ParsingCell<double?>(sheet, col, row, ref errCount);
                                break;
                            case 18:
                                dataRow.ValueHardCurr = ParsingCell<double?>(sheet, col, row, ref errCount);
                                break;
                            case 19:
                                break;
                            case 20:
                                dataRow.PurchasingDoc = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 21:
                                dataRow.TotalQuantity = ParsingCell<int?>(sheet, col, row, ref errCount);
                                break;
                            case 22:
                                dataRow.Item = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 23:
                                dataRow.PurchaseOrderText = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 24:
                                dataRow.Name = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 25:
                                dataRow.UserName = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            case 26:
                                dataRow.Period = ParsingCell<string>(sheet, col, row, ref errCount);
                                break;
                            default:
                                break;
                        }

                        if (errCount == 0)
                            ModelsFromExcel.Add(dataRow);
                    }
              
                }
            }

            return Task.FromResult(0);
        }

        protected override List<ColumnHelper> GetColumns()
        {
            //Set columns to parse data
            //Remember column start at 0!
            return new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, MasterExcelColumn.DOC_NUMBER.ToDescription(), 50),
                ColumnHelper.SetColumn(1, MasterExcelColumn.DOC_NUMBER.ToDescription(), 50),
                ColumnHelper.SetColumn(2, MasterExcelColumn.REF_NUMBER.ToDescription(), 20),
                ColumnHelper.SetColumn(3, MasterExcelColumn.OFFSETTING_ACC.ToDescription(), 5),
                ColumnHelper.SetColumn(4, MasterExcelColumn.DOC_TYPE.ToDescription(), 50),
                ColumnHelper.SetColumn(5, MasterExcelColumn.ORIGINAL_TRANS.ToDescription(), 50),
                ColumnHelper.SetColumn(6, MasterExcelColumn.PROJECT_DEFINITION.ToDescription(), 50),
                ColumnHelper.SetColumn(7, MasterExcelColumn.WBS.ToDescription(), 50),
                ColumnHelper.SetColumn(8, MasterExcelColumn.CO_NAME.ToDescription(), 500),
                ColumnHelper.SetColumn(9, MasterExcelColumn.COST_ELEMENT.ToDescription(), 50),
                ColumnHelper.SetColumn(10, MasterExcelColumn.COST_ELEMENT_DESC.ToDescription(), 500),
                ColumnHelper.SetColumn(11, MasterExcelColumn.FISCAL_YEAR.ToDescription()),
                ColumnHelper.SetColumn(12, MasterExcelColumn.DOC_DATE.ToDescription()),
                ColumnHelper.SetColumn(13, MasterExcelColumn.POST_DATE.ToDescription()),
                ColumnHelper.SetColumn(14, MasterExcelColumn.CREATED_ON.ToDescription()),
                ColumnHelper.SetColumn(15, MasterExcelColumn.TRANSACTION_CURR.ToDescription(), 10),
                ColumnHelper.SetColumn(16, MasterExcelColumn.VAL_TRANSACTION.ToDescription()),
                ColumnHelper.SetColumn(17, MasterExcelColumn.CO_AREA.ToDescription()),
                ColumnHelper.SetColumn(18, MasterExcelColumn.VAL_HARD.ToDescription()),
                ColumnHelper.SetColumn(19, MasterExcelColumn.VAL_HARD2.ToDescription()),
                ColumnHelper.SetColumn(20, MasterExcelColumn.PURCHASING_DOC.ToDescription(), 50),
                ColumnHelper.SetColumn(21, MasterExcelColumn.TOTAL_QTY.ToDescription()),
                ColumnHelper.SetColumn(22, MasterExcelColumn.ITEM.ToDescription(), 50),
                ColumnHelper.SetColumn(23, MasterExcelColumn.PO_TEXT.ToDescription(), 500),
                ColumnHelper.SetColumn(24, MasterExcelColumn.NAME.ToDescription(), 500),
                ColumnHelper.SetColumn(25, MasterExcelColumn.USERNAME.ToDescription(), 50),
                ColumnHelper.SetColumn(26, MasterExcelColumn.PERIOD.ToDescription(), 50)
            };
        }
    }
}