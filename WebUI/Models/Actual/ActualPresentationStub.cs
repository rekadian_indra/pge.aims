﻿using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class ActualPresentationStub : BasePresentationStub<ActualCost, ActualPresentationStub>
    {
        public int ActualCostId { get; set; }
        public string WBSElement { get; set; }
        public string DocNumber { get; set; }
        public string DocNumber2 { get; set; }
        public string RefNumber { get; set; }
        public string OffsettingAccountType { get; set; }
        public string DocType { get; set; }
        public string CoObjectName { get; set; }
        public string CostElement { get; set; }
        public string CostElementDesc { get; set; }
        public short? FiscalYear { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string TransactionCurrency { get; set; }
        public double? ValueTranCurr { get; set; }
        public double? ValueCoArea { get; set; }
        public double? ValueHardCurr { get; set; }
        public string PurchasingDoc { get; set; }
        public int? TotalQuantity { get; set; }
        public string Item { get; set; }
        public string PurchaseOrderText { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Period { get; set; }

        public double MappingValueIdr { get; set; }
        public double Balance { get; set; }        
        public bool CanDeleted { get; set; }

        public ActualPresentationStub() : base()
        {
        }

        public ActualPresentationStub(ActualCost dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here            
        }

        public ActualPresentationStub(ViewActual dbObject)
        {
            ObjectMapper.MapObject<ViewActual, ActualPresentationStub>(dbObject, this);            
        }

        public ActualPresentationStub(ViewActual dbObject, List<MappingAssetActualCost> actualCosts)
        {
            ObjectMapper.MapObject<ViewActual, ActualPresentationStub>(dbObject, this);
            CanDeleted = actualCosts.Any(n => n.ActualCostId == dbObject.ActualCostId) ? false : true;
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}