﻿namespace WebUI.Models
{
    /// <summary>
    /// model untuk mengembalikan response ke client side via ajax
    /// </summary>
    public class ResponseModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }

        public ResponseModel() { }

        public ResponseModel(bool success)
        {
            Success = success;
        }

        public void SetFail(string message)
        {
            Success = false;
            Message = message;
        }

        public class FileResponseModel
        {
            public string filepath { get; set; }
            public string filename { get; set; }
            public string absolutepath { get; set; }
            public string duration { get; set; }
        }
    }
}