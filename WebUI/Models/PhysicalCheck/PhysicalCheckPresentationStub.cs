﻿using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class PhysicalCheckPresentationStub : BasePresentationStub<PhysicalCheck, PhysicalCheckPresentationStub>
    {
        public int PhysicalCheckId { get; set; }
        public System.DateTime PhysicalCheckDate { get; set; }
        public int AssetId { get; set; }
        public string Condition { get; set; }
        public string CheckedBy { get; set; }

        public string Description { get; set; }
        public string AssetNumber { get; set; }
        public string Plant { get; set; }

        public PhysicalCheckPresentationStub() : base()
        {
        }

        public PhysicalCheckPresentationStub(PhysicalCheck dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            Description = dbObject.Asset.Description;
            AssetNumber = dbObject.Asset.AssetNumber;
            Common.Enums.Condition condition = (Condition)Enum.Parse(typeof(Condition), dbObject.Condition);
            Condition = condition.ToDescription();
        }

        public PhysicalCheckPresentationStub(ViewPhysicalCheck dbObject)
        {
            ObjectMapper.MapObject<ViewPhysicalCheck, PhysicalCheckPresentationStub>(dbObject, this);

            //TODO: Manual mapping object here
            Description = dbObject.Asset.Description;
            AssetNumber = dbObject.Asset.AssetNumber;
            Common.Enums.Condition condition = (Condition)Enum.Parse(typeof(Condition), dbObject.Condition);
            Condition = condition.ToDescription();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}