﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class ImagePresentationStub : BaseFormStub<PhysicalCheckAsset, ImagePresentationStub>
    {
        private const string DEFAULT_PHOTO = "~/Content/theme/noimage.png";

        public int PhysicalCheckId { get; set; }
        public int ImageId { get; set; }
        public string Image { get; set; }

        public string Url { get; set; }

        public ImagePresentationStub()
        {

        }

        public ImagePresentationStub(PhysicalCheckAsset dbItem)
        {
            ObjectMapper.MapObject<PhysicalCheckAsset, ImagePresentationStub>(dbItem, this);

            Url = VirtualPathUtility.ToAbsolute(Image);
           
            if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(Image)) == false || Image == null)
            {
                Url = VirtualPathUtility.ToAbsolute(DEFAULT_PHOTO);
            }
        }

        public override void MapDbObject(PhysicalCheckAsset dbObject)
        {
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here

        }
        protected override void Init()
        {
            
        }
    }
}