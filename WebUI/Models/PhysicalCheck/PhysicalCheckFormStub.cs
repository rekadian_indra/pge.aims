﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class PhysicalCheckFormStub : BaseFormStub<PhysicalCheck, PhysicalCheckFormStub>
    {

        public int PhysicalCheckId { get; set; }
        [DisplayName("Tgl. Physical Check")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public DateTime PhysicalCheckDate { get; set; }

        [DisplayName("Asset")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int AssetId { get; set; }

        public string AssetName { get; set; }

        [DisplayName("Kondisi")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Condition { get; set; }

        public string CheckedBy { get; set; }
        public bool? IsDeleted { get; set; }

        public List<SelectListItem> OptionsConditions { get; set; }

        [DisplayName("Dokumentasi")]
        public string ImageGallery { get; set; }
        public string TempImageGallery { get; set; }

        [DisplayName("Terakhir Physical Check")]
        public string LastChecked { get; set; }

        public PhysicalCheckFormStub() : base()
        {

        }

        public PhysicalCheckFormStub(Asset asset, PhysicalCheck lastCheck)
        {
            AssetId = asset.AssetId;
            AssetName = string.Format("{0}-{1}", asset.AssetNumber, asset.Description);
            PhysicalCheckDate = DateTime.Now;
            CheckedBy = User.UserName;

            LastChecked = lastCheck!= null ? string.Format("{0} - {1}", lastCheck.PhysicalCheckDate.ToString("dd MMM yyy HH:mm"), lastCheck.CheckedBy) : "-";
        }

        public PhysicalCheckFormStub(PhysicalCheck dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
        }

        public PhysicalCheckFormStub(PhysicalCheck dbObject, List<PhysicalCheckAsset> gallery)
        {
            //kamus
            List<ImagePresentationStub> galleries = new List<ImagePresentationStub>();

            ObjectMapper.MapObject<PhysicalCheck, PhysicalCheckFormStub>(dbObject, this);

            galleries = ListMapper.MapList<PhysicalCheckAsset, ImagePresentationStub>(gallery);
            ImageGallery = JsonConvert.SerializeObject(galleries);
        }

        public override void MapDbObject(PhysicalCheck dbObject)
        {
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
            dbObject.CheckedBy = User.UserName;

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            IsDeleted = false;
            OptionsConditions = DisplayFormat.EnumToSelectList<Condition>();
            Condition = Common.Enums.Condition.GOOD.ToString();
            PhysicalCheckDate = DateTime.Now;
        }
    }
}