﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class DummyFormStub
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [DisplayName("Dropdown Example")]
        public string Dropdown { get; set; }

        public List<SelectListItem> DropdownOptions { get; set; }

        public DummyFormStub()
        {
            FillDropdownOptions();
        }

        public DummyFormStub(DummyTable dbItem) : this()
        {
            ObjectMapper.MapObject<DummyTable, DummyFormStub>(dbItem, this);
        }

        public void MapDbObject(DummyTable dbObject)
        {
            ObjectMapper.MapObject<DummyFormStub, DummyTable>(this, dbObject);
        }

        public void FillDropdownOptions()
        {
            DropdownOptions = new List<SelectListItem>();

            foreach (Month item in EnumExtension.EnumToList<Month>().ToList())
            {
                DropdownOptions.Add(new SelectListItem { Value = item.ToString(), Text = EnumExtension.ToDescription(item) });
            }
        }
    }
}