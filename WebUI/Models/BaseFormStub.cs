﻿using Business.Infrastructure;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public abstract class BaseFormStub<TEntity, TModel>
        where TEntity : class
        where TModel : class
    {
        public LogActivityModel<TEntity> LogActivity { get; set; }

        protected HttpServerUtility Server
        {
            get
            {
                return HttpContext.Current.Server;
            }
        }

        protected Principal User
        {
            get
            {
                return HttpContext.Current.User as Principal;
            }
        }

        public BaseFormStub()
        {
            Init();
        }

        public BaseFormStub(TEntity dbObject) : this()
        {
            ObjectMapper.MapObject<TEntity, TModel>(dbObject, this);

            LogActivity = new LogActivityModel<TEntity>(dbObject);
        }

        public virtual void MapDbObject(TEntity dbObject)
        {
            ObjectMapper.MapObject<TModel, TEntity>(this, dbObject);

            if (LogActivity == null)
            {
                LogActivity = new LogActivityModel<TEntity>();
            }

            ObjectMapper.MapObject<LogActivityModel<TEntity>, TEntity>(LogActivity, dbObject);
        }

        //set the init object here to be called in all constructor
        protected abstract void Init();
    }
}