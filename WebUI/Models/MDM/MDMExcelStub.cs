﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class MDMExcelStub : BaseImportStub
    {
        public MDMExcelStub()
        {
        }

        public XSSFWorkbook SetData(FileStream fileStream, MDMRequest dbItem, List<ViewMappingAsset> assets, Principal user)
        {
            //kamus
            XSSFWorkbook workbook = new XSSFWorkbook();

            //algo
            workbook = new XSSFWorkbook(fileStream);
            XSSFSheet sheet; XSSFRow row; XSSFCell cell; XSSFCellStyle style;
            int iterator; int no; int cellIterator;
            double sumOriginalValue = 0;
            MDMAsset temp;
            List<int> hiddenCol = new List<int>() { 11, 12, 13, 14, 15, 20, 21, 22, 25, 26, 27, 28, 29, 30,31, 32, 33,
            34, 40, 41, 44, 45, 46};

            sheet = (XSSFSheet)workbook.GetSheetAt(0);

            sheet.GetRow(2).GetCell(4).SetCellValue(dbItem.RequestTitle);
            sheet.GetRow(3).GetCell(4).SetCellValue(user.UserName);
            sheet.GetRow(4).GetCell(4).SetCellValue(DateTime.Now.ToString(DisplayFormat.CompactDateFormat));
            sheet.GetRow(9).GetCell(4).SetCellValue(dbItem.LegalRequestor);

            foreach (int col in hiddenCol) {
                sheet.SetColumnHidden(col, true);
            }

            iterator = 16; no = 1;
            foreach(ViewMappingAsset a in assets)
            {
                temp = dbItem.MDMAssets.Where(x => x.AssetId == a.AssetId).FirstOrDefault();

                cellIterator = 0;

                row = (XSSFRow)sheet.CreateRow(iterator);
                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(no);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue("Create");
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(temp.AssetClass);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(a.AssetNumber);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(int.Parse(a.SubNumber));
                cell.SetCellType(CellType.Numeric);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(2022);
                cell.SetCellType(CellType.Numeric);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(temp.PostCap);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(a.Description);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(temp.Description2);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(temp.SerialNumber);
                sheet.AutoSizeColumn(cellIterator);

                //capitalize on
                cellIterator += 6;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(a.EndDate?.ToString(DisplayFormat.ShortDateFormatStrip));
                sheet.AutoSizeColumn(cellIterator);

                //cost Center
                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(a.CostCenter);
                sheet.AutoSizeColumn(cellIterator);

                //plant
                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(a.Plant);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(a.Location);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator += 4;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(temp.EvaluationGroup1);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(temp.AssetCondition);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator += 11;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(temp.OriginalAsset);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(a.EndDate?.ToString(DisplayFormat.ShortDateFormatStrip));
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(a.EndDate.Value.Year);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                cell.SetCellValue(a.ValueIdr);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetDataFormat(HSSFDataFormat.GetBuiltinFormat("#,##0"));
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellType(CellType.Numeric);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(temp.BookDepreKey);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator += 3;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(temp.UsefulLife01);
                sheet.AutoSizeColumn(cellIterator);

                cellIterator++;
                cell = (XSSFCell)row.CreateCell(cellIterator);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.SetFillForegroundColor(new XSSFColor(new byte[] { 248, 244, 186 }));
                style.FillPattern = FillPattern.SolidForeground;
                cell.CellStyle = style;
                cell.SetCellValue(temp.UsefulLife03);
                sheet.AutoSizeColumn(cellIterator);

                no++;
                iterator++;
                sumOriginalValue += a.ValueIdr;
            }

            iterator += 1;

            row = (XSSFRow)sheet.CreateRow(iterator);
            cell = (XSSFCell)row.CreateCell(37);
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            IFont fontBold = workbook.CreateFont();
            fontBold.Boldweight = (short)FontBoldWeight.Bold;
            style.SetFont(fontBold);
            cell.CellStyle = style;
            cell.SetCellValue("Total");

            cell = (XSSFCell)row.CreateCell(38);
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetDataFormat(HSSFDataFormat.GetBuiltinFormat("#,##0"));
            style.SetFont(fontBold);
            cell.CellStyle = style;
            cell.SetCellValue(sumOriginalValue);

            return workbook;
        }


        private static string StripHTML(string input)
        {
            if (input == null)
                return "";

            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public class TimeSpanHelper
        {
            public static List<string> GenerateTimeSpan()
            {
                List<string> options = new List<string>();
                DateTime start = DateTime.ParseExact("00:00", "HH:mm", null);
                DateTime end = DateTime.ParseExact("23:59", "HH:mm", null);
                TimeSpan interval = new TimeSpan(0, 1, 0); //interval every one minute

                while (start <= end)
                {
                    options.Add(start.ToString(DisplayFormat.SqlShortTimeFormat));
                    start = start.Add(interval);
                }

                return options;
            }
        }
    }
}