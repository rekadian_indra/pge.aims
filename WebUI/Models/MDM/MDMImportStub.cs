﻿using Business.Entities;
using Common.Enums;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace WebUI.Models
{
    public class MDMImportStub : BaseImportStub
    {
        public List<MDMAssetFinishFormStub> ModelsFromExcel { get; set; }        

        public MDMImportStub() { }

        public async void ParseFile(HttpPostedFileBase[] files)
        {
            // kamus            
            ErrParseExcel = new List<string>();
            XSSFWorkbook book = null;
            XSSFSheet sheet = null;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            List<Task> taskMapping = null;

            // algoritma
            if (files != null && files.Any())
            {
                if (files[0].ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
                    Path.GetExtension(files[0].FileName.ToLower()) == ".xlsx")
                {

                    try
                    {
                        FileName = files[0].FileName;
                        book = new XSSFWorkbook(files.First().InputStream);

                        sheet = (XSSFSheet)book.GetSheetAt(0);

                        Valid = sheet != null ? CheckTemplate(sheet, Columns) : false;

                        if (Valid && sheet.GetRow(15) != null)
                        {
                            int rowCount = sheet.LastRowNum;
                            taskMapping = new List<Task>();

                            for (int r = 16; r <= rowCount - 2; r++)
                                taskMapping.Add(MappingObjectAsync(sheet, r));

                            await Task.WhenAll(taskMapping);
                        }
                        else
                        {
                            ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                        }
                    }
                    catch(Exception e)
                    {
                        ErrParseExcel.Add(string.Format(MSG_ERROR, FileName));
                    }
                }
                else
                {
                    ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                }
            }
        }

        protected override Task MappingObjectAsync(XSSFSheet sheet, int row)
        {
            int errCount = 0;
            MDMAssetFinishFormStub dataRow = null;
            string AssetDesc;
            ColumnHelper AssetCol = ColumnHelper.SetColumn(8, "Description 1*", 500);

            if (sheet.GetRow(row) != null)
            {
                dataRow = new MDMAssetFinishFormStub();

                AssetDesc = ParsingRequiredCell<string>(sheet, AssetCol, row, ref errCount);
                AssetDesc = string.IsNullOrEmpty(AssetDesc) ? null : AssetDesc.ToLower();

                if (AssetCol != null)
                {
                    int position = ModelsFromExcel.FindIndex(m => m.Description.ToLower() == AssetDesc);
                    if (position > -1)
                    {
                        string assetNumber = ParsingRequiredCell<string>(sheet, Columns.ElementAt(1), row, ref errCount);
                        if (assetNumber != null && assetNumber != "")
                            ModelsFromExcel.ElementAt(position).AssetNumber = assetNumber;
                    }
                    else
                    {
                        errCount += 1;
                        ErrParseExcel.Add(string.Format(MSG_NOTFOUND_WBS, FileName, sheet.SheetName, (row + 1), AssetCol.ColumnName));
                    }
                }
                else
                {
                    errCount += 1;
                    ErrParseExcel.Add(string.Format(MSG_NOTFOUND_WBS, FileName, sheet.SheetName, (row + 1), AssetCol.ColumnName));
                }
            }

            return Task.FromResult(0);
        }

        protected override List<ColumnHelper> GetColumns()
        {
            //Set columns to parse data
            //Remember column start at 0!
            return new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(8, "Description 1*", 500),
                ColumnHelper.SetColumn(4, "Asset\nNumber", 50),              
            };
        }

        protected override bool CheckTemplate(XSSFSheet sheet, List<ColumnHelper> columns)
        {
            int colTemplate = sheet.GetRow(15) != null ? sheet.GetRow(15).Cells.Count() : 0;
            int counter = 0;
            string temp = null;
            bool any = false;

            for (int i = 4; i < 9; i++)
            {
                if (sheet.GetRow(15).GetCell(i) != null)
                {
                    sheet.GetRow(15).GetCell(i).SetCellType(CellType.String);
                    temp = sheet.GetRow(15).GetCell(i).StringCellValue;
                    if (!string.IsNullOrEmpty(temp))
                    {
                        any = columns.Any(x => x.ColumnName.ToLower() == temp.ToLower());
                        if (any)
                            ++counter;
                    }
                }
                i += 3;
            }

            if (counter == columns.Count)
                return true;            

            return false;
        }
    }
}