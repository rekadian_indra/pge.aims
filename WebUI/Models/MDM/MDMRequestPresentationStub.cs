﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;

namespace WebUI.Models
{
    public class MDMRequestPresentationStub : BasePresentationStub<MDMRequest, MDMRequestPresentationStub>
    {
        public int MDMRequestId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Status { get; set; }
        public string LegalRequestor { get; set; }
        public string RequestTitle { get; set; }
        public string Plant { get; set; }

        public MDMRequestPresentationStub() : base()
        {

        }

        public MDMRequestPresentationStub(MDMRequest dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            if (dbObject != null && dbObject.Status != null)
                Status = ((StatusMDM)Enum.Parse(typeof(StatusMDM), dbObject.Status)).ToDescription();
        }

        public MDMRequestPresentationStub(ViewMDM dbObject)
        {
            //TODO: Manual mapping object here
            ObjectMapper.MapObject<ViewMDM, MDMRequestPresentationStub>(dbObject, this);
            if (dbObject != null && dbObject.Status != null)
                Status = ((StatusMDM)Enum.Parse(typeof(StatusMDM), dbObject.Status)).ToDescription();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}