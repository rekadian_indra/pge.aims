﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using Common.Enums;
using Newtonsoft.Json;
using Business.Infrastructure;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class MDMRequestFormStub : BaseFormStub<MDMRequest, MDMRequestFormStub>
    {
        public int MDMRequestId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Status { get; set; }
        [DisplayName("Legal Requestor")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string LegalRequestor { get; set; }
        [DisplayName("Judul Request")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string RequestTitle { get; set; }
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string MDMAsset { get; set; }
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string TempDelMDMAsset { get; set; }
        public MDMRequestFormStub() : base()
        {
            //MDMAsset = "";
            LegalRequestor = User.Identity.Name;
            SubmitDate = DateTime.Now;
        }

        public MDMRequestFormStub(MDMRequest dbObject) : base(dbObject)
        {
            if(dbObject != null && dbObject.MDMAssets != null)
            {
                MDMAsset = JsonConvert.SerializeObject(ListMapper.MapList<MDMAsset, MDMAssetPresentationStub>(dbObject.MDMAssets));
            }
        }

        public override void MapDbObject(MDMRequest dbObject)
        {
            //lib

            //algoritma
            if(MDMRequestId == 0)
            {
                Status = StatusMDM.PROGRESS.ToString();
                SubmitDate = DateTime.Now;
            }
            base.MapDbObject(dbObject);            
            //TODO: Manual mapping object here
        }

        protected override void Init()
        {
        }

    }
}