﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using Business.Entities;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class MDMAssetFinishFormStub : BaseFormStub<MDMAsset, MDMAssetFinishFormStub>
    {
        public int MDMRequestId { get; set; }
        public int AssetId { get; set; }     
        public string AssetClass { get; set; }        
        [DisplayName("Asset Number")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string AssetNumber { get; set; }
        public string SubNumber { get; set; }
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }       
        public MDMAssetFinishFormStub() : base()
        {            
        }

        public MDMAssetFinishFormStub(MDMAsset dbObject) : base(dbObject)
        {
            if(dbObject != null && dbObject.Asset != null)
            {
                AssetNumber = dbObject.Asset.AssetNumber;
                SubNumber = dbObject.Asset.SubNumber;
                Description = dbObject.Asset.Description;                
            }            
        }

        public override void MapDbObject(MDMAsset dbObject)
        {
            //lib

            //algoritma
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
        }
        public void MapDbObject(Asset dbObject)
        {
            dbObject.AssetId = AssetId;
            dbObject.AssetNumber = AssetNumber;
        }

        protected override void Init()
        {                     
        }

    }
}