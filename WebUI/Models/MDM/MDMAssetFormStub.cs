﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using Business.Entities;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class MDMAssetFormStub : BaseFormStub<MDMAsset, MDMAssetFormStub>
    {
        public int MDMRequestId { get; set; }
        [DisplayName("Asset")]        
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int AssetId { get; set; }
        [DisplayName("Asset Class")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string AssetClass { get; set; }
        [DisplayName("Post Cap")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string PostCap { get; set; }
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public string AssetNumber { get; set; }
        public string Location { get; set; }
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [DisplayName("Description 2")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Description2 { get; set; }
        [DisplayName("Serial Number")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string SerialNumber { get; set; }
        [DisplayName("Evaluation Group 1")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string EvaluationGroup1 { get; set; }
        [DisplayName("Asset Condition")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string AssetCondition { get; set; }
        [DisplayName("Original Asset")]
        //[StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string OriginalAsset { get; set; }
        [DisplayName("Orig. Acquis. Date")]        
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public System.DateTime OrigAcquisDate { get; set; }
        [DisplayName("Book Deprey Key")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string BookDepreKey { get; set; }
        [DisplayName("Useful Life 01")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string UsefulLife01 { get; set; }
        [DisplayName("Useful Life 03")]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string UsefulLife03 { get; set; }
        public MDMAssetFormStub() : base()
        {
            OrigAcquisDate = DateTime.Now;
        }

        public MDMAssetFormStub(MDMAsset dbObject) : base(dbObject)
        {

        }

        public override void MapDbObject(MDMAsset dbObject)
        {
            //lib

            //algoritma
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
        }             

        protected override void Init()
        {                     
        }

    }
}