﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business.Entities;

namespace WebUI.Models
{
    public class MDMAssetPresentationStub : BasePresentationStub<MDMAsset, MDMAssetPresentationStub>
    {
        public int MDMRequestId { get; set; }
        public int AssetId { get; set; }
        public string AssetClass { get; set; }
        public string PostCap { get; set; }        
        public string Description2 { get; set; }
        public string SerialNumber { get; set; }
        public string EvaluationGroup1 { get; set; }
        public string AssetCondition { get; set; }
        public string OriginalAsset { get; set; }
        public System.DateTime OrigAcquisDate { get; set; }
        public string BookDepreKey { get; set; }
        public string UsefulLife01 { get; set; }
        public string UsefulLife03 { get; set; }
        public string Description { get; set; }
        public string AssetNumber { get; set; }
        public string Location { get; set; }

        public MDMAssetPresentationStub() : base()
        {

        }

        public MDMAssetPresentationStub(MDMAsset dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            if(dbObject!=null && dbObject.Asset != null)
            {
                Description = dbObject.Asset.Description;
                AssetNumber = dbObject.Asset.AssetNumber;
                Location = dbObject.Asset.Location.Name;
            }

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}