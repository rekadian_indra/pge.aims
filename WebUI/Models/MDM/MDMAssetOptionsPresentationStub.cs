﻿using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class MDMAssetOptionsPresentationStub : BasePresentationStub<ViewMappingAsset, MDMAssetOptionsPresentationStub>
    {
        public int AssetId { get; set; }        
        public string AssetNumber { get; set; }       
        public string Description { get; set; }      
        public string Location { get; set; }       

        public MDMAssetOptionsPresentationStub() : base()
        {
        }

        public MDMAssetOptionsPresentationStub(ViewMappingAsset dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here          

        }      

        //public CrewPresentationStub(ViewCrew dbObject, DateTime now) : this(dbObject)
        //{
        //    //This example constructor with many parameter.
        //    //This called by MapList in Business.Infrastructure.ListMapper via controller
        //    DateTime dateTimeNow = now;
        //}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}