﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class AUCFormStub : BaseFormStub<AUC, AUCFormStub>
    {
        [DisplayName("WBS Element")]
        public string WBSElement { get; set; }

        [DisplayName("Asset Number")]
        public string AssetNumber { get; set; }       

        [DisplayName("Asset Description")]
        public string Description { get; set; }

        [DisplayName("Plant")]
        public string Plant { get; set; }

        [DisplayName("Cost Center")]
        public string CostCenter { get; set; }

        [DisplayName("Book Val.")]
        public double? BookVal { get; set; }

        [DisplayName("Asset Class")]
        public string AssetClass { get; set; }

        [DisplayName("Sub Number")]
        public string SubNumber { get; set; }

        [DisplayName("Capitalized On")]
        public DateTime? CapitalizedOn { get; set; }

        [DisplayName("Acquisition Val. (IDR)")]
        public double AcquisValIdr { get; set; }

        [DisplayName("Accum. Dep.")]
        public double? AccumDep { get; set; }

        [DisplayName("Currency")]
        public string Currency { get; set; }

        [DisplayName("Company Code")]
        public string CompanyCode { get; set; }

        [DisplayName("Original Asset")]
        public string OriginalAsset { get; set; }

        [DisplayName("Serial Number")]
        public string SerialNumber { get; set; }

        [DisplayName("Useful Life")]
        public short? UsefulLife { get; set; }

        [DisplayName("Useful Life Period")]
        public short? UsefulPeriod { get; set; }

        [DisplayName("Scrap Value")]
        public double? ScrapVal { get; set; }

        [DisplayName("Profit Center")]
        public string ProfitCenter { get; set; }

        [DisplayName("Ordinary Dep. Posted")]
        public double? OrdinaryDep { get; set; }

        [DisplayName("First Acuisition On")]
        public DateTime? FirstAcquisition { get; set; }

        [DisplayName("Asset Condition")]
        public string AssetCondition { get; set; }

        [DisplayName("Evaluation Group 1")]
        public string EvaluationGroup1 { get; set; }

        [DisplayName("Evaluation Group 3")]
        public string EvaluationGroup3 { get; set; }

        [DisplayName("Evaluation Group 4")]
        public string EvaluationGroup4 { get; set; }

        [DisplayName("Evaluation Group 5")]
        public string EvaluationGroup5 { get; set; }

        [DisplayName("Location")]
        public string Location { get; set; }

        [DisplayName("Balance Sheet Item")]
        public string BalanceSheetItem { get; set; }

        [DisplayName("Original Value")]
        public double? OriginalValue { get; set; }

        [DisplayName("Original Acquisition Year")]
        public short? OriginalAcquisitionYear { get; set; }

        [DisplayName("Depreciation Key")]
        public string DepreciationKey { get; set; }

        [DisplayName("Accum. Ordinary Dep.")]
        public double? AccumOrdinaryDep { get; set; }

        [DisplayName("Planned Ordinary Dep.")]
        public double? PlannedOrdinaryDep { get; set; }

        [DisplayName("No. of Units Deprec.")]
        public int? NoUnitDeprec { get; set; }

        [DisplayName("Acquisition Val. (USD)")]
        public double AcquisValUsd { get; set; }



        public AUCFormStub() : base()
        {
        }

        public AUCFormStub(AUC dbItem) : this()
        {
            ObjectMapper.MapObject<AUC, AUCFormStub>(dbItem, this);            
        }

        public override void MapDbObject(AUC dbObject)
        {
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here            
        }

        //public AssetFormStub(int index) : this()
        //{
        //    CrewId = GenerateBarcode(index);
        //    RegisterDate = DateTime.Now.Date;
        //}

        //public AssetFormStub(Crew dbObject) : base(dbObject)
        //{
        //    //TODO: Manual mapping object here
        //    if (dbObject.Companies.Any())
        //        CompanyId = dbObject.Companies.FirstOrDefault().CompanyId;
        //}

        //public override void MapDbObject(Crew dbObject)
        //{
        //    base.MapDbObject(dbObject);

        //    //TODO: Manual mapping object here

        //}

        //public void MapDbObject(Crew dbObject, Company company)
        //{
        //    MapDbObject(dbObject);

        //    dbObject.Companies.Clear();
        //    dbObject.Companies.Add(company);
        //}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            //StatusOption = DisplayFormat.EnumToSelectList<CrewStatus>();
        }

    }
}