﻿using Business.Entities;
using Common.Enums;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace WebUI.Models
{
    public class AUCImportStub : BaseImportStub
    {
        public List<AUCFormStub> ModelsFromExcel { get; set; }        

        public AUCImportStub() { }

        public async void ParseFile(HttpPostedFileBase[] files)
        {
            // kamus            
            ErrParseExcel = new List<string>();
            ModelsFromExcel = new List<AUCFormStub>();
            XSSFWorkbook book = null;
            XSSFSheet sheet = null;
            XSSFSheet sheetUsd = null;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            List<Task> taskMapping = null;

            // algoritma
            if (files != null && files.Any())
            {
                if (files[0].ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
                    Path.GetExtension(files[0].FileName.ToLower()) == ".xlsx")
                {

                    try
                    {
                        FileName = files[0].FileName;
                        book = new XSSFWorkbook(files.First().InputStream);

                        sheetUsd = (XSSFSheet)book.GetSheet(SheetExcel.USD.ToString());
                        sheet = (XSSFSheet)book.GetSheet(SheetExcel.IDR.ToString());
                                                
                        Valid = sheet != null ? CheckTemplate(sheet, Columns) : false;
                        Valid = Valid && sheetUsd != null ? CheckTemplate(sheetUsd, Columns) : false;

                        if (Valid && sheet.GetRow(1) != null && sheetUsd.GetRow(1) != null )
                        {
                            int rowCount = sheet.LastRowNum;
                            taskMapping = new List<Task>();

                            for (int r = 1; r <= rowCount; r++)
                                taskMapping.Add(MappingObjectAsync(sheet,sheetUsd, r));

                            await Task.WhenAll(taskMapping);
                        }
                        else
                        {
                            ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                        }
                    }
                    catch(Exception ex)
                    {
                        ErrParseExcel.Add(string.Format(MSG_ERROR, FileName));
                    }
                }
                else
                {
                    ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));
                }
            }
        }

        protected Task MappingObjectAsync(XSSFSheet sheet, XSSFSheet sheetUsd, int row)
        {
            int errCount = 0;
            AUCFormStub dataRow = null;
            string wbsIdr, wbsUsd;
            ColumnHelper wbsCol = ColumnHelper.SetColumn(0, MasterExcelColumn.ASSET.ToDescription(), 50);

            if (sheet.GetRow(row) != null)
            {
                int valCount = sheet.GetRow(row).Cells.Where(m=>m.CellType != CellType.Blank).Count();
                if (valCount > 0)
                {
                    dataRow = new AUCFormStub();

                    wbsIdr = ParsingRequiredCell<string>(sheet, wbsCol, row, ref errCount);
                    wbsIdr = string.IsNullOrEmpty(wbsIdr) ? null : wbsIdr.ToLower();
                    wbsUsd = ParsingCell<string>(sheetUsd, wbsCol, row, ref errCount);
                    wbsUsd = string.IsNullOrEmpty(wbsUsd) ? null : wbsUsd.ToLower();


                    if (wbsIdr == wbsUsd && wbsIdr != null)
                    {
                        foreach (var col in Columns)
                        {
                            switch (col.ColumnNum)
                            {
                                case 0:
                                    dataRow.AssetNumber = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 1:
                                    dataRow.SubNumber = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 2:
                                    dataRow.CapitalizedOn = ParsingCell<DateTime?>(sheet, col, row, ref errCount);
                                    break;
                                case 3:
                                    dataRow.WBSElement = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 4:
                                    dataRow.Description = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 5:
                                    dataRow.AcquisValIdr = ParsingRequiredCell<double>(sheet, col, row, ref errCount);
                                    dataRow.AcquisValUsd = ParsingRequiredCell<double>(sheetUsd, col, row, ref errCount);
                                    break;
                                case 6:
                                    dataRow.AccumDep = ParsingCell<double?>(sheet, col, row, ref errCount);
                                    break;
                                case 7:
                                    dataRow.BookVal = ParsingCell<double?>(sheet, col, row, ref errCount);
                                    break;
                                case 8:
                                    dataRow.Currency = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 9:
                                    dataRow.CompanyCode = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 10:
                                    dataRow.AssetClass = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 11:
                                    dataRow.OriginalAsset = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 12:
                                    dataRow.Plant = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 13:
                                    dataRow.SerialNumber = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 14:
                                    dataRow.UsefulLife = ParsingCell<short?>(sheet, col, row, ref errCount);
                                    break;
                                case 15:
                                    dataRow.UsefulPeriod = ParsingCell<short?>(sheet, col, row, ref errCount);
                                    break;
                                case 16:
                                    dataRow.ScrapVal = ParsingCell<double?>(sheet, col, row, ref errCount);
                                    break;
                                case 17:
                                    dataRow.ProfitCenter = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 18:
                                    dataRow.OrdinaryDep = ParsingCell<double?>(sheet, col, row, ref errCount);
                                    break;
                                case 19:
                                    dataRow.FirstAcquisition = ParsingCell<DateTime?>(sheet, col, row, ref errCount);
                                    break;
                                case 20:
                                    dataRow.CostCenter = ParsingRequiredCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 21:
                                    dataRow.AssetCondition = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 22:
                                    dataRow.EvaluationGroup1 = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 23:
                                    dataRow.EvaluationGroup3 = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 24:
                                    dataRow.EvaluationGroup4 = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 25:
                                    dataRow.Location = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 26:
                                    dataRow.EvaluationGroup5 = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 27:
                                    dataRow.BalanceSheetItem = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 28:
                                    dataRow.OriginalValue = ParsingCell<int?>(sheet, col, row, ref errCount);
                                    break;
                                case 29:
                                    dataRow.OriginalAcquisitionYear = ParsingCell<short?>(sheet, col, row, ref errCount);
                                    break;
                                case 30:
                                    dataRow.DepreciationKey = ParsingCell<string>(sheet, col, row, ref errCount);
                                    break;
                                case 31:
                                    dataRow.AccumOrdinaryDep = ParsingCell<double?>(sheet, col, row, ref errCount);
                                    break;
                                case 32:
                                    dataRow.PlannedOrdinaryDep = ParsingCell<double?>(sheet, col, row, ref errCount);
                                    break;
                                case 33:
                                    dataRow.NoUnitDeprec = ParsingCell<int?>(sheet, col, row, ref errCount);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else
                    {
                        errCount += 1;
                        ErrParseExcel.Add(string.Format(MSG_NOTFOUND_WBS, FileName, sheetUsd.SheetName, (row + 1), wbsCol.ColumnName));
                    }

                    if (errCount == 0)
                        ModelsFromExcel.Add(dataRow);
                }


                
            }

            return Task.FromResult(0);
        }

        protected override List<ColumnHelper> GetColumns()
        {
            //Set columns to parse data
            //Remember column start at 0!
            return new List<ColumnHelper>
            {
                ColumnHelper.SetColumn(0, MasterExcelColumn.ASSET.ToDescription(), 50),
                ColumnHelper.SetColumn(1, MasterExcelColumn.SUB_NUMBER.ToDescription(), 50),
                ColumnHelper.SetColumn(2, MasterExcelColumn.CAPITALIZED_ON.ToDescription()),
                ColumnHelper.SetColumn(3, MasterExcelColumn.WBS.ToDescription(), 50),
                ColumnHelper.SetColumn(4, MasterExcelColumn.ASSET_DESC.ToDescription(), 500),
                ColumnHelper.SetColumn(5, MasterExcelColumn.ACQUI_VAL.ToDescription()),
                ColumnHelper.SetColumn(6, MasterExcelColumn.ACCUM_DEP.ToDescription()),
                ColumnHelper.SetColumn(7, MasterExcelColumn.BOOK_VAL.ToDescription()),
                ColumnHelper.SetColumn(8, MasterExcelColumn.CURRENCY.ToDescription(), 5),
                ColumnHelper.SetColumn(9, MasterExcelColumn.COMPANY_CODE.ToDescription(), 10),
                ColumnHelper.SetColumn(10, MasterExcelColumn.ASSET_CLASS.ToDescription(), 5),
                ColumnHelper.SetColumn(11, MasterExcelColumn.ORIG_ASSET.ToDescription(), 50),
                ColumnHelper.SetColumn(12, MasterExcelColumn.PLANT.ToDescription(), 50),
                ColumnHelper.SetColumn(13, MasterExcelColumn.SERIAL_NUMBER.ToDescription(), 50),
                ColumnHelper.SetColumn(14, MasterExcelColumn.USEFUL_LIFE.ToDescription()),
                ColumnHelper.SetColumn(15, MasterExcelColumn.USEFUL_PERIOD.ToDescription()),
                ColumnHelper.SetColumn(16, MasterExcelColumn.SCRAP_VALUE.ToDescription()),
                ColumnHelper.SetColumn(17, MasterExcelColumn.PROFIT_CENTER.ToDescription(),50),
                ColumnHelper.SetColumn(18, MasterExcelColumn.ORDINARY_DEP_POST.ToDescription()),
                ColumnHelper.SetColumn(19, MasterExcelColumn.FIRST_ACQUI.ToDescription()),
                ColumnHelper.SetColumn(20, MasterExcelColumn.COST_CENTER.ToDescription(), 10),
                ColumnHelper.SetColumn(21, MasterExcelColumn.ASSET_CONDITION.ToDescription(), 500),
                ColumnHelper.SetColumn(22, MasterExcelColumn.EVALUATION_GROUP1.ToDescription(), 250),
                ColumnHelper.SetColumn(23, MasterExcelColumn.EVALUATION_GROUP3.ToDescription(), 250),
                ColumnHelper.SetColumn(24, MasterExcelColumn.EVALUATION_GROUP4.ToDescription(), 250),
                ColumnHelper.SetColumn(25, MasterExcelColumn.LOCATION.ToDescription(), 250),
                ColumnHelper.SetColumn(26, MasterExcelColumn.EVALUATION_GROUP5.ToDescription(), 250),
                ColumnHelper.SetColumn(27, MasterExcelColumn.BALANCE_SHEET_ITEM.ToDescription(), 50),
                ColumnHelper.SetColumn(28, MasterExcelColumn.ORIG_VALUE.ToDescription()),
                ColumnHelper.SetColumn(29, MasterExcelColumn.ORIG_ACQUI_YEAR.ToDescription()),
                ColumnHelper.SetColumn(30, MasterExcelColumn.DEPREC_KEY.ToDescription(), 50),
                ColumnHelper.SetColumn(31, MasterExcelColumn.ACCUM_ORDINARY.ToDescription()),
                ColumnHelper.SetColumn(32, MasterExcelColumn.PLANNED_ORDINARY.ToDescription()),
                ColumnHelper.SetColumn(33, MasterExcelColumn.NO_UNIT_DEPREC.ToDescription()),
            };
        }
    }
}