﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class AUCPresentationStub : BasePresentationStub<AUC, AUCPresentationStub>
    {
        public string WBSElement { get; set; }
        public string AssetNumber { get; set; }
        public string Description { get; set; }
        public string Plant { get; set; }
        public string CostCenter { get; set; }
        public double AcquisValIdr { get; set; }
        public Nullable<double> BookVal { get; set; }
        public string AssetClass { get; set; }
        public string SubNumber { get; set; }
        public Nullable<System.DateTime> CapitalizedOn { get; set; }
        public Nullable<double> AccumDep { get; set; }
        public string Currency { get; set; }
        public string CompanyCode { get; set; }
        public string OriginalAsset { get; set; }
        public string SerialNumber { get; set; }
        public Nullable<short> UsefulLife { get; set; }
        public Nullable<short> UsefulPeriod { get; set; }
        public Nullable<double> ScrapVal { get; set; }
        public string ProfitCenter { get; set; }
        public Nullable<double> OrdinaryDep { get; set; }
        public Nullable<System.DateTime> FirstAcquisition { get; set; }
        public string AssetCondition { get; set; }
        public string EvaluationGroup1 { get; set; }
        public string EvaluationGroup3 { get; set; }
        public string EvaluationGroup4 { get; set; }
        public string Location { get; set; }
        public string EvaluationGroup5 { get; set; }
        public string BalanceSheetItem { get; set; }
        public Nullable<double> OriginalValue { get; set; }
        public Nullable<short> OriginalAcquisitionYear { get; set; }
        public string DepreciationKey { get; set; }
        public Nullable<double> AccumOrdinaryDep { get; set; }
        public Nullable<double> PlannedOrdinaryDep { get; set; }
        public Nullable<int> NoUnitDeprec { get; set; }
        public double AcquisValUsd { get; set; }
        public bool CanDeleted { get; set; }

        public AUCPresentationStub() : base()
        {
        }

        public AUCPresentationStub(AUC dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
        }

        public AUCPresentationStub(AUC dbObject, List<MappingAssetActualCost> mappings)
        {
            ObjectMapper.MapObject<AUC, AUCPresentationStub>(dbObject, this);
            CanDeleted = mappings.Any(n => n.WBSElement.ToLower() == dbObject.WBSElement.ToLower()) ? false : true;
        }

        //public CrewPresentationStub(ViewCrew dbObject, DateTime now) : this(dbObject)
        //{
        //    //This example constructor with many parameter.
        //    //This called by MapList in Business.Infrastructure.ListMapper via controller
        //    DateTime dateTimeNow = now;
        //}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}