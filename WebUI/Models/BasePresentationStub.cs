﻿using Business.Extension;
using Business.Infrastructure;
using System;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public abstract class BasePresentationStub<TEntity, TModel>
        where TEntity : class
        where TModel : class
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }

        protected HttpServerUtility Server
        {
            get
            {
                return HttpContext.Current.Server;
            }
        }

        protected Principal User
        {
            get
            {
                return HttpContext.Current.User as Principal;
            }
        }

        public BasePresentationStub()
        {
            Init();
        }

        public BasePresentationStub(TEntity dbObject) : this()
        {
            ObjectMapper.MapObject<TEntity, TModel>(dbObject, this);
            ToLocalTime();
        }

        protected virtual void ToLocalTime()
        {
            CreatedDateTimeUtc = CreatedDateTimeUtc.ToLocalDateTime();
            ModifiedDateTimeUtc = ModifiedDateTimeUtc.ToLocalDateTime();
        }

        //set the init object here to be called in all constructor
        protected abstract void Init();
    }
}