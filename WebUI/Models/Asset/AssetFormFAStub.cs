﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class AssetFormFAStub : AssetFormStub
    {
        [DisplayName("Barcode")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string BarcodeImage { get; set; }
        public string BarcodeImageTemp { get; set; }

        [DisplayName("Map Location")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string LocationMap { get; set; }

        public AssetFormFAStub()
        {
        }

        public AssetFormFAStub(Asset dbObject) : base(dbObject)
        {
            MappingId = dbObject.MappingAssets.FirstOrDefault().MappingId;
            Location = dbObject.Location != null ? dbObject.Location.Name : "-";
            Latitude = dbObject.Latitude.ToString().Replace(",", ".");
            Longitude = dbObject.Longitude.ToString().Replace(",", ".");
            BarcodeImage = dbObject.BarcodeImage;
        }

        public AssetFormFAStub(Asset dbObject, int id) : base(dbObject, id)
        {
            MappingId = id;
            Location = dbObject.Location != null ? dbObject.Location.Name : "-";
            Latitude = dbObject.Latitude.ToString().Replace(",", ".");
            Longitude = dbObject.Longitude.ToString().Replace(",", ".");
            BarcodeImage = dbObject.BarcodeImage;
        }

        public override void MapDbObject(Asset dbObject)
        {
            //lib
            MappingAsset temp;

            //algoritma
            base.MapDbObject(dbObject);

            Latitude = Latitude?.Replace(".", ",");
            Longitude = Longitude?.Replace(".", ",");
            dbObject.BarcodeImage = BarcodeImage;
            dbObject.Latitude = Convert.ToDouble(Latitude);
            dbObject.Longitude = Convert.ToDouble(Longitude);

            //TODO: Manual mapping object here
            if (dbObject.AssetId == 0)
            {
                dbObject.IsDeleted = false;
                //mapping asset
                temp = new MappingAsset()
                {
                    MappingId = this.MappingId
                };
                dbObject.MappingAssets.Add(temp);
            }
           
        }

        //public void MapDbObject(Crew dbObject, Company company)
        //{
        //    MapDbObject(dbObject);

        //    dbObject.Companies.Clear();
        //    dbObject.Companies.Add(company);
        //}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            //StatusOption = DisplayFormat.EnumToSelectList<CrewStatus>();
        }

    }
}