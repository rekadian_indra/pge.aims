﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class AssetFormStub : BaseFormStub<Asset, AssetFormStub>
    {
        public int AssetId { get; set; }

        [DisplayName("Fixed Asset Number")]
        //[StringLength(100, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [RequiredIf("SubNumber", 1, ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [RegularExpression(@"([-a-zA-Z.0-9\s]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string AssetNumber { get; set; }

        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [DisplayName("Fixed Asset Sub Number")]
        public string SubNumber { get; set; }        

        [AllowHtml]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [DisplayName("Fixed Asset Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        
        public double ValueIdr { get; set; }
        
        public double ValueUsd { get; set; }

        public string Location { get; set; }

        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [DisplayName("Asset Photo")]
        public string Photo { get; set; }
        
        public string Latitude { get; set; }
        
        public string Longitude { get; set; }

        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [DisplayName("Physical Location")]
        public int LocationId { get; set; }

        public int MappingId { get; set; }

        //[DisplayName("Barcode")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //public string BarcodeImage { get; set; }
        //public string BarcodeImageTemp { get; set; }

        //[DisplayName("Map Location")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //public string LocationMap { get; set; }

        public AssetFormStub() : base()
        {
        }
        public AssetFormStub(int id) : base()
        {
            MappingId = id;
        }

        public AssetFormStub(Asset dbObject) : base(dbObject)
        {
            
        }

        public AssetFormStub(Asset dbObject, int id) : base(dbObject)
        {
            MappingId = id;
        }       

        public override void MapDbObject(Asset dbObject)
        {
            //lib
            MappingAsset temp;

            //algoritma
            base.MapDbObject(dbObject);

            //TODO: Manual mapping object here
            if (dbObject.AssetId == 0)
            {
                dbObject.IsDeleted = false;
                //mapping asset
                temp = new MappingAsset()
                {
                    MappingId = this.MappingId
                };
                dbObject.MappingAssets.Add(temp);
            }
           
        }

        //public void MapDbObject(Crew dbObject, Company company)
        //{
        //    MapDbObject(dbObject);

        //    dbObject.Companies.Clear();
        //    dbObject.Companies.Add(company);
        //}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            //StatusOption = DisplayFormat.EnumToSelectList<CrewStatus>();
        }

    }
}