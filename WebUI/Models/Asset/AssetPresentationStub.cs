﻿using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class AssetPresentationStub : BasePresentationStub<Asset, AssetPresentationStub>
    {
        public int AssetId { get; set; }
        public int AssetIdOffline { get; set; }
        public string AssetNumber { get; set; }
        public string SubNumber { get; set; }
        public string Description { get; set; }
        public double ValueIdr { get; set; }
        public double ValueUsd { get; set; }
        public string Location { get; set; }
        public string Photo { get; set; }
        public int LocationId { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public int MappingId { get; set; }

        public int MappingAssetId { get; set; }

        public string StatusMapping { get; set; }

        public int? TCRId { get; set; }

        public string StatusTCR { get; set; }

        public int? MDMRequestId { get; set; }
        public string StatusMDM { get; set; }

        public int? SPCRequestId { get; set; }
        public string StatusSPC { get; set; }

        public string NoRequestSPC005 { get; set; }
        public string NoRequestSPC011 { get; set; }

        public string CostCenter { get; set; }

        public string Plant { get; set; }

        public string Condition { get; set; }

        private string urlImageDefault = VirtualPathUtility.ToAbsolute("~/Images/no-image-width.jpg");

        public string AssetHolder { get; set; }
        public string BarcodeImage { get; set; }

        public AssetPresentationStub() : base()
        {
        }

        public AssetPresentationStub(Asset dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            Location = dbObject.Location.Name;
            if (!string.IsNullOrEmpty(dbObject.Photo))
            {
                string serverPath = Server.MapPath(dbObject.Photo);
                if (System.IO.File.Exists(serverPath))
                {
                    Photo = VirtualPathUtility.ToAbsolute(dbObject.Photo);
                }
                else
                {
                    Photo = VirtualPathUtility.ToAbsolute(urlImageDefault);
                }

                serverPath = Server.MapPath(dbObject.BarcodeImage);
                if (System.IO.File.Exists(serverPath))
                    BarcodeImage = VirtualPathUtility.ToAbsolute(dbObject.BarcodeImage);                
                else
                    BarcodeImage = VirtualPathUtility.ToAbsolute(urlImageDefault);                
            }
        }

        public AssetPresentationStub(ViewMappingAsset dbObject)
        {
            ObjectMapper.MapObject<ViewMappingAsset, AssetPresentationStub>(dbObject, this);
            if (!string.IsNullOrEmpty(dbObject.Photo))
            {
                string serverPath = Server.MapPath(dbObject.Photo);
                if (System.IO.File.Exists(serverPath))
                {
                    Photo = VirtualPathUtility.ToAbsolute(dbObject.Photo);
                }
                else
                {
                    Photo = VirtualPathUtility.ToAbsolute(urlImageDefault);
                }

                serverPath = Server.MapPath(dbObject.BarcodeImage);
                if (System.IO.File.Exists(serverPath))
                    BarcodeImage = VirtualPathUtility.ToAbsolute(dbObject.BarcodeImage);
                else
                    BarcodeImage = VirtualPathUtility.ToAbsolute(urlImageDefault);
            }
        }        

        //public CrewPresentationStub(ViewCrew dbObject, DateTime now) : this(dbObject)
        //{
        //    //This example constructor with many parameter.
        //    //This called by MapList in Business.Infrastructure.ListMapper via controller
        //    DateTime dateTimeNow = now;
        //}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}