﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class ReviseFormStub
    {
        public int Id { get; set; }
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [DisplayName("Catatan")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]        
        public string Notes { get; set; }


        public ReviseFormStub()
        {

        }

        public void MapDbObject(RejectMappingHistory dbObject)
        {
            //lib

            //algoritma
            dbObject.MappingId = Id;
            dbObject.Notes = Notes;
           
        }

        public void MapDbObject(RejectTCRHistory dbObject)
        {
            //lib

            //algoritma
            dbObject.TCRId = Id;
            dbObject.Notes = Notes;

        }
    }
}