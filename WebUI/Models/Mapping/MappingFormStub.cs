﻿using Business.Entities;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;
using System.Linq;
using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class MappingFormStub : BaseFormStub<Mapping, MappingFormStub>
    {

        public int MappingId { get; set; }

        [DisplayName("Area")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? PlantId { get; set; }

        [DisplayName("Submit Date")]
        public DateTime SubmitDate { get; set; }

        [AllowHtml]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Description { get; set; }
        public string Status { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

        public List<MappingAssetFormStub> MappingAssets { get; set; }

        public MappingFormStub() : base()
        {

        }

        public MappingFormStub(Mapping dbObject) : base(dbObject) 
        {
            //Assets = new List<AssetPresentationStub>();
            //AssetActualCosts = new List<MappingAssetActualCostFormStub>();
            //LocalStorage = new Dictionary<string, string>();
            //foreach (MappingAsset a in dbObject.MappingAssets)
            //{
            //    Assets.Add(new AssetPresentationStub(a.Asset));

            //    foreach (MappingAssetActualCost map in a.MappingAssetActualCosts)
            //    {
            //        AssetActualCosts.Add(new MappingAssetActualCostFormStub(map));
            //    }
            //    LocalStorage.Add($"{storage.AUCSTRORAGE.ToDescription()}{a.AssetId}", JsonConvert.SerializeObject(AssetActualCosts));
            //}
            //LocalStorage.Add($"{storage.FASTORAGE}", JsonConvert.SerializeObject(Assets));
        }

        public void MapDbObject(AssetPresentationStub source, Asset dbObject, bool isNew = false)
        {
            ObjectMapper.MapObject<AssetPresentationStub, Asset>(source, dbObject);

            //set if new item
            if (isNew)
                dbObject.AssetId = 0;
        }

        public override void MapDbObject(Mapping dbObject)
        {
            base.MapDbObject(dbObject);            
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            SubmitDate = DateTime.Now;
            Status = StatusAsset.DRAFT.ToString();
            IsDeleted = false;
        }
    }
}