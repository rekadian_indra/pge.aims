﻿using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class MappingPresentationStub : BasePresentationStub<Mapping, MappingPresentationStub>
    {

        public int MappingId { get; set; }
        public int? PlantId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime SubmitDate { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string StatusPretify { get; set; }
        public string Notes { get; set; }
        public string Plant { get; set; }

        public MappingPresentationStub() : base()
        {
            
        }

        public MappingPresentationStub(Mapping dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            StatusAsset status = (StatusAsset)Enum.Parse(typeof(StatusAsset), dbObject.Status);
            StatusPretify = status.ToDescription();
            
            if(dbObject.Plant != null)
            {
                Code = dbObject.Plant.Code;
                Name = dbObject.Plant.Name;
            }
        }

        public MappingPresentationStub(ViewMapping dbObject)
        {            
            ObjectMapper.MapObject<ViewMapping, MappingPresentationStub>(dbObject, this);
            //TODO: Manual mapping object here
            StatusAsset status = (StatusAsset)Enum.Parse(typeof(StatusAsset), dbObject.Status);
            StatusPretify = status.ToDescription();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}