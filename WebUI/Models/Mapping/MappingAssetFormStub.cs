﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class MappingAssetFormStub : BaseFormStub<MappingAsset, MappingAssetFormStub>
    {
        public int MappingAssetId { get; set; }
        public int MappingId { get; set; }
        public int AssetId { get; set; }

        public MappingAssetFormStub()
        {

        }

        public override void MapDbObject(MappingAsset dbObject)
        {
            base.MapDbObject(dbObject);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}