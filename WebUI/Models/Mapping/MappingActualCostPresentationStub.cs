﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class MappingActualCostPresentationStub
    {
        public int MappingAssetId { get; set; }
        public int MappingId { get; set; }
        public int AssetId { get; set; }
        public string Status { get; set; }

        public MappingActualCostPresentationStub() {
        }
    }
}