﻿using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class MappingAssetActualCostFormStub : BaseFormStub<MappingAssetActualCost, MappingAssetActualCostFormStub>
    {
        public int AssetId { get; set; }
        public int MappingAssetActualCostId { get; set; }
        public int MappingAssetId { get; set; }
        public int ActualCostId { get; set; }
        public string WBSElement { get; set; }
        public double ValueIdr { get; set; }
        public double ValueUsd { get; set; }
        public double Balance { get; set; }

        public string Status { get; set; }

        public string DocNumber { get; set; }

        public string AucNumber { get; set; }
        public AssetPresentationStub Asset { get; set; }
        public AUCPresentationStub AUC { get; set; }
        public MappingAssetActualCostFormStub()
        {

        }

        public MappingAssetActualCostFormStub(MappingAssetActualCost dbObject) : base(dbObject) 
        {
            AssetId = dbObject.MappingAsset.AssetId;
            DocNumber = dbObject.ActualCost.DocNumber;            
        }

        public MappingAssetActualCostFormStub(ViewMappingAssetActualCost dbObject)
        {
            ObjectMapper.MapObject<ViewMappingAssetActualCost, MappingAssetActualCostFormStub>(dbObject, this);
            Asset = new AssetPresentationStub(dbObject.Asset);
            if(dbObject.AUC != null)
                AUC = new AUCPresentationStub(dbObject.AUC);            
        }

        public MappingAssetActualCostFormStub(ViewAsset dbObject)
        {
            ObjectMapper.MapObject<ViewAsset, MappingAssetActualCostFormStub>(dbObject, this);
            Asset = new AssetPresentationStub(dbObject.Asset);
            if (dbObject.AUC != null)
                AUC = new AUCPresentationStub(dbObject.AUC);
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            ValueIdr = 0;
            ValueUsd = 0;
        }
    }
}