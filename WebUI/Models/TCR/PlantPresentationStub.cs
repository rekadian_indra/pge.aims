﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class PlantPresentationStub : BasePresentationStub<Plant , PlantPresentationStub>
    {
        public int PlantId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public PlantPresentationStub() : base()
        {
            
        }

        public PlantPresentationStub(Plant dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here      

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}