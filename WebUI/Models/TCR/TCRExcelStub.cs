﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class TCRExcelStub : BaseImportStub
    {
        public TCRExcelStub()
        {
        }

        public static byte[] GenerateTemplate(TCR tcr, List<ViewMappingAsset> assets)
        {
            //culture
            string TITLE = "TECHNICAL COMPLETION REPORT (TCR)";
            string TITLE2ND = "(laporan / pernyataan teknis selesai)";
            ViewMappingAsset tempAsset;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US"); //supaya file tidak corrupt
            CellRangeAddress mergeCell;

            //XSSFCellStyle styleDate;
            XSSFCellStyle styleNumeric;
            XSSFCellStyle styleDecimal;

            //kamus
            IWorkbook workbook = new XSSFWorkbook();
            ICellStyle styleTimeSpan = workbook.CreateCellStyle();
            ICellStyle styleDate = workbook.CreateCellStyle();
            IDataFormat dataFormat = workbook.CreateDataFormat();
            XSSFSheet sheet; XSSFRow row = null; XSSFCell cell = null;
            XSSFCellStyle style;
            //XSSFFont font;
            ICellStyle boldStyle = workbook.CreateCellStyle();
            IFont fontBold = workbook.CreateFont();

            List<string> timeSpans = TimeSpanHelper.GenerateTimeSpan();

            //set bold style
            fontBold.Boldweight = (short)FontBoldWeight.Bold;
            boldStyle.SetFont(fontBold);

            styleNumeric = (XSSFCellStyle)workbook.CreateCellStyle();
            styleNumeric.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0");

            styleDecimal = (XSSFCellStyle)workbook.CreateCellStyle();
            styleDecimal.DataFormat = workbook.CreateDataFormat().GetFormat("0.00");
            
            styleDate.DataFormat = dataFormat.GetFormat(DisplayFormat.SqlDateFormat);
            styleTimeSpan.DataFormat = dataFormat.GetFormat(DisplayFormat.SqlShortTimeFormat);

            sheet = (XSSFSheet)workbook.CreateSheet("TCR");
            sheet.SetColumnWidth(5, 5000);
            //generate
            row = (XSSFRow)sheet.CreateRow(0);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(0,0,2,7);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(2);
            cell.SetCellValue(TITLE);
            boldStyle.Alignment = HorizontalAlignment.Center;
            cell.CellStyle = boldStyle;

            row = (XSSFRow)sheet.CreateRow(1);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(1, 1, 2, 7);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(2);
            cell.SetCellValue(TITLE2ND);
            cell.CellStyle = boldStyle;

            row = (XSSFRow)sheet.CreateRow(3);
            cell = (XSSFCell)row.CreateCell(0);
            cell.SetCellValue("Kepada");
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue(": Manager Finance");

            row = (XSSFRow)sheet.CreateRow(4);
            cell = (XSSFCell)row.CreateCell(0);
            cell.SetCellValue("Dari");
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue(": Manager HR");

            row = (XSSFRow)sheet.CreateRow(6);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(6, 6, 1, 9);
            sheet.AddMergedRegion(mergeCell);
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.Alignment = HorizontalAlignment.Center;
            style.BorderBottom = style.BorderTop = style.BorderRight = style.BorderLeft = BorderStyle.Thin;
            cell = (XSSFCell)row.CreateCell(0);
            cell.SetCellValue("NO");
            cell.CellStyle = style;
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("URAIAN");
            cell.CellStyle = style;
            RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderLeft((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            row = (XSSFRow)sheet.CreateRow(7);
            cell = (XSSFCell)row.CreateCell(0);
            cell.SetCellValue("I");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.BorderRight = BorderStyle.Thin;
            cell.CellStyle = style;
            cell = (XSSFCell)row.CreateCell(9);
            cell.CellStyle = style;

            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("UMUM");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            cell.CellStyle = style;

            row = (XSSFRow)sheet.CreateRow(8);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            SetBorderCell(9, sheet, ref row, out cell, workbook);

            //proyek name
            row = (XSSFRow)sheet.CreateRow(9);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(9, 9, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Nama Proyek");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(9, 9, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue($": {tcr.ProjectName}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //wbs
            row = (XSSFRow)sheet.CreateRow(10);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(10, 10, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("WBS");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(10, 10, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue($": {tcr.WBS}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //tanggal mulai
            row = (XSSFRow)sheet.CreateRow(11);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(11, 11, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Tanggal Mulai");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(11, 11, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue($": {tcr.StartDate.ToString(DisplayFormat.CompactDateFormat)}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //tanggal selesai
            row = (XSSFRow)sheet.CreateRow(12);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(12, 12, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Tanggal Selesai");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(12, 12, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue($": {tcr.EndDate.ToString(DisplayFormat.CompactDateFormat)}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //Definisi Proyek
            row = (XSSFRow)sheet.CreateRow(13);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(13, 13, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Definisi Proyek");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(13, 13, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue($": {StripHTML(tcr.ProjectDefinition)}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            row = (XSSFRow)sheet.CreateRow(14);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            SetBorderCell(9, sheet, ref row, out cell, workbook);

            //Cost Center
            row = (XSSFRow)sheet.CreateRow(15);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(15, 15, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Cost Center");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(15, 15, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue($": {tcr.CostCenter1.Name}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //Asset Holder
            row = (XSSFRow)sheet.CreateRow(16);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(16, 16, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Asset Holder");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(16, 16, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue($": {tcr.AssetHolder}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //Plant
            row = (XSSFRow)sheet.CreateRow(17);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(17, 17, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Plant");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(17, 17, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue($": {tcr.Plant1?.Name}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            row = (XSSFRow)sheet.CreateRow(18);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            SetBorderCell(9, sheet, ref row, out cell, workbook);

            //new section
            row = (XSSFRow)sheet.CreateRow(19);
            cell = (XSSFCell)row.CreateCell(0);
            cell.SetCellValue("II");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.BorderRight = style.BorderTop = BorderStyle.Thin;
            cell.CellStyle = style;

            mergeCell = new NPOI.SS.Util.CellRangeAddress(19, 19, 1, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("PERINCIAN BIAYA");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            cell.CellStyle = style;
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            row = (XSSFRow)sheet.CreateRow(20);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            SetBorderCell(9, sheet, ref row, out cell, workbook);

            //Jumlah Anggaran
            row = (XSSFRow)sheet.CreateRow(21);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(21, 21, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Jumlah Anggaran");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(21, 21, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            if (tcr.Currency == Currency.IDR.ToString())
                cell.SetCellValue($": Rp {DisplayFormat.NumberFormatNoDecimal(tcr.Budget)}");
            else
                cell.SetCellValue($": ${DisplayFormat.NumberFormatNoDecimal(tcr.Budget)}");

            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //Realisasi Biaya
            row = (XSSFRow)sheet.CreateRow(22);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(22, 22, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Realisasi Biaya");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(22, 22, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            if (tcr.Currency == Currency.IDR.ToString())
                cell.SetCellValue($": Rp {DisplayFormat.NumberFormatNoDecimal(tcr.ValueIdr)}");
            else
                cell.SetCellValue($": ${DisplayFormat.NumberFormatNoDecimal(tcr.ValueUsd)}");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //Rincian Asset
            row = (XSSFRow)sheet.CreateRow(23);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(23, 23, 1, 3);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("Rincian Asset");

            mergeCell = new NPOI.SS.Util.CellRangeAddress(23, 23, 4, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(4);
            cell.SetCellValue(":");
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //tabel Asset
            row = (XSSFRow)sheet.CreateRow(24);
            SetBorderCell(0, sheet, ref row, out cell, workbook);

            //no
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("No");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.BorderRight = style.BorderTop = style.BorderBottom = BorderStyle.Thin;
            style.Alignment = HorizontalAlignment.Center;
            cell.CellStyle = style;

            //Nama Asset
            cell = (XSSFCell)row.CreateCell(2);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(24, 24, 2, 4);
            sheet.AddMergedRegion(mergeCell);
            cell.SetCellValue("Nama Asset");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.Alignment = HorizontalAlignment.Center;
            cell.CellStyle = style;
            RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //Nilai
            cell = (XSSFCell)row.CreateCell(5);
            cell.SetCellValue("Nilai");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.BorderRight = style.BorderTop = style.BorderBottom = BorderStyle.Thin;
            style.Alignment = HorizontalAlignment.Center;
            cell.CellStyle = style;

            ////Asset Holder
            //cell = (XSSFCell)row.CreateCell(6);
            //mergeCell = new NPOI.SS.Util.CellRangeAddress(23, 23, 6, 7);
            //sheet.AddMergedRegion(mergeCell);
            //cell.SetCellValue("Asset Holder");
            //style = (XSSFCellStyle)workbook.CreateCellStyle();
            //style.SetFont(fontBold);
            //style.Alignment = HorizontalAlignment.Center;
            //cell.CellStyle = style;
            //RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            //RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            //RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //Lokasi
            cell = (XSSFCell)row.CreateCell(6);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(24, 24, 6, 7);
            sheet.AddMergedRegion(mergeCell);
            cell.SetCellValue("Lokasi");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.Alignment = HorizontalAlignment.Center;
            cell.CellStyle = style;
            RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            SetBorderCell(9, sheet, ref row, out cell, workbook);

            int iRow = 25;
            int number = 1;
            foreach(Asset a in tcr.Assets.Where(x => x.IsDeleted == false))
            {
                tempAsset = assets.Find(x => x.AssetId == a.AssetId);
                row = (XSSFRow)sheet.CreateRow(iRow);
                SetBorderCell(0, sheet, ref row, out cell, workbook);

                //no
                cell = (XSSFCell)row.CreateCell(1);
                cell.SetCellValue(number);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.BorderRight = style.BorderTop = style.BorderBottom = BorderStyle.Thin;
                style.Alignment = HorizontalAlignment.Center;
                cell.CellStyle = style;

                //Nama Asset
                cell = (XSSFCell)row.CreateCell(2);
                mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 2, 4);
                sheet.AddMergedRegion(mergeCell);
                cell.SetCellValue(tempAsset.Description);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                cell.CellStyle = style;
                RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
                RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);
                RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

                //Nilai
                cell = (XSSFCell)row.CreateCell(5);
                if (tcr.Currency == Currency.IDR.ToString())
                    cell.SetCellValue($"Rp {DisplayFormat.NumberFormatNoDecimal(tempAsset.ValueIdr)}");
                else
                    cell.SetCellValue($"${DisplayFormat.NumberFormatNoDecimal(tempAsset.ValueUsd)}");
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                style.BorderRight = style.BorderTop = style.BorderBottom = BorderStyle.Thin;
                cell.CellStyle = style;

                ////Asset Holder
                //cell = (XSSFCell)row.CreateCell(6);
                //mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 6, 7);
                //sheet.AddMergedRegion(mergeCell);
                //cell.SetCellValue(tcr.AssetHolder);
                //style = (XSSFCellStyle)workbook.CreateCellStyle();
                //style.Alignment = HorizontalAlignment.Center;
                //cell.CellStyle = style;
                //RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
                //RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);
                //RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

                //Lokasi
                cell = (XSSFCell)row.CreateCell(6);
                mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 6, 7);
                sheet.AddMergedRegion(mergeCell);
                cell.SetCellValue(tempAsset.Location);
                style = (XSSFCellStyle)workbook.CreateCellStyle();
                cell.CellStyle = style;
                RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
                RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);
                RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);
                SetBorderCell(9, sheet, ref row, out cell, workbook);

                iRow++;
                number++;
            }
            
            row = (XSSFRow)sheet.CreateRow(iRow);
            SetBorderCell(0, sheet, ref row, out cell, workbook);

            cell = (XSSFCell)row.CreateCell(1);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 1, 9);
            sheet.AddMergedRegion(mergeCell);
            RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            //total
            iRow += 1;
            row = (XSSFRow)sheet.CreateRow(iRow);
            SetBorderCell(0, sheet, ref row, out cell, workbook);

            cell = (XSSFCell)row.CreateCell(1);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 1, 4);
            sheet.AddMergedRegion(mergeCell);
            cell.SetCellValue("Total");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.Alignment = HorizontalAlignment.Center;
            cell.CellStyle = style;
            RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            cell = (XSSFCell)row.CreateCell(5);
            if (tcr.Currency == Currency.IDR.ToString())
                cell.SetCellValue($"Rp {DisplayFormat.NumberFormatNoDecimal(tcr.ValueIdr)}");
            else
                cell.SetCellValue($"${DisplayFormat.NumberFormatNoDecimal(tcr.ValueUsd)}");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.BorderRight = style.BorderBottom = BorderStyle.Thin;
            style.SetFont(fontBold);
            cell.CellStyle = style;

            cell = (XSSFCell)row.CreateCell(6);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 6, 9);
            sheet.AddMergedRegion(mergeCell);
            RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            iRow += 1;
            row = (XSSFRow)sheet.CreateRow(iRow);
            SetBorderCell(0, sheet, ref row, out cell, workbook);
            SetBorderCell(9, sheet, ref row, out cell, workbook);

            //new section
            iRow += 1;
            row = (XSSFRow)sheet.CreateRow(iRow);
            cell = (XSSFCell)row.CreateCell(0);
            cell.SetCellValue("III");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            style.BorderRight = style.BorderTop = BorderStyle.Thin;
            cell.CellStyle = style;

            mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 1, 9);
            sheet.AddMergedRegion(mergeCell);
            cell = (XSSFCell)row.CreateCell(1);
            cell.SetCellValue("KETERANGAN TAMBAHAN");
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.SetFont(fontBold);
            cell.CellStyle = style;
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderTop((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            iRow += 1;
            row = (XSSFRow)sheet.CreateRow(iRow);
            SetBorderCell(0, sheet, ref row, out cell, workbook);

            cell = (XSSFCell)row.CreateCell(1);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 1, 9);
            cell.SetCellValue(StripHTML(tcr.Notes));
            sheet.AddMergedRegion(mergeCell);
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);

            iRow += 1;
            row = (XSSFRow)sheet.CreateRow(iRow);
            cell = (XSSFCell)row.CreateCell(0);
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.BorderRight = style.BorderBottom = BorderStyle.Thin;
            cell.CellStyle = style;

            cell = (XSSFCell)row.CreateCell(1);
            mergeCell = new NPOI.SS.Util.CellRangeAddress(iRow, iRow, 1, 9);
            sheet.AddMergedRegion(mergeCell);
            RegionUtil.SetBorderBottom((int)BorderStyle.Thin, mergeCell, sheet, workbook);
            RegionUtil.SetBorderRight((int)BorderStyle.Thin, mergeCell, sheet, workbook);


            //ttd
            iRow += 3;
            row = (XSSFRow)sheet.CreateRow(iRow);
            cell = (XSSFCell)row.CreateCell(6);
            cell.SetCellValue(DateTime.Now.ToString(DisplayFormat.FullDateFormat));

            iRow += 1;
            row = (XSSFRow)sheet.CreateRow(iRow);
            cell = (XSSFCell)row.CreateCell(6);
            cell.SetCellValue("Manager HR,");

            iRow += 4;
            row = (XSSFRow)sheet.CreateRow(iRow);
            cell = (XSSFCell)row.CreateCell(6);
            cell.SetCellValue("{Nama Manager HR}");

            //write to byte[]
            MemoryStream ms = new MemoryStream();

            workbook.Write(ms);

            return ms.ToArray();
        }

        private static void SetBorderCell(int col, XSSFSheet sheet, ref XSSFRow rowObject, out XSSFCell cell, IWorkbook workbook) {
            //kamus
            XSSFCellStyle style;

            //algo
            cell = (XSSFCell)rowObject.CreateCell(col);
            style = (XSSFCellStyle)workbook.CreateCellStyle();
            style.BorderRight = BorderStyle.Thin;
            cell.CellStyle = style;
        }

        private static string StripHTML(string input)
        {
            if (input == null)
                return "";

            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public class TimeSpanHelper
        {
            public static List<string> GenerateTimeSpan()
            {
                List<string> options = new List<string>();
                DateTime start = DateTime.ParseExact("00:00", "HH:mm", null);
                DateTime end = DateTime.ParseExact("23:59", "HH:mm", null);
                TimeSpan interval = new TimeSpan(0, 1, 0); //interval every one minute

                while (start <= end)
                {
                    options.Add(start.ToString(DisplayFormat.SqlShortTimeFormat));
                    start = start.Add(interval);
                }

                return options;
            }
        }
    }
}