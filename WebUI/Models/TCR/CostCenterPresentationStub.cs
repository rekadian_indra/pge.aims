﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class CostCenterPresentationStub : BasePresentationStub<CostCenter, CostCenterPresentationStub>
    {

        public int CostCenterId { get; set; }
        public string Name { get; set; }
        public string AssetHolder { get; set; }

        public CostCenterPresentationStub() : base()
        {
            
        }

        public CostCenterPresentationStub(CostCenter dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}