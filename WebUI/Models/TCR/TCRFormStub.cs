﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class TCRFormStub : BaseFormStub<TCR, TCRFormStub>
    {
        public int TCRId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Status { get; set; }

        [DisplayName("Nama Proyek")]
        [StringLength(250, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [RegularExpression(@"([-a-zA-Z.0-9\s]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ProjectName { get; set; }

        [DisplayName("WBS")]
        [StringLength(200, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string WBS { get; set; }

        [DisplayName("Tanggal Mulai")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public System.DateTime StartDate { get; set; }

        [DisplayName("Tanggal Selesai")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public System.DateTime EndDate { get; set; }

        [AllowHtml]
        [DisplayName("Definisi Proyek")]
        [StringLength(500, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ProjectDefinition { get; set; }

        [DisplayName("Cost Center")]
        //[StringLength(200, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int CostCenter { get; set; }

        [DisplayName("Plant")]
        //[StringLength(200, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? Plant { get; set; }

        [DisplayName("Lokasi")]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int LocationId { get; set; }

        [DisplayName("Asset Holder")]
        [StringLength(150, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [RegularExpression(@"([-a-zA-Z.0-9\s]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string AssetHolder { get; set; }

        [DisplayName("Anggaran")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public double Budget { get; set; }

        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Currency { get; set; }

        [AllowHtml]
        [StringLength(500, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Notes { get; set; }

        [DisplayName("Realisasi Biaya (IDR)")]
        public double ValueIdr { get; set; }
        [DisplayName("Realisasi Biaya (USD)")]
        public double ValueUsd { get; set; }

        public List<int> AssetIds { get; set; }

        public TCRFormStub() : base()
        {
        }

        public TCRFormStub(TCR dbObject) : base(dbObject)
        {
            
        }

        public override void MapDbObject(TCR dbObject)
        {
            //lib

            //algoritma
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
            if (dbObject.TCRId == 0)
            {
                dbObject.IsDeleted = false;
            }
        }

        public void MapDbObjectAsset(TCR dbObject, List<Asset> assets)
        {
            if (AssetIds.Any())
            {
                dbObject.Assets.Clear();
                foreach (int i in AssetIds)
                {
                    dbObject.Assets.Add(assets.Where(x => x.AssetId == i).FirstOrDefault());
                }
            }
        }

        //public void MapDbObject(Crew dbObject, Company company)
        //{
        //    MapDbObject(dbObject);

        //    dbObject.Companies.Clear();
        //    dbObject.Companies.Add(company);
        //}

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
            //StatusOption = DisplayFormat.EnumToSelectList<CrewStatus>();
            SubmitDate = DateTime.Now;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            Status = StatusTCR.DRAFT.ToString();
        }

    }
}