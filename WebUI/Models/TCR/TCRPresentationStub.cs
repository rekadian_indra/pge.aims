﻿using Business.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class TCRPresentationStub : BasePresentationStub<TCR, TCRPresentationStub>
    {

        public int TCRId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Status { get; set; }
        public string ProjectName { get; set; }
        public string WBS { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public string ProjectDefinition { get; set; }
        public int CostCenter { get; set; }
        public int? Plant { get; set; }
        public string LocationId { get; set; }
        public string AssetHolder { get; set; }
        public double Budget { get; set; }
        public string Currency { get; set; }
        public string Notes { get; set; }

        public double ValueIdr { get; set; }
        public double ValueUsd { get; set; }

        public string StatusPretify { get; set; }

        public TCRPresentationStub() : base()
        {
            
        }

        public TCRPresentationStub(TCR dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            StatusAsset status = (StatusAsset)Enum.Parse(typeof(StatusAsset), dbObject.Status);
            StatusPretify = status.ToDescription();

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}