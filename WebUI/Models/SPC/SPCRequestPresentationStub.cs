﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business.Entities;
using Common.Enums;

namespace WebUI.Models
{
    public class SPCRequestPresentationStub : BasePresentationStub<SPCRequest, SPCRequestPresentationStub>
    {
        public int SPCRequestId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Status { get; set; }
        public string ScrapType { get; set; }
        public string NoRequestSPC005 { get; set; }
        public string NoRequestSPC011 { get; set; }
        public string Plant { get; set; }
        public string CostCenter { get; set; }
        public string AcquisitionType { get; set; }
        public Nullable<bool> DocABVN { get; set; }
        public string PreparedBy { get; set; }
        public string LegalRequestor { get; set; }        

        public SPCRequestPresentationStub() : base()
        {

        }

        public SPCRequestPresentationStub(SPCRequest dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            if (dbObject != null && dbObject.Status != null)
            Status = ((StatusSPC)Enum.Parse(typeof(StatusSPC), dbObject.Status)).ToDescription();
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}