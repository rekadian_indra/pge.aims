﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business.Entities;
using Business.Entities.Views;

namespace WebUI.Models
{
    public class SPCRequestAssetPresentationStub : BasePresentationStub<SPCRequestAsset, SPCRequestAssetPresentationStub>
    {
        public int SPCRequestId { get; set; }
        public int AssetId { get; set; }
        public int MappingAssetId { get; set; }

        public System.DateTime DocDate { get; set; }
        public System.DateTime PostingDate { get; set; }
        public System.DateTime AssetValueDate { get; set; }
        public string PostingPeriod { get; set; }
        public string TransactionType { get; set; }
        public string AssetTransaction { get; set; }
        
        public string AssetNumber { get; set; }
        public string SubNumber { get; set; }
        public string Description { get; set; }
        public double ValueIdr { get; set; }
        public double ValueUsd { get; set; }
        public string Location { get; set; }
        public string Photo { get; set; }      

        public SPCRequestAssetPresentationStub() : base()
        {

        }

        public SPCRequestAssetPresentationStub(SPCRequestAsset dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            if(dbObject!=null && dbObject.Asset != null)
            {
                Description = dbObject.Asset.Description;
                AssetNumber = dbObject.Asset.AssetNumber;
                if (!string.IsNullOrEmpty(dbObject.Asset.Photo))
                {
                    Photo = VirtualPathUtility.ToAbsolute(dbObject.Asset.Photo);
                }                
            }

        }

        public SPCRequestAssetPresentationStub(SPCRequestAsset dbObject, ViewMappingAsset viewDbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here
            if (dbObject != null && dbObject.Asset != null)
            {
                Description = dbObject.Asset.Description;
                AssetNumber = dbObject.Asset.AssetNumber;
                SubNumber = dbObject.Asset.SubNumber;
                ValueIdr = dbObject.Asset.ValueIdr;
                ValueUsd = dbObject.Asset.ValueUsd;
                //Location = dbObject.Asset.Location.Name;
                if (!string.IsNullOrEmpty(dbObject.Asset.Photo))
                {
                    Photo = VirtualPathUtility.ToAbsolute(dbObject.Asset.Photo);
                }
            }
            if(viewDbObject != null)
            {
                MappingAssetId = viewDbObject.MappingAssetId;
                Location = viewDbObject.Location;
            }

        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}