﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class SPCExcelStub : BaseImportStub
    {
        public SPCExcelStub()
        {
        }

        public XSSFWorkbook SetData(FileStream fileStream, SPCRequest dbItem, List<ViewMappingAsset> assets, Principal user)
        {
            //kamus
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet; XSSFRow row; XSSFCell cell; XSSFCellStyle style;
            const string _COMPANYCODE = "2022";
            int iterator; int no; int cellIterator;
            ViewMappingAsset temp;
            TCR tempTcr;
            string plantCode = dbItem.Plant1 != null ? dbItem.Plant1.Code : string.Empty;

            //algo
            workbook = new XSSFWorkbook(fileStream);
            if(dbItem.NoRequestSPC011 == null || dbItem.NoRequestSPC011 == "")
            {
                workbook.RemoveSheetAt(2);
                workbook.RemoveSheetAt(2);
            }
            //List<int> hiddenCol = new List<int>() { 11, 12, 13, 14, 15, 20, 21, 22, 25, 26, 27, 28, 29, 30,31, 32, 33,
            //34, 40, 41, 44, 45, 46};

            //sheet SPC 05
            sheet = (XSSFSheet)workbook.GetSheetAt(0);

            sheet.GetRow(7).GetCell(3).SetCellValue(user.UserName);
            sheet.GetRow(8).GetCell(3).SetCellValue(DateTime.Now.ToString(DisplayFormat.CompactDateFormat));
            sheet.GetRow(12).GetCell(3).SetCellValue(dbItem.CostCenter);
            sheet.GetRow(14).GetCell(3).SetCellValue(dbItem.NoRequestSPC005);
            sheet.GetRow(15).GetCell(3).SetCellValue(plantCode);

            if(dbItem.ScrapType == ScrapType.NON_CAPITALIZATION.ToString())
            {
                sheet.GetRow(8).GetCell(6).SetCellValue("X");
            }
            else
            {
                sheet.GetRow(9).GetCell(6).SetCellValue("X");
            }

            if (dbItem.AcquisitionType == AcquisitionType.BEFORE.ToString())
            {
                sheet.GetRow(14).GetCell(6).SetCellValue("X");
            }
            else
            {
                sheet.GetRow(15).GetCell(6).SetCellValue("X");
            }

            sheet.GetRow(41).GetCell(6).SetCellValue(dbItem.LegalRequestor);

            //lampiran sheet spc 05
            sheet = (XSSFSheet)workbook.GetSheetAt(1);
            sheet.GetRow(2).GetCell(3).SetCellValue($": {dbItem.NoRequestSPC005}");
            sheet.GetRow(3).GetCell(3).SetCellValue($": {DateTime.Now.ToString(DisplayFormat.CompactDateFormat)}");

            //data auc
            no = 1; iterator = 7;
            IEnumerable<string> wbsElements = new List<string>();
            List<MappingAssetActualCost> ma =  new List<MappingAssetActualCost>();
            foreach (SPCRequestAsset sra in dbItem.SPCRequestAssets)
            {
                if (sra.Asset.MappingAssets.Any())
                {
                    wbsElements = sra.Asset.MappingAssets.First().MappingAssetActualCosts.Select(x => x.WBSElement).Distinct();

                    foreach(string x in wbsElements)
                    {
                        ma = sra.Asset.MappingAssets.First().MappingAssetActualCosts.Where(y => y.WBSElement == x).ToList();

                        cellIterator = 1;
                        row = (XSSFRow)sheet.CreateRow(iterator);
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(no);
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(_COMPANYCODE);
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(ma.First().AUC.AssetNumber);
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(ma.First().AUC.SubNumber);
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(sra.DocDate.ToString(DisplayFormat.ShortDateFormatId));
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(sra.PostingDate.ToString(DisplayFormat.ShortDateFormatId));
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(DateTime.Now.ToString(DisplayFormat.ShortDateFormatId));
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue($"CC_{ma.First().AUC.AssetNumber}_to_{sra.Asset.AssetNumber}_2022");
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(sra.PostingPeriod);
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(sra.TransactionType.Substring(sra.TransactionType.Length - 3));
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(ma.First().AUC.AssetNumber);
                        
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        cell.SetCellValue(_COMPANYCODE);

                        temp = assets.Where(y => y.AssetId == sra.AssetId).FirstOrDefault();
                        cell = (XSSFCell)row.CreateCell(cellIterator++);
                        style = (XSSFCellStyle)workbook.CreateCellStyle();
                        style.SetDataFormat(HSSFDataFormat.GetBuiltinFormat("#,##0"));
                        cell.CellStyle = style;
                        cell.SetCellValue(ma.Sum(y => y.ValueIdr));

                        tempTcr = sra.Asset.TCRs.FirstOrDefault();
                        if(tempTcr.EndDate.Year != DateTime.Now.Year)
                        {
                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue("X");
                        }
                        else
                        {
                            cellIterator += 1;
                            cell = (XSSFCell)row.CreateCell(cellIterator);
                            cell.SetCellValue("X");
                        }

                        no++;
                        iterator++;
                    }
                }
            }

            //sheet spc 11
            if (dbItem.NoRequestSPC011 != null && dbItem.NoRequestSPC011 != "")
            {
                sheet = (XSSFSheet)workbook.GetSheetAt(2);

                sheet.GetRow(7).GetCell(3).SetCellValue(user.UserName);
                sheet.GetRow(8).GetCell(3).SetCellValue(DateTime.Now.ToString(DisplayFormat.CompactDateFormat));
                sheet.GetRow(12).GetCell(3).SetCellValue(dbItem.CostCenter);
                sheet.GetRow(14).GetCell(3).SetCellValue(dbItem.NoRequestSPC011);
                sheet.GetRow(15).GetCell(3).SetCellValue(plantCode);

                sheet.GetRow(43).GetCell(6).SetCellValue(dbItem.LegalRequestor);

                sheet = (XSSFSheet)workbook.GetSheetAt(3);
                sheet.GetRow(2).GetCell(3).SetCellValue($": {dbItem.NoRequestSPC011}");
                sheet.GetRow(3).GetCell(3).SetCellValue($": {DateTime.Now.ToString(DisplayFormat.CompactDateFormat)}");

                //data auc
                no = 1; iterator = 6;
                wbsElements = new List<string>();
                ma = new List<MappingAssetActualCost>();
                foreach (SPCRequestAsset sra in dbItem.SPCRequestAssets)
                {
                    if (sra.Asset.MappingAssets.Any())
                    {
                        wbsElements = sra.Asset.MappingAssets.First().MappingAssetActualCosts.Select(x => x.WBSElement).Distinct();

                        foreach (string x in wbsElements)
                        {
                            ma = sra.Asset.MappingAssets.First().MappingAssetActualCosts.Where(y => y.WBSElement == x).ToList();

                            cellIterator = 1;
                            row = (XSSFRow)sheet.CreateRow(iterator);
                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(no);

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(_COMPANYCODE);

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(sra.Asset.AssetNumber);

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(sra.Asset.SubNumber);

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(sra.DocDate.ToString(DisplayFormat.ShortDateFormatId));

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(sra.PostingDate.ToString(DisplayFormat.ShortDateFormatId));

                            //fiscal period
                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(sra.PostingPeriod);

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(sra.AssetTransaction.Substring(sra.AssetTransaction.Length - 3));

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.SetDataFormat(HSSFDataFormat.GetBuiltinFormat("#,##0"));
                            cell.CellStyle = style;
                            cell.SetCellValue(ma.Sum(y => y.ValueIdr));

                            tempTcr = sra.Asset.TCRs.FirstOrDefault();
                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(tempTcr.EndDate.ToString(DisplayFormat.ShortDateFormatId));

                            cellIterator++;

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue($"CC_{ma.First().AUC.AssetNumber}_to_{sra.Asset.AssetNumber}_2022");

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue("AA");

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(_COMPANYCODE);

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            cell.SetCellValue(ma.First().AUC.AssetNumber);

                            cell = (XSSFCell)row.CreateCell(cellIterator++);
                            style = (XSSFCellStyle)workbook.CreateCellStyle();
                            style.SetDataFormat(HSSFDataFormat.GetBuiltinFormat("#,##0"));
                            cell.CellStyle = style;
                            cell.SetCellValue(ma.Sum(y => y.ValueUsd));

                            //cell = (XSSFCell)row.CreateCell(cellIterator++);
                            //cell.SetCellValue(ma.ActualCost.DocNumber);
                        }
                    }
                }
            }

            return workbook;
        }


        private static string StripHTML(string input)
        {
            if (input == null)
                return "";

            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public class TimeSpanHelper
        {
            public static List<string> GenerateTimeSpan()
            {
                List<string> options = new List<string>();
                DateTime start = DateTime.ParseExact("00:00", "HH:mm", null);
                DateTime end = DateTime.ParseExact("23:59", "HH:mm", null);
                TimeSpan interval = new TimeSpan(0, 1, 0); //interval every one minute

                while (start <= end)
                {
                    options.Add(start.ToString(DisplayFormat.SqlShortTimeFormat));
                    start = start.Add(interval);
                }

                return options;
            }
        }
    }
}