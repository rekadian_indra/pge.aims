﻿using Business.Entities;
using Business.Entities.Views;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Web;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class SPCAssetOptionsPresentationStub : BasePresentationStub<ViewMappingAsset, SPCAssetOptionsPresentationStub>
    {
        public int AssetId { get; set; }
        public int MappingAssetId { get; set; }
        public string AssetNumber { get; set; }
        public string SubNumber { get; set; }
        public string Description { get; set; }
        public double ValueIdr { get; set; }
        public double ValueUsd { get; set; }
        public string Location { get; set; }
        public string Photo { get; set; }

        public SPCAssetOptionsPresentationStub() : base()
        {
        }

        public SPCAssetOptionsPresentationStub(ViewMappingAsset dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here          
            if (!string.IsNullOrEmpty(dbObject.Photo))
            {
                Photo = VirtualPathUtility.ToAbsolute(dbObject.Photo);
            }
        }            

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}