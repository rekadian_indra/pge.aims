﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using Business.Entities;
using System.Web.Mvc;
using Common.Enums;

namespace WebUI.Models
{
    public class SPCRequestAssetFormStub : BaseFormStub<SPCRequestAsset, SPCRequestAssetFormStub>
    {
        public int SPCRequestId { get; set; }
        [DisplayName("Asset")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int AssetId { get; set; }
        [DisplayName("Document Date")]        
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public System.DateTime DocDate { get; set; }
        [DisplayName("Posting Date")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public System.DateTime PostingDate { get; set; }
        [DisplayName("Asset Value Date")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public System.DateTime AssetValueDate { get; set; }
        [DisplayName("Posting Period")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string PostingPeriod { get; set; }
        [DisplayName("Transaction Type")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]        
        public string TransactionType { get; set; }
        [DisplayName("Asset Transaction")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string AssetTransaction { get; set; }
        public List<SelectListItem> OptTransactionType { get; set; }
        public List<SelectListItem> OptAssetTransaction { get; set; }

        public SPCRequestAssetFormStub() : base()
        {
            DocDate = DateTime.Now;
            PostingDate = DateTime.Now;
            AssetValueDate = DateTime.Now;
        }

        public SPCRequestAssetFormStub(SPCRequestAsset dbObject) : base(dbObject)
        {

        }

        public override void MapDbObject(SPCRequestAsset dbObject)
        {
            //lib

            //algoritma
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
        }             

        protected override void Init()
        {
            OptTransactionType = DisplayFormat.EnumToSelectList<TransactionType>();
            OptAssetTransaction = DisplayFormat.EnumToSelectList<AssetTransaction>();
        }

    }
}