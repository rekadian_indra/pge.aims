﻿using Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebUI.Infrastructure;
using Common.Enums;
using Newtonsoft.Json;
using Business.Infrastructure;
using System.Web.Mvc;
using Business.Entities.Views;

namespace WebUI.Models
{
    public class SPCRequestFormStub : BaseFormStub<SPCRequest, SPCRequestFormStub>
    {
        public int SPCRequestId { get; set; }

        [DisplayName("Plant")]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public int? PlantId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Status { get; set; }
        [DisplayName("Jenis Scrap")]
        //[StringLength(255, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ScrapType { get; set; }
        [DisplayName("No.Request SPC - 005")]
        [StringLength(100, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string NoRequestSPC005 { get; set; }
        [DisplayName("No.Request SPC - 011")]
        [StringLength(100, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string NoRequestSPC011 { get; set; }
        [DisplayName("Plant")]
        [StringLength(100, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Plant { get; set; }
        [DisplayName("Cost Center")]
        //[StringLength(100, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string CostCenter { get; set; }
        [DisplayName("Jenis Akuisisi")]
        [StringLength(50, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string AcquisitionType { get; set; }
        [DisplayName("Terdapat Dok Lampiran ABZON ")]        
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public bool Doc { get; set; }
        public Nullable<bool> DocABVN { get; set; }
        public string PreparedBy { get; set; }
        [DisplayName("Legal Requestor")]
        [StringLength(100, ErrorMessageResourceName = GlobalErrorField.MaxStringLength, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string LegalRequestor { get; set; }        
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        //[Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [DisplayName("Asset")]
        public string SPCRequestAssets { get; set; }
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string TempDel { get; set; }

        public List<SelectListItem> OptionsScrapType { get; set; }
        public List<SelectListItem> OptAcquisitionType { get; set; }

        public SPCRequestFormStub() : base()
        {
            //MDMAsset = "";
            PreparedBy = User.Identity.Name;
            SubmitDate = DateTime.Now;
        }

        public SPCRequestFormStub(SPCRequest dbObject) : base(dbObject)
        {
            if(dbObject != null && dbObject.SPCRequestAssets != null)
            {
                SPCRequestAssets = JsonConvert.SerializeObject(ListMapper.MapList<SPCRequestAsset, SPCRequestAssetPresentationStub>(dbObject.SPCRequestAssets));
                Doc = dbObject.DocABVN.HasValue ? dbObject.DocABVN.Value : false;                
            }
        }

        public SPCRequestFormStub(SPCRequest dbObject, List<ViewMappingAsset> listViewMappings) : base(dbObject)
        {
            if (dbObject != null && dbObject.SPCRequestAssets != null)
            {
                ViewMappingAsset db;
                List<SPCRequestAssetPresentationStub> models = new List<SPCRequestAssetPresentationStub>();
                foreach (SPCRequestAsset dbSPCReqAsset in dbObject.SPCRequestAssets)
                {                    
                    if (listViewMappings.HasValue())
                    {
                        db = listViewMappings.Where(m => m.AssetId == dbSPCReqAsset.AssetId).FirstOrDefault();
                    }
                    else
                    {
                        db = new ViewMappingAsset();
                    }
                    SPCRequestAssetPresentationStub model = new SPCRequestAssetPresentationStub(dbSPCReqAsset, db);
                    models.Add(model);
                }
                //SPCRequestAssets = JsonConvert.SerializeObject(ListMapper.MapList<SPCRequestAsset, SPCRequestAssetPresentationStub>(dbObject.SPCRequestAssets));
                SPCRequestAssets = JsonConvert.SerializeObject(models);
                Doc = dbObject.DocABVN.HasValue ? dbObject.DocABVN.Value : false;
            }
        }

        public override void MapDbObject(SPCRequest dbObject)
        {
            //lib

            //algoritma
            
            if(SPCRequestId == 0)
            {
                Status = StatusSPC.DRAFT.ToString();
                SubmitDate = DateTime.Now;
            }
            DocABVN = Doc;
            base.MapDbObject(dbObject);
            //TODO: Manual mapping object here
        }

        protected override void Init()
        {
            OptionsScrapType = DisplayFormat.EnumToSelectList<ScrapType>();
            OptAcquisitionType = DisplayFormat.EnumToSelectList<AcquisitionType>();
        }

    }
}