﻿using Business.Entities;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using WebUI.Extension;

namespace WebUI.Models
{
    public abstract class BaseImportStub
    {
        protected const string MSG_REQUIRED = "File {0} at Row {1}, Column [{2}] is Required.";
        protected const string MSG_MAX_LENGTH = "File {0} at Row {1}, Column [{2}] max. length {3} character.";
        protected const string MSG_NUMBER = "File {0} at Row {1}, Column [{2}] must be number.";
        protected const string MSG_DATE = "File {0} at Row {1}, Column [{2}] not valid string date format.";
        protected const string MSG_RELATION = "File {0} at Row {1}, Column [{2}] relation not found. Upload data {2} first.";
        protected const string MSG_TEMPLATE = "File {0} upload excel failed, wrong template.";
        protected const string MSG_ERROR = "File {0} upload excel failed, please contact Administrator.";
        protected const string MSG_NOTFOUND_WBS = "File {0} at Sheet {1}, Row {2}, Column [{3}] relation not found. Please check worksheet.";

        [DisplayName("File Excel")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string FilePath { get; set; }
        public List<string> ErrParseExcel { get; set; }

        protected List<ColumnHelper> Columns
        {
            get
            {
                return GetColumns();
            }
        }
        protected bool Valid { get; set; }        

        protected string FileName { get; set; }

        protected HttpServerUtility Server
        {
            get
            {
                if (HttpContext.Current == null)
                    return null;

                return HttpContext.Current.Server;
            }
        }

        public BaseImportStub()
        {
        }

        protected string Trim(string s)
        {
            if (!string.IsNullOrEmpty(s))
                s = Regex.Replace(s, @"\s+-", "-");

            return s;
        }

        protected virtual Task MappingObjectAsync(XSSFSheet sheet, int row)
        {
            throw new NotImplementedException();
        }

        protected virtual List<ColumnHelper> GetColumns()
        {
            throw new NotImplementedException();
        }

        protected virtual bool CheckTemplate(XSSFSheet sheet, List<ColumnHelper> columns)
        {
            int colTemplate = sheet.GetRow(0) != null ? sheet.GetRow(0).Cells.Count() : 0;            
            int counter = 0;
            string temp = null;
            bool any = false;

            for (int i = 0; i < colTemplate; i++)
            {
                if (sheet.GetRow(0).GetCell(i) != null)
                {
                    sheet.GetRow(0).GetCell(i).SetCellType(CellType.String);
                    temp = sheet.GetRow(0).GetCell(i).StringCellValue;
                    if (!string.IsNullOrEmpty(temp))
                    {
                        any = columns.Any(x => x.ColumnName.ToLower() == temp.ToLower());
                        if (any)
                            ++counter;
                    }
                }
            }

            if (counter == columns.Count)
                return true;

            //ErrParseExcel.Add(string.Format(MSG_TEMPLATE, FileName));

            return false;
        }

        protected virtual T ParsingRequiredCell<T>(XSSFSheet sheet, ColumnHelper column, int row, ref int errCount)
        {
            int col = column.ColumnNum;
            int curRow = row + 1;
            string temp = string.Empty;
            object result = new object();
            Type type = typeof(T);

            if (sheet.GetRow(row) != null && sheet.GetRow(row).GetCell(col) != null)
            {
                sheet.GetRow(row).GetCell(col).SetCellType(CellType.String);
                temp = sheet.GetRow(row).GetCell(col).StringCellValue;
                if (!string.IsNullOrEmpty(temp))
                {
                    if (temp.Length <= column.MaxLength)
                    {
                        if (type == typeof(string))
                            result = temp;
                        else if (type == typeof(int) || type == typeof(double) || type == typeof(float) || type == typeof(short) || type == typeof(decimal))
                        {
                            try
                            {
                                if (type == typeof(int))
                                    result = int.Parse(temp);
                                else if (type == typeof(double))
                                    result = double.Parse(temp);
                                else if (type == typeof(float))
                                    result = float.Parse(temp);
                                else if (type == typeof(short))
                                    result = short.Parse(temp);
                                else if (type == typeof(decimal))
                                    result = decimal.Parse(temp);
                            }
                            catch
                            {
                                ++errCount;
                                InstanceNullType<T>(ref result);
                                ErrParseExcel.Add(string.Format(MSG_NUMBER, FileName, curRow, column.ColumnName));
                            }
                        }
                        else if (type == typeof(DateTime) || type == typeof(DateTime?))
                        {
                            try
                            {
                                result = temp.ParseExcelDate();
                            }
                            catch
                            {
                                ++errCount;
                                InstanceNullType<T>(ref result);
                                ErrParseExcel.Add(string.Format(MSG_DATE, FileName, curRow, column.ColumnName));
                            }
                        }
                        else
                            throw new NotSupportedException(type.ToString());
                    }
                    else
                    {
                        ++errCount;
                        InstanceNullType<T>(ref result);
                        ErrParseExcel.Add(string.Format(MSG_MAX_LENGTH, FileName, curRow, column.ColumnName, column.MaxLength));
                    }
                }
                else
                {
                    ++errCount;
                    InstanceNullType<T>(ref result);
                    ErrParseExcel.Add(string.Format(MSG_REQUIRED, FileName, curRow, column.ColumnName));
                }
            }
            else
            {
                ++errCount;
                InstanceNullType<T>(ref result);
                ErrParseExcel.Add(string.Format(MSG_REQUIRED, FileName, curRow, column.ColumnName));
            }

            return (T)result;
        }

        protected virtual T ParsingCell<T>(XSSFSheet sheet, ColumnHelper column, int row, ref int errCount)
        {
            int col = column.ColumnNum;
            int curRow = row + 1;
            string temp = string.Empty;
            object result = new object();
            Type type = typeof(T);

            if (sheet.GetRow(row) != null && sheet.GetRow(row).GetCell(col) != null)
            {
                sheet.GetRow(row).GetCell(col).SetCellType(CellType.String);
                temp = sheet.GetRow(row).GetCell(col).StringCellValue;
                if (!string.IsNullOrEmpty(temp))
                {
                    if (temp.Length <= column.MaxLength)
                    {
                        if (type == typeof(string))
                            result = temp;
                        else if (type == typeof(int) || type == typeof(double) || type == typeof(float) || type == typeof(short) || type == typeof(decimal))
                        {
                            try
                            {
                                if (type == typeof(int))
                                    result = int.Parse(temp);
                                else if (type == typeof(double))
                                    result = double.Parse(temp);
                                else if (type == typeof(float))
                                    result = float.Parse(temp);
                                else if (type == typeof(short))
                                    result = short.Parse(temp);
                                else if (type == typeof(decimal))
                                    result = decimal.Parse(temp);
                            }
                            catch
                            {
                                ++errCount;
                                InstanceNullType<T>(ref result);
                                ErrParseExcel.Add(string.Format(MSG_NUMBER, FileName, curRow, column.ColumnName));
                            }
                        }
                        else if (type == typeof(int?) || type == typeof(double?) || type == typeof(float?) || type == typeof(short?) || type == typeof(decimal?))
                        {
                            try
                            {
                                if (type == typeof(int?))
                                {
                                    if (int.Parse(temp) == 0)
                                        result = null;
                                    else
                                        result = int.Parse(temp);
                                }
                                else if (type == typeof(double?))
                                {
                                    if (double.Parse(temp) == 0)
                                        result = null;
                                    else
                                        result = double.Parse(temp);
                                }
                                else if (type == typeof(float?))
                                {
                                    if (float.Parse(temp) == 0)
                                        result = null;
                                    else
                                        result = float.Parse(temp);
                                }
                                else if (type == typeof(short?))
                                {
                                    if (short.Parse(temp) == 0)
                                        result = null;
                                    else
                                        result = short.Parse(temp);
                                }
                                else if (type == typeof(decimal?))
                                {
                                    if (decimal.Parse(temp) == 0)
                                        result = null;
                                    else
                                        result = decimal.Parse(temp);
                                }

                            }
                            catch
                            {
                                ++errCount;
                                InstanceNullType<T>(ref result);
                                ErrParseExcel.Add(string.Format(MSG_NUMBER, FileName, curRow, column.ColumnName));
                            }
                        }
                        else if (type == typeof(DateTime) || type == typeof(DateTime?))
                        {
                            try
                            {
                                result = temp.ParseExcelDate();
                                if ((DateTime)result == DateTime.MinValue)
                                {
                                    ++errCount;
                                    ErrParseExcel.Add(string.Format(MSG_DATE, FileName, curRow, column.ColumnName));
                                }
                            }
                            catch
                            {
                                ++errCount;
                                InstanceNullType<T>(ref result);
                                ErrParseExcel.Add(string.Format(MSG_DATE, FileName, curRow, column.ColumnName));
                            }
                        }
                        else
                            throw new NotSupportedException(type.ToString());
                    }
                    else
                    {
                        ++errCount;
                        InstanceNullType<T>(ref result);
                        ErrParseExcel.Add(string.Format(MSG_MAX_LENGTH, FileName, curRow, column.ColumnName, column.MaxLength));
                    }
                }
                else
                    InstanceNullType<T>(ref result);
            }
            else
                InstanceNullType<T>(ref result);

            return (T)result;
        }

        protected virtual T FindRelation<T, U>(IEnumerable<U> list, ColumnHelper column, int row, string code, ref int errCount, bool required = false)
        {
            int curRow = row + 1;
            object result = new object();
            bool success = false;
            Type type = typeof(U);

            if (type == typeof(AUC))
            {

                var temp = list.Cast<AUC>().FirstOrDefault(n => n.WBSElement.ToLower() == code.ToLower());
                if (temp != null)
                {
                    result = temp.WBSElement;
                    success = true;
                }
                else
                    InstanceNullType<T>(ref result);
            }           
            else
                throw new NotSupportedException(type.ToString());

            if (!success && required)
            {
                ++errCount;
                ErrParseExcel.Add(string.Format(MSG_RELATION, FileName, curRow, column.ColumnName));
            }

            return (T)result;
        }

        private void InstanceNullType<T>(ref object o)
        {
            Type t = typeof(T);

            if (t == typeof(string) || t == typeof(int?) || t == typeof(double?) || t == typeof(DateTime?) || t == typeof(short?) || t == typeof(decimal?))
                o = null;
            else if (t == typeof(int) || t == typeof(double) || t == typeof(short) || t == typeof(decimal))
                o = 0;
            else if (t == typeof(DateTime))
                o = DateTime.MinValue;
            else
                throw new NotSupportedException(t.ToString());
        }

        protected class ColumnHelper
        {
            public int ColumnNum { get; set; }
            public string ColumnName { get; set; }
            public int MaxLength { get; set; }

            protected ColumnHelper()
            {
            }

            public static ColumnHelper SetColumn(int num, string name, int length = 500)
            {
                if (length > 500)
                    throw new NotSupportedException("Value of length must <= 500");
                return new ColumnHelper
                {
                    ColumnNum = num,
                    ColumnName = name,
                    MaxLength = length
                };
            }
        }
    }
}