﻿using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public partial class LogOnModel
    {
        [Display(Name = "User ID")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        [Required]
        public string DeviceId { get; set; }

        public bool EnablePasswordReset { get; set; }
    }
}
