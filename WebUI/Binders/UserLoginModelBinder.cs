﻿using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Binders
{
    public class UserLoginModelBinder : IModelBinder
    {
        /*
         * If WebUI.Infrastructure.Principal is set, this useless
         */
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            Business.Models.SGUser userLogin = (Business.Models.SGUser)controllerContext.HttpContext.Session[AppSession.UserLogin];

            return userLogin;
        }
    }
}