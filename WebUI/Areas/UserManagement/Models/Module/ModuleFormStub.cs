﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebUI.Infrastructure;

namespace WebUI.Models
{
    public class ModuleFormStub
    {
        public Guid ModuleId { get; set; }

        [Display(Name = "Module Name")]
        //[RegularExpression(@"([a-zA-Z-._0-9]*)", ErrorMessageResourceName = GlobalErrorField.Symbol, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Remote(ActionSite.ModuleNameExists, ControllerSite.Module, AreaSite.UserManagement, AdditionalFields = "CurrentModuleName", ErrorMessageResourceName = GlobalErrorField.Unique, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        [Required(ErrorMessageResourceName = GlobalErrorField.Required, ErrorMessageResourceType = typeof(Resources.AppGlobalError))]
        public string ModuleName { get; set; }
        [Display(Name = "Parent Module")]
        public Guid? ParentModuleId { get; set; }
        public string CurrentModuleName { get; set; }

        public ModuleFormStub()
        {
            ModuleId = Guid.NewGuid();
        }
        

        public ModuleFormStub(Module dbObject) : this()
        {
            //TO DO: map db object to model
            ObjectMapper.MapObject<Module, ModuleFormStub>(dbObject, this);

            CurrentModuleName = dbObject.ModuleName;
        }

        public virtual void MapDbObject(Module dbObject)
        {
            //TO DO: map model to db object
            ObjectMapper.MapObject<ModuleFormStub, Module>(this, dbObject);
        }
    }
    
}