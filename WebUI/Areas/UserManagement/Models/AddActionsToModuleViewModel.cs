﻿using System.Web.Mvc;

namespace WebUI.Areas.UserManagement.Models
{
    public class AddActionsToModuleViewModel
    {
        public string GUID { get; set; }
        public string ModuleName { get; set; }
        public SelectList AvailableActions { get; set; }
        public SelectList AddedActions { get; set; }
    }
}
