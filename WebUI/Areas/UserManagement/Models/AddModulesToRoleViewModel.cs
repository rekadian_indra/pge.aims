﻿using System.Collections.Generic;

namespace WebUI.Areas.UserManagement.Models
{
    public class AddModulesToRoleViewModel
    {
        public string GUID { get; set; }
        public string RoleName { get; set; }

        public List<Business.Entities.Action> Actions { get; set; }

        public string Render { get; set; }

        //public SelectList AvailableActions { get; set; }
        //public SelectList AddedActions { get; set; }
    }
}
