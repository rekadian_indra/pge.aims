﻿using System;
using System.Collections.Generic;

namespace WebUI.Models
{
    public class AssignRoleStub
    {
        public Guid UserId { get; set; }
        public List<RoleFormStub> Roles { get; set; }

        public AssignRoleStub()
        {
			Roles = new List<RoleFormStub>();
        }
    }
}