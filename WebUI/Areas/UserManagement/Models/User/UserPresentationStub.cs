﻿using Business.Entities;
using Business.Entities.Views;
using Common.Enums;
using System;
using WebUI.Models;

namespace WebUI.Models
{
    public class UserPresentationStub : BasePresentationStub<ViewUserArea, UserPresentationStub>
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public Nullable<int> AreaId { get; set; }
        public string Name { get;set;}
        public UserPresentationStub() : base()
        {
        }

        //public UserPresentationStub(User dbObject) : base(dbObject)
        //{
        //    //TODO: Manual mapping object here
        //    Membership membership = dbObject.Membership;

        //    if (membership != null)
        //    {
        //        Email = membership.Email;
        //        CreateDate = membership.CreateDate.ToLocalDateTime();
        //        LastLoginDate = membership.LastLoginDate.ToLocalDateTime();
        //    }
        //}

        public UserPresentationStub(ViewUserArea dbObject) : base(dbObject)
        {
            //TODO: Manual mapping object here            
        }

        protected override void Init()
        {
            //TODO: Set the init object here to be called in all constructor
        }
    }
}