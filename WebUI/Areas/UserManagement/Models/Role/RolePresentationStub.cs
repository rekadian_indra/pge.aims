﻿using Business.Entities;
using Business.Infrastructure;
using System;

namespace WebUI.Models
{
    public class RolePresentationStub
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }

        public RolePresentationStub()
        {
        }

        public RolePresentationStub(Role dbObject) : this()
        {
            ObjectMapper.MapObject<Role, RolePresentationStub>(dbObject, this);
        }
    }
}
