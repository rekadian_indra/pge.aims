using Business.Abstract;
using Common.Enums;
using SecurityGuard.Interfaces;
using SecurityGuard.Services;
using WebUI.Areas.UserManagement.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using WebUI.Controllers;
using WebUI.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using Business.Entities;
using MvcSiteMapProvider;
using Newtonsoft.Json;
using Business.Infrastructure;
using WebUI.Models;

namespace WebUI.Areas.UserManagement.Controllers
{
    [AuthorizeUser(Roles = new object[] { UserRole.Superadmin})]
    public partial class RoleController : BaseController<RoleFormStub>
    {
        private IRoleService RoleService { get; set; }
        private IActionRepository RepoAction { get; set; }        
        private IModulesInRoleRepository RepoModulesInRole { get; set; }
        private IApplicationRepository RepoApplication { get; set; }

        public RoleController(
            IModuleRepository repoModule,
            IActionRepository repoAction,
            IRoleRepository repoRole,
            IModulesInRoleRepository repoModulesInRole,
            IApplicationRepository repoApplication
        )
        {
            RoleService = new RoleService(Roles.Provider);

            RepoModule = repoModule;
            RepoAction = repoAction;
            RepoRole = repoRole;
            RepoModulesInRole = repoModulesInRole;
            RepoApplication = repoApplication;
        }

        [MvcSiteMapNode(Title = TitleSite.Role, Area = AreaSite.UserManagement, ParentKey = KeySite.DashboardUserManagement, Key = KeySite.IndexRole)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        public override async Task<ActionResult> Create()
        {
            RoleFormStub model = new RoleFormStub
            {
                ApplicationId = await RepoApplication.GetApplicationIdAsync(System.Web.Security.Membership.ApplicationName),
                RoleId = Guid.NewGuid()
            };

            return PartialView("_Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Create(RoleFormStub model)
        {
            ResponseModel response = new ResponseModel(true);
            if (ModelState.IsValid)
            {
                Role dbObject = new Role();

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoRole.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.CreateSuccess).ToString();
                response.Message = string.Format(template, model.RoleName.ToString());
            }
            else
            {
                response.SetFail("Please fill required field.");
            }

            await Task.Delay(0);

            return Json(response);
        }

        public override async Task<ActionResult> Edit(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            Guid primaryKey = id.ElementAt(0).ToGuid();
            Role dbObject = await RepoRole.FindByPrimaryKeyAsync(primaryKey);
            RoleFormStub model = new RoleFormStub(dbObject);

            return PartialView("_Form", model);
        }

        [HttpPost]
        public override async Task<ActionResult> Edit(RoleFormStub model)
        {
            ResponseModel response = new ResponseModel(true);
            if (ModelState.IsValid)
            {
                Role dbObject = await RepoRole.FindByPrimaryKeyAsync(model.RoleId);

                //map dbObject
                model.MapDbObject(dbObject);

                //save dbObject
                await RepoRole.SaveAsync(dbObject);

                //message
                string template = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.EditSuccess).ToString();
                response.Message = string.Format(template, model.RoleName.ToString());
            }
            else
            {
                response.SetFail("Please fill required field.");
            }

            await Task.Delay(0);

            return Json(response);
        }

        [HttpPost]
        public override async Task<ActionResult> Delete(params object[] id)
        {
            if (id == null)
                throw new ArgumentNullException();

            Guid primaryKey = id.ElementAt(0).ToGuid();
            Role dbObject = await RepoRole.FindByPrimaryKeyAsync(primaryKey);
            ResponseModel response = new ResponseModel(true);

            if (!(await RepoRole.DeleteAsync(dbObject)))
            {
                string message = HttpContext.GetGlobalResourceObject(GlobalResourceType.AppGlobalMessage, GlobalMessageField.DeleteFailed).ToString();
                response.SetFail(message);
            }

            return Json(response);
        }

        public async Task<JsonResult> RoleNameExists(string RoleName, string CurrentRoleName)
        {
            //lib
            bool exist;

            //algorithm
            if (string.IsNullOrWhiteSpace(RoleName))
            {
                exist = true;
            }
            else if (RoleName.ToLower() == CurrentRoleName.ToLower())
            {
                exist = false;
            }
            else
            {
                FilterQuery filter = new FilterQuery(
                    Field.RoleName,
                    FilterOperator.Equals,
                    RoleName
                );
                Role role = await RepoRole.FindAsync(filter);

                exist = role != null;
            }

            return Json(!exist, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> AssignModules(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            Guid primaryKey = id.ToGuid();
            Role dbObject = await RepoRole.FindByPrimaryKeyAsync(primaryKey);
            RoleFormStub model = new RoleFormStub(dbObject);

            return PartialView("_AssignModules", model);
        }

        [HttpPost]
        public async Task<ActionResult> AssignModules(AssignModuleStub model)
        {
            ResponseModel response = new ResponseModel(true);
            Role role = await RepoRole.FindByPrimaryKeyAsync(model.RoleId);
            List<Module> modules = new List<Module>();

            foreach (ModuleFormStub m in model.Modules)
            {
                Module module = new Module();

                m.MapDbObject(module);
                modules.Add(module);
            }

            await RepoRole.AssignModuleAsync(role, modules);

            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> RevokeModules(AssignModuleStub model)
        {
            ResponseModel response = new ResponseModel(true);
            Role role = await RepoRole.FindByPrimaryKeyAsync(model.RoleId);
            List<Module> modules = new List<Module>();

            foreach (ModuleFormStub m in model.Modules)
            {
                Module module = new Module();

                m.MapDbObject(module);
                modules.Add(module);
            }

            await RepoRole.RevokeModuleAsync(role, modules);

            return Json(response);
        }

        #region Binding

        public override async Task<string> Binding(params object[] args)
        {
            //lib
            int count;
            List<Role> dbObjects;
            List<RolePresentationStub> models;
            Task<List<Role>> dbObjectsTask = null;
            Task<int> countTask = null;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            dbObjectsTask = RepoRole.FindAllAsync(param.Skip, param.Take, param.Sorts, param.Filter);
            countTask = RepoRole.CountAsync(param.Filter);

            //get and count data from db in background thread
            await Task.WhenAll(dbObjectsTask, countTask);

            //get callback from task
            dbObjects = dbObjectsTask.Result;
            count = countTask.Result;

            //map db data to model
            models = ListMapper.MapList<Role, RolePresentationStub>(dbObjects);

            return JsonConvert.SerializeObject(new { total = count, data = models });
        }

        public async Task<string> BindingModules()
        {
            //lib
            List<ModulesInRole> dbObjects;
            List<Module> moduleList = new List<Module>();
            List<Module> module = new List<Module>();
            List<ModulePresentationStub> models;
            QueryRequestParameter param = QueryRequestParameter.Current;

            //algorithm
            if (param.Filter.Filters.First().Operator == FilterOperator.NotEquals)
            {
                param.Filter.Filters.First().Operator = FilterOperator.Equals;
                dbObjects = await RepoModulesInRole.FindAllAsync(param.Filter);
                if (dbObjects.Any())
                {
                    moduleList = await RepoModule.FindAllAsync();
                    foreach (Module item in moduleList)
                    {
                        bool isAny = false;
                        foreach (ModulesInRole modulesInRole in dbObjects)
                        {
                            if (item.ModuleId == modulesInRole.ModuleId)
                            {
                                isAny = true;
                            }
                        }
                        if (!isAny)
                        {
                            module.Add(item);
                        }
                    }
                    moduleList = module;
                }
                else
                {
                    moduleList = await RepoModule.FindAllAsync();
                }
            }
            else
            {
                dbObjects = await RepoModulesInRole.FindAllAsync(param.Filter);
                foreach (ModulesInRole moduleInRole in dbObjects)
                {
                    moduleList.Add(moduleInRole.Module);
                }
            }
            module = new List<Module>();
            foreach (Module item in moduleList)
            {
                if (!item.Modules1.Any())
                {
                    module.Add(item);
                }
            }
            moduleList = module;
            models = ListMapper.MapList<Module, ModulePresentationStub>(moduleList);
            return JsonConvert.SerializeObject(new { data = models });
        }

        #endregion
    }
}
