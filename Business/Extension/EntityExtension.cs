﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;

namespace Business.Extension
{
    public static class EntityExtension
    {
        public static IEnumerable<string> GetPrimaryKeyFields<T>(this DbContext context) where T : class
        {
            ObjectSet<T> objectSet = ((IObjectContextAdapter)context).ObjectContext.CreateObjectSet<T>();
            IEnumerable<string> keyFields = objectSet.EntitySet.ElementType.KeyMembers.Select(k => k.Name);
            return keyFields;
        }

        public static string GetPrimaryKeyField<T>(this DbContext context) where T : class
        {
            IEnumerable<string> keyFields = context.GetPrimaryKeyFields<T>();
            return keyFields.FirstOrDefault();
        }

        public static IEnumerable<Type> GetPrimaryKeyTypes<T>(this DbContext context) where T : class
        {
            IEnumerable<string> keyFields = context.GetPrimaryKeyFields<T>();
            IEnumerable<Type> types = keyFields.Select(keyField => typeof(T).GetProperty(keyField).PropertyType);
            return types;
        }

        public static Type GetPrimaryKeyType<T>(this DbContext context) where T : class
        {
            IEnumerable<Type> types = context.GetPrimaryKeyTypes<T>();
            return types.FirstOrDefault();
        }

        public static IEnumerable<object> GetPrimaryKeyValues<T>(this DbContext context, T dbItem) where T : class
        {
            List<object> values = new List<object>();
            IEnumerable<string> keyFields = context.GetPrimaryKeyFields<T>();

            foreach (string keyField in keyFields)
            {
                object value = dbItem.GetPropertyValue(keyField);
                values.Add(value);
            }

            return values;
        }

        public static object GetPrimaryKeyValue<T>(this DbContext context, T dbItem) where T : class
        {
            IEnumerable<object> values = context.GetPrimaryKeyValues<T>(dbItem);
            return values.FirstOrDefault();
        }
        
        public static int CountPrimaryKey<T>(this DbContext context) where T : class
        {
            IEnumerable<string> keyFields = context.GetPrimaryKeyFields<T>();
            return keyFields.Count();
        }

        public static string GetKeyFieldView<T>() where T : class
        {
            Type type = typeof(T);
            PropertyInfo property = type.GetProperties().FirstOrDefault(m => Attribute.IsDefined(m, typeof(KeyAttribute)));

            if (property == null)
            {
                property = type.GetProperties().FirstOrDefault();
            }

            return property.Name;
        }
    }
}
