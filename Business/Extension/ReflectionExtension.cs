﻿using System;
using System.Reflection;

namespace Business.Extension
{
    public static class ReflectionExtension
    {
        public static object GetPropertyValue(this object obj, string name)
        {
            if (obj == null)
                return null;

            Type type = obj.GetType();
            PropertyInfo info = type.GetProperty(name);

            if (info == null)
                return null;

            obj = info.GetValue(obj, null);

            return obj;
        }

        public static T GetPropertyValue<T>(this object obj, string name)
        {
            object retval = GetPropertyValue(obj, name);
            if (retval == null)
                return default(T);

            return (T)retval;
        }

        public static void SetPropertyValue(this object obj, string name, object value)
        {
            if (obj == null)
                throw new NullReferenceException(obj.ToString());

            Type type = obj.GetType();
            PropertyInfo info = type.GetProperty(name);

            if (info == null)
                throw new NullReferenceException(obj.ToString());

            info.SetValue(obj, value, null);
        }
    }
}
