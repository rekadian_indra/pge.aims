﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMultiPKRepository : IRepository<MultiPKTable>
    {
    }
}
