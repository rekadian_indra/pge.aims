﻿using Business.Entities;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IMDMRequestRepository : IRepository<MDMRequest>
    {                
    }
}
