﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewMappingAssetActualCostRepository : IRepository<ViewMappingAssetActualCost>
    {
    }
}
