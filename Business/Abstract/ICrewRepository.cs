﻿using Business.Entities;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface ICrewRepository : IRepository<Crew>
    {
        Company FindCompanyByKey(int key);
        Task<Company> FindCompanyByKeyAsync(int key);
    }
}
