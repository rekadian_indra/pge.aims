﻿using Business.Entities;
using Business.Infrastructure;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IPhysicalCheckRepository : IRepository<PhysicalCheck>
    {
        List<PhysicalCheck> FindAllDistinct(
           int? skip = null,
           int? take = null,
           List<SortQuery> sorts = null,
           FilterQuery filter = null);
        int CountDistinct(FilterQuery filter = null);
    }
}
