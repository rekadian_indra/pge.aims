﻿using Business.Entities;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface ISPCRequestRepository : IRepository<SPCRequest>
    {                
    }
}
