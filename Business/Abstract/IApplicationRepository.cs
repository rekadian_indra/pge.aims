﻿using Business.Entities;
using System;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IApplicationRepository : IRepository<Application>
    {
        Guid GetApplicationId(string applicationName);

        Task<Guid> GetApplicationIdAsync(string applicationName);
    }
}
