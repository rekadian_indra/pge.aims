﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMappingAssetRepository : IRepository<MappingAsset>
    {

    }
}
