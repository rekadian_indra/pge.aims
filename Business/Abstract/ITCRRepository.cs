﻿using Business.Entities;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface ITCRRepository : IRepository<TCR>
    {
        List<Asset> FindAllAssetForMapping();
    }
}
