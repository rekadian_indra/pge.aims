﻿using Business.Entities;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IActualCostRepository : IRepository<ActualCost>
    {
        ActualCost FindActualCostByKey(int key);
        Task<ActualCost> FindActualCostByKeyAsync(int key);
    }
}
