﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewMappingRepository : IRepository<ViewMapping>
    {
    }
}
