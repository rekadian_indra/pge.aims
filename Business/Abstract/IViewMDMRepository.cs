﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewMDMRepository : IRepository<ViewMDM>
    {
    }
}
