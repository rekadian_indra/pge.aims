﻿using Business.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IRoleRepository : IRepository<Role>
    {
        void AssignModule(Role role, List<Module> modules);
        void RevokeModule(Role role, List<Module> modules);

        Task AssignModuleAsync(Role role, List<Module> modules);
        Task RevokeModuleAsync(Role role, List<Module> modules);
    }
}
