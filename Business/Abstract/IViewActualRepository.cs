﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewActualRepository : IRepository<ViewActual>
    {
    }
}
