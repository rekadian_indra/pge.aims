﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewAssetRepository : IRepository<ViewAsset>
    {
    }
}
