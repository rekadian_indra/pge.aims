﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IAUCRepository : IRepository<AUC>
    {
    }
}
