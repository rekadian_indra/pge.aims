﻿using Business.Entities;

namespace Business.Abstract
{
    public interface ILocationRepository : IRepository<Location>
    {
    }
}
