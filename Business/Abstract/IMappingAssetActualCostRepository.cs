﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMappingAssetActualCostRepository : IRepository<MappingAssetActualCost>
    {

    }
}
