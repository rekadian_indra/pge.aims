﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewAUCRepository : IRepository<ViewAuc>
    {
    }
}
