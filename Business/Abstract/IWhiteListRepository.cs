﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IWhitelistRepository : IRepository<Whitelist>
    {
    }
}
