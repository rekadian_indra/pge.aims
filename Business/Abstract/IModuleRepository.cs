﻿using Business.Entities;
using Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IModuleRepository : IRepository<Module>
    {
        //Module FindByName(string moduleName);
        //void Delete(string moduleName, bool foreignKey);
        //void addAction(Guid moduleId, Guid actionId);
        //void removeAction(Guid moduleId, Guid actionId);
        //void GetAllChildrenInModule(string moduleName, ref List<Module> result, int lvl);
        //List<Entities.Action> GetActionsInModule(string moduleName);

        Task<List<Entities.Action>> GetActionsInModuleAsync(string moduleName);
        void Create(Module module);
        void Delete(string moduleName, bool foreignKey);
        List<Module> Find(int skip = 0, int? take = null, List<SortQuery> sortings = null, FilterQuery filters = null, string filterLogic = null);
        int Count(List<FilterQuery> filters, string filterLogic);
        Module FindByPk(Guid id);
        Module FindByName(string moduleName);

        void GetAllChildrenInModule(string moduleName, ref List<Module> result, int lvl);

        void addAction(Guid moduleId, Guid actionId);
        void removeAction(Guid moduleId, Guid actionId);

        List<Entities.Action> GetActionsInModule(string moduleName);
    }
}
