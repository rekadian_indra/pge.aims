﻿using Business.Entities.Views;
using Business.Infrastructure;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IViewPhysicalCheckRepository : IRepository<ViewPhysicalCheck>
    {
        List<ViewPhysicalCheck> FindAllDistinct(
          int? skip = null,
          int? take = null,
          List<SortQuery> sorts = null,
          FilterQuery filter = null);
        int CountDistinct(FilterQuery filter = null);
    }
}
