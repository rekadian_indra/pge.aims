﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IDummyFlagRepository : IRepository<DummyFlagTable>
    {
    }
}
