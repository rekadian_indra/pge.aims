﻿using Business.Entities.Views;

namespace Business.Abstract
{
    public interface IViewMappingAssetRepository : IRepository<ViewMappingAsset>
    {
    }
}
