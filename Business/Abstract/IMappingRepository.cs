﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IMappingRepository : IRepository<Mapping>
    {

    }
}
