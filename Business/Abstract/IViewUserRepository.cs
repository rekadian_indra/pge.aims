﻿using Business.Entities;
using Business.Entities.Views;
using System.Collections.Generic;

namespace Business.Abstract
{
    public interface IViewUserRepository : IRepository<ViewUserArea>
    {
    }
}
