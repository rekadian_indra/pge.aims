﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IPhysicalCheckAssetRepository : IRepository<PhysicalCheckAsset>
    {
    }
}
