﻿using Business.Entities;

namespace Business.Abstract
{
    public interface IAssetRepository : IRepository<Asset>
    {
    }
}
