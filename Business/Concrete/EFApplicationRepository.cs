﻿using Business.Abstract;
using Business.Entities;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFApplicationRepository : EFBaseRepository<Application, UmEntities>, IApplicationRepository
    {
        public Guid GetApplicationId(string applicationName)
        {
            if (string.IsNullOrEmpty(applicationName))
                throw new ArgumentNullException();

            return EntitySet.FirstOrDefault(m => m.ApplicationName == applicationName).ApplicationId;
        }

        public Task<Guid> GetApplicationIdAsync(string applicationName)
        {
            return Task.FromResult(GetApplicationId(applicationName));
        }
    }
}
