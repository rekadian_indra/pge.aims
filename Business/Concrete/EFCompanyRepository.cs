﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFCompanyRepository : EFBaseRepository<Company>, ICompanyRepository
    {
        public EFCompanyRepository() : base(true)
        {
        }
    }
}
