﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewMDMRepository : EFBaseRepository<ViewMDM>, IViewMDMRepository
    {
        public EFViewMDMRepository() : base()
        {
        }

        protected override IQueryable<ViewMDM> GenerateView()
        {
            var result = Context.MDMRequests
                .Join(
                    Context.MDMAssets,
                    req => req.MDMRequestId,
                    ast => ast.MDMRequestId,
                    (req, ast) => new { Request = req, Asset = ast })
                .Join(
                    Context.MappingAssets.Where(n=>n.Asset.IsDeleted == false).Select(n => new { n.AssetId, n.MappingAssetId, n.MappingId }),
                    mdm => mdm.Asset.AssetId,
                    mas => mas.AssetId,
                    (mdm, mas) => new { Request = mdm.Request, MappingAsset = mas })
                .Join(
                    Context.Mappings,
                    mas => mas.MappingAsset.MappingId,
                    m=>m.MappingId,
                    (mas, m)=> new { Request = mas.Request, Plant = m.Plant != null ? m.Plant.Code : null }
                ).Select(n => new ViewMDM
                {
                    MDMRequestId = n.Request.MDMRequestId,
                    LegalRequestor = n.Request.LegalRequestor,
                    Plant = n.Plant,
                    RequestTitle = n.Request.RequestTitle,
                    SubmitDate = n.Request.SubmitDate,
                    Status = n.Request.Status
                }).Distinct();

            return result;
        }
    }
}
