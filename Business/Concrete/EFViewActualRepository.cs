﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewActualRepository : EFBaseRepository<ViewActual>, IViewActualRepository
    {
        public EFViewActualRepository() : base()
        {
        }

        protected override IQueryable<ViewActual> GenerateView()
        {
            return Context.ActualCosts.Select(o => new ViewActual
            {
                MappingValueIdr = o.MappingAssetActualCosts.Any() ? o.MappingAssetActualCosts.Sum(x => x.ValueIdr) : 0,
                Balance = o.ValueCoArea != null ? o.ValueCoArea.Value - (o.MappingAssetActualCosts.Any() ? o.MappingAssetActualCosts.Sum(x => x.ValueIdr) : 0) : 0,

                ActualCostId = o.ActualCostId,
                WBSElement = o.WBSElement,
                DocNumber = o.DocNumber,
                DocNumber2 = o.DocNumber2,
                RefNumber = o.RefNumber,
                OffsettingAccountType = o.OffsettingAccountType,
                DocType = o.DocType,
                CoObjectName = o.CoObjectName,
                CostElement = o.CostElement,
                CostElementDesc = o.CostElementDesc,
                FiscalYear = o.FiscalYear,
                DocDate = o.DocDate,
                PostingDate = o.PostingDate,
                CreatedOn = o.CreatedOn,
                TransactionCurrency = o.TransactionCurrency,
                ValueTranCurr = o.ValueTranCurr,
                ValueCoArea = o.ValueCoArea,
                ValueHardCurr = o.ValueHardCurr,
                PurchasingDoc = o.PurchasingDoc,
                TotalQuantity = o.TotalQuantity,
                Item = o.Item,
                PurchaseOrderText = o.PurchaseOrderText,
                Name = o.Name,
                UserName = o.UserName,
                Period = o.Period,
                ProjectDefinition = o.ProjectDefinition,
                Plant = o.MappingAssetActualCosts.Any() && o.MappingAssetActualCosts.FirstOrDefault().AUC != null ?
                                o.MappingAssetActualCosts.AsQueryable().FirstOrDefault().AUC.Plant : string.Empty
            });
        }
    }
}
