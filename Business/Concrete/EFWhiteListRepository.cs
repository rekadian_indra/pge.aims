﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFWhitelistRepository : EFBaseRepository<Whitelist>, IWhitelistRepository
    {
        public EFWhitelistRepository() : base()
        {
        }
    }
}
