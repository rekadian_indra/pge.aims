﻿using Business.Abstract;
using Business.Entities;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFMappingAssetActualCostRepository : EFBaseRepository<MappingAssetActualCost>, IMappingAssetActualCostRepository
    {
        public EFMappingAssetActualCostRepository() : base(true)
        {
        }
    }
}
