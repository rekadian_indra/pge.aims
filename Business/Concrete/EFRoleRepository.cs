﻿using Business.Abstract;
using Business.Entities;
using Business.Extension;
using Business.Infrastructure;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFRoleRepository : EFBaseRepository<Role, UmEntities>, IRoleRepository
    {
        protected override void CustomFilterProcess(FilterQuery filter, ref IQueryable<Role> queryable)
        {
            //lib
            FilterQuery removedFilter;
            object value;
            FilterOperator @operator;

            //algorithm
            if (filter != null && filter.Filters.Any(m => m.Field == Field.UserId || m.Field == Field.UserName))
            {
                //filter UserId
                removedFilter = filter.RemoveFilter(Field.UserId);

                if (removedFilter.HasValue())
                {
                    value = removedFilter.Value;
                    @operator = removedFilter.Operator.Value;

                    switch (@operator)
                    {
                        case FilterOperator.Equals:
                            queryable = queryable.Where(m => m.Users.Any(n => n.UserId.ToString() == value.ToString()));

                            break;
                        case FilterOperator.NotEquals:
                            queryable = queryable.Where(m => !m.Users.Any(n => n.UserId.ToString() == value.ToString()));

                            break;
                        default:
                            throw new NotSupportedException(@operator.ToString());
                    }
                }

                //filter UserName
                removedFilter = filter.RemoveFilter(Field.UserName);

                if (removedFilter.HasValue())
                {
                    value = removedFilter.Value;
                    @operator = removedFilter.Operator.Value;

                    switch (@operator)
                    {
                        case FilterOperator.Equals:
                            queryable = queryable.Where(m => m.Users.Any(n => n.UserName == value.ToString()));

                            break;
                        case FilterOperator.NotEquals:
                            queryable = queryable.Where(m => !m.Users.Any(n => n.UserName == value.ToString()));

                            break;
                        default:
                            throw new NotSupportedException(@operator.ToString());
                    }
                }
            }
        }

        public void AssignModule(Role role, List<Module> modules)
        {
            if (role == null)
                throw new ArgumentNullException(typeof(Role).Name);

            if (modules == null)
                throw new ArgumentNullException(typeof(Module).Name);

            if (modules.Any())
            {
                foreach (Module m in modules)
                {
                    Module module = Context.Modules.Find(m.ModuleId);

                    if (module != null)
                    {
                        role.ModulesInRoles.Add(new ModulesInRole { Id = Guid.NewGuid(), RoleId = role.RoleId, ModuleId = module.ModuleId });
                    }
                }

                Context.SaveChanges();
            }
        }

        public void RevokeModule(Role role, List<Module> modules)
        {
            if (role == null)
                throw new ArgumentNullException(typeof(Role).Name);

            if (modules == null)
                throw new ArgumentNullException(typeof(Module).Name);

            if (modules.Any() && role.ModulesInRoles.Any())
            {
                foreach (Module m in modules)
                {
                    ModulesInRole module = role.ModulesInRoles.FirstOrDefault(n => n.ModuleId == m.ModuleId);

                    if (module != null)
                    {
                        Context.ModulesInRoles.Remove(module);
                    }
                }

                Context.SaveChanges();
            }
        }

        public override bool Delete(Role dbItem)
        {
            if (dbItem == null)
                throw new ArgumentNullException();

            //clear ModulesInRole
            if (dbItem.ModulesInRoles.Any())
                Context.ModulesInRoles.RemoveRange(dbItem.ModulesInRoles);

            //clear UsersInRoles
            dbItem.Users.Clear();

            return base.Delete(dbItem);
        }

        public Task AssignModuleAsync(Role role, List<Module> modules)
        {
            AssignModule(role, modules);

            return Task.FromResult(0);
        }

        public Task RevokeModuleAsync(Role role, List<Module> modules)
        {
            RevokeModule(role, modules);

            return Task.FromResult(0);
        }
    }
}
