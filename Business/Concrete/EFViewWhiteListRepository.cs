﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewWhitelistRepository : EFBaseRepository<ViewWhitelist>, IViewWhitelistRepository
    {
        public EFViewWhitelistRepository() : base()
        {
        }

        protected override IQueryable<ViewWhitelist> GenerateView()
        {
            //DefaultIfEmpty() method meaning LEFT OUTER JOIN. Just remove DefaultIfEmpty() method for INNER JOIN
            return Context.Whitelists.SelectMany(m => m.Crew.Companies.Where(n => n.IsDeleted == false && n.Crews.Contains(m.Crew)).DefaultIfEmpty(), (m, n) => new
            {
                whiteList = m,
                company = n
            })
            //Only use parameterless constructor. Entity to LINQ doesnot support constructor with parameter
            .Select(o => new ViewWhitelist
            {
                WhitelistId = o.whiteList.WhitelistId,
                CrewId = o.whiteList.Crew.CrewId,
                AirportId = o.whiteList.Airport.AirportId,
                StartDate = o.whiteList.StartDate,
                EndDate = o.whiteList.EndDate,
                CrewName = o.whiteList.Crew.CrewName,
                Status = o.whiteList.Crew.Status,
                AirportName = o.whiteList.Airport.AirportName,
                CompanyId = o.company != null ? o.company.CompanyId : (int?)null,
                CompanyName = o.company != null ? o.company.CompanyName : string.Empty,
                CreatedBy = o.whiteList.CreatedBy,
                CreatedDateTimeUtc = o.whiteList.CreatedDateTimeUtc,
                ModifiedBy = o.whiteList.ModifiedBy,
                ModifiedDateTimeUtc = o.whiteList.ModifiedDateTimeUtc
            });
        }
    }
}
