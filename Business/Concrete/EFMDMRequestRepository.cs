﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFMDMRequestRepository : EFBaseRepository<MDMRequest>, IMDMRequestRepository
    {
        public EFMDMRequestRepository() : base()
        {
        }
    }
}
