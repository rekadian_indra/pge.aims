﻿using Business.Abstract;
using Business.Entities;
using System.Linq;
using System.Collections.Generic;

namespace Business.Concrete
{
    public class EFTCRRepository : EFBaseRepository<TCR>, ITCRRepository
    {
        public EFTCRRepository() : base(true)
        {

        }

        public List<Asset> FindAllAssetForMapping() {
            return Context.Assets.Where(x => x.IsDeleted == false).ToList();
        }
    }
}
