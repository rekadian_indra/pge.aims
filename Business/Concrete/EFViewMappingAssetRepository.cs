﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewMappingAssetRepository : EFBaseRepository<ViewMappingAsset>, IViewMappingAssetRepository
    {
        public EFViewMappingAssetRepository() : base()
        {
        }

        protected override IQueryable<ViewMappingAsset> GenerateView()
        {
            //DefaultIfEmpty() method meaning LEFT OUTER JOIN. Just remove DefaultIfEmpty() method for INNER JOIN
            IQueryable<ViewMappingAsset> result = Context.MappingAssets.Where(x => x.Asset.IsDeleted == false)
            .SelectMany(x => x.Asset.TCRs.Where(a => a.IsDeleted == false).Where(y => y.Assets.Contains(x.Asset)).DefaultIfEmpty(), (x, y) => new {
                MappingAsset = x,
                TCR = y
            }).SelectMany(z => z.MappingAsset.Asset.MDMAssets.Where(t => t.Asset.AssetId == z.MappingAsset.AssetId).DefaultIfEmpty(), (z, t) => new {
                MappingAsset = z.MappingAsset,
                TCR = z.TCR,
                MDMAsset = t
            }).SelectMany(a => a.MappingAsset.Asset.SPCRequestAssets.Where(b => b.Asset.AssetId == a.MappingAsset.AssetId).DefaultIfEmpty(), (a, b) => new {
                MappingAsset = a.MappingAsset,
                TCR = a.TCR,
                MDMAsset = a.MDMAsset,
                SPCRequest = b
            })
            .Select(o => new ViewMappingAsset
            {
                AssetId = o.MappingAsset.AssetId,
                LocationId = o.MappingAsset.Asset.LocationId,
                AssetNumber = o.MappingAsset.Asset.AssetNumber,
                SubNumber = o.MappingAsset.Asset.SubNumber,
                Description = o.MappingAsset.Asset.Description,
                ValueIdr = o.MappingAsset.MappingAssetActualCosts.Any() ? o.MappingAsset.MappingAssetActualCosts.Sum(x => x.ValueIdr) : 0,
                ValueUsd = o.MappingAsset.MappingAssetActualCosts.Any() ? o.MappingAsset.MappingAssetActualCosts.Sum(x => x.ValueUsd) : 0,
                Photo = o.MappingAsset.Asset.Photo,
                Latitude = o.MappingAsset.Asset.Latitude,
                Longitude = o.MappingAsset.Asset.Longitude,
                IsDeleted = o.MappingAsset.Asset.IsDeleted,
                MappingId = o.MappingAsset.MappingId,
                Location = o.MappingAsset.Asset.Location.Name,
                MappingAssetId = o.MappingAsset.MappingAssetId,
                StatusMapping = o.MappingAsset.Mapping.Status,
                TCRId = o.TCR.TCRId,
                EndDate = o.TCR.EndDate,
                StatusTCR = o.TCR.Status,
                MDMRequestId = o.MDMAsset.MDMRequestId,
                StatusMDM = o.MDMAsset.MDMRequest.Status,
                SPCRequestId = o.SPCRequest.SPCRequestId,
                StatusSPC = o.SPCRequest.SPCRequest.Status,
                NoRequestSPC005 = o.SPCRequest.SPCRequest.NoRequestSPC005,
                NoRequestSPC011 = o.SPCRequest.SPCRequest.NoRequestSPC011,
                CostCenter = o.TCR.CostCenter1.Name,
                Plant = o.TCR.Plant1 != null ? o.TCR.Plant1.Name : null,
                AssetHolder = o.TCR.AssetHolder != null ? o.TCR.AssetHolder : null,
                PlantMapping = o.MappingAsset != null && o.MappingAsset.Mapping != null && o.MappingAsset.Mapping.Plant != null ?
                                o.MappingAsset.Mapping.Plant.Name : null,
                BarcodeImage = o.MappingAsset.Asset.BarcodeImage
            }).GroupBy(y => y.AssetId).Select(z => z.FirstOrDefault());

            return result;

            //return Context.MappingAssets.Where(x => x.Asset.IsDeleted == false).Select(o => new ViewMappingAsset
            //{
            //    AssetId = o.AssetId,
            //    LocationId = o.Asset.LocationId,
            //    AssetNumber = o.Asset.AssetNumber,
            //    SubNumber = o.Asset.SubNumber,
            //    Description = o.Asset.Description,
            //    ValueIdr = o.MappingAssetActualCosts.Any() ? o.MappingAssetActualCosts.Sum(x => x.ValueIdr) : 0,
            //    ValueUsd = o.MappingAssetActualCosts.Any() ? o.MappingAssetActualCosts.Sum(x => x.ValueUsd) : 0,
            //    Photo = o.Asset.Photo,
            //    Latitude = o.Asset.Latitude,
            //    Longitude = o.Asset.Longitude,
            //    IsDeleted = o.Asset.IsDeleted,
            //    MappingId = o.MappingId,
            //    Location = o.Asset.Location.Name,
            //    MappingAssetId = o.MappingAssetId,
            //    StatusMapping = o.Mapping.Status,
            //    //TCRId = o.Asset.TCRs.Any() ? o.Asset.TCRs.Max(x => x.TCRId) : (int?)null
            //});
        }
    }
}
