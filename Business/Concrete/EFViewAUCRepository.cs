﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewAUCRepository : EFBaseRepository<ViewAuc>, IViewAUCRepository
    {
        public EFViewAUCRepository() : base()
        {
        }

        protected override IQueryable<ViewAuc> GenerateView()
        {
            return Context.AUCs.Select(o => new ViewAuc
            {
                MappingValueIdr = o.MappingAssetActualCosts.Any() ? o.MappingAssetActualCosts.Sum(x => x.ValueIdr) : 0,
                Balance = o.AcquisValIdr - (o.MappingAssetActualCosts.Any() ? o.MappingAssetActualCosts.Sum(x => x.ValueIdr) : 0),
                Convertion = o.AcquisValUsd == 0 ? 0 : o.AcquisValIdr / o.AcquisValUsd,

                WBSElement = o.WBSElement,
                AssetNumber = o.AssetNumber,
                SubNumber = o.SubNumber,
                Description = o.Description,
                AssetClass = o.AssetClass,
                Plant = o.Plant,
                CostCenter = o.CostCenter,
                BookVal = o.BookVal != null ? o.BookVal.Value : 0,

                AcquisValIdr = o.AcquisValIdr,
                CapitalizedOn = o.CapitalizedOn,
                AccumDep = o.AccumDep,
                Currency = o.Currency,
                CompanyCode = o.CompanyCode,
                OriginalAsset = o.OriginalAsset,
                SerialNumber = o.SerialNumber,
                UsefulLife = o.UsefulLife,
                UsefulPeriod = o.UsefulPeriod,
                ScrapVal = o.ScrapVal,
                ProfitCenter = o.ProfitCenter,
                OrdinaryDep = o.OrdinaryDep,
                FirstAcquisition = o.FirstAcquisition,
                AssetCondition = o.AssetCondition,
                EvaluationGroup1 = o.EvaluationGroup1,
                EvaluationGroup3 = o.EvaluationGroup3,
                EvaluationGroup4 = o.EvaluationGroup4,
                Location = o.Location,
                EvaluationGroup5 = o.EvaluationGroup5,
                BalanceSheetItem = o.BalanceSheetItem,
                OriginalValue = o.OriginalValue,
                OriginalAcquisitionYear = o.OriginalAcquisitionYear,
                DepreciationKey = o.DepreciationKey,
                AccumOrdinaryDep = o.AccumOrdinaryDep,
                PlannedOrdinaryDep = o.PlannedOrdinaryDep,
                NoUnitDeprec = o.NoUnitDeprec,
                AcquisValUsd = o.AcquisValUsd
            });
        }
    }
}
