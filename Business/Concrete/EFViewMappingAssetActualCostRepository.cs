﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewMappingAssetActualCostRepository : EFBaseRepository<ViewMappingAssetActualCost>, IViewMappingAssetActualCostRepository
    {
        public EFViewMappingAssetActualCostRepository() : base()
        {
        }

        protected override IQueryable<ViewMappingAssetActualCost> GenerateView()
        {
            //DefaultIfEmpty() method meaning LEFT OUTER JOIN. Just remove DefaultIfEmpty() method for INNER JOIN
            return Context.MappingAssetActualCosts.Select(o => new ViewMappingAssetActualCost
            {
                MappingAssetActualCostId = o.MappingAssetActualCostId,
                MappingAssetId = o.MappingAssetId,
                ActualCostId = o.ActualCostId,
                DocNumber = o.ActualCost.DocNumber,
                WBSElement = o.WBSElement,
                ValueIdr = o.ValueIdr,
                ValueUsd = o.ValueUsd,
                Balance = o.ActualCost.ValueCoArea != null ? o.ActualCost.ValueCoArea.Value - (o.ActualCost.MappingAssetActualCosts.Any() ? o.ActualCost.MappingAssetActualCosts.Sum(x => x.ValueIdr) : 0) : 0,
                Status = o.MappingAsset.Mapping.Status,      
                Asset = o.MappingAsset.Asset,                
                AUC = o.AUC,
                AucNumber = o.AUC != null ? o.AUC.AssetNumber : string.Empty,
            });
        }
    }
}
