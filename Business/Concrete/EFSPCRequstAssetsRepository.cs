﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFSPCRequstAssetsRepository : EFBaseRepository<SPCRequestAsset>, ISPCRequestAssetsRepository
    {
        public EFSPCRequstAssetsRepository() : base()
        {
        }
    }
}
