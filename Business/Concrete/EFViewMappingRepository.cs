﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewMappingRepository : EFBaseRepository<ViewMapping>, IViewMappingRepository
    {
        public EFViewMappingRepository() : base(true)
        {
        }

        protected override IQueryable<ViewMapping> GenerateView()
        {
            var emptyAsset = Context.Mappings.Where(n=>!n.MappingAssets.Any() && n.IsDeleted == false).Select(n => new ViewMapping
            {
                Description = n.Description,
                IsDeleted = n.IsDeleted,
                MappingId = n.MappingId,
                Notes = n.Notes,
                Plant = string.Empty,
                Status = n.Status,
                SubmitDate = n.SubmitDate
            }).Distinct();

            var mapping = Context.Mappings.Where(n=>n.MappingAssets.Any() && n.IsDeleted == false)
                .Join(
                    Context.MappingAssets.Where(n => n.Asset.IsDeleted == false),
                    mp => mp.MappingId,
                    ma => ma.MappingId,                                                                                                                                                                                                     
                    (mp, ma) => new { Mapping = mp, MappingAsset = ma })
                .Join(
                    Context.MappingAssetActualCosts.Select(n => new { n.MappingAssetId, n.WBSElement }),
                    mas => mas.MappingAsset.MappingAssetId,
                    mac => mac.MappingAssetId,
                    (mas, mac) => new { Mapping = mas.Mapping, MappingAssectActualCost = mac })
                .Join(
                    Context.AUCs.Select(n => new { n.WBSElement, n.Plant }),
                    mac => mac.MappingAssectActualCost.WBSElement,
                    au => au.WBSElement,
                    (mac, au) => new { Mapping = mac.Mapping, AUC = au }
                ).Select(n => new ViewMapping
                {
                    Description = n.Mapping.Description,
                    IsDeleted = n.Mapping.IsDeleted,
                    MappingId = n.Mapping.MappingId,
                    Notes = n.Mapping.Notes,
                    Plant = n.AUC.Plant,
                    Status = n.Mapping.Status,
                    SubmitDate = n.Mapping.SubmitDate
                }).Distinct();

            var result = mapping;

            return result;
        }
    }
}
