﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFMDMAssetsRepository : EFBaseRepository<MDMAsset>, IMDMAssetsRepository
    {
        public EFMDMAssetsRepository() : base()
        {
        }
    }
}
