﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFLocationRepository : EFBaseRepository<Location>, ILocationRepository
    {
        public EFLocationRepository() : base(true)
        {
        }
    }
}
