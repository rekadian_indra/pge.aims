﻿using Business.Abstract;
using Business.Entities;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFMappingRepository : EFBaseRepository<Mapping>, IMappingRepository
    {
        public EFMappingRepository() : base(true)
        {
        }
    }
}
