﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFSPCRequestRepository : EFBaseRepository<SPCRequest>, ISPCRequestRepository
    {
        public EFSPCRequestRepository() : base()
        {
        }
    }
}
