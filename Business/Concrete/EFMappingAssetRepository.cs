﻿using Business.Abstract;
using Business.Entities;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFMappingAssetRepository : EFBaseRepository<MappingAsset>, IMappingAssetRepository
    {
        public EFMappingAssetRepository() : base(true)
        {
        }
    }
}
