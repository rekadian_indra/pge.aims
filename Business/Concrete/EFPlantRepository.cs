﻿using Business.Abstract;
using Business.Entities;
using System.Linq;
using System.Collections.Generic;

namespace Business.Concrete
{
    public class EFPlantRepository : EFBaseRepository<Plant>, IPlantRepository
    {
        public EFPlantRepository() : base()
        {

        }
    }
}
