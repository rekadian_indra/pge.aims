﻿using Business.Abstract;
using Business.Entities;
using System.Linq;
using System.Collections.Generic;

namespace Business.Concrete
{
    public class EFCostCenterRepository : EFBaseRepository<CostCenter>, ICostCenterRepository
    {
        public EFCostCenterRepository() : base()
        {

        }      
    }
}
