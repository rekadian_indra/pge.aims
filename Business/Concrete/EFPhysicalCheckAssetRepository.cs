﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFPhysicalCheckAssetRepository : EFBaseRepository<PhysicalCheckAsset>, IPhysicalCheckAssetRepository
    {
        public EFPhysicalCheckAssetRepository() : base()
        {
        }
    }
}
