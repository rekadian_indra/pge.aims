﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFAssetRepository : EFBaseRepository<Asset>, IAssetRepository
    {
        public EFAssetRepository() : base(true)
        {
        }
    }
}
