﻿using Business.Abstract;
using Business.Entities.Views;
using Business.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewPhysicalCheckRepository : EFBaseRepository<ViewPhysicalCheck>, IViewPhysicalCheckRepository
    {
        public EFViewPhysicalCheckRepository() : base(true)
        {
        }

        protected override IQueryable<ViewPhysicalCheck> GenerateView()
        {
            var result = Context.PhysicalChecks
                .Join(
                    Context.MappingAssets.Where(n => n.Asset.IsDeleted == false).Select(n => new { n.AssetId, n.MappingAssetId, n.MappingId }),
                    pc => pc.AssetId,
                    mas => mas.AssetId,
                    (pc, mas) => new { PhysicalCheck = pc, MappingAsset = mas })                    
               .Join(
                    Context.Mappings,
                    mas => mas.MappingAsset.MappingId,
                    m=>m.MappingId,
                    (mas, m) => new { PhysicalCheck = mas.PhysicalCheck, Plant = m.Plant}
                ).Select(n => new ViewPhysicalCheck
                {
                    AssetId = n.PhysicalCheck.AssetId,
                    CheckedBy = n.PhysicalCheck.CheckedBy,
                    Condition = n.PhysicalCheck.Condition,
                    IsDeleted = n.PhysicalCheck.IsDeleted,
                    PhysicalCheckDate = n.PhysicalCheck.PhysicalCheckDate,
                    PhysicalCheckId = n.PhysicalCheck.PhysicalCheckId,
                    Plant = n.Plant != null ? n.Plant.Code : string.Empty,
                    Asset = n.PhysicalCheck.Asset
                }).Distinct();

            return result;
        }

        public List<ViewPhysicalCheck> FindAllDistinct(
           int? skip = null,
           int? take = null,
           List<SortQuery> sorts = null,
           FilterQuery filter = null)
        {
            ThrowIfDisposed();

            //lib
            IQueryable<ViewPhysicalCheck> queryable = Queryable;
            queryable = queryable.GroupBy(a => a.AssetId).SelectMany(b => b.OrderByDescending(c => c.PhysicalCheckDate).Take(1));
            FilterQuery copyFilter = null;

            //algorithm
            if (filter != null)
                copyFilter = filter.Clone();

            CustomFilterProcess(copyFilter, ref queryable);
            FilterSortProcess(sorts, copyFilter, ref queryable);

            //skip
            if (skip.HasValue)
            {
                queryable = queryable.Skip(skip.Value);
            }

            //take
            if (take.HasValue)
            {
                queryable = queryable.Take(take.Value);
            }

            return queryable.ToList();
        }

        public int CountDistinct(FilterQuery filter = null)
        {
            ThrowIfDisposed();

            //lib
            IQueryable<ViewPhysicalCheck> queryable = Queryable;
            queryable = queryable.GroupBy(a => a.AssetId).SelectMany(b => b.OrderByDescending(c => c.PhysicalCheckDate).Take(1));
            FilterQuery copyFilter = null;

            //algorithm
            if (filter != null)
                copyFilter = filter.Clone();

            CustomFilterProcess(copyFilter, ref queryable);
            FilterSortProcess(null, copyFilter, ref queryable);

            return queryable.Count();
        }
    }
}
