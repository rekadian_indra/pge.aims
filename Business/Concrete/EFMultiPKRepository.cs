﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFMultiPKRepository : EFBaseRepository<MultiPKTable>, IMultiPKRepository
    {
    }
}
