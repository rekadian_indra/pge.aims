﻿using Business.Abstract;
using Business.Entities;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFActualCostRepository : EFBaseRepository<ActualCost>, IActualCostRepository
    {
        public EFActualCostRepository() : base(true)
        {
        }

        public ActualCost FindActualCostByKey(int key)
        {
            return Context.ActualCosts.Find(key);
        }

        public Task<ActualCost> FindActualCostByKeyAsync(int key)
        {
            return Task.FromResult(FindActualCostByKey(key));
        }
    }
}
