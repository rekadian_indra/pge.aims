﻿using Business.Entities;
using Business.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Linq;
using System.Data.Entity;
using Business.Abstract;
using System;

namespace Business.Concrete
{
    public class EFMembershipRepository : EFBaseRepository<Membership, UmEntities>, IMembershipRepository
    {
        public EFMembershipRepository() : base()
        {
        }
    }
}
