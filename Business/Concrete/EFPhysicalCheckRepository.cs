﻿using Business.Abstract;
using Business.Entities;
using Business.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace Business.Concrete
{
    public class EFPhysicalCheckRepository : EFBaseRepository<PhysicalCheck>, IPhysicalCheckRepository
    {
        public EFPhysicalCheckRepository() : base(true)
        {
        }
        public List<PhysicalCheck> FindAllDistinct(
           int? skip = null,
           int? take = null,
           List<SortQuery> sorts = null,
           FilterQuery filter = null)
        {
            ThrowIfDisposed();

            //lib
            IQueryable<PhysicalCheck> queryable = Queryable;
            queryable = queryable.GroupBy(a => a.AssetId).SelectMany(b => b.OrderByDescending(c => c.PhysicalCheckDate).Take(1)); 
            FilterQuery copyFilter = null;

            //algorithm
            if (filter != null)
                copyFilter = filter.Clone();

            CustomFilterProcess(copyFilter, ref queryable);
            FilterSortProcess(sorts, copyFilter, ref queryable);

            //skip
            if (skip.HasValue)
            {
                queryable = queryable.Skip(skip.Value);
            }

            //take
            if (take.HasValue)
            {
                queryable = queryable.Take(take.Value);
            }

            return queryable.ToList();
        }

        public int CountDistinct(FilterQuery filter = null)
        {
            ThrowIfDisposed();

            //lib
            IQueryable<PhysicalCheck> queryable = Queryable;
            queryable = queryable.GroupBy(a => a.AssetId).SelectMany(b => b.OrderByDescending(c => c.PhysicalCheckDate).Take(1));
            FilterQuery copyFilter = null;

            //algorithm
            if (filter != null)
                copyFilter = filter.Clone();

            CustomFilterProcess(copyFilter, ref queryable);
            FilterSortProcess(null, copyFilter, ref queryable);

            return queryable.Count();
        }
    }
}
