﻿using Business.Abstract;
using Business.Entities.Views;
using System.Linq;

namespace Business.Concrete
{
    public class EFViewAssetRepository : EFBaseRepository<ViewAsset>, IViewAssetRepository
    {
        public EFViewAssetRepository() : base()
        {
        }

        protected override IQueryable<ViewAsset> GenerateView()
        {
            var result = Context.Assets.Where(n => n.IsDeleted == false)
                .Join(
                    Context.MappingAssets,
                    ast => ast.AssetId,
                    ma => ma.AssetId,
                    (ast, ma) => new { Asset = ast, MappingAsset = ma })
                .Join(
                    Context.MappingAssetActualCosts,
                    ma => ma.MappingAsset.MappingAssetId,
                    mc => mc.MappingAssetId,
                    (ma, mc) => new { Asset = ma.Asset, MappingAsset = ma.MappingAsset, MappingAssetActualCost = mc })
                .Join(
                    Context.ActualCosts,
                    mc => mc.MappingAssetActualCost.ActualCostId,
                    ac => ac.ActualCostId,
                    (mc, ac) => new { Asset = mc.Asset, MappingAsset = mc.MappingAsset, MappingAssetActualCost = mc.MappingAssetActualCost, ActualCost = ac })
                .Join(
                    Context.AUCs,
                    mc=>mc.MappingAssetActualCost.WBSElement,
                    au=>au.WBSElement,
                    (mc, au) => new { Asset = mc.Asset, MappingAsset = mc.MappingAsset, MappingAssetActualCost = mc.MappingAssetActualCost, ActualCost = mc.ActualCost, AUC = au })                
                .Select(n => new ViewAsset{
                    Asset = n.Asset,
                    AUC = n.AUC,
                    Balance = n.ActualCost.ValueCoArea.HasValue ? n.ActualCost.ValueCoArea.Value - (n.ActualCost.MappingAssetActualCosts.Any() ? n.ActualCost.MappingAssetActualCosts.Sum(m=>m.ValueIdr) : 0) : 0,
                    DocNumber = n.ActualCost.DocNumber,
                    MappingAssetId = n.MappingAsset.MappingAssetId,                    
                    Status = n.MappingAsset.Mapping.Status,
                    ValueIdr =n.MappingAssetActualCost.ValueIdr,
                    ValueUsd = n.MappingAssetActualCost.ValueUsd,
                    WBSElement = n.AUC.WBSElement,                   
                }).Distinct();                                

            return result;
        }
    }
}
