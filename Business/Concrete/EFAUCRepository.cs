﻿using Business.Abstract;
using Business.Entities;

namespace Business.Concrete
{
    public class EFAUCRepository : EFBaseRepository<AUC>, IAUCRepository
    {
        public EFAUCRepository() : base(true)
        {
        }
    }
}
