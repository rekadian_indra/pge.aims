﻿using Business.Abstract;
using Business.Entities;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class EFCrewRepository : EFBaseRepository<Crew>, ICrewRepository
    {
        public EFCrewRepository() : base()
        {
        }

        public Company FindCompanyByKey(int key)
        {
            return Context.Companies.Find(key);
        }

        public Task<Company> FindCompanyByKeyAsync(int key)
        {
            return Task.FromResult(FindCompanyByKey(key));
        }
    }
}
