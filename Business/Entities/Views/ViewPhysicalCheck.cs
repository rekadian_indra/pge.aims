﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewPhysicalCheck : IView
    {
        [Key]
        public int PhysicalCheckId { get; set; }
        public System.DateTime PhysicalCheckDate { get; set; }
        public int AssetId { get; set; }
        public string Condition { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string CheckedBy { get; set; }
        public string Plant { get; set; }
        public Asset Asset { get; set; }

    }
}
