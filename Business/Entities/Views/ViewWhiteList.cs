﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewWhitelist : IView
    {
        [Key] //Must have one key for default sorting or first property to be set key
        public int WhitelistId { get; set; }
        public string CrewId { get; set; }
        public int AirportId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CrewName { get; set; }
        public string Status { get; set; }
        public string AirportName { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDateTimeUtc { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTimeUtc { get; set; }
    }
}
