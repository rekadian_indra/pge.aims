﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewMappingAsset : IView
    {
        public int AssetId { get; set; }
        public int LocationId { get; set; }
        public string AssetNumber { get; set; }
        public string SubNumber { get; set; }
        public string Description { get; set; }
        public double ValueIdr { get; set; }
        public double ValueUsd { get; set; }
        public string Photo { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool? IsDeleted { get; set; }
        public int MappingId { get; set; }
        public string Location { get; set; }
        public int MappingAssetId { get; set; }
        public string StatusMapping { get; set; }

        public int? TCRId { get; set; }
        public string StatusTCR { get; set; }
        public DateTime? EndDate { get; set; }

        public int? MDMRequestId { get; set; }
        public string StatusMDM { get; set; }

        public int? SPCRequestId { get; set; }
        public string StatusSPC { get; set; }

        public string NoRequestSPC005 { get; set; }
        public string NoRequestSPC011 { get; set; }

        public string CostCenter { get; set; }

        public string Plant { get; set; }

        public string AssetHolder { get; set; }
        public string PlantMapping { get; set; }
        public string BarcodeImage { get; set; }

    }
}
