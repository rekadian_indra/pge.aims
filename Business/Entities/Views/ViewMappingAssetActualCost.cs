﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewMappingAssetActualCost : IView
    {
        [Key]
        public int MappingAssetActualCostId { get; set; }
        public int MappingAssetId { get; set; }
        public int ActualCostId { get; set; }
        public double Balance { get; set; }
        public string DocNumber { get; set; }
        public string WBSElement { get; set; }
        public double ValueIdr { get; set; }
        public double ValueUsd { get; set; }
        public string Status { get; set; }       
        public string AucNumber { get; set; }
        public Asset Asset { get; set; }
        public AUC AUC { get; set; }
    }
}
