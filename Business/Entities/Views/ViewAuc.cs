﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewAuc : IView
    {
        public string WBSElement { get; set; }
        public string AssetNumber { get; set; }
        public string SubNumber { get; set; }
        public string Description { get; set; }
        public string AssetClass { get; set; }
        public string Plant { get; set; }
        public string CostCenter { get; set; }
        public double? BookVal { get; set; }
        public double AcquisValIdr { get; set; }
        public DateTime? CapitalizedOn { get; set; }
        public double? AccumDep { get; set; }
        public string Currency { get; set; }
        public string CompanyCode { get; set; }
        public string OriginalAsset { get; set; }
        public string SerialNumber { get; set; }
        public short? UsefulLife { get; set; }
        public short? UsefulPeriod { get; set; }
        public double? ScrapVal { get; set; }
        public string ProfitCenter { get; set; }
        public double? OrdinaryDep { get; set; }
        public DateTime? FirstAcquisition { get; set; }
        public string AssetCondition { get; set; }
        public string EvaluationGroup1 { get; set; }
        public string EvaluationGroup3 { get; set; }
        public string EvaluationGroup4 { get; set; }
        public string Location { get; set; }
        public string EvaluationGroup5 { get; set; }
        public string BalanceSheetItem { get; set; }
        public double? OriginalValue { get; set; }
        public short? OriginalAcquisitionYear { get; set; }
        public string DepreciationKey { get; set; }
        public double? AccumOrdinaryDep { get; set; }
        public double? PlannedOrdinaryDep { get; set; }
        public int? NoUnitDeprec { get; set; }
        public double AcquisValUsd { get; set; }

        public double MappingValueIdr { get; set; }
        public double Balance { get; set; }
        public double Convertion { get; set; }

    }
}
