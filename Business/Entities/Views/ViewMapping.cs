﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewMapping : IView
    {
        [Key]
        public int MappingId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string Notes { get; set; }
        public string Plant { get; set; }

    }
}
