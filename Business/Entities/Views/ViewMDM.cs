﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.Entities.Views
{
    public class ViewMDM : IView
    {
        [Key]
        public int MDMRequestId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Status { get; set; }
        public string LegalRequestor { get; set; }
        public string RequestTitle { get; set; }
        public string Plant { get; set; }

    }
}
