﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class AppEntities : DbContext
    {
        public AppEntities()
            : base("name=AppEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ActualCost> ActualCosts { get; set; }
        public virtual DbSet<AUC> AUCs { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<MappingAsset> MappingAssets { get; set; }
        public virtual DbSet<MappingAssetActualCost> MappingAssetActualCosts { get; set; }
        public virtual DbSet<MDMRequest> MDMRequests { get; set; }
        public virtual DbSet<PhysicalCheckAsset> PhysicalCheckAssets { get; set; }
        public virtual DbSet<RejectMappingHistory> RejectMappingHistories { get; set; }
        public virtual DbSet<RejectTCRHistory> RejectTCRHistories { get; set; }
        public virtual DbSet<SPCRequestAsset> SPCRequestAssets { get; set; }
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<PhysicalCheck> PhysicalChecks { get; set; }
        public virtual DbSet<CostCenter> CostCenters { get; set; }
        public virtual DbSet<MDMAsset> MDMAssets { get; set; }
        public virtual DbSet<Plant> Plants { get; set; }
        public virtual DbSet<TCR> TCRs { get; set; }
        public virtual DbSet<Mapping> Mappings { get; set; }
        public virtual DbSet<SPCRequest> SPCRequests { get; set; }
    }
}
