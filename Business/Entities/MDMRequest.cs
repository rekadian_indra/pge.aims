//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Business.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class MDMRequest
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MDMRequest()
        {
            this.MDMAssets = new HashSet<MDMAsset>();
        }
    
        public int MDMRequestId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string Status { get; set; }
        public string LegalRequestor { get; set; }
        public string RequestTitle { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MDMAsset> MDMAssets { get; set; }
    }
}
