﻿using Common.Enums;

namespace Business.Infrastructure
{
    public class SortQuery
    {
        public Field? Field { get; set; }
        public FieldHelper RelationField { get; set; }
        public SortOrder? Direction { get; set; }

        public SortQuery(Field? field, SortOrder? direction)
        {
            Field = field;
            Direction = direction;
        }

        public SortQuery(FieldHelper relationField, SortOrder? direction)
        {
            RelationField = relationField;
            Direction = direction;
        }

        public static SortOrder? ParseOrder(string order)
        {
            switch (order)
            {
                //asc
                case "asc":
                    return SortOrder.Ascending;
                //desc
                case "desc":
                    return SortOrder.Descending;
                default:
                    return null;
            }
        }

        /**
         * mengubah kata original menjadi modification
         */
        public void ReplaceField(Field original, Field modification)
        {
            if (Field == original)
                Field = modification;
        }

        /**
         * mengubah kata original menjadi modification
         */
        public void ReplaceField(FieldHelper original, FieldHelper modification)
        {
            if (RelationField.Is(original))
                RelationField = modification;
        }

        /**
         * mengubah sentence case menjadi underscore
         * mis: ContractorId -> contractor_id
         */
        //public void FormatSortOnToUnderscore()
        //{
        //dikomen karena tipe data SortOn sudah bukan string lagi
        //SortOn = System.Text.RegularExpressions.Regex.Replace(SortOn, @"(\p{Ll})(\p{Lu})", "$1_$2");
        //}
    }
}