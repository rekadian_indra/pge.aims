USE [PGE.AIMS.APP]
GO
/****** Object:  Table [dbo].[ActualCost]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActualCost](
	[ActualCostId] [int] IDENTITY(1,1) NOT NULL,
	[WBSElement] [varchar](50) NOT NULL,
	[DocNumber] [varchar](50) NOT NULL,
	[RefNumber] [varchar](50) NOT NULL,
	[DocNumber2] [varchar](50) NULL,
	[OffsettingAccountType] [varchar](5) NULL,
	[DocType] [varchar](50) NULL,
	[OriginalBusTrans] [varchar](50) NULL,
	[ProjectDefinition] [varchar](50) NULL,
	[CoObjectName] [varchar](500) NULL,
	[CostElement] [varchar](50) NULL,
	[CostElementDesc] [varchar](500) NULL,
	[FiscalYear] [smallint] NULL,
	[DocDate] [datetime] NULL,
	[PostingDate] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[TransactionCurrency] [varchar](10) NULL,
	[ValueTranCurr] [float] NULL,
	[ValueCoArea] [float] NULL,
	[ValueHardCurr] [float] NULL,
	[PurchasingDoc] [varchar](50) NULL,
	[TotalQuantity] [int] NULL,
	[Item] [varchar](50) NULL,
	[PurchaseOrderText] [varchar](500) NULL,
	[Name] [varchar](500) NULL,
	[UserName] [varchar](50) NULL,
	[Period] [varchar](50) NULL,
 CONSTRAINT [PK_ActualCost] PRIMARY KEY CLUSTERED 
(
	[ActualCostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Asset]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Asset](
	[AssetId] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [int] NOT NULL,
	[AssetNumber] [varchar](50) NULL,
	[SubNumber] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[ValueIdr] [float] NOT NULL,
	[ValueUsd] [float] NOT NULL,
	[Photo] [varchar](max) NOT NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[IsDeleted] [bit] NULL,
	[BarcodeImage] [varchar](max) NULL,
 CONSTRAINT [PK_Asset] PRIMARY KEY CLUSTERED 
(
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AUC]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AUC](
	[WBSElement] [varchar](50) NOT NULL,
	[AssetNumber] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[Plant] [varchar](50) NOT NULL,
	[CostCenter] [varchar](10) NOT NULL,
	[AcquisValIdr] [float] NOT NULL,
	[BookVal] [float] NULL,
	[AssetClass] [varchar](5) NULL,
	[SubNumber] [varchar](50) NULL,
	[CapitalizedOn] [datetime] NULL,
	[AccumDep] [float] NULL,
	[Currency] [varchar](5) NULL,
	[CompanyCode] [varchar](10) NULL,
	[OriginalAsset] [varchar](50) NULL,
	[SerialNumber] [varchar](50) NULL,
	[UsefulLife] [smallint] NULL,
	[UsefulPeriod] [smallint] NULL,
	[ScrapVal] [float] NULL,
	[ProfitCenter] [varchar](50) NULL,
	[OrdinaryDep] [float] NULL,
	[FirstAcquisition] [datetime] NULL,
	[AssetCondition] [varchar](500) NULL,
	[EvaluationGroup1] [varchar](250) NULL,
	[EvaluationGroup3] [varchar](250) NULL,
	[EvaluationGroup4] [varchar](250) NULL,
	[Location] [varchar](250) NULL,
	[EvaluationGroup5] [varchar](250) NULL,
	[BalanceSheetItem] [varchar](50) NULL,
	[OriginalValue] [float] NULL,
	[OriginalAcquisitionYear] [smallint] NULL,
	[DepreciationKey] [varchar](50) NULL,
	[AccumOrdinaryDep] [float] NULL,
	[PlannedOrdinaryDep] [float] NULL,
	[NoUnitDeprec] [int] NULL,
	[AcquisValUsd] [float] NOT NULL,
 CONSTRAINT [PK_AUC] PRIMARY KEY CLUSTERED 
(
	[WBSElement] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CostCenter]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CostCenter](
	[CostCenterId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[AssetHolder] [varchar](150) NOT NULL,
 CONSTRAINT [PK_CostCenter] PRIMARY KEY CLUSTERED 
(
	[CostCenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Location]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[LocationId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mapping]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mapping](
	[MappingId] [int] IDENTITY(1,1) NOT NULL,
	[PlantId] [int] NULL,
	[SubmitDate] [datetime] NOT NULL,
	[Description] [varchar](256) NULL,
	[Status] [varchar](50) NOT NULL,
	[IsDeleted] [bit] NULL CONSTRAINT [DF_Mapping_IsDeleted]  DEFAULT ((0)),
	[Notes] [varchar](500) NULL,
 CONSTRAINT [PK_Mapping] PRIMARY KEY CLUSTERED 
(
	[MappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MappingAsset]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MappingAsset](
	[MappingAssetId] [int] IDENTITY(1,1) NOT NULL,
	[MappingId] [int] NOT NULL,
	[AssetId] [int] NOT NULL,
 CONSTRAINT [PK_MappingAsset] PRIMARY KEY CLUSTERED 
(
	[MappingAssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MappingAssetActualCost]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MappingAssetActualCost](
	[MappingAssetActualCostId] [int] IDENTITY(1,1) NOT NULL,
	[MappingAssetId] [int] NOT NULL,
	[ActualCostId] [int] NOT NULL,
	[WBSElement] [varchar](50) NULL,
	[ValueIdr] [float] NOT NULL,
	[ValueUsd] [float] NOT NULL,
 CONSTRAINT [PK_MappingAssetActualCost_1] PRIMARY KEY CLUSTERED 
(
	[MappingAssetActualCostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MDMAsset]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MDMAsset](
	[MDMRequestId] [int] NOT NULL,
	[AssetId] [int] NOT NULL,
	[AssetClass] [varchar](50) NOT NULL,
	[PostCap] [varchar](50) NOT NULL,
	[Description2] [varchar](max) NOT NULL,
	[SerialNumber] [varchar](255) NOT NULL,
	[EvaluationGroup1] [varchar](255) NOT NULL,
	[AssetCondition] [varchar](255) NOT NULL,
	[OriginalAsset] [varchar](255) NOT NULL,
	[OrigAcquisDate] [datetime] NOT NULL,
	[BookDepreKey] [varchar](255) NOT NULL,
	[UsefulLife01] [varchar](255) NOT NULL,
	[UsefulLife03] [varchar](255) NOT NULL,
 CONSTRAINT [PK_MDMAsset] PRIMARY KEY CLUSTERED 
(
	[MDMRequestId] ASC,
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MDMRequest]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MDMRequest](
	[MDMRequestId] [int] IDENTITY(1,1) NOT NULL,
	[SubmitDate] [datetime] NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[LegalRequestor] [varchar](255) NOT NULL,
	[RequestTitle] [varchar](255) NOT NULL,
 CONSTRAINT [PK_MDMRequest] PRIMARY KEY CLUSTERED 
(
	[MDMRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhysicalCheck]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhysicalCheck](
	[PhysicalCheckId] [int] IDENTITY(1,1) NOT NULL,
	[PhysicalCheckDate] [datetime] NOT NULL,
	[AssetId] [int] NOT NULL,
	[Condition] [varchar](20) NOT NULL,
	[IsDeleted] [bit] NULL,
	[CheckedBy] [varchar](200) NOT NULL,
 CONSTRAINT [PK_PhysicalCheck] PRIMARY KEY CLUSTERED 
(
	[PhysicalCheckId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhysicalCheckAsset]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhysicalCheckAsset](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[PhysicalCheckId] [int] NOT NULL,
	[Image] [varchar](200) NOT NULL,
 CONSTRAINT [PK_PhysicalCheckAsset] PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Plant]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Plant](
	[PlantId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Plant] PRIMARY KEY CLUSTERED 
(
	[PlantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RejectMappingHistory]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RejectMappingHistory](
	[RejectId] [int] IDENTITY(1,1) NOT NULL,
	[MappingId] [int] NOT NULL,
	[Notes] [varchar](max) NOT NULL,
 CONSTRAINT [PK_RejectMappingHistory] PRIMARY KEY CLUSTERED 
(
	[RejectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RejectTCRHistory]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RejectTCRHistory](
	[RejectId] [int] IDENTITY(1,1) NOT NULL,
	[TCRId] [int] NOT NULL,
	[Notes] [varchar](max) NOT NULL,
 CONSTRAINT [PK_RejectTCRHistory] PRIMARY KEY CLUSTERED 
(
	[RejectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SPCRequest]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SPCRequest](
	[SPCRequestId] [int] IDENTITY(1,1) NOT NULL,
	[PlantId] [int] NULL,
	[SubmitDate] [datetime] NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[ScrapType] [varchar](50) NOT NULL,
	[NoRequestSPC005] [varchar](100) NULL,
	[NoRequestSPC011] [varchar](100) NULL,
	[Plant] [varchar](100) NULL,
	[CostCenter] [varchar](100) NULL,
	[AcquisitionType] [varchar](50) NOT NULL,
	[DocABVN] [bit] NULL,
	[PreparedBy] [varchar](100) NOT NULL,
	[LegalRequestor] [varchar](100) NOT NULL,
 CONSTRAINT [PK_SPCRequest] PRIMARY KEY CLUSTERED 
(
	[SPCRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SPCRequestAsset]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SPCRequestAsset](
	[SPCRequestId] [int] NOT NULL,
	[AssetId] [int] NOT NULL,
	[DocDate] [datetime] NOT NULL,
	[PostingDate] [datetime] NOT NULL,
	[AssetValueDate] [datetime] NOT NULL,
	[PostingPeriod] [varchar](50) NOT NULL,
	[TransactionType] [varchar](100) NOT NULL,
	[AssetTransaction] [varchar](100) NOT NULL,
 CONSTRAINT [PK_SPCRequestAsset] PRIMARY KEY CLUSTERED 
(
	[SPCRequestId] ASC,
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TCR]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TCR](
	[TCRId] [int] IDENTITY(1,1) NOT NULL,
	[SubmitDate] [datetime] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[ProjectName] [varchar](250) NOT NULL,
	[WBS] [varchar](200) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[ProjectDefinition] [varchar](500) NOT NULL,
	[CostCenter] [int] NOT NULL,
	[Plant] [int] NULL,
	[LocationId] [int] NULL,
	[AssetHolder] [varchar](150) NOT NULL,
	[Budget] [float] NOT NULL,
	[Currency] [varchar](10) NOT NULL,
	[Notes] [varchar](500) NULL,
	[ValueIdr] [float] NOT NULL,
	[ValueUsd] [float] NOT NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TCR] PRIMARY KEY CLUSTERED 
(
	[TCRId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TCRAsset]    Script Date: 28/03/2019 11:26:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TCRAsset](
	[TCRId] [int] NOT NULL,
	[AssetId] [int] NOT NULL,
 CONSTRAINT [PK_TCRAsset] PRIMARY KEY CLUSTERED 
(
	[TCRId] ASC,
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ActualCost] ON 

GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (1, N'N22/16/I/263/S/N1-01', N'1145164251', N'2000017380', N'2000017380', N'S', N'ZV', N'RFBU', N'N22/16/I/263', N'LHD-Perkuatan Lereng Area Lahendong', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-30 00:00:00.000' AS DateTime), CAST(N'2017-12-30 00:00:00.000' AS DateTime), CAST(N'2017-12-30 00:00:00.000' AS DateTime), N'IDR', -319000000, -319000000, -23545.92, NULL, NULL, N'0', NULL, N'Reverse Jurnal Biaya PEKERJAAN AUTOMATIC GATE LHD', N'ARYABAYU', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (2, N'N22/16/I/263/S/N1-01', N'1144860846', N'2000016094', N'2000016094', N'S', N'ZV', N'RFBU', N'N22/16/I/263', N'LHD-Perkuatan Lereng Area Lahendong', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-12 00:00:00.000' AS DateTime), CAST(N'2017-12-12 00:00:00.000' AS DateTime), CAST(N'2017-12-12 00:00:00.000' AS DateTime), N'IDR', -331100000, -331100000, -24836.85, NULL, NULL, N'0', NULL, N'Jurnal Biaya PEKERJAAN PAGAR BATAS & PAVING UNIT 5', N'ARYABAYU', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (3, N'N22/16/I/263/S/N1-01', N'1145168052', N'100064150', N'100064150', N'S', N'SA', N'RFBU', N'N22/16/I/263', N'LHD-Perkuatan Lereng Area Lahendong', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-25 00:00:00.000' AS DateTime), CAST(N'2017-12-25 00:00:00.000' AS DateTime), CAST(N'2017-12-30 00:00:00.000' AS DateTime), N'IDR', 46800000, 46800000, 3444.47, NULL, NULL, N'0', NULL, N'Reclass DED Mitigasi Longsor Jalur Pipa CL LHD24', N'ARYABAYU', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (4, N'N22/16/I/263/S/N1-01', N'1145168052', N'100064150', N'100064150', N'S', N'SA', N'RFBU', N'N22/16/I/263', N'LHD-Perkuatan Lereng Area Lahendong', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-25 00:00:00.000' AS DateTime), CAST(N'2017-12-25 00:00:00.000' AS DateTime), CAST(N'2017-12-30 00:00:00.000' AS DateTime), N'IDR', 46400000, 46400000, 3415.03, NULL, NULL, N'0', NULL, N'Reclass DED Mitigasi Longsor Cluster P', N'ARYABAYU', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (5, N'N22/16/I/263/S/N1-01', N'1145168052', N'100064150', N'100064150', N'S', N'SA', N'RFBU', N'N22/16/I/263', N'LHD-Perkuatan Lereng Area Lahendong', N'6001014200', N'ENGINEERING SERVICE', 2017, CAST(N'2017-12-25 00:00:00.000' AS DateTime), CAST(N'2017-12-25 00:00:00.000' AS DateTime), CAST(N'2017-12-30 00:00:00.000' AS DateTime), N'IDR', 49300000, 49300000, 3628.47, NULL, NULL, N'0', NULL, N'Reclass Kajian Mitigasi Longsor PT PGE LHD', N'ARYABAYU', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (6, N'N22/16/I/263/S/N1-01', N'1145154150', N'2000017339', N'2000017339', N'S', N'ZV', N'RFBU', N'N22/16/I/263', N'LHD-Perkuatan Lereng Area Lahendong', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', -197700000, -197700000, -14911.76, NULL, NULL, N'0', NULL, N'Reverse Jurnal Biaya PEKERJAAN PEMBUATAN PLATFORM', N'ARYABAYU', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (7, N'N22/16/I/263/S/N1-01', N'1139932990', N'5007573000', N'5000010894', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2016, CAST(N'2016-12-31 00:00:00.000' AS DateTime), CAST(N'2016-12-31 00:00:00.000' AS DateTime), CAST(N'2017-01-02 00:00:00.000' AS DateTime), N'IDR', 275215000, 275215000, 20483.4, N'3900421130', 1, N'1', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'Tag.1 Kajian Teknis Mitigasi Longsor LHD', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (8, N'N22/16/I/263/S/N1-01', N'1139932990', N'5007573000', N'5000010894', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2016, CAST(N'2016-12-31 00:00:00.000' AS DateTime), CAST(N'2016-12-31 00:00:00.000' AS DateTime), CAST(N'2017-01-02 00:00:00.000' AS DateTime), N'IDR', 14485000, 14485000, 1078.07, N'3900421130', 1, N'1', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'Tag.2 Kajian Teknis Mitigasi Longsor LHD', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (9, N'N22/16/I/263/S/N1-01', N'1140690563', N'5007632680', N'5000001565', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2016-12-31 00:00:00.000' AS DateTime), CAST(N'2017-02-28 00:00:00.000' AS DateTime), CAST(N'2017-03-03 00:00:00.000' AS DateTime), N'IDR', -14485000, -14485000, -1078.07, N'3900421130', -1, N'1', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'Tag.2 Kajian Teknis Mitigasi Longsor LHD', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (10, N'N22/16/I/263/S/N1-01', N'1140691721', N'5007632727', N'5000001590', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2017-02-28 00:00:00.000' AS DateTime), CAST(N'2017-03-03 00:00:00.000' AS DateTime), N'IDR', 14485000, 14485000, 1085.26, N'3900421130', 1, N'1', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'Tag.2 Kajian Teknis Mitigasi Longsor LHD', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (11, N'N22/16/I/263/S/N1-01', N'1140690561', N'5007632679', N'5000001564', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2016-12-31 00:00:00.000' AS DateTime), CAST(N'2017-02-28 00:00:00.000' AS DateTime), CAST(N'2017-03-03 00:00:00.000' AS DateTime), N'IDR', -275215000, -275215000, -20483.4, N'3900421130', -1, N'1', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'Tag.1 Kajian Teknis Mitigasi Longsor LHD', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (12, N'N22/16/I/263/S/N1-01', N'1140691721', N'5007632727', N'5000001590', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2016-09-01 00:00:00.000' AS DateTime), CAST(N'2017-02-28 00:00:00.000' AS DateTime), CAST(N'2017-03-03 00:00:00.000' AS DateTime), N'IDR', 275215000, 275215000, 20619.99, N'3900421130', 1, N'1', N'Kajian Teknis Mtigasi Longsor PT PGE LHD', N'Tag.1 Kajian Teknis Mitigasi Longsor LHD', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (13, N'N22/16/I/263/S/N1-01', N'1138173085', N'5007424121', N'5000006778', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Lereng Jalur Pipa Unit-3&4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2016, CAST(N'2016-09-09 00:00:00.000' AS DateTime), CAST(N'2016-09-13 00:00:00.000' AS DateTime), CAST(N'2016-09-13 00:00:00.000' AS DateTime), N'IDR', 104637750, 104637750, 7994.33, N'3900414584', 1, N'1', N'Perbaikan Lereng Jalur Pipa Unit-3&4', N'Tag.1 Perbaikan Lereng Jalur Pipa Unit-3', N'SPATANGKE', N'9')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (14, N'N22/16/I/263/S/N1-01', N'1139176871', N'5007503820', N'5000008575', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemeliharaan Lereng Jalur Pipa Unit 3&4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2016, CAST(N'2016-11-15 00:00:00.000' AS DateTime), CAST(N'2016-11-15 00:00:00.000' AS DateTime), CAST(N'2016-11-17 00:00:00.000' AS DateTime), N'IDR', 5507250, 5507250, 412.9, N'3900414584', 1, N'2', N'Pemeliharaan Lereng Jalur Pipa Unit 3&4', N'Pemeliharaan Lereng Jalur Pipa Unit 3&4', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (15, N'N22/16/I/263/S/N1-01', N'1142679573', N'5007796160', N'5000004316', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Dinding Penahan Tanah & Akses Jln LHD-02', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-07-25 00:00:00.000' AS DateTime), CAST(N'2017-07-25 00:00:00.000' AS DateTime), CAST(N'2017-07-25 00:00:00.000' AS DateTime), N'IDR', 662350000, 662350000, 49725.98, N'3900429571', 1, N'1', N'Dinding Penahan Tanah & Akses Jln LHD-02', N'Tag.1 Pek.Dnd Tnh, Akses Jln & LB3 Cls-2', N'SPATANGKE', N'7')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (16, N'N22/16/I/263/S/N1-01', N'1143421500', N'5007853317', N'5000005399', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Dinding Penahan Tanah & Akses Jln LHD-02', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-09-11 00:00:00.000' AS DateTime), CAST(N'2017-09-11 00:00:00.000' AS DateTime), CAST(N'2017-09-11 00:00:00.000' AS DateTime), N'IDR', 449500455, 449500455, 34172.15, N'3900429571', 1, N'1', N'Dinding Penahan Tanah & Akses Jln LHD-02', N'Tag.2 Pek.Dnd Tnh, Akses Jln & LB3 Cls-2', N'SPATANGKE', N'9')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (17, N'N22/16/I/263/S/N1-01', N'1144488992', N'5007944785', N'5000007155', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pmlharaan Ddg Penahan Tanah&Akses Jln 02', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-21 00:00:00.000' AS DateTime), CAST(N'2017-11-21 00:00:00.000' AS DateTime), CAST(N'2017-11-21 00:00:00.000' AS DateTime), N'IDR', 58518445, 58518445, 4320.62, N'3900429571', 1, N'2', N'Pmlharaan Ddg Penahan Tanah&Akses Jln 02', N'Tag.3 Pek.Dnd Tnh, Akses Jln & LB3 Cls-2', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (18, N'N22/16/I/263/S/N1-01', N'1143216832', N'5007838172', N'5000005153', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Lantai Samping Scrubber 1-2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-08-24 00:00:00.000' AS DateTime), CAST(N'2017-08-29 00:00:00.000' AS DateTime), CAST(N'2017-08-29 00:00:00.000' AS DateTime), N'IDR', -68759100, -68759100, -5148.95, N'3900431018', -1, N'3', N'Perbaikan Lantai Samping Scrubber 1-2', N'TAG I, PERBAIKAN LANTAI SAMPING SCRUBBER', N'SPATANGKE', N'8')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (19, N'N22/16/I/263/S/N1-01', N'1143150627', N'5007832082', N'5000005076', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Lantai Samping Scrubber 1-2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-08-24 00:00:00.000' AS DateTime), CAST(N'2017-08-24 00:00:00.000' AS DateTime), CAST(N'2017-08-24 00:00:00.000' AS DateTime), N'IDR', 68759100, 68759100, 5148.95, N'3900431018', 1, N'3', N'Perbaikan Lantai Samping Scrubber 1-2', N'TAG I, PERBAIKAN LANTAI SAMPING SCRUBBER', N'SPATANGKE', N'8')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (20, N'N22/16/I/263/S/N1-01', N'1143261899', N'5007842016', N'5000005235', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Lantai Samping Scrubber 1-2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-08-24 00:00:00.000' AS DateTime), CAST(N'2017-08-31 00:00:00.000' AS DateTime), CAST(N'2017-08-31 00:00:00.000' AS DateTime), N'IDR', 68759100, 68759100, 5150.11, N'3900431018', 1, N'3', N'Perbaikan Lantai Samping Scrubber 1-2', N'TAG I, PERBAIKAN LANTAI SAMPING SCRUBBER', N'SPATANGKE', N'8')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (21, N'N22/16/I/263/S/N1-01', N'1143523325', N'5007864204', N'5000005591', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Lantai Samping Scrubber 1-2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-09-18 00:00:00.000' AS DateTime), CAST(N'2017-09-19 00:00:00.000' AS DateTime), CAST(N'2017-09-19 00:00:00.000' AS DateTime), N'IDR', 76399000, 76399000, 5762.48, N'3900431018', 1, N'3', N'Perbaikan Lantai Samping Scrubber 1-2', N'TAG 2, PERBAIKAN LANTAI SAMPING SCRUBBER', N'SPATANGKE', N'9')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (22, N'N22/16/I/263/S/N1-01', N'1144888915', N'5007974986', N'5000007709', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pmeliharaan Lantai Samping Scrubber 1-2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-09-19 00:00:00.000' AS DateTime), CAST(N'2017-12-13 00:00:00.000' AS DateTime), CAST(N'2017-12-13 00:00:00.000' AS DateTime), N'IDR', 7639900, 7639900, 562.21, N'3900431018', 1, N'4', N'Pmeliharaan Lantai Samping Scrubber 1-2', N'Tag.3 Jasa Lantai Samping Scrubber 1-2', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (23, N'N22/16/I/263/S/N1-01', N'1143478846', N'5007859956', N'5000005503', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Infrastr Cls-13 Pasca Banjir &', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-09-15 00:00:00.000' AS DateTime), CAST(N'2017-09-15 00:00:00.000' AS DateTime), CAST(N'2017-09-15 00:00:00.000' AS DateTime), N'IDR', 543500000, 543500000, 40984.84, N'3900430061', 1, N'1', N'Perbaikan Infrastr Cls-13 Pasca Banjir &', N'TAG 1, PERBAIKAN INFRASTRUKTUR PEMBORAN', N'SPATANGKE', N'9')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (24, N'N22/16/I/263/S/N1-01', N'1143478846', N'5007859956', N'5000005503', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Infrastr Cls-13 Pasca Banjir &', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-09-15 00:00:00.000' AS DateTime), CAST(N'2017-09-15 00:00:00.000' AS DateTime), CAST(N'2017-09-15 00:00:00.000' AS DateTime), N'IDR', 7500000, 7500000, 565.57, N'3900430061', 1, N'1', N'Perbaikan Infrastr Cls-13 Pasca Banjir &', N'TAG 2, PERBAIKAN INFRASTRUKTUR PEMBORAN', N'SPATANGKE', N'9')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (25, N'N22/16/I/263/S/N1-01', N'1144640852', N'5007954802', N'5000007308', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pmeliharaan Infrastr Cls-13 Pasca Banjir', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-29 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), N'IDR', 29000000, 29000000, 2145.76, N'3900430061', 1, N'2', N'Pmeliharaan Infrastr Cls-13 Pasca Banjir', N'TAG 3, PERBAIKAN INFRASTRUKTUR PEMBORAN', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (26, N'N22/16/I/263/S/N1-01', N'1143478846', N'5007859956', N'5000005503', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Cls-24 Pasca Banjir & Mit.Long', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-09-15 00:00:00.000' AS DateTime), CAST(N'2017-09-15 00:00:00.000' AS DateTime), CAST(N'2017-09-15 00:00:00.000' AS DateTime), N'IDR', 481650000, 481650000, 36320.79, N'3900430061', 1, N'3', N'Perbaikan Cls-24 Pasca Banjir & Mit.Long', N'TAG 2, PERBAIKAN INFRASTRUKTUR PEMBORAN', N'SPATANGKE', N'9')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (27, N'N22/16/I/263/S/N1-01', N'1144640852', N'5007954802', N'5000007308', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pmlharaan Cls-24 Pasca Banjir & Mit.Long', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-29 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), N'IDR', 25350000, 25350000, 1875.69, N'3900430061', 1, N'4', N'Pmlharaan Cls-24 Pasca Banjir & Mit.Long', N'TAG 3, PERBAIKAN INFRASTRUKTUR PEMBORAN', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (28, N'N22/16/I/263/S/N1-01', N'1143435242', N'5007854792', N'5000005427', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Mitgasi Longsor Sekitar Cls-24 ke Cls-13', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-09-11 00:00:00.000' AS DateTime), CAST(N'2017-09-12 00:00:00.000' AS DateTime), CAST(N'2017-09-12 00:00:00.000' AS DateTime), N'IDR', 301140000, 301140000, 22893.42, N'3900430062', 1, N'1', N'Mitgasi Longsor Sekitar Cls-24 ke Cls-13', N'TAG 1, PEKERJAAN MITIGASI LONGSOR', N'SPATANGKE', N'9')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (29, N'N22/16/I/263/S/N1-01', N'1143435242', N'5007854792', N'5000005427', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Mitgasi Longsor Sekitar Cls-24 ke Cls-13', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-09-11 00:00:00.000' AS DateTime), CAST(N'2017-09-12 00:00:00.000' AS DateTime), CAST(N'2017-09-12 00:00:00.000' AS DateTime), N'IDR', 376425000, 376425000, 28616.77, N'3900430062', 1, N'1', N'Mitgasi Longsor Sekitar Cls-24 ke Cls-13', N'TAG 2, PEKERJAAN MITIGASI LONGSOR', N'SPATANGKE', N'9')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (30, N'N22/16/I/263/S/N1-01', N'1144640855', N'5007954806', N'5000007309', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Mitgasi Longsor Sekitar Cls-24 ke Cls-13', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-28 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), N'IDR', 376425000, 376425000, 27852.39, N'3900430062', 1, N'1', N'Mitgasi Longsor Sekitar Cls-24 ke Cls-13', N'TAG 3, PEKERJAAN MITIGASI LONGSOR', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (31, N'N22/16/I/263/S/N1-01', N'1144640855', N'5007954806', N'5000007309', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Mitgasi Longsor Sekitar Cls-24 ke Cls-13', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-28 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), N'IDR', 376425000, 376425000, 27852.39, N'3900430062', 1, N'1', N'Mitgasi Longsor Sekitar Cls-24 ke Cls-13', N'TAG 4, PEKERJAAN MITIGASI LONGSOR', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (32, N'N22/16/I/263/S/N1-01', N'1144640855', N'5007954806', N'5000007309', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pmlharaan Mtgsi Longsor Cls-24 ke Cls-13', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-28 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), CAST(N'2017-11-29 00:00:00.000' AS DateTime), N'IDR', 75285000, 75285000, 5570.48, N'3900430062', 1, N'2', N'Pmlharaan Mtgsi Longsor Cls-24 ke Cls-13', N'TAG 5, PEKERJAAN MITIGASI LONGSOR', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (33, N'N22/16/I/263/S/N1-01', N'1144743803', N'5007960762', N'5000007385', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Jasa Saluran Air Jalur Pipa Desa Kamanga', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-04 00:00:00.000' AS DateTime), CAST(N'2017-12-04 00:00:00.000' AS DateTime), CAST(N'2017-12-04 00:00:00.000' AS DateTime), N'IDR', 50000000, 50000000, 3696.31, N'3900433382', 1, N'3', N'Jasa Saluran Air Jalur Pipa Desa Kamanga', N'Tag.1 Pemasangan Patok Batas Tanah Cls-P', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (34, N'N22/16/I/263/S/N1-01', N'1145092665', N'5007994242', N'5000008164', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Jasa Saluran Air Jalur Pipa Desa Kamanga', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-15 00:00:00.000' AS DateTime), CAST(N'2017-12-27 00:00:00.000' AS DateTime), CAST(N'2017-12-27 00:00:00.000' AS DateTime), N'IDR', 21628670, 21628670, 1595.27, N'3900433382', 1, N'3', N'Jasa Saluran Air Jalur Pipa Desa Kamanga', N'Tag.2 Pemasangan Patok Batas Tanah Cls-P', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (35, N'N22/16/I/263/S/N1-01', N'1145092665', N'5007994242', N'5000008164', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemeliharaan Air Jalur Pipa Desa Kamanga', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-15 00:00:00.000' AS DateTime), CAST(N'2017-12-27 00:00:00.000' AS DateTime), CAST(N'2017-12-27 00:00:00.000' AS DateTime), N'IDR', 3769930, 3769930, 278.06, N'3900433382', 1, N'4', N'Pemeliharaan Air Jalur Pipa Desa Kamanga', N'Tag.2 Pemasangan Patok Batas Tanah Cls-P', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (36, N'N22/16/I/263/S/N1-01', N'1146564603', N'5008108941', N'5000002029', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemeliharaan Air Jalur Pipa Desa Kamanga', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-15 00:00:00.000' AS DateTime), CAST(N'2018-03-31 00:00:00.000' AS DateTime), CAST(N'2018-04-02 00:00:00.000' AS DateTime), N'IDR', -3769930, -3769930, -278.06, N'3900433382', -1, N'4', N'Pemeliharaan Air Jalur Pipa Desa Kamanga', N'Tag.2 Pemasangan Patok Batas Tanah Cls-P', N'SPATANGKE', N'3')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (37, N'N22/16/I/263/S/N1-01', N'1146564604', N'5008108942', N'5000002030', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemeliharaan Air Jalur Pipa Desa Kamanga', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-15 00:00:00.000' AS DateTime), CAST(N'2018-03-31 00:00:00.000' AS DateTime), CAST(N'2018-04-02 00:00:00.000' AS DateTime), N'IDR', 3769930, 3769930, 274.06, N'3900433382', 1, N'4', N'Pemeliharaan Air Jalur Pipa Desa Kamanga', N'Tag.2 Pemasangan Patok Batas Tanah Cls-P', N'SPATANGKE', N'3')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (38, N'N22/16/I/263/S/N1-01', N'1142707322', N'5007798332', N'5000004473', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Jasa Pemipaan Jalur WPS LHD-37 Cluster-5', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-07-26 00:00:00.000' AS DateTime), CAST(N'2017-07-26 00:00:00.000' AS DateTime), CAST(N'2017-07-26 00:00:00.000' AS DateTime), N'IDR', 49973000, 49973000, 3747.79, N'4100864839', 1, N'1', N'Jasa Pemipaan Jalur WPS LHD-37 Cluster-5', N'Jasa Pemipaan Jalur WPS LHD-37 Cluster-5', N'SPATANGKE', N'7')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (39, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 47200000, 47200000, 3483.91, N'3900437394', 1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 1. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (40, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 59000000, 59000000, 4354.89, N'3900437394', 1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 2. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (41, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 59000000, 59000000, 4354.89, N'3900437394', 1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 3. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (42, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 59000000, 59000000, 4354.89, N'3900437394', 1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 4. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (43, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 11800000, 11800000, 870.98, N'3900437394', 1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 5. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (44, N'N22/16/I/263/S/N1-01', N'1146099807', N'5008073469', N'5000001128', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -59000000, -59000000, -4354.89, N'3900437394', -1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 3. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (45, N'N22/16/I/263/S/N1-01', N'1146099807', N'5008073469', N'5000001128', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -59000000, -59000000, -4354.89, N'3900437394', -1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 4. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (46, N'N22/16/I/263/S/N1-01', N'1146099807', N'5008073469', N'5000001128', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -11800000, -11800000, -870.98, N'3900437394', -1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 5. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (47, N'N22/16/I/263/S/N1-01', N'1146100612', N'5008073470', N'5000001130', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 11800000, 11800000, 860.87, N'3900437394', 1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 5. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (48, N'N22/16/I/263/S/N1-01', N'1146100612', N'5008073470', N'5000001130', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 59000000, 59000000, 4304.37, N'3900437394', 1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 3. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (49, N'N22/16/I/263/S/N1-01', N'1146100612', N'5008073470', N'5000001130', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 59000000, 59000000, 4304.37, N'3900437394', 1, N'2', N'Perbaikan Primeter, Pagar & Saluran Cls4', N'Tag 4. Primeter, Pagar & Saluran cl-4', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (50, N'N22/16/I/263/S/N1-01', N'1145028529', N'5007990685', N'5000008135', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2017-12-22 00:00:00.000' AS DateTime), N'IDR', 212500000, 212500000, 15673.4, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 4 Perapihan Cluster LHD-37.', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (51, N'N22/16/I/263/S/N1-01', N'1145028529', N'5007990685', N'5000008135', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2017-12-22 00:00:00.000' AS DateTime), N'IDR', 42500000, 42500000, 3134.68, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 5 Perapihan Cluster LHD-37.', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (52, N'N22/16/I/263/S/N1-01', N'1144905295', N'5007977234', N'5000007803', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-14 00:00:00.000' AS DateTime), CAST(N'2017-12-14 00:00:00.000' AS DateTime), CAST(N'2017-12-14 00:00:00.000' AS DateTime), N'IDR', 170000000, 170000000, 12532.25, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 1 Perapihan Cluster LHD-37.', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (53, N'N22/16/I/263/S/N1-01', N'1144905295', N'5007977234', N'5000007803', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-14 00:00:00.000' AS DateTime), CAST(N'2017-12-14 00:00:00.000' AS DateTime), CAST(N'2017-12-14 00:00:00.000' AS DateTime), N'IDR', 212500000, 212500000, 15665.32, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 2 Perapihan Cluster LHD-37.', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (54, N'N22/16/I/263/S/N1-01', N'1144905295', N'5007977234', N'5000007803', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-14 00:00:00.000' AS DateTime), CAST(N'2017-12-14 00:00:00.000' AS DateTime), CAST(N'2017-12-14 00:00:00.000' AS DateTime), N'IDR', 212500000, 212500000, 15665.32, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 3 Perapihan Cluster LHD-37.', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (55, N'N22/16/I/263/S/N1-01', N'1146099769', N'5008073408', N'5000001117', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 42500000, 42500000, 3100.61, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 5 Perapihan Cluster LHD-37.', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (56, N'N22/16/I/263/S/N1-01', N'1146099783', N'5008073439', N'5000001120', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -212500000, -212500000, -15503.03, N'3900435338', -1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 4 Perapihan Cluster LHD-37.', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (57, N'N22/16/I/263/S/N1-01', N'1146099783', N'5008073439', N'5000001120', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -42500000, -42500000, -3100.61, N'3900435338', -1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 5 Perapihan Cluster LHD-37.', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (58, N'N22/16/I/263/S/N1-01', N'1146099768', N'5008073407', N'5000001116', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -212500000, -212500000, -15673.4, N'3900435338', -1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 4 Perapihan Cluster LHD-37.', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (59, N'N22/16/I/263/S/N1-01', N'1146099768', N'5008073407', N'5000001116', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -42500000, -42500000, -3134.68, N'3900435338', -1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 5 Perapihan Cluster LHD-37.', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (60, N'N22/16/I/263/S/N1-01', N'1146099769', N'5008073408', N'5000001117', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 212500000, 212500000, 15503.03, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 4 Perapihan Cluster LHD-37.', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (61, N'N22/16/I/263/S/N1-01', N'1146099785', N'5008073440', N'5000001121', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 42500000, 42500000, 3100.61, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 5 Perapihan Cluster LHD-37.', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (62, N'N22/16/I/263/S/N1-01', N'1146099785', N'5008073440', N'5000001121', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pekerjaan Perapihan Cluster LHD-37', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-22 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 212500000, 212500000, 15503.03, N'3900435338', 1, N'1', N'Pekerjaan Perapihan Cluster LHD-37', N'Tag. 4 Perapihan Cluster LHD-37.', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (63, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 277100000, 277100000, 20453.2, N'3900437394', 1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag1. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (64, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 346375000, 346375000, 25566.5, N'3900437394', 1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag2. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (65, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 346375000, 346375000, 25566.5, N'3900437394', 1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag3. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (66, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 346375000, 346375000, 25566.5, N'3900437394', 1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag4. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (67, N'N22/16/I/263/S/N1-01', N'1145142721', N'5008000754', N'5000008360', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 69275000, 69275000, 5113.3, N'3900437394', 1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag5. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (68, N'N22/16/I/263/S/N1-01', N'1146099807', N'5008073469', N'5000001128', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -346375000, -346375000, -25566.5, N'3900437394', -1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag3. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (69, N'N22/16/I/263/S/N1-01', N'1146099807', N'5008073469', N'5000001128', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -346375000, -346375000, -25566.5, N'3900437394', -1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag4. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (70, N'N22/16/I/263/S/N1-01', N'1146099807', N'5008073469', N'5000001128', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -69275000, -69275000, -5113.3, N'3900437394', -1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag5. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (71, N'N22/16/I/263/S/N1-01', N'1146100612', N'5008073470', N'5000001130', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 346375000, 346375000, 25269.94, N'3900437394', 1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag3. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (72, N'N22/16/I/263/S/N1-01', N'1146100612', N'5008073470', N'5000001130', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 346375000, 346375000, 25269.94, N'3900437394', 1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag4. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (73, N'N22/16/I/263/S/N1-01', N'1146100612', N'5008073470', N'5000001130', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 69275000, 69275000, 5053.99, N'3900437394', 1, N'3', N'Pemb. Turap, Drainase Pagar & Pond LHD25', N'Tag5. Turap, Drainase Pagar & Pond LHD25', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (74, N'N22/16/I/263/S/N1-01', N'1144661334', N'5007956051', N'5000007333', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Jasa Pembuatan Pagar Batas PLTP Unit 5&6', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-28 00:00:00.000' AS DateTime), CAST(N'2017-11-30 00:00:00.000' AS DateTime), CAST(N'2017-11-30 00:00:00.000' AS DateTime), N'IDR', 40260000, 40260000, 2978.91, N'3900435047', 1, N'2', N'Jasa Pembuatan Pagar Batas PLTP Unit 5&6', N'Tag.1 Pemb. Platform Cls27&Pagar Bts 5&6', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (75, N'N22/16/I/263/S/N1-01', N'1144661334', N'5007956051', N'5000007333', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Jasa Pembuatan Pagar Batas PLTP Unit 5&6', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-28 00:00:00.000' AS DateTime), CAST(N'2017-11-30 00:00:00.000' AS DateTime), CAST(N'2017-11-30 00:00:00.000' AS DateTime), N'IDR', 264400000, 264400000, 19563.45, N'3900435047', 1, N'2', N'Jasa Pembuatan Pagar Batas PLTP Unit 5&6', N'Tag.2 Pemb. Platform Cls27&Pagar Bts 5&6', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (76, N'N22/16/I/263/S/N1-01', N'1144661334', N'5007956051', N'5000007333', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Jasa Pembuatan Pagar Batas PLTP Unit 5&6', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-28 00:00:00.000' AS DateTime), CAST(N'2017-11-30 00:00:00.000' AS DateTime), CAST(N'2017-11-30 00:00:00.000' AS DateTime), N'IDR', 26440000, 26440000, 1956.34, N'3900435047', 1, N'2', N'Jasa Pembuatan Pagar Batas PLTP Unit 5&6', N'Tag.3 Pemb. Platform Cls27&Pagar Bts 5&6', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (77, N'N22/16/I/263/S/N1-01', N'1144661334', N'5007956051', N'5000007333', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Jasa Pembuatan Platform Cluster LHD-27', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-11-28 00:00:00.000' AS DateTime), CAST(N'2017-11-30 00:00:00.000' AS DateTime), CAST(N'2017-11-30 00:00:00.000' AS DateTime), N'IDR', 197700000, 197700000, 14628.19, N'3900435047', 1, N'1', N'Jasa Pembuatan Platform Cluster LHD-27', N'Tag.1 Pemb. Platform Cls27&Pagar Bts 5&6', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (78, N'N22/16/I/263/S/N1-01', N'1145142102', N'5008000709', N'5000008359', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 15950000, 15950000, 1177.3, N'3900437393', 1, N'1', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'Tag3. Automatic Manual Gate Cls-27,R1/R2', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (79, N'N22/16/I/263/S/N1-01', N'1145142102', N'5008000709', N'5000008359', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 143550000, 143550000, 10595.66, N'3900437393', 1, N'1', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'Tag1. Automatic Manual Gate Cls-27,R1/R2', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (80, N'N22/16/I/263/S/N1-01', N'1145142102', N'5008000709', N'5000008359', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2017, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2017-12-29 00:00:00.000' AS DateTime), N'IDR', 159500000, 159500000, 11772.96, N'3900437393', 1, N'1', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'Tag2. Automatic Manual Gate Cls-27,R1/R2', N'SPATANGKE', N'12')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (81, N'N22/16/I/263/S/N1-01', N'1146099794', N'5008073465', N'5000001126', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -159500000, -159500000, -11772.96, N'3900437393', -1, N'1', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'Tag2. Automatic Manual Gate Cls-27,R1/R2', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (82, N'N22/16/I/263/S/N1-01', N'1146099794', N'5008073465', N'5000001126', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', -15950000, -15950000, -1177.3, N'3900437393', -1, N'1', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'Tag3. Automatic Manual Gate Cls-27,R1/R2', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (83, N'N22/16/I/263/S/N1-01', N'1146099796', N'5008073467', N'5000001127', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 159500000, 159500000, 11636.39, N'3900437393', 1, N'1', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'Tag2. Automatic Manual Gate Cls-27,R1/R2', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (84, N'N22/16/I/263/S/N1-01', N'1146099796', N'5008073467', N'5000001127', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'6000004180', N'LOCATION, SITE REHAB & MAINTENANCE SERVI', 2018, CAST(N'2017-12-29 00:00:00.000' AS DateTime), CAST(N'2018-02-28 00:00:00.000' AS DateTime), CAST(N'2018-03-02 00:00:00.000' AS DateTime), N'IDR', 15950000, 15950000, 1163.64, N'3900437393', 1, N'1', N'Pemb. Automatic Manual Gate Cls-27,R1/R2', N'Tag3. Automatic Manual Gate Cls-27,R1/R2', N'SPATANGKE', N'2')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (85, N'N22/16/I/263/S/N1-01', N'1144463049', N'5007942655', N'5000007100', N'S', N'WE', N'RMWE', N'N22/16/I/263', N'Pws Mitigasi Bnjir&Lngsor LHD 13&24', N'6001014250', N'CONSTRUCTION SERVICE', 2017, CAST(N'2017-11-20 00:00:00.000' AS DateTime), CAST(N'2017-11-20 00:00:00.000' AS DateTime), CAST(N'2017-11-20 00:00:00.000' AS DateTime), N'IDR', 49615280, 49615280, 3667.33, N'4100878945', 1, N'1', N'Pws Mitigasi Bnjir&Lngsor LHD 13&24', N'Pws Mitigasi Bnjir&Lngsor LHD 13&24', N'SPATANGKE', N'11')
GO
INSERT [dbo].[ActualCost] ([ActualCostId], [WBSElement], [DocNumber], [RefNumber], [DocNumber2], [OffsettingAccountType], [DocType], [OriginalBusTrans], [ProjectDefinition], [CoObjectName], [CostElement], [CostElementDesc], [FiscalYear], [DocDate], [PostingDate], [CreatedOn], [TransactionCurrency], [ValueTranCurr], [ValueCoArea], [ValueHardCurr], [PurchasingDoc], [TotalQuantity], [Item], [PurchaseOrderText], [Name], [UserName], [Period]) VALUES (112, N'N55/17/I/133/S/A4-04', N'1145227336', N'5008008925', N'5000009018', N'S', N'WE', N'RMWE', N'N55/17/I/133', N'forklift 8  ton', N'6001013120', N'TRANSPORTATION MATERIAL & EQUIPMENT CONS', 2017, CAST(N'2017-11-03 00:00:00.000' AS DateTime), CAST(N'2017-12-31 00:00:00.000' AS DateTime), CAST(N'2018-01-03 00:00:00.000' AS DateTime), N'IDR', 735000000, 735000000, 54251.55, N'4500167545', 1, N'1', N'FORKLIFT', N'GR Untuk Accrual tahun 2017 (03 Januari 2018)', N'ROSSALINAHA', N'12')
GO
SET IDENTITY_INSERT [dbo].[ActualCost] OFF
GO
SET IDENTITY_INSERT [dbo].[Asset] ON 

GO
INSERT [dbo].[Asset] ([AssetId], [LocationId], [AssetNumber], [SubNumber], [Description], [ValueIdr], [ValueUsd], [Photo], [Latitude], [Longitude], [IsDeleted], [BarcodeImage]) VALUES (1, 1, N'500000816', N'0', N' LHD - RETAINING WALL JALUR PIPA INTERKONEKSI 3&amp;4', 0, 0, N'~/Uploads/cf9bd7f7-d948-4f90-9f9d-0bf0b187f17a_lhd-retaining-wall-jalur-pipa-interkoneksi-34.jpg', -7.1469686067580005, 107.78931140899658, 0, NULL)
GO
INSERT [dbo].[Asset] ([AssetId], [LocationId], [AssetNumber], [SubNumber], [Description], [ValueIdr], [ValueUsd], [Photo], [Latitude], [Longitude], [IsDeleted], [BarcodeImage]) VALUES (2, 7, N'7000001077', N'0', N'KRH-Forklif 8 ton (TEST AIMS)', 0, 0, N'~/Uploads/9bb0e3ad-acc5-4505-8bc7-6b59af3117c4_saluran-primer-lhd-13.jpg', NULL, NULL, 0, NULL)
GO
INSERT [dbo].[Asset] ([AssetId], [LocationId], [AssetNumber], [SubNumber], [Description], [ValueIdr], [ValueUsd], [Photo], [Latitude], [Longitude], [IsDeleted], [BarcodeImage]) VALUES (3, 142, NULL, N'0', N'afdsfsda', 0, 0, N'~/Uploads/aed2c55c-b9cc-4c12-b02b-9e0601530fd4_frostie.jpg', NULL, NULL, 0, NULL)
GO
INSERT [dbo].[Asset] ([AssetId], [LocationId], [AssetNumber], [SubNumber], [Description], [ValueIdr], [ValueUsd], [Photo], [Latitude], [Longitude], [IsDeleted], [BarcodeImage]) VALUES (4, 142, NULL, N'0', N'ss', 0, 0, N'~/Uploads/a47ddb71-8bd7-4866-8891-3a63d7aadb41_capture.JPG', NULL, NULL, 0, NULL)
GO
INSERT [dbo].[Asset] ([AssetId], [LocationId], [AssetNumber], [SubNumber], [Description], [ValueIdr], [ValueUsd], [Photo], [Latitude], [Longitude], [IsDeleted], [BarcodeImage]) VALUES (5, 56, N'KMJ-V-123', N'0', N'KMJ-V-123', 0, 0, N'~/Uploads/0792cb64-7245-407b-84f6-f92432a084d1_dams-flow.jpg', -6.9370119, 107.69213590000004, 0, N'~/Uploads/QRCode/KMJ-V-123.png')
GO
INSERT [dbo].[Asset] ([AssetId], [LocationId], [AssetNumber], [SubNumber], [Description], [ValueIdr], [ValueUsd], [Photo], [Latitude], [Longitude], [IsDeleted], [BarcodeImage]) VALUES (6, 143, NULL, N'0', N'Sumur', 0, 0, N'~/Uploads/16c6bb8c-3a0d-4fda-8293-620f8d16c3bf_image-logo-pge-merah-14.png', NULL, NULL, 0, NULL)
GO
INSERT [dbo].[Asset] ([AssetId], [LocationId], [AssetNumber], [SubNumber], [Description], [ValueIdr], [ValueUsd], [Photo], [Latitude], [Longitude], [IsDeleted], [BarcodeImage]) VALUES (1004, 680, NULL, N'0', N'Cellar LMB 1-1', 0, 0, N'~/Uploads/63d5dcae-bc7e-44f6-a27d-4037d478316a_login.JPG', NULL, NULL, 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[Asset] OFF
GO
INSERT [dbo].[AUC] ([WBSElement], [AssetNumber], [Description], [Plant], [CostCenter], [AcquisValIdr], [BookVal], [AssetClass], [SubNumber], [CapitalizedOn], [AccumDep], [Currency], [CompanyCode], [OriginalAsset], [SerialNumber], [UsefulLife], [UsefulPeriod], [ScrapVal], [ProfitCenter], [OrdinaryDep], [FirstAcquisition], [AssetCondition], [EvaluationGroup1], [EvaluationGroup3], [EvaluationGroup4], [Location], [EvaluationGroup5], [BalanceSheetItem], [OriginalValue], [OriginalAcquisitionYear], [DepreciationKey], [AccumOrdinaryDep], [PlannedOrdinaryDep], [NoUnitDeprec], [AcquisValUsd]) VALUES (N'N22/16/I/263/S/N1-01', N'70000045', N'LHD-Perkuatan Lereng Area Lahendong', N'E003', N'C0221001', 7130592953, NULL, N'1014', N'0', CAST(N'2016-09-13 00:00:00.000' AS DateTime), NULL, N'IDR', N'2022', NULL, NULL, NULL, NULL, NULL, N'C1502013', NULL, CAST(N'2016-09-13 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, N'1104013', NULL, NULL, N'0', NULL, NULL, NULL, 530433.04)
GO
INSERT [dbo].[AUC] ([WBSElement], [AssetNumber], [Description], [Plant], [CostCenter], [AcquisValIdr], [BookVal], [AssetClass], [SubNumber], [CapitalizedOn], [AccumDep], [Currency], [CompanyCode], [OriginalAsset], [SerialNumber], [UsefulLife], [UsefulPeriod], [ScrapVal], [ProfitCenter], [OrdinaryDep], [FirstAcquisition], [AssetCondition], [EvaluationGroup1], [EvaluationGroup3], [EvaluationGroup4], [Location], [EvaluationGroup5], [BalanceSheetItem], [OriginalValue], [OriginalAcquisitionYear], [DepreciationKey], [AccumOrdinaryDep], [PlannedOrdinaryDep], [NoUnitDeprec], [AcquisValUsd]) VALUES (N'N55/17/I/133/S/A4-04', N'80000184', N'KRH-Forklif 8 ton', N'E002', N'C0122001', 735000000, 735000000, N'1015', N'0', CAST(N'2017-12-30 00:00:00.000' AS DateTime), NULL, N'IDR', N'2022', NULL, NULL, NULL, NULL, NULL, N'C1506018', NULL, CAST(N'2017-12-30 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, N'KMJ', NULL, N'1104013', NULL, NULL, N'0', NULL, NULL, NULL, 54251.55)
GO
SET IDENTITY_INSERT [dbo].[CostCenter] ON 

GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (1, N'C0231001', N'General Manager Area Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (2, N'C0001001', N'President Director')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (3, N'C0001002', N'Exploration and Development Director')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (4, N'C0001003', N'Operation Director')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (5, N'C0001004', N'Finance Director ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (6, N'C0002001', N'Commissioner')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (7, N'C0002002', N'Commissioner Secretary')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (8, N'C0002003', N'Committee')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (9, N'C0011001', N'VP Strategic Planning and Business Development')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (10, N'C0011002', N'Portfolio and Business Development ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (11, N'C0011003', N'Planning and Evaluation ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (12, N'C0011004', N'Risk Management ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (13, N'C0011005', N'Quality Management ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (14, N'C0012001', N'VP Exploration')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (15, N'C0012002', N'Geoscience Region I')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (16, N'C0012003', N'Geoscience Region II')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (17, N'C0012004', N'Exploration Planning Operation')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (18, N'C0012006', N'Bukit Daun Project')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (19, N'C0013001', N'VP Project Development')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (20, N'C0013002', N'Project Planning and Control ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (21, N'C0013003', N'Project Management Officer ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (22, N'C0013004', N'Lumut Balai Project ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (23, N'C0013005', N'Hululais Project ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (24, N'C0013006', N'Karaha Project ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (25, N'C0013007', N'Sungai Penuh Project ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (26, N'C0013008', N'Kotamobagu Project ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (27, N'C0014001', N'Advisor Exploration and Development')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (28, N'C0015001', N'VP Exploitation')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (29, N'C0015002', N'Exploitation Geoscience and Reservoir Engineering')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (30, N'C0015003', N'Reservoir Management')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (31, N'C0021001', N'VP Operation and Engineering')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (32, N'C0021002', N'Engineering ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (33, N'C0021003', N'Production ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (34, N'C0021004', N'Reliability and Operation Excellence ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (35, N'C0022001', N'VP Drilling')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (36, N'C0022002', N'Drilling Planning  and Support ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (37, N'C0022003', N'Drilling Operation  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (38, N'C0022004', N'Advisor Drilling')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (39, N'C0023001', N'VP Partnership')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (40, N'C0023002', N'Partnership Planning and Evaluation  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (41, N'C0023003', N'Partnership Operation Control ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (42, N'C0024001', N'Advisor Engineering')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (43, N'C0031001', N'VP Controller')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (44, N'C0031002', N'Mgmt. Acc. And Policy Dev. and Assurance (Pda)  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (45, N'C0031003', N'Financial Accounting and Reporting ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (46, N'C0032001', N'VP Treasury')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (47, N'C0032002', N'Fund Management  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (48, N'C0032003', N'Financing and Tax  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (49, N'C0041001', N'Corporate Secretary')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (50, N'C0041002', N'Legal ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (51, N'C0041003', N'Government and Public Relation ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (52, N'C0042001', N'Head Of Internal Audit')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (53, N'C0042002', N'Internal Audit Region Sumatera  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (54, N'C0042003', N'Internal Audit Region Jawa and Kti  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (55, N'C0042004', N'Internal Audit Head Office  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (56, N'C0043001', N'Head Of HSSE')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (57, N'C0043002', N'Health and Safety  ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (58, N'C0043003', N'Environmental ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (59, N'C0043004', N'Inspection ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (60, N'C0043005', N'Security ')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (61, N'C0044001', N'Human Capital')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (62, N'C0045001', N'Supply Chain Management')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (63, N'C0046001', N'Geomatics and ICT')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (64, N'C0111001', N'Manager Operation Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (65, N'C0111002', N'Ass. Man. Production Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (66, N'C0111003', N'Ass. Man. Laboratory Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (67, N'C0111004', N'Junior Engineer Reservoir Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (68, N'C0121001', N'Manager Maintenance Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (69, N'C0121002', N'Ass. Man. Plant and Facility Maintenance Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (70, N'C0121003', N'Senior Engineer Maintenance Planning Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (71, N'C0121004', N'Senior Engineer Construction Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (72, N'C0131001', N'General Manager Area Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (73, N'C0131002', N'Manager Finance Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (74, N'C0131003', N'Area Manager HR Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (75, N'C0131004', N'Ass. Man. Government and Public Relation Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (76, N'C0131005', N'Legal Counsel Area Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (77, N'C0141001', N'Manager Planning and Engineering Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (78, N'C0141002', N'Area Manager HSSE Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (79, N'C0141003', N'Area Manager SCM Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (80, N'C0141004', N'Junior Analyst ICT Kamojang')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (81, N'C0211001', N'Manager Operation Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (82, N'C0211002', N'Ass. Man. Production Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (83, N'C0211003', N'Ass. Man. Laboratory Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (84, N'C0211004', N'Junior Engineer Reservoir Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (85, N'C0211005', N'ASSISTANT MANAGER PRODUCTION TOMPASO')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (86, N'C0221001', N'Manager Maintenance Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (87, N'C0221002', N'Ass. Man. Plant and Facility Maintenance Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (88, N'C0221003', N'Senior Engineer Maintenance Planning Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (89, N'C0221004', N'Senior Engineer Construction Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (91, N'C0231002', N'Manager Finance Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (92, N'C0231003', N'Area Manager HR Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (93, N'C0231004', N'Ass. Man. Government and Public Relation Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (94, N'C0231005', N'Legal Counsel Area Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (95, N'C0241001', N'Manager Planning and Engineering Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (96, N'C0241002', N'Area Manager HSSE Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (97, N'C0241003', N'Area Manager SCM Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (98, N'C0241004', N'Junior Analyst ICT Lahendong')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (99, N'C0311001', N'Senior Supervisor Operation and HSSE')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (100, N'C0331001', N'Manager Area Sibayak')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (101, N'C0331002', N'Assistant Administration')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (102, N'C0411001', N'Manager Operation Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (103, N'C0411002', N'Ass. Man. Production Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (104, N'C0411003', N'Ass. Man. Laboratory Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (105, N'C0411004', N'Junior Engineer Reservoir Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (106, N'C0421001', N'Manager Maintenance Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (107, N'C0421002', N'Ass. Man. Plant and Facility Maintenance Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (108, N'C0421003', N'Senior Engineer Maintenance Planning Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (109, N'C0421004', N'Senior Engineer Construction Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (110, N'C0431001', N'General Manager Area Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (111, N'C0431002', N'Manager Finance Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (112, N'C0431003', N'Area Manager HR Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (113, N'C0431004', N'Ass. Man. Government and Public Relation Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (114, N'C0431005', N'Legal Counsel Area Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (115, N'C0441001', N'Manager Planning and Engineering Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (116, N'C0441002', N'Area Manager HSSE Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (117, N'C0441003', N'Area Manager SCM Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (118, N'C0441004', N'Junior Analyst ICT Ulubelu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (119, N'C0511001', N'Manager Operation Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (120, N'C0511002', N'Assistant Manager Production Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (121, N'C0511003', N'Junior Engineer Reservoir Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (122, N'C0521001', N'Manager Maintenance Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (123, N'C0521002', N'Asman Plant and Facility Maintenance Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (124, N'C0521003', N'Engineer Maintenance Planning Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (125, N'C0531001', N'Area Manager Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (126, N'C0531002', N'Asman Finance Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (127, N'C0531003', N'Asman Hr Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (128, N'C0531004', N'Supervisor Government and Public Relation Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (129, N'C0531005', N'Legal Counsel Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (130, N'C0541001', N'Manager Planning and Engineering Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (131, N'C0541002', N'Asman Hsse Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (132, N'C0541003', N'Asman Scm Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (133, N'C0541004', N'Assistant Ict Karaha')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (134, N'C2001001', N'Management Lawu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (135, N'C2002001', N'EXPLORATION LAWU')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (136, N'C2003001', N'OPERATION LAWU')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (137, N'C2004001', N'Finance and Bussiness Support Lawu')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (138, N'C3001001', N'MANAGEMENT SEULAWAH')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (139, N'C3002001', N'OPERASIONAL SEULAWAH')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (140, N'C3003001', N'KEUANGAN SEULAWAH')
GO
INSERT [dbo].[CostCenter] ([CostCenterId], [Name], [AssetHolder]) VALUES (141, N'C3004001', N'SDM and Umum Seulawah')
GO
SET IDENTITY_INSERT [dbo].[CostCenter] OFF
GO
SET IDENTITY_INSERT [dbo].[Location] ON 

GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (1, N'LHD- CLS24')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (2, N'LHD- CLS22')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (3, N'LHD- CLS13')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (4, N'LHD-SMR37')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (5, N'LHD- CLS25')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (6, N'LHD- CLS2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (7, N'PGE-LHNDOG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (8, N'PGE-HO')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (9, N'KMJ-3340')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (10, N'KMJ-5354')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (11, N'KMJ-5672')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (12, N'KMJ-BKL1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (13, N'KMJ-BKL2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (14, N'KMJ-CHRA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (15, N'KMJ-CIKR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (16, N'KMJ-CTPS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (17, N'KMJ-DIPA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (18, N'KMJ-DRJ2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (19, N'KMJ-DRJ3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (20, N'KMJ-GD01')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (21, N'KMJ-GD02')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (22, N'KMJ-GK3L')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (23, N'KMJ-GLG1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (24, N'KMJ-GLG3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (25, N'KMJ-GRUT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (26, N'KMJ-GSPT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (27, N'KMJ-KGRT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (28, N'KMJ-KM00')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (29, N'KMJ-KM06')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (30, N'KMJ-KM07')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (31, N'KMJ-KM08')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (32, N'KMJ-KM09')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (33, N'KMJ-KM10')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (34, N'KMJ-KM11')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (35, N'KMJ-KM12')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (36, N'KMJ-KM13')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (37, N'KMJ-KM14')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (38, N'KMJ-KM15')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (39, N'KMJ-KM16')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (40, N'KMJ-KM17')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (41, N'KMJ-KM18')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (42, N'KMJ-KM19')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (43, N'KMJ-KM20')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (44, N'KMJ-KM21')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (45, N'KMJ-KM22')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (46, N'KMJ-KM23')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (47, N'KMJ-KM24')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (48, N'KMJ-KM25')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (49, N'KMJ-KM26')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (50, N'KMJ-KM27')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (51, N'KMJ-KM28')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (52, N'KMJ-KM29')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (53, N'KMJ-KM30')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (54, N'KMJ-KM31')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (55, N'KMJ-KM32')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (56, N'KMJ-KM33')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (57, N'KMJ-KM34')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (58, N'KMJ-KM35')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (59, N'KMJ-KM36')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (60, N'KMJ-KM37')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (61, N'KMJ-KM38')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (62, N'KMJ-KM39')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (63, N'KMJ-KM40')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (64, N'KMJ-KM41')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (65, N'KMJ-KM42')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (66, N'KMJ-KM43')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (67, N'KMJ-KM44')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (68, N'KMJ-KM45')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (69, N'KMJ-KM46')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (70, N'KMJ-KM47')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (71, N'KMJ-KM48')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (72, N'KMJ-KM49')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (73, N'KMJ-KM50')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (74, N'KMJ-KM51')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (75, N'KMJ-KM52')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (76, N'KMJ-KM53')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (77, N'KMJ-KM54')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (78, N'KMJ-KM55')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (79, N'KMJ-KM56')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (80, N'KMJ-KM57')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (81, N'KMJ-KM58')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (82, N'KMJ-KM59')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (83, N'KMJ-KM60')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (84, N'KMJ-KM61')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (85, N'KMJ-KM62')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (86, N'KMJ-KM63')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (87, N'KMJ-KM64')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (88, N'KMJ-KM65')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (89, N'KMJ-KM66')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (90, N'KMJ-KM67')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (91, N'KMJ-KM68')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (92, N'KMJ-KM69')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (93, N'KMJ-KM70')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (94, N'KMJ-KM71')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (95, N'KMJ-KM72')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (96, N'KMJ-KM73')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (97, N'KMJ-KM74')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (98, N'KMJ-KM76')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (99, N'KMJ-KMB2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (100, N'KMJ-KMJG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (101, N'KMJ-KOPS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (102, N'KMJ-KORPRI')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (103, N'KMJ-KPLN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (104, N'KMJ-KTEL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (105, N'KMJ-KTIN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (106, N'KMJ-KTPO')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (107, N'KMJ-KTR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (108, N'KMJ-LABR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (109, N'KMJ-LOK A')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (110, N'KMJ-LOK B')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (111, N'KMJ-LOK C')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (112, N'KMJ-LOK D')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (113, N'KMJ-LOKS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (114, N'KMJ-MESS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (115, N'KMJ-MKMJ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (116, N'KMJ-MONU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (117, N'KMJ-P401')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (118, N'KMJ-P402')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (119, N'KMJ-P403')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (120, N'KMJ-P404')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (121, N'KMJ-PKBL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (122, N'KMJ-RDPA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (123, N'KMJ-RENG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (124, N'KMJ-RGPA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (125, N'KMJ-RINF')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (126, N'KMJ-RJSU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (127, N'KMJ-RKEU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (128, N'KMJ-ROPR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (129, N'KMJ-ROPS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (130, N'KMJ-RPLM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (131, N'KMJ-RRPA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (132, N'KMJ-RTEK')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (133, N'PGE-SIBAYK')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (134, N'SBY-GDLA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (135, N'SBY-KTOR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (136, N'SBY-LKSA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (137, N'SBY-LKSB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (138, N'SBY-LKSC')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (139, N'SBY-LKSD')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (140, N'SBY-MESS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (141, N'SBY-TNHK')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (142, N'KMJ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (143, N'KMJ-3340')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (144, N'KMJ-5354')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (145, N'KMJ-5672')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (146, N'KMJ-BKL1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (147, N'KMJ-BKL2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (148, N'KMJ-BNGKL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (149, N'KMJ-BNGKL1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (150, N'KMJ-CHRA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (151, N'KMJ-CIHARU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (152, N'KMJ-CIKARO')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (153, N'KMJ-CIKR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (154, N'KMJ-CLUS14')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (155, N'KMJ-CLUST4')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (156, N'KMJ-DIPA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (157, N'KMJ-DRJ2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (158, N'KMJ-DRJ3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (159, N'KMJ-DRJT2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (160, N'KMJ-DRJT3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (161, N'KMJ-ENG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (162, N'KMJ-FASUJ8')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (163, N'KMJ-FSDW02')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (164, N'KMJ-FSOB01')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (165, N'KMJ-FSPC03')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (166, N'KMJ-FSUJ10')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (167, N'KMJ-FSUJ12')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (168, N'KMJ-G.SP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (169, N'KMJ-GARUT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (170, N'KMJ-GD01')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (171, N'KMJ-GD02')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (172, N'KMJ-GDNG 1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (173, N'KMJ-GDNG 2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (174, N'KMJ-GDNG 3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (175, N'KMJ-GDNG1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (176, N'KMJ-GK3L')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (177, N'KMJ-GLG1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (178, N'KMJ-GLG3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (179, N'KMJ-GRUT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (180, N'KMJ-GSPT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (181, N'KMJ-HILIR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (182, N'KMJ-HULU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (183, N'KMJ-INFKM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (184, N'KMJ-JSU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (185, N'KMJ-K3LL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (186, N'KMJ-KANTIN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (187, N'KMJ-KEU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (188, N'KMJ-KGRT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (189, N'KMJ-KM06')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (190, N'KMJ-KM07')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (191, N'KMJ-KM11')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (192, N'KMJ-KM12')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (193, N'KMJ-KM13')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (194, N'KMJ-KM14')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (195, N'KMJ-KM15')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (196, N'KMJ-KM17')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (197, N'KMJ-KM18')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (198, N'KMJ-KM19')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (199, N'KMJ-KM20')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (200, N'KMJ-KM21')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (201, N'KMJ-KM22')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (202, N'KMJ-KM23')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (203, N'KMJ-KM24')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (204, N'KMJ-KM25')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (205, N'KMJ-KM26')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (206, N'KMJ-KM27')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (207, N'KMJ-KM28')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (208, N'KMJ-KM29')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (209, N'KMJ-KM30')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (210, N'KMJ-KM31')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (211, N'KMJ-KM32')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (212, N'KMJ-KM33')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (213, N'KMJ-KM34')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (214, N'KMJ-KM35')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (215, N'KMJ-KM36')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (216, N'KMJ-KM37')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (217, N'KMJ-KM38')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (218, N'KMJ-KM39')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (219, N'KMJ-KM40')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (220, N'KMJ-KM41')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (221, N'KMJ-KM42')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (222, N'KMJ-KM43')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (223, N'KMJ-KM44')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (224, N'KMJ-KM45')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (225, N'KMJ-KM46')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (226, N'KMJ-KM47')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (227, N'KMJ-KM48')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (228, N'KMJ-KM49')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (229, N'KMJ-KM50')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (230, N'KMJ-KM51')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (231, N'KMJ-KM52')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (232, N'KMJ-KM53')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (233, N'KMJ-KM54')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (234, N'KMJ-KM55')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (235, N'KMJ-KM56')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (236, N'KMJ-KM57')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (237, N'KMJ-KM58')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (238, N'KMJ-KM59')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (239, N'KMJ-KM60')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (240, N'KMJ-KM61')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (241, N'KMJ-KM62')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (242, N'KMJ-KM63')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (243, N'KMJ-KM64')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (244, N'KMJ-KM65')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (245, N'KMJ-KM66')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (246, N'KMJ-KM67')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (247, N'KMJ-KM68')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (248, N'KMJ-KM69')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (249, N'KMJ-KM70')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (250, N'KMJ-KM71')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (251, N'KMJ-KM72')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (252, N'KMJ-KM73')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (253, N'KMJ-KM74')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (254, N'KMJ-KM76')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (255, N'KMJ-KMB2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (256, N'KMJ-KMJG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (257, N'KMJ-KOPS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (258, N'KMJ-KORPRI')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (259, N'KMJ-KPJAWA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (260, N'KMJ-KPLN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (261, N'KMJ-KTEL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (262, N'KMJ-KTIN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (263, N'KMJ-KTPO')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (264, N'KMJ-KTR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (265, N'KMJ-LAB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (266, N'KMJ-LABR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (267, N'KMJ-LHD')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (268, N'KMJ-LOK A')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (269, N'KMJ-LOK B')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (270, N'KMJ-LOK C')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (271, N'KMJ-LOK D')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (272, N'KMJ-LOKS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (273, N'KMJ-MESS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (274, N'KMJ-MKMJ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (275, N'KMJ-MONMN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (276, N'KMJ-MONU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (277, N'KMJ-OPRASI')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (278, N'KMJ-OPRTR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (279, N'KMJ-P A')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (280, N'KMJ-P1401')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (281, N'KMJ-P1718')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (282, N'KMJ-P1811')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (283, N'KMJ-P1901')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (284, N'KMJ-P1953')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (285, N'KMJ-P2241')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (286, N'KMJ-P2425')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (287, N'KMJ-P2443')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (288, N'KMJ-P2524')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (289, N'KMJ-P2736')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (290, N'KMJ-P3101')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (291, N'KMJ-P3138')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (292, N'KMJ-P3331')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (293, N'KMJ-P3601')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (294, N'KMJ-P3831')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (295, N'KMJ-P401')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (296, N'KMJ-P402')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (297, N'KMJ-P403')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (298, N'KMJ-P404')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (299, N'KMJ-P405')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (300, N'KMJ-P4301')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (301, N'KMJ-P4324')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (302, N'KMJ-P4819')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (303, N'KMJ-P4869')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (304, N'KMJ-P5319')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (305, N'KMJ-P6327')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (306, N'KMJ-P63CH')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (307, N'KMJ-P6948')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (308, N'KMJ-P7069')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (309, N'KMJ-PCH63')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (310, N'KMJ-PI3013')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (311, N'KMJ-PI3015')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (312, N'KMJ-PI3020')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (313, N'KMJ-PI3021')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (314, N'KMJ-PI3032')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (315, N'KMJ-PI3035')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (316, N'KMJ-PI3047')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (317, N'KMJ-PI3054')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (318, N'KMJ-PI3055')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (319, N'KMJ-PI3066')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (320, N'KMJ-PKBL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (321, N'KMJ-PLM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (322, N'KMJ-PLN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (323, N'KMJ-PLTP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (324, N'KMJ-PP 401')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (325, N'KMJ-PP 402')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (326, N'KMJ-PP 403')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (327, N'KMJ-PP 404')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (328, N'KMJ-RDPA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (329, N'KMJ-RENG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (330, N'KMJ-RGPA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (331, N'KMJ-RINF')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (332, N'KMJ-RJSU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (333, N'KMJ-RKEU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (334, N'KMJ-ROPR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (335, N'KMJ-ROPS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (336, N'KMJ-RPLM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (337, N'KMJ-RRPA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (338, N'KMJ-RTEK')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (339, N'KMJ-S33/34')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (340, N'KMJ-S53/54')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (341, N'KMJ-S56/72')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (342, N'KMJ-SCRUBR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (343, N'KMJ-SMR 11')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (344, N'KMJ-SMR 12')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (345, N'KMJ-SMR 13')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (346, N'KMJ-SMR 14')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (347, N'KMJ-SMR 15')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (348, N'KMJ-SMR 17')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (349, N'KMJ-SMR 18')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (350, N'KMJ-SMR 19')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (351, N'KMJ-SMR 20')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (352, N'KMJ-SMR 21')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (353, N'KMJ-SMR 22')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (354, N'KMJ-SMR 23')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (355, N'KMJ-SMR 24')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (356, N'KMJ-SMR 25')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (357, N'KMJ-SMR 26')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (358, N'KMJ-SMR 27')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (359, N'KMJ-SMR 28')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (360, N'KMJ-SMR 29')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (361, N'KMJ-SMR 30')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (362, N'KMJ-SMR 31')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (363, N'KMJ-SMR 32')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (364, N'KMJ-SMR 33')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (365, N'KMJ-SMR 34')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (366, N'KMJ-SMR 35')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (367, N'KMJ-SMR 36')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (368, N'KMJ-SMR 37')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (369, N'KMJ-SMR 38')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (370, N'KMJ-SMR 39')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (371, N'KMJ-SMR 40')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (372, N'KMJ-SMR 41')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (373, N'KMJ-SMR 42')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (374, N'KMJ-SMR 43')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (375, N'KMJ-SMR 44')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (376, N'KMJ-SMR 45')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (377, N'KMJ-SMR 46')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (378, N'KMJ-SMR 47')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (379, N'KMJ-SMR 48')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (380, N'KMJ-SMR 49')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (381, N'KMJ-SMR 50')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (382, N'KMJ-SMR 51')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (383, N'KMJ-SMR 52')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (384, N'KMJ-SMR 53')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (385, N'KMJ-SMR 54')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (386, N'KMJ-SMR 55')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (387, N'KMJ-SMR 56')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (388, N'KMJ-SMR 57')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (389, N'KMJ-SMR 58')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (390, N'KMJ-SMR 59')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (391, N'KMJ-SMR 6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (392, N'KMJ-SMR 60')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (393, N'KMJ-SMR 61')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (394, N'KMJ-SMR 62')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (395, N'KMJ-SMR 63')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (396, N'KMJ-SMR 64')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (397, N'KMJ-SMR 65')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (398, N'KMJ-SMR 66')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (399, N'KMJ-SMR 67')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (400, N'KMJ-SMR 68')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (401, N'KMJ-SMR 69')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (402, N'KMJ-SMR 7')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (403, N'KMJ-SMR 70')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (404, N'KMJ-SMR 71')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (405, N'KMJ-SMR 72')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (406, N'KMJ-SMR 73')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (407, N'KMJ-SMR 74')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (408, N'KMJ-SMR 75')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (409, N'KMJ-SMR 76')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (410, N'KMJ-SMR 77')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (411, N'KMJ-SMR 78')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (412, N'KMJ-SMR 79')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (413, N'KMJ-SMR 80')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (414, N'KMJ-SMR 81')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (415, N'KMJ-SMR 82')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (416, N'KMJ-SMR 83')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (417, N'KMJ-SMR 84')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (418, N'KMJ-SMR 85')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (419, N'KMJ-SMR 86')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (420, N'KMJ-SMR 87')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (421, N'KMJ-SMR B2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (422, N'KMJ-SMR RA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (423, N'KMJ-SPR 74')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (424, N'KMJ-SPRTR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (425, N'KMJ-TEKNIK')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (426, N'KMJ-TELKM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (427, N'KMJ-TOPO')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (428, N'KMJ-UMUM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (429, N'KMJ-WINANG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (430, N'PGE-KMJANG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (431, N'SBY-GDLA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (432, N'SBY-KTOR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (433, N'SBY-LKSA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (434, N'SBY-LKSB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (435, N'SBY-LKSC')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (436, N'SBY-LKSD')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (437, N'SBY-MESS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (438, N'SBY-TNHK')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (439, N'LHD')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (440, N'LHD - P&E')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (441, N'LHD -AP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (442, N'LHD -FIN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (443, N'LHD -HR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (444, N'LHD -HSSE')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (445, N'LHD -ICT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (446, N'LHD -LEGAL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (447, N'LHD -MAINT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (448, N'LHD -OPR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (449, N'LHD -PLTP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (450, N'LHD -SCM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (451, N'LHD -SGA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (452, N'LHD -SM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (453, N'LHD -SQB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (454, N'LHD -SQC')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (455, N'LHD -UCB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (456, N'LHD -UFC')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (457, N'LHD -UST')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (458, N'LHD -USU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (459, N'LHD -USV')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (460, N'LHD -UYA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (461, N'LHD -UYF')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (462, N'LHD -UZW')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (463, N'LHD -XCR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (464, N'LHD -XFT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (465, N'LHD -XJ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (466, N'LHD -XLG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (467, N'LHD -XLO')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (468, N'LHD -XML')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (469, N'LHD -ZWD')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (470, N'LHD- CLS1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (471, N'LHD- CLS13')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (472, N'LHD- CLS2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (473, N'LHD- CLS24')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (474, N'LHD- CLS25')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (475, N'LHD- CLS26')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (476, N'LHD- CLS27')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (477, N'LHD- CLS3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (478, N'LHD- CLS32')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (479, N'LHD- CLS4')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (480, N'LHD- CLS5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (481, N'LHD- CLS6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (482, N'LHD- CLS7')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (483, N'LHD- CLSR1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (484, N'LHD- CLSR2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (485, N'LHD-2P-U5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (486, N'LHD-2P-U6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (487, N'LHD-BRN U5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (488, N'LHD-BRN U6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (489, N'LHD-CR12')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (490, N'LHD-FCOM56')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (491, N'LHD-HILIR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (492, N'LHD-HULU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (493, N'LHD-OFFICE')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (494, N'LHD-PINAB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (495, N'LHD-PLTP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (496, N'LHD-PP DRN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (497, N'LHD-REINJ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (498, N'LHD-SAGS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (499, N'LHD-SCR3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (500, N'LHD-SCR4')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (501, N'LHD-SEP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (502, N'LHD-SEP A')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (503, N'LHD-SEP B')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (504, N'LHD-SMR 01')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (505, N'LHD-SMR 02')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (506, N'LHD-SMR 03')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (507, N'LHD-SMR 04')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (508, N'LHD-SMR 05')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (509, N'LHD-SMR 06')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (510, N'LHD-SMR 07')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (511, N'LHD-SMR 08')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (512, N'LHD-SMR 09')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (513, N'LHD-SMR 10')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (514, N'LHD-SMR 11')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (515, N'LHD-SMR 12')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (516, N'LHD-SMR 13')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (517, N'LHD-SMR 14')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (518, N'LHD-SMR 15')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (519, N'LHD-SMR 16')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (520, N'LHD-SMR 17')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (521, N'LHD-SMR 18')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (522, N'LHD-SMR 19')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (523, N'LHD-SMR 20')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (524, N'LHD-SMR 21')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (525, N'LHD-SMR 22')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (526, N'LHD-SMR 23')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (527, N'LHD-SMR 24')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (528, N'LHD-SMR 25')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (529, N'LHD-SMR 26')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (530, N'LHD-SMR 27')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (531, N'LHD-SMR 28')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (532, N'LHD-SMR 29')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (533, N'LHD-SMR 30')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (534, N'LHD-SMR 31')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (535, N'LHD-SMR 32')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (536, N'LHD-SMR 33')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (537, N'LHD-SMR 34')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (538, N'LHD-SMR 35')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (539, N'LHD-SMR 36')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (540, N'LHD-SMR 37')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (541, N'LHD-SMR 38')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (542, N'LHD-SMR 39')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (543, N'LHD-SMR 40')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (544, N'LHD-SMR 41')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (545, N'LHD-SMR 42')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (546, N'LHD-SMR 43')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (547, N'LHD-SMR 44')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (548, N'LHD-SMR 45')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (549, N'LHD-SMR 46')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (550, N'LHD-SMR 47')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (551, N'LHD-SMR 48')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (552, N'LHD-SMR 49')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (553, N'LHD-SMR 50')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (554, N'LHD-SMR 51')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (555, N'LHD-STM U5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (556, N'LHD-STM U6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (557, N'LHD-TOMP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (558, N'LHD-UMUM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (559, N'LHD-UNIT5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (560, N'LHD-UNIT6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (561, N'LHD-VENT-5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (562, N'LHD-VENT-6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (563, N'LHD-VENT56')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (564, N'LHD5&6-HUL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (565, N'PGE-LHNDOG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (566, N'PWR PLN U5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (567, N'PWR PLN U6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (568, N'UBL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (569, N'UBL-AP 12 ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (570, N'UBL-AP 13 ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (571, N'UBL-BRINE')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (572, N'UBL-CCR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (573, N'UBL-CLS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (574, N'UBL-CLS A')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (575, N'UBL-CLS B')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (576, N'UBL-CLS C')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (577, N'UBL-CLS D')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (578, N'UBL-CLS E')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (579, N'UBL-CLS F')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (580, N'UBL-CLS G')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (581, N'UBL-CLS H')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (582, N'UBL-CLS I')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (583, N'UBL-CLS J')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (584, N'UBL-CLS K')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (585, N'UBL-CLS R1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (586, N'UBL-CLS R2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (587, N'UBL-COND')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (588, N'UBL-DRAIN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (589, N'UBL-ENG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (590, N'UBL-EPCC')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (591, N'UBL-FIN')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (592, N'UBL-GDLB3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (593, N'UBL-GDLOG')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (594, N'UBL-GDOLH')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (595, N'UBL-GDPML')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (596, N'UBL-GPROD')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (597, N'UBL-HEADER')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (598, N'UBL-HILIR ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (599, N'UBL-HR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (600, N'UBL-HSSE')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (601, N'UBL-HULU  ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (602, N'UBL-ICT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (603, N'UBL-INTER ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (604, N'UBL-INTRFC')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (605, N'UBL-JLR AH')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (606, N'UBL-JLR BC')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (607, N'UBL-JLR C1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (608, N'UBL-JLR CA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (609, N'UBL-JLR D1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (610, N'UBL-JLR DF')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (611, N'UBL-JLR GB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (612, N'UBL-JLR IB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (613, N'UBL-JLR R1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (614, N'UBL-JLRPLT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (615, N'UBL-JLRSEP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (616, N'UBL-LAB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (617, N'UBL-LEGAL')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (618, N'UBL-MAINT')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (619, N'UBL-OFFICE')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (620, N'UBL-OPR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (621, N'UBL-PLTP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (622, N'UBL-PP0')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (623, N'UBL-PP3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (624, N'UBL-PP4')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (625, N'UBL-RM    ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (626, N'UBL-SCB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (627, N'UBL-SCB   ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (628, N'UBL-SCM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (629, N'UBL-SCRB')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (630, N'UBL-SEP')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (631, N'UBL-SEP 01')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (632, N'UBL-SEP 02')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (633, N'UBL-SEP 03')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (634, N'UBL-SMR 1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (635, N'UBL-SMR 10')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (636, N'UBL-SMR 11')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (637, N'UBL-SMR 12')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (638, N'UBL-SMR 13')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (639, N'UBL-SMR 14')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (640, N'UBL-SMR 15')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (641, N'UBL-SMR 16')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (642, N'UBL-SMR 17')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (643, N'UBL-SMR 18')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (644, N'UBL-SMR 19')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (645, N'UBL-SMR 2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (646, N'UBL-SMR 20')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (647, N'UBL-SMR 21')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (648, N'UBL-SMR 22')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (649, N'UBL-SMR 23')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (650, N'UBL-SMR 24')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (651, N'UBL-SMR 25')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (652, N'UBL-SMR 26')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (653, N'UBL-SMR 27')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (654, N'UBL-SMR 28')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (655, N'UBL-SMR 29')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (656, N'UBL-SMR 3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (657, N'UBL-SMR 30')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (658, N'UBL-SMR 31')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (659, N'UBL-SMR 32')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (660, N'UBL-SMR 33')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (661, N'UBL-SMR 34')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (662, N'UBL-SMR 35')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (663, N'UBL-SMR 36')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (664, N'UBL-SMR 37')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (665, N'UBL-SMR 38')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (666, N'UBL-SMR 39')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (667, N'UBL-SMR 4')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (668, N'UBL-SMR 5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (669, N'UBL-SMR 6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (670, N'UBL-SMR 7')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (671, N'UBL-SMR 8')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (672, N'UBL-SMR 9')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (673, N'UBL-SMR K1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (674, N'UBL-SMR K2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (675, N'UBL-SMR K3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (676, N'UBL-SMR S1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (677, N'UBL-SMR S2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (678, N'UBL-SMR S3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (679, N'UBL-STEAM')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (680, N'UBL-UMUM  ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (681, N'UBL-WTP   ')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (682, N'UBL-YARD')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (683, N'LUMUTBALAI')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (684, N'KRH-CLS 1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (685, N'KRH-CLS 2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (686, N'KRH-CLS 3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (687, N'KRH-CLS 4')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (688, N'KRH-CLS 5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (689, N'KRH-CLS 6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (690, N'KRH-GDOPR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (691, N'KRH-HILIR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (692, N'KRH-HILIR1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (693, N'KRH-HULU')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (694, N'KRH-HULU1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (695, N'KRH-KANTOR')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (696, N'KRH-PP 1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (697, N'KRH-SMR1K2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (698, N'KRH-SMR1K4')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (699, N'KRH-SMR1K5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (700, N'KRH-SMR1K6')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (701, N'KRH-SMR1T3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (702, N'KRH-SMR2K5')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (703, N'KRH-SMR2T3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (704, N'KRH-TLG 1')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (705, N'KRH-TLG 2')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (706, N'KRH-TLG 3')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (707, N'KRH-YARD')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (708, N'PGE-KARAHA')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (709, N'HULULAIS')
GO
INSERT [dbo].[Location] ([LocationId], [Name]) VALUES (710, N'SUNGAIPENU')
GO
SET IDENTITY_INSERT [dbo].[Location] OFF
GO
SET IDENTITY_INSERT [dbo].[Mapping] ON 

GO
INSERT [dbo].[Mapping] ([MappingId], [PlantId], [SubmitDate], [Description], [Status], [IsDeleted], [Notes]) VALUES (1, NULL, CAST(N'2018-09-18 14:50:00.000' AS DateTime), N'Mapping Aset Lahendong', N'APPROVE_BY_FINANCE', 0, NULL)
GO
INSERT [dbo].[Mapping] ([MappingId], [PlantId], [SubmitDate], [Description], [Status], [IsDeleted], [Notes]) VALUES (2, NULL, CAST(N'2018-10-02 15:04:00.000' AS DateTime), N'KRH-Forklif 8 ton (TEST AIMS)', N'APPROVE_BY_FINANCE', 0, NULL)
GO
INSERT [dbo].[Mapping] ([MappingId], [PlantId], [SubmitDate], [Description], [Status], [IsDeleted], [Notes]) VALUES (3, NULL, CAST(N'2018-10-10 18:55:00.000' AS DateTime), N'asdfasdffasd', N'REJECT_BY_FINANCE', 1, N'<p>Kurang ok</p><p>&nbsp;</p>')
GO
INSERT [dbo].[Mapping] ([MappingId], [PlantId], [SubmitDate], [Description], [Status], [IsDeleted], [Notes]) VALUES (4, NULL, CAST(N'2018-11-13 10:40:00.000' AS DateTime), N'tes', N'DRAFT', 1, NULL)
GO
INSERT [dbo].[Mapping] ([MappingId], [PlantId], [SubmitDate], [Description], [Status], [IsDeleted], [Notes]) VALUES (5, 14, CAST(N'2018-11-15 07:36:00.000' AS DateTime), N'Mapping Asset Kamojang', N'APPROVE_BY_FINANCE', 0, NULL)
GO
INSERT [dbo].[Mapping] ([MappingId], [PlantId], [SubmitDate], [Description], [Status], [IsDeleted], [Notes]) VALUES (6, 18, CAST(N'2018-12-07 14:47:00.000' AS DateTime), N'Mapping example', N'DRAFT', 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[Mapping] OFF
GO
SET IDENTITY_INSERT [dbo].[MappingAsset] ON 

GO
INSERT [dbo].[MappingAsset] ([MappingAssetId], [MappingId], [AssetId]) VALUES (1, 1, 1)
GO
INSERT [dbo].[MappingAsset] ([MappingAssetId], [MappingId], [AssetId]) VALUES (2, 2, 2)
GO
INSERT [dbo].[MappingAsset] ([MappingAssetId], [MappingId], [AssetId]) VALUES (3, 3, 3)
GO
INSERT [dbo].[MappingAsset] ([MappingAssetId], [MappingId], [AssetId]) VALUES (4, 4, 4)
GO
INSERT [dbo].[MappingAsset] ([MappingAssetId], [MappingId], [AssetId]) VALUES (5, 5, 5)
GO
INSERT [dbo].[MappingAsset] ([MappingAssetId], [MappingId], [AssetId]) VALUES (6, 6, 6)
GO
INSERT [dbo].[MappingAsset] ([MappingAssetId], [MappingId], [AssetId]) VALUES (1005, 6, 1004)
GO
SET IDENTITY_INSERT [dbo].[MappingAsset] OFF
GO
SET IDENTITY_INSERT [dbo].[MappingAssetActualCost] ON 

GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (1, 1, 84, N'N22/16/I/263/S/N1-01', 15950000, 1186.494172892104)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (3, 1, 83, N'N22/16/I/263/S/N1-01', 53486900, 3978.8022053957793)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (4, 1, 85, N'N22/16/I/263/S/N1-01', 49615280, 3690.79878410095)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (5, 2, 112, N'N55/17/I/133/S/A4-04', 735000000, 54251.55)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (6, 3, 78, N'N22/16/I/263/S/N1-01', 15950000, 1186.494172892104)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (7, 3, 83, N'N22/16/I/263/S/N1-01', 106013100, 7886.1395235252612)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (8, 5, 79, N'N22/16/I/263/S/N1-01', 43550000, 3239.6126162665287)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (9, 5, 80, N'N22/16/I/263/S/N1-01', 59500000, 4426.1067891586326)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (1027, 1005, 77, N'N22/16/I/263/S/N1-01', 150000000, 11158.252409643612)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (1028, 1005, 76, N'N22/16/I/263/S/N1-01', 5000000, 371.94174698812037)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (1032, 6, 79, NULL, 0, 0)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (1033, 6, 80, NULL, 0, 0)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (1034, 6, 81, NULL, 0, 0)
GO
INSERT [dbo].[MappingAssetActualCost] ([MappingAssetActualCostId], [MappingAssetId], [ActualCostId], [WBSElement], [ValueIdr], [ValueUsd]) VALUES (1035, 6, 82, NULL, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[MappingAssetActualCost] OFF
GO
INSERT [dbo].[MDMAsset] ([MDMRequestId], [AssetId], [AssetClass], [PostCap], [Description2], [SerialNumber], [EvaluationGroup1], [AssetCondition], [OriginalAsset], [OrigAcquisDate], [BookDepreKey], [UsefulLife01], [UsefulLife03]) VALUES (1, 1, N'5145', N'0', N'0', N'0', N'0', N'0', N'0', CAST(N'2018-03-10 10:00:00.000' AS DateTime), N'ZB02', N'10', N'20')
GO
INSERT [dbo].[MDMAsset] ([MDMRequestId], [AssetId], [AssetClass], [PostCap], [Description2], [SerialNumber], [EvaluationGroup1], [AssetCondition], [OriginalAsset], [OrigAcquisDate], [BookDepreKey], [UsefulLife01], [UsefulLife03]) VALUES (2, 2, N'7148', N'0', N'KRH-Forklif 8 ton (TEST AIMS)', N'0', N'0', N'DB', N'', CAST(N'2018-10-01 17:00:00.000' AS DateTime), N'ZB02', N'5', N'10')
GO
INSERT [dbo].[MDMAsset] ([MDMRequestId], [AssetId], [AssetClass], [PostCap], [Description2], [SerialNumber], [EvaluationGroup1], [AssetCondition], [OriginalAsset], [OrigAcquisDate], [BookDepreKey], [UsefulLife01], [UsefulLife03]) VALUES (3, 5, N'Class 5', N'Post', N'Desc 2', N'1234', N'123', N'Good', N'', CAST(N'2018-11-14 17:00:00.000' AS DateTime), N'12312', N'5 Tahun', N'2 Tahun')
GO
SET IDENTITY_INSERT [dbo].[MDMRequest] ON 

GO
INSERT [dbo].[MDMRequest] ([MDMRequestId], [SubmitDate], [Status], [LegalRequestor], [RequestTitle]) VALUES (1, CAST(N'2018-09-18 15:32:17.000' AS DateTime), N'COMPLETE', N'Nurul Amalia Lubis', N'kapitalisasi NBD LHD September 2018')
GO
INSERT [dbo].[MDMRequest] ([MDMRequestId], [SubmitDate], [Status], [LegalRequestor], [RequestTitle]) VALUES (2, CAST(N'2018-10-02 15:49:06.993' AS DateTime), N'COMPLETE', N'Nurul Amalia Lubis', N'Kapitalisasi KRH TEST AIMS')
GO
INSERT [dbo].[MDMRequest] ([MDMRequestId], [SubmitDate], [Status], [LegalRequestor], [RequestTitle]) VALUES (3, CAST(N'2018-11-15 07:49:46.473' AS DateTime), N'COMPLETE', N'mk.myudistiro', N'Perubahan Vesel Data')
GO
SET IDENTITY_INSERT [dbo].[MDMRequest] OFF
GO
SET IDENTITY_INSERT [dbo].[PhysicalCheck] ON 

GO
INSERT [dbo].[PhysicalCheck] ([PhysicalCheckId], [PhysicalCheckDate], [AssetId], [Condition], [IsDeleted], [CheckedBy]) VALUES (1, CAST(N'2018-09-18 15:57:47.000' AS DateTime), 1, N'GOOD', 0, N'superadmin')
GO
INSERT [dbo].[PhysicalCheck] ([PhysicalCheckId], [PhysicalCheckDate], [AssetId], [Condition], [IsDeleted], [CheckedBy]) VALUES (2, CAST(N'2018-09-18 16:10:48.000' AS DateTime), 1, N'GOOD', 0, N'superadmin')
GO
INSERT [dbo].[PhysicalCheck] ([PhysicalCheckId], [PhysicalCheckDate], [AssetId], [Condition], [IsDeleted], [CheckedBy]) VALUES (3, CAST(N'2018-10-02 16:08:00.000' AS DateTime), 2, N'GOOD', 0, N'superadmin')
GO
INSERT [dbo].[PhysicalCheck] ([PhysicalCheckId], [PhysicalCheckDate], [AssetId], [Condition], [IsDeleted], [CheckedBy]) VALUES (4, CAST(N'2018-11-13 15:23:00.000' AS DateTime), 2, N'GOOD', 0, N'superadmin')
GO
INSERT [dbo].[PhysicalCheck] ([PhysicalCheckId], [PhysicalCheckDate], [AssetId], [Condition], [IsDeleted], [CheckedBy]) VALUES (5, CAST(N'2018-11-15 07:51:00.000' AS DateTime), 5, N'GOOD', 0, N'mk.myudistiro')
GO
SET IDENTITY_INSERT [dbo].[PhysicalCheck] OFF
GO
SET IDENTITY_INSERT [dbo].[PhysicalCheckAsset] ON 

GO
INSERT [dbo].[PhysicalCheckAsset] ([ImageId], [PhysicalCheckId], [Image]) VALUES (1, 1, N'~/Uploads/0640018b-e147-4838-83e8-c38b59310aca_15372610856086332216519943478866.jpg')
GO
INSERT [dbo].[PhysicalCheckAsset] ([ImageId], [PhysicalCheckId], [Image]) VALUES (2, 2, N'~/Uploads/19056500-1f76-4191-b349-2db1e884d827_1537261696708864321764614993845.jpg')
GO
INSERT [dbo].[PhysicalCheckAsset] ([ImageId], [PhysicalCheckId], [Image]) VALUES (3, 3, N'~/Uploads/8156d53c-fd43-48b2-88ee-75dd7f6be71d_saluran-primer-lhd-13.jpg')
GO
SET IDENTITY_INSERT [dbo].[PhysicalCheckAsset] OFF
GO
SET IDENTITY_INSERT [dbo].[Plant] ON 

GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (2, N'E003', N'PGE Area Lahendong')
GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (12, N'CE01', N'PGE Kantor Pusat ')
GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (13, N'E001', N'PGE Area Sibayak')
GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (14, N'E002', N'PGE Area Kamojang')
GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (16, N'E004', N'PGE Area Ulubelu')
GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (17, N'E005', N'PGE Lumut Balai')
GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (18, N'E006', N'PGE Area Karaha ')
GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (19, N'E007', N'PGE Hululais')
GO
INSERT [dbo].[Plant] ([PlantId], [Code], [Name]) VALUES (20, N'E009', N'PGE Sungai Penuh')
GO
SET IDENTITY_INSERT [dbo].[Plant] OFF
GO
SET IDENTITY_INSERT [dbo].[SPCRequest] ON 

GO
INSERT [dbo].[SPCRequest] ([SPCRequestId], [PlantId], [SubmitDate], [Status], [ScrapType], [NoRequestSPC005], [NoRequestSPC011], [Plant], [CostCenter], [AcquisitionType], [DocABVN], [PreparedBy], [LegalRequestor]) VALUES (1, NULL, CAST(N'2018-09-18 15:46:00.000' AS DateTime), N'COMPLETED', N'CAPITALIZATION', N'001', N'002', N'CE01', N'C0', N'CURRENT', 0, N'superadmin', N'Nurul Amalia Lubis')
GO
INSERT [dbo].[SPCRequest] ([SPCRequestId], [PlantId], [SubmitDate], [Status], [ScrapType], [NoRequestSPC005], [NoRequestSPC011], [Plant], [CostCenter], [AcquisitionType], [DocABVN], [PreparedBy], [LegalRequestor]) VALUES (2, NULL, CAST(N'2018-10-02 15:57:47.000' AS DateTime), N'COMPLETED', N'CAPITALIZATION', N'01', N'02', N'E003', N'C00000', N'CURRENT', 0, N'superadmin', N'Nurul Amalia Lubis')
GO
INSERT [dbo].[SPCRequest] ([SPCRequestId], [PlantId], [SubmitDate], [Status], [ScrapType], [NoRequestSPC005], [NoRequestSPC011], [Plant], [CostCenter], [AcquisitionType], [DocABVN], [PreparedBy], [LegalRequestor]) VALUES (3, 14, CAST(N'2018-11-15 07:50:51.707' AS DateTime), N'COMPLETED', N'NON_CAPITALIZATION', N'56789', N'', NULL, N'Cost Center', N'CURRENT', 0, N'mk.myudistiro', N'mk.myudistiro')
GO
SET IDENTITY_INSERT [dbo].[SPCRequest] OFF
GO
INSERT [dbo].[SPCRequestAsset] ([SPCRequestId], [AssetId], [DocDate], [PostingDate], [AssetValueDate], [PostingPeriod], [TransactionType], [AssetTransaction]) VALUES (1, 1, CAST(N'2018-09-18 10:00:00.000' AS DateTime), CAST(N'2018-09-18 10:00:00.000' AS DateTime), CAST(N'2018-09-18 10:00:00.000' AS DateTime), N'09', N'CC_SETTLEMENT_CURRENT_YEAR_200', N'CAPITALIZATION_100')
GO
INSERT [dbo].[SPCRequestAsset] ([SPCRequestId], [AssetId], [DocDate], [PostingDate], [AssetValueDate], [PostingPeriod], [TransactionType], [AssetTransaction]) VALUES (2, 2, CAST(N'2018-10-02 10:00:00.000' AS DateTime), CAST(N'2018-10-02 10:00:00.000' AS DateTime), CAST(N'2018-10-02 10:00:00.000' AS DateTime), N'10', N'CC_SETTLEMENT_CURRENT_YEAR_200', N'CAPITALIZATION_100')
GO
INSERT [dbo].[SPCRequestAsset] ([SPCRequestId], [AssetId], [DocDate], [PostingDate], [AssetValueDate], [PostingPeriod], [TransactionType], [AssetTransaction]) VALUES (3, 5, CAST(N'2018-10-31 17:00:00.000' AS DateTime), CAST(N'2018-10-31 17:00:00.000' AS DateTime), CAST(N'2018-11-14 17:00:00.000' AS DateTime), N'4', N'CC_SETTLEMENT_CURRENT_YEAR_200', N'CAPITALIZATION_100')
GO
SET IDENTITY_INSERT [dbo].[TCR] ON 

GO
INSERT [dbo].[TCR] ([TCRId], [SubmitDate], [Status], [ProjectName], [WBS], [StartDate], [EndDate], [ProjectDefinition], [CostCenter], [Plant], [LocationId], [AssetHolder], [Budget], [Currency], [Notes], [ValueIdr], [ValueUsd], [IsDeleted]) VALUES (1, CAST(N'2018-09-18 15:17:36.000' AS DateTime), N'APPROVE_BY_FINANCE', N'LHD-Perkuatan Lereng Area Lahendong', N'N22/16/I/263/S/N1-01', CAST(N'2016-06-27 00:00:00.000' AS DateTime), CAST(N'2018-03-10 00:00:00.000' AS DateTime), N'Perkuatan lereng di cluster LHD', 1, 2, 1, N'GM Area lahendong', 8550000000, N'IDR', NULL, 119052180, 8856.1, 0)
GO
INSERT [dbo].[TCR] ([TCRId], [SubmitDate], [Status], [ProjectName], [WBS], [StartDate], [EndDate], [ProjectDefinition], [CostCenter], [Plant], [LocationId], [AssetHolder], [Budget], [Currency], [Notes], [ValueIdr], [ValueUsd], [IsDeleted]) VALUES (2, CAST(N'2018-10-02 15:45:25.000' AS DateTime), N'APPROVE_BY_FINANCE', N'KRH Forklif 8 ton TEST AIMS', N'N55/17/I/133/S/A4-04', CAST(N'2017-06-22 00:00:00.000' AS DateTime), CAST(N'2018-04-06 00:00:00.000' AS DateTime), N'KRH Forklif 8 ton TEST AIMS', 1, 2, NULL, N'GM Area lahendong', 800000000, N'IDR', N'KRH Forklif 8 ton TEST AIMS', 735000000, 54251.55, 0)
GO
INSERT [dbo].[TCR] ([TCRId], [SubmitDate], [Status], [ProjectName], [WBS], [StartDate], [EndDate], [ProjectDefinition], [CostCenter], [Plant], [LocationId], [AssetHolder], [Budget], [Currency], [Notes], [ValueIdr], [ValueUsd], [IsDeleted]) VALUES (3, CAST(N'2018-11-15 07:47:26.000' AS DateTime), N'APPROVE_BY_FINANCE', N'KMJ-Pengadaan Rak Arsip', N'N11/17/I/910/S/A4-01', CAST(N'2018-11-01 00:00:00.000' AS DateTime), CAST(N'2018-11-15 00:00:00.000' AS DateTime), N'KMJ-V-123', 15, 14, NULL, N'Geoscience Region I', 20000000, N'IDR', NULL, 103050000, 7665.72, 0)
GO
SET IDENTITY_INSERT [dbo].[TCR] OFF
GO
INSERT [dbo].[TCRAsset] ([TCRId], [AssetId]) VALUES (1, 1)
GO
INSERT [dbo].[TCRAsset] ([TCRId], [AssetId]) VALUES (2, 2)
GO
INSERT [dbo].[TCRAsset] ([TCRId], [AssetId]) VALUES (3, 5)
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_Location] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Location] ([LocationId])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_Location]
GO
ALTER TABLE [dbo].[Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Mapping_Plant] FOREIGN KEY([PlantId])
REFERENCES [dbo].[Plant] ([PlantId])
GO
ALTER TABLE [dbo].[Mapping] CHECK CONSTRAINT [FK_Mapping_Plant]
GO
ALTER TABLE [dbo].[MappingAsset]  WITH CHECK ADD  CONSTRAINT [FK_MappingAsset_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
GO
ALTER TABLE [dbo].[MappingAsset] CHECK CONSTRAINT [FK_MappingAsset_Asset]
GO
ALTER TABLE [dbo].[MappingAsset]  WITH CHECK ADD  CONSTRAINT [FK_MappingAsset_Mapping] FOREIGN KEY([MappingId])
REFERENCES [dbo].[Mapping] ([MappingId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MappingAsset] CHECK CONSTRAINT [FK_MappingAsset_Mapping]
GO
ALTER TABLE [dbo].[MappingAssetActualCost]  WITH CHECK ADD  CONSTRAINT [FK_MappingAssetActualCost_ActualCost] FOREIGN KEY([ActualCostId])
REFERENCES [dbo].[ActualCost] ([ActualCostId])
GO
ALTER TABLE [dbo].[MappingAssetActualCost] CHECK CONSTRAINT [FK_MappingAssetActualCost_ActualCost]
GO
ALTER TABLE [dbo].[MappingAssetActualCost]  WITH CHECK ADD  CONSTRAINT [FK_MappingAssetActualCost_AUC] FOREIGN KEY([WBSElement])
REFERENCES [dbo].[AUC] ([WBSElement])
GO
ALTER TABLE [dbo].[MappingAssetActualCost] CHECK CONSTRAINT [FK_MappingAssetActualCost_AUC]
GO
ALTER TABLE [dbo].[MappingAssetActualCost]  WITH CHECK ADD  CONSTRAINT [FK_MappingAssetActualCost_MappingAsset] FOREIGN KEY([MappingAssetId])
REFERENCES [dbo].[MappingAsset] ([MappingAssetId])
GO
ALTER TABLE [dbo].[MappingAssetActualCost] CHECK CONSTRAINT [FK_MappingAssetActualCost_MappingAsset]
GO
ALTER TABLE [dbo].[MDMAsset]  WITH CHECK ADD  CONSTRAINT [FK_MDMAsset_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MDMAsset] CHECK CONSTRAINT [FK_MDMAsset_Asset]
GO
ALTER TABLE [dbo].[MDMAsset]  WITH CHECK ADD  CONSTRAINT [FK_MDMAsset_MDMRequest] FOREIGN KEY([MDMRequestId])
REFERENCES [dbo].[MDMRequest] ([MDMRequestId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MDMAsset] CHECK CONSTRAINT [FK_MDMAsset_MDMRequest]
GO
ALTER TABLE [dbo].[PhysicalCheck]  WITH CHECK ADD  CONSTRAINT [FK_PhysicalCheck_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
GO
ALTER TABLE [dbo].[PhysicalCheck] CHECK CONSTRAINT [FK_PhysicalCheck_Asset]
GO
ALTER TABLE [dbo].[PhysicalCheckAsset]  WITH CHECK ADD  CONSTRAINT [FK_PhysicalCheckAsset_PhysicalCheck] FOREIGN KEY([PhysicalCheckId])
REFERENCES [dbo].[PhysicalCheck] ([PhysicalCheckId])
GO
ALTER TABLE [dbo].[PhysicalCheckAsset] CHECK CONSTRAINT [FK_PhysicalCheckAsset_PhysicalCheck]
GO
ALTER TABLE [dbo].[RejectMappingHistory]  WITH CHECK ADD  CONSTRAINT [FK_RejectMappingHistory_Mapping] FOREIGN KEY([MappingId])
REFERENCES [dbo].[Mapping] ([MappingId])
GO
ALTER TABLE [dbo].[RejectMappingHistory] CHECK CONSTRAINT [FK_RejectMappingHistory_Mapping]
GO
ALTER TABLE [dbo].[RejectTCRHistory]  WITH CHECK ADD  CONSTRAINT [FK_RejectTCRHistory_TCR] FOREIGN KEY([TCRId])
REFERENCES [dbo].[TCR] ([TCRId])
GO
ALTER TABLE [dbo].[RejectTCRHistory] CHECK CONSTRAINT [FK_RejectTCRHistory_TCR]
GO
ALTER TABLE [dbo].[SPCRequest]  WITH CHECK ADD  CONSTRAINT [FK_SPCRequest_Plant] FOREIGN KEY([PlantId])
REFERENCES [dbo].[Plant] ([PlantId])
GO
ALTER TABLE [dbo].[SPCRequest] CHECK CONSTRAINT [FK_SPCRequest_Plant]
GO
ALTER TABLE [dbo].[SPCRequestAsset]  WITH CHECK ADD  CONSTRAINT [FK_SPCRequestAsset_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
GO
ALTER TABLE [dbo].[SPCRequestAsset] CHECK CONSTRAINT [FK_SPCRequestAsset_Asset]
GO
ALTER TABLE [dbo].[SPCRequestAsset]  WITH CHECK ADD  CONSTRAINT [FK_SPCRequestAsset_SPCRequest] FOREIGN KEY([SPCRequestId])
REFERENCES [dbo].[SPCRequest] ([SPCRequestId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SPCRequestAsset] CHECK CONSTRAINT [FK_SPCRequestAsset_SPCRequest]
GO
ALTER TABLE [dbo].[TCR]  WITH CHECK ADD  CONSTRAINT [FK_TCR_CostCenter] FOREIGN KEY([CostCenter])
REFERENCES [dbo].[CostCenter] ([CostCenterId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TCR] CHECK CONSTRAINT [FK_TCR_CostCenter]
GO
ALTER TABLE [dbo].[TCR]  WITH CHECK ADD  CONSTRAINT [FK_TCR_Location] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Location] ([LocationId])
GO
ALTER TABLE [dbo].[TCR] CHECK CONSTRAINT [FK_TCR_Location]
GO
ALTER TABLE [dbo].[TCR]  WITH CHECK ADD  CONSTRAINT [FK_TCR_Plant] FOREIGN KEY([Plant])
REFERENCES [dbo].[Plant] ([PlantId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TCR] CHECK CONSTRAINT [FK_TCR_Plant]
GO
ALTER TABLE [dbo].[TCRAsset]  WITH CHECK ADD  CONSTRAINT [FK_TCRAsset_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
GO
ALTER TABLE [dbo].[TCRAsset] CHECK CONSTRAINT [FK_TCRAsset_Asset]
GO
ALTER TABLE [dbo].[TCRAsset]  WITH CHECK ADD  CONSTRAINT [FK_TCRAsset_TCR] FOREIGN KEY([TCRId])
REFERENCES [dbo].[TCR] ([TCRId])
GO
ALTER TABLE [dbo].[TCRAsset] CHECK CONSTRAINT [FK_TCRAsset_TCR]
GO
USE [master]
GO
ALTER DATABASE [PGE.AIMS.APP] SET  READ_WRITE 
GO
