USE [master]
GO
/****** Object:  Database [PGE.AIMS.APP]    Script Date: 5/9/2018 2:15:21 PM ******/
CREATE DATABASE [PGE.AIMS.APP]
GO
ALTER DATABASE [PGE.AIMS.APP] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PGE.AIMS.APP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PGE.AIMS.APP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET ARITHABORT OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PGE.AIMS.APP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PGE.AIMS.APP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PGE.AIMS.APP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PGE.AIMS.APP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET RECOVERY FULL 
GO
ALTER DATABASE [PGE.AIMS.APP] SET  MULTI_USER 
GO
ALTER DATABASE [PGE.AIMS.APP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PGE.AIMS.APP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PGE.AIMS.APP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PGE.AIMS.APP] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PGE.AIMS.APP] SET DELAYED_DURABILITY = DISABLED 
GO
USE [PGE.AIMS.APP]
GO
/****** Object:  UserDefinedFunction [dbo].[bebas]    Script Date: 5/9/2018 2:15:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[bebas]()
returns int 
as 
begin
	declare @Hasil int;
		set @Hasil = (select count(*) from Crew);
	return @Hasil;

end
GO
/****** Object:  Table [dbo].[Airport]    Script Date: 5/9/2018 2:15:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Airport](
	[AirportId] [int] IDENTITY(1,1) NOT NULL,
	[AirportName] [varchar](100) NOT NULL,
	[AirportAddress] [varchar](250) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Airport] PRIMARY KEY CLUSTERED 
(
	[AirportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 5/9/2018 2:15:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](100) NOT NULL,
	[CompanyAddress] [varchar](250) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Crew]    Script Date: 5/9/2018 2:15:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Crew](
	[CrewId] [varchar](20) NOT NULL,
	[CrewName] [varchar](100) NOT NULL,
	[Status] [varchar](20) NOT NULL,
	[RegisterDate] [date] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_Crew] PRIMARY KEY CLUSTERED 
(
	[CrewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CrewCompany]    Script Date: 5/9/2018 2:15:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CrewCompany](
	[CrewId] [varchar](20) NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_CrewCompany] PRIMARY KEY CLUSTERED 
(
	[CrewId] ASC,
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Whitelist]    Script Date: 5/9/2018 2:15:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Whitelist](
	[WhitelistId] [int] IDENTITY(1,1) NOT NULL,
	[CrewId] [varchar](20) NOT NULL,
	[AirportId] [int] NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDateTimeUtc] [datetime] NOT NULL,
	[ModifiedBy] [varchar](50) NOT NULL,
	[ModifiedDateTimeUtc] [datetime] NOT NULL,
 CONSTRAINT [PK_WhiteList] PRIMARY KEY CLUSTERED 
(
	[WhitelistId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Airport] ON 

INSERT [dbo].[Airport] ([AirportId], [AirportName], [AirportAddress], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Soekarno Hatta', N'Jakarta', N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), 0)
INSERT [dbo].[Airport] ([AirportId], [AirportName], [AirportAddress], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'Abdurrachman Saleh', N'Malang', N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), 0)
INSERT [dbo].[Airport] ([AirportId], [AirportName], [AirportAddress], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, N'Husein Sastranegara', N'Bandung', N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), 0)
INSERT [dbo].[Airport] ([AirportId], [AirportName], [AirportAddress], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (4, N'Adi Sucipto', N'Yogyakarta', N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), 0)
INSERT [dbo].[Airport] ([AirportId], [AirportName], [AirportAddress], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (5, N'Ngurah Rai', N'Bali', N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T08:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Airport] OFF
SET IDENTITY_INSERT [dbo].[Company] ON 

INSERT [dbo].[Company] ([CompanyId], [CompanyName], [CompanyAddress], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (1, N'Garuda Indonesia', NULL, N'superadmin', CAST(N'2018-04-14T06:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T06:00:00.000' AS DateTime), 0)
INSERT [dbo].[Company] ([CompanyId], [CompanyName], [CompanyAddress], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (2, N'Batavia Air', NULL, N'superadmin', CAST(N'2018-04-14T07:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T07:00:00.000' AS DateTime), 0)
INSERT [dbo].[Company] ([CompanyId], [CompanyName], [CompanyAddress], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc], [IsDeleted]) VALUES (3, N'Lion Air', NULL, N'superadmin', CAST(N'2018-04-14T07:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T07:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Company] OFF
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180414-001', N'TONY HIDAYAT', N'PILOT', CAST(N'2018-04-14' AS Date), N'superadmin', CAST(N'2018-04-14T06:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-23T15:44:24.637' AS DateTime))
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180415-002', N'DEDE PUSPITA', N'STEWARDESS', CAST(N'2018-04-15' AS Date), N'superadmin', CAST(N'2018-04-15T02:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-23T15:44:19.443' AS DateTime))
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180415-003', N'VERDIANA VERA', N'STEWARDESS', CAST(N'2018-04-15' AS Date), N'superadmin', CAST(N'2018-04-15T03:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-23T15:44:12.080' AS DateTime))
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180416-004', N'AKBAR SATRIANDI', N'PILOT', CAST(N'2018-04-16' AS Date), N'superadmin', CAST(N'2018-04-15T18:54:54.777' AS DateTime), N'superadmin', CAST(N'2018-04-23T15:44:05.250' AS DateTime))
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180416-005', N'ADRIAN RAMADHAN', N'PILOT', CAST(N'2018-04-16' AS Date), N'superadmin', CAST(N'2018-04-15T18:59:41.377' AS DateTime), N'superadmin', CAST(N'2018-04-23T15:43:57.860' AS DateTime))
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180416-006', N'SARAH WIDIANTI', N'PILOT', CAST(N'2018-04-16' AS Date), N'superadmin', CAST(N'2018-04-15T21:41:04.737' AS DateTime), N'superadmin', CAST(N'2018-04-23T15:43:47.857' AS DateTime))
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180423-007', N'TIARA SANDRA', N'STEWARDESS', CAST(N'2018-04-23' AS Date), N'superadmin', CAST(N'2018-04-23T15:50:31.833' AS DateTime), N'superadmin', CAST(N'2018-04-23T15:50:31.833' AS DateTime))
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180423-008', N'NAMIRAH HASSAN', N'STEWARDESS', CAST(N'2018-04-23' AS Date), N'superadmin', CAST(N'2018-04-23T15:51:27.323' AS DateTime), N'superadmin', CAST(N'2018-04-25T20:56:10.443' AS DateTime))
INSERT [dbo].[Crew] ([CrewId], [CrewName], [Status], [RegisterDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (N'20180423-009', N'TYREX', N'STEWARDESS', CAST(N'2018-04-23' AS Date), N'superadmin', CAST(N'2018-04-23T15:51:27.323' AS DateTime), N'superadmin', CAST(N'2018-04-25T20:56:10.443' AS DateTime))
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180414-001', 1)
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180415-002', 3)
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180415-003', 3)
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180416-004', 1)
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180416-005', 1)
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180416-006', 2)
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180423-007', 1)
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180423-008', 1)
INSERT [dbo].[CrewCompany] ([CrewId], [CompanyId]) VALUES (N'20180423-009', 2)
SET IDENTITY_INSERT [dbo].[Whitelist] ON 

INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (1, N'20180414-001', 4, CAST(N'2018-04-14' AS Date), CAST(N'2018-04-16' AS Date), N'superadmin', CAST(N'2018-04-14T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-14T00:00:00.000' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (2, N'20180415-002', 2, CAST(N'2018-04-15' AS Date), CAST(N'2018-04-17' AS Date), N'superadmin', CAST(N'2018-04-15T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-15T00:00:00.000' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (3, N'20180415-002', 3, CAST(N'2018-04-19' AS Date), CAST(N'2018-04-21' AS Date), N'superadmin', CAST(N'2018-04-19T00:00:00.000' AS DateTime), N'superadmin', CAST(N'2018-04-19T00:00:00.000' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (4, N'20180414-001', 5, CAST(N'2018-04-18' AS Date), CAST(N'2018-04-20' AS Date), N'superadmin', CAST(N'2018-04-23T07:48:29.123' AS DateTime), N'superadmin', CAST(N'2018-04-23T08:38:43.380' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (5, N'20180415-003', 1, CAST(N'2018-04-22' AS Date), CAST(N'2018-04-24' AS Date), N'superadmin', CAST(N'2018-04-23T08:39:30.327' AS DateTime), N'superadmin', CAST(N'2018-04-23T08:58:54.037' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (6, N'20180416-004', 5, CAST(N'2018-04-23' AS Date), CAST(N'2018-04-25' AS Date), N'superadmin', CAST(N'2018-04-23T13:44:45.220' AS DateTime), N'superadmin', CAST(N'2018-04-23T13:44:45.220' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (7, N'20180416-004', 1, CAST(N'2018-04-27' AS Date), CAST(N'2018-04-29' AS Date), N'superadmin', CAST(N'2018-04-23T13:52:44.430' AS DateTime), N'superadmin', CAST(N'2018-04-24T14:51:20.057' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (8, N'20180416-006', 5, CAST(N'2018-04-23' AS Date), CAST(N'2018-04-25' AS Date), N'superadmin', CAST(N'2018-04-23T13:54:08.407' AS DateTime), N'superadmin', CAST(N'2018-04-23T13:54:08.407' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (9, N'20180423-008', 5, CAST(N'2018-04-23' AS Date), CAST(N'2018-04-25' AS Date), N'superadmin', CAST(N'2018-04-23T15:52:14.693' AS DateTime), N'superadmin', CAST(N'2018-04-23T17:27:41.730' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (11, N'20180415-002', 2, CAST(N'2018-04-25' AS Date), CAST(N'2018-04-27' AS Date), N'superadmin', CAST(N'2018-04-25T13:37:10.497' AS DateTime), N'superadmin', CAST(N'2018-04-25T13:37:10.497' AS DateTime))
INSERT [dbo].[Whitelist] ([WhitelistId], [CrewId], [AirportId], [StartDate], [EndDate], [CreatedBy], [CreatedDateTimeUtc], [ModifiedBy], [ModifiedDateTimeUtc]) VALUES (12, N'20180415-003', 4, CAST(N'2018-04-25' AS Date), CAST(N'2018-04-27' AS Date), N'superadmin', CAST(N'2018-04-25T13:40:00.757' AS DateTime), N'superadmin', CAST(N'2018-04-25T13:40:00.757' AS DateTime))
SET IDENTITY_INSERT [dbo].[Whitelist] OFF
ALTER TABLE [dbo].[CrewCompany]  WITH CHECK ADD  CONSTRAINT [FK_CrewCompany_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CrewCompany] CHECK CONSTRAINT [FK_CrewCompany_Company]
GO
ALTER TABLE [dbo].[CrewCompany]  WITH CHECK ADD  CONSTRAINT [FK_CrewCompany_Crew] FOREIGN KEY([CrewId])
REFERENCES [dbo].[Crew] ([CrewId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CrewCompany] CHECK CONSTRAINT [FK_CrewCompany_Crew]
GO
ALTER TABLE [dbo].[Whitelist]  WITH CHECK ADD  CONSTRAINT [FK_WhiteList_Airport] FOREIGN KEY([AirportId])
REFERENCES [dbo].[Airport] ([AirportId])
GO
ALTER TABLE [dbo].[Whitelist] CHECK CONSTRAINT [FK_WhiteList_Airport]
GO
ALTER TABLE [dbo].[Whitelist]  WITH CHECK ADD  CONSTRAINT [FK_WhiteList_Crew] FOREIGN KEY([CrewId])
REFERENCES [dbo].[Crew] ([CrewId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Whitelist] CHECK CONSTRAINT [FK_WhiteList_Crew]
GO
USE [master]
GO
ALTER DATABASE [PGE.AIMS.APP] SET  READ_WRITE 
GO
